var customersArray = [];
var customersArraySearch = [];
var BTPrinterObj = "";
var branchArray = [];
var branchArraySearch = [];
var itemsArray = [];
var itemsArraySearch = [];
var itemsArraySearchNew = [];
var farmerArraySearch = [];
var pairedBluetoothDevices = [];
var usersArray = [];
var usersArraySearch = [];
var usersArrayUnChanged = [];

var workOrderReportArray = [];
var stockTakeReportArray = [];
var stockTakeReportItems = [];
var workOrderProductionReportArray = [];

var stockRequisitionApprovalArray = [];
var stockIssueArray = [];

var advanceManufactureArray = [];

var manufactureItemsArraySearch = [];

var salesUsersArray = [];

var userStoreBalanceArray = [];
 
var posTransactionsArraySearch = [];
var posTransactionsArray = [];

var routeArray = [];
var routeArraySearch = [];

var getTripsArray = [];

var branchcusArray = [];
var branchcusArraySearch = [];

var posTransactionsArraySearchPayments = [];
var posTransactionsArraySearchItems = [];
var posTransactionsObjectString = {};

var tasksArray = [];
var targetArray = [];

var paymentTypesUsed = [];

var historyReportArray = [];

var stt = true;
var stripStartStatus = false;

var customersArray2 = [];
var customersArraySearch2 = [];

var salespersonArray = [];
var salespersonArraySearch = [];

var orderReportArray = [];
var orderDetailsArray = [];
var taskReportArray = [];
var customerPaymentArray = [];
var customerPaymentReportArray = [];
var salesExpensesReportArray = [];
var salesExpensesArray = [];
var bookingsReportArray = [];
var reportFilterArray = [];

var pricesArray = [];
var pricesArraySearch = [];

var mpesaArray = [];
var mpesaArraySearch = [];

var creditNoteArray = [];
var creditNoteArraySearch = [];

var posItems = [];
var storeItems = [];
var advManuItems = [];
var advManuProdItems = [];
var bookingItems = [];
var bookingItemClient = [];
var orderItems = [];
var routeItems = [];

var notSetCustomersArray = [];
var customersNotesArray = [];
var generalNotesArray = [];


var woReqArr = [];

var gblTel = "";
var gblSysUsers = "";
var gblSysUsersb = "";
var bookingUpdate = "0";
var gbltargettt = "0";
var globalDairyRoutes = "";

var selectedFarmerObj = {};

var dairyCollectedGrader = [];
var orderStatusPdfArr = [];

var salesSummaryArr = [];
var salesSummaryArrpdf = [];

var originStoreArray = [];
var destinationStoreArray = [];

var selv = "";
var itmwo = "";
var itmwot = "";

var userbatch = "";

var formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'Kes',
  minimumFractionDigits: 2
});

var formatterWeight = new Intl.NumberFormat();

var posItemsBkp = [
      {
        item_option: 'Fábio Rogério',
        price: '1000',
        quantity: 1
      },
      {
        item_option: 'Steve Jobs',
        price: '200',
        quantity: 1
      }
    ];

    var indexEdit=-1; //Just example in memory

var users = [
              {
                name: 'Fábio Rogério',
                email: 'fabio@rogerio.com'
              },
              {
                name: 'Steve Jobs',
                email: 'steve@jobs.com'
              }
            ];
// local
var birds = [
    {"title":"Southern Screamer","id":1},
    {"title":"Horned Screamer","id":2},
    {"title":"Magpie-goose","id":3}
];


var bookingPayId = "";
var posTotal = 0;
var posBalance = 0;
var orderTotal = 0;
var partialPaymentTotal = 0;
var itm = "";
var oitm = "";
var posReceiptRef = "";

var sunmiInnerPrinter;
var specReportData = [];

var justify_center = '\x1B\x61\x01';
var justify_left   = '\x1B\x61\x00';
var gTransID = "";
var gAuto = "";
var globalBatteryLevel = "";
var vlayer = "";
var map;
var vectorLayer;
var lineLayer;
var watchId;

var glat = 0; 
var glon = 0;
var gspeed = 0;
var gheading = 0; 
var ggps_time = 0;
var gaccuracy = 0;
var platform;



document.addEventListener('deviceready', function () {

    //checkConnection();
//setSiteUrl
//window.localStorage.getItem("setSiteUrl");
    document.addEventListener("backbutton", onBackKeyDown);

    console.log("SETSITE: "+ window.localStorage.getItem("setSite"));
    console.log("SETSITE URL: "+ window.localStorage.getItem("setSiteUrl"));
    console.log("SETSITE NAME: "+ window.localStorage.getItem("setSiteName"));
    console.log("SETSITE BRANCHES: "+ window.localStorage.getItem("setSiteBraches"));
    console.log("LOOGEDIN: "+ window.localStorage.getItem("loggedIn"));

    var extssname = window.localStorage.getItem("setSiteName");

    $("#exitsidenav").text("Exit "+ extssname.toUpperCase());

        var networkStateAll = navigator.connection.type;

        if(networkStateAll == Connection.NONE){
          onOffline();
        } else {
          
          onOnline();
        }
        
        

     


});

function onBackKeyDown(e) {
  //frmPOSBarcodeScannerMain

  e.preventDefault(); 
  //alert(window.location.href);
  //alert('setSite:'+ window.localStorage.getItem("setSite") +' # loggedIn:'+window.localStorage.getItem("loggedIn")); 


  if(parseInt(window.localStorage.getItem("setSite")) == 1) {



    if(parseInt(window.localStorage.getItem("loggedIn")) == 1) {

      calculateTotal('frmPOSBarcodeScannerMain');
      calculateTotal('frmPOSBarcodeScanner');
      calculateTotal('frmBookingBarcodeScanner');
      EmbeddedBarcodeReader.stopCamera();

      

    } else if(parseInt(window.localStorage.getItem("loggedIn")) == 0)  {
      logout();
    } else if(window.localStorage.getItem("loggedIn") == null)  {
      logout();
    } else {

      logout();
    }

  } else if(parseInt(window.localStorage.getItem("setSite")) == 0){
    sitelogout();
  } else if(window.localStorage.getItem("setSite") == null){
    sitelogout();
  } else {
    sitelogout();
  }

}

function clearBookingScannedItems() {
  bookingItems.length = 0;
}

function clearScannedItems() {
  posItems.length = 0;
}

function clearOrderScannedItems() {
  orderItems.length = 0;
}

function onOnline() {
    // Handle the online event
    //alert("Internet Ok!!");

    document.addEventListener("offline", onOffline2, false);

    platform = cordova.platformId;

    //alert(platform);

    
    
    //alert("Device ready");
    //cordova.plugins.printer.print(); setSite
    if(parseInt(window.localStorage.getItem("setSite")) == 1) {

      if(parseInt(window.localStorage.getItem("loggedIn")) == 1) {
          // Logged In
          // Redirect to first page after logged in.
          //openPage('invoices', {usr:window.localStorage.getItem("username"), org:window.localStorage.getItem("orgname")}, functionOpenHome);
          //$("#usr").text(window.localStorage.getItem("username"));
          //$("#org").text(window.localStorage.getItem("orgname"));
          var roles = JSON.parse(window.localStorage.getItem("roles"));

          if(roles.indexOf("tTask") !== -1){
              console.log("tTask YES");
              $('#assign-task').css("visibility", "visible");
          } else {
              $('#assign-task').css({"pointer-events": "none"});
              $('#assign-task').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
          }

          if(roles.indexOf("dDairy") !== -1){
              console.log("dDairy YES");
              $('#dairy').css("visibility", "visible");
          } else {
              $('#dairy').css({"pointer-events": "none"});
              $('#dairy').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
          }

          getSystemUsers();
          loadCustomers();

          requestLocationAccuracy(); 

      if(watchId == undefined) {

        console.log("############### WATCH ID ##################");
        console.log(watchId);
        watchId = navigator.geolocation.watchPosition(onSuccess, onError, { maximumAge: 3000, timeout: 5000, enableHighAccuracy: true }); 
      

      } else {
        console.log("############### STILL TRACKING ##################");
        console.log(watchId);
      }

      window.addEventListener("batterystatus", onBatteryStatus, false);



      BTPrinterObj = BTPrinter; 
 
      getReceiptHeaderVals();
        
      //loadUsers();

      //loadPOSTransaction();       

      FCMPlugin.getToken(function(token){
          //alert("GetToken: "+token);

          registerUserToken(token);
      });

      FCMPlugin.onNotification(function(data){
          if(data.wasTapped){
            //Notification was received on device tray and tapped by the user.
            alert( data.title + "\n" + data.body );
          }else{
            //Notification was received in foreground. Maybe the user needs to be notified.
            //alert( JSON.stringify(data) );
            alert( data.title + "\n" + data.body );
          }
      });


      timerOnLocationSender();
   
          
      } else {
          // Redirect to login page.
          logout();
          //openPage('login',{}, setDefaultBranches);
      }

    } else {
      sitelogout();
      //openPage('company');
    }

    

}

function setbldev(index){
  //delete posItems[index];

  $('#resa').html("");
  $('#resb').html("");
  $('#resc').html("");

  $.each(pairedBluetoothDevices, function(i,v){
      pairedBluetoothDevices[i].status = "0";
  });

  //alert(index +" - "+pairedBluetoothDevices[index].name);
  pairedBluetoothDevices[index].status =  "1";
  window.localStorage.setItem("wname",pairedBluetoothDevices[index].name);
  window.localStorage.setItem("wmac",pairedBluetoothDevices[index].address);

  $('#resb').append("Device: "+ pairedBluetoothDevices[index].name +" Connectiong...<br>");

    try {
      bluetoothSerial.connectInsecure(pairedBluetoothDevices[index].address, function(){
          $('#resb').append("Weigh Connected!!<br>");
          $('#resb').append("Ready For Reading...<br>");
          bluetoothSerial.read(function(data) {
            //From weigh scale you will get every milli second updated values in array so choose the recent one
            
          }, function(err){

              $('#resc').append("error reading "+err +"<br>");
          });
      }, function(err){

        $('#resc').append("error connecting "+err +"<br>");
      });

    } catch(err) {

    }

  
}

function removebldev(index){
  //delete posItems[index];
  //alert(index +" - "+pairedBluetoothDevices[index].name);

  $('#resa').html("");
  $('#resb').html("");
  $('#resc').html("");

  $.each(pairedBluetoothDevices, function(i,v){
      pairedBluetoothDevices[i].status = "0";
  });
  pairedBluetoothDevices[index].status =  "0";
  window.localStorage.removeItem("wname");
  window.localStorage.removeItem("wmac");

  $('#resb').append("Device: "+ pairedBluetoothDevices[index].name +" Disconnectiong...<br>");

  try {
    bluetoothSerial.disconnect(function(data) {
        //From weigh scale you will get every milli second updated values in array so choose the recent one
        
      $('#resb').append("Done");
      }, function(err){

          $('#resc').append("error disconnecting "+err +"<br>");
      });
    } catch(err) {

    }
  
}

function checkIfScaleIsConneted(){
    var devnn = window.localStorage.getItem("wname");
    var devmac = window.localStorage.getItem("wmac");
    if(devnn == null && devmac == null){
        openPage('pairing',{}, getBlutoothPairedDevices);
    } else {
      
       
       try {
          bluetoothSerial.connectInsecure(devmac, function(){
              $('#weighingRes').removeClass("red").addClass("green");
              
              bluetoothSerial.read(function(data) {
                //From weigh scale you will get every milli second updated values in array so choose the recent one

                $('#weighingRes').text("0.0");
              }, function(err){

                  $('#weighingRes').removeClass("green").addClass("red");
                  $('#weighingRes').text("READING ERROR");
              });
          }, function(err){

              $('#weighingRes').removeClass("green").addClass("red");
              $('#weighingRes').text("CONNECTION ERROR");
          });
         } catch(err) {
            $('#weighingRes').removeClass("green").addClass("red");
            $('#weighingRes').text("CONNECTION ERROR");
            //READING ERROR CONNECTION ERROR
        }

            
    }
}

function getBlutoothPairedDevices() {

    var devnn = window.localStorage.getItem("wname");
    var devmac = window.localStorage.getItem("wmac");

    //devnn = "QH";

    //alert("DEVNN: "+ devnn);

    var status = "0";
  

    pairedBluetoothDevices.length = 0;

    try {

        BTPrinterObj.status(function(data){
         // alert(" BT: "+ data);
          //console.log(data) // bt status: true or false
            if(data){

                BTPrinterObj.list(function(res){
                
                  var pcout = res.length / 3;
                  var k = 0;
                  for(var i=0; i<pcout; i++) {

                    if(devnn == null){

                      status = "0";

                    } else {
                      if(devnn == res[k]){
                        status = "1";
                      }

                    }

                    var item = {name:res[k],address: res[k+1],status: status};

                    pairedBluetoothDevices.push(item);

                    k = k + 3;

                  }



                  //$('#res').text(res.length);

                  //pairedBluetoothDevices
                  //console.log(pairedBluetoothDevices);


                },function(err){
                  //alert("NO PAIRED DEVICES");
                });

            } else {
              alert("Please turn on bluetooth");
            }
        },function(err){
          
        });

            


    } catch(err) {
      //alert("NO BTPRINTER!!");
    }


}

function goToPrev() {
  backPage();
  checkBluetoothConnectionStatus();
}

function checkBluetoothConnectionStatus() {

    try {

      bluetoothSerial.isConnected(function() {

        $('#weighingRes').removeClass("red").addClass("green");
        $('#weighingRes').text("0.0");
          
      }, function() {
          
          $('#weighingRes').removeClass("green").addClass("red");
          $('#weighingRes').text("CONNECTION ERROR");
          
      });

    } catch(err) {

    }

}

function readWeighingScale(){

    checkBluetoothConnectionStatus();

    try {

       bluetoothSerial.read(function(data) {
          //From weigh scale you will get every milli second updated values in array so choose the recent one
          //.replace(/\s/g,"")
          data =  data.toString();
          if(data == ""){
            $('#weighingRes').text("0.0");
          } else {
            var newdd = data.replace(/�/g,"").replace(/D/g,"").replace(/E/g,"").replace(/F/g,"").replace(/G/g,"");  
            var newddArr =  newdd.split(" ");
            var newddArrSize = newddArr.length;
            var actWeight = newddArr[parseInt(newddArrSize) - 1];
            
            $('#weighingRes').text(actWeight);
          }
        }, function(err){

            $('#weighingRes').removeClass("green").addClass("red");
            $('#weighingRes').text("R");
        });

      } catch(err) {

    }


}

function openStockTransfer() {

    storeItems.length = 0;
    loadOriginStore();

}

function openStockTake() {

    storeItems.length = 0;
    loadOriginStore();

}

function openManufacture() {

    
    loadManufacture();

}

function openingDairy() {

    var today = new Date();
    var hr = today.getHours();

    var timeOfDay = "";
  
   if(hr >= 00 && hr <= 11) {
        timeOfDay = '0';
   
    } else if (hr >= 12 && hr <= 23) {
        timeOfDay = '1';
    
    }

    $('#time').val(timeOfDay);

    checkIfScaleIsConneted();
    getDairyRoutes();

}

function getDairyRoutes() {
    //GET ALL DAIRY ROUTES IN THE SYSTEM
    $.ajax({
      type: "GET",
      //url: window.localStorage.getItem("setSiteUrl") + "process.php",
      url: "https://dairy.digerp.com/milkfarming/routes/routes.php",
      beforeSend: function(){

        globalDairyRoutes = "";
       
      },
      success: function(data){
        console.log(data);
        //var jsonFed = JSON.parse(data);
        //console.log(jsonFed.routes);

        globalDairyRoutes += '<option value="0">Select Route</option>';

        $.each(data.routes, function(i,v){
        

        globalDairyRoutes += '<option value="'+v.id+'">'+v.rname+'</option>';

        
      });

        $('#route').html(globalDairyRoutes);



        getDairyFarmers();

      }
    });
}

function getSelectedFarmer(num, name, contact, id) {

  selectedFarmerObj = {member_no: num, supp_name: name, contact: contact, supplier_id: id};

  console.log(selectedFarmerObj);
  backPage();
  $('#farmer_option').val(name);
  $('#memberNo').text(num);



}

function getDairyFarmers() {
    //GET ALL DAIRY FARMERS IN THE SYSTEM
    $.ajax({
      type: "GET",
      //url: window.localStorage.getItem("setSiteUrl") + "process.php",
      url: "https://dairy.digerp.com/milkfarming/farmers/suppliers.php",
      beforeSend: function(){
        
       
      },
      success: function(data){

      
        farmerArraySearch = data.farmers

      }
    });
}

function collectMilk() {

  var objdairy = MobileUI.objectByForm('frmDairy');


  var shift  = $('#time option:selected').text();
  var route_id  = objdairy.route;
  var route_name  = $('#route option:selected').text();

  var farmer_option  = objdairy.farmer_option;

  var total = $('#weighingRes').text();

  var supplier_id = selectedFarmerObj.supplier_id;
  var member_no = selectedFarmerObj.member_no;
  var supplier_name = $('#farmer_option').val();
  var grader_username = $('#grader').text();

  if(route_id == "0"){
    alert("Please Select a Route");
  } else if(farmer_option == ""){
    alert("Please Select a Farmer");
  } else if(total == "0.0" || total == "CONNECTION ERROR" || total == "READING ERROR"){
    alert("Invalid Weight");
  } else {
//READING ERROR CONNECTION ERROR
    var currday = new Date();
    var dd = currday.getDate();
    var mm = currday.getMonth()+1; //January is 0!
    var yyyy = currday.getFullYear();
       if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 

    var ord_date = yyyy+'-'+mm+'-'+dd;

    //var allcollectdd = "SUPPLIER-ID: "+supplier_id+"<br>SHIFT: "+shift+"<br>WEIGHT: "+total+"<br>DATE: "+ord_date+"<br>GRADER: "+grader_username+"<br>ROUTE: "+route_id;

    console.log("SUPPLIER-ID: "+supplier_id);
    console.log("SHIFT: "+shift);
    console.log("WEIGHT: "+total);
    console.log("DATE: "+ord_date);
    console.log("GRADER: "+grader_username);
    console.log("ROUTE: "+route_id);

    var collectedMilkData = {
          supplier_id: supplier_id,
          supplier_name: supplier_name,
          member_no: member_no,
          route_id: route_id,
          route_name: route_name,
          shift: shift,
          total: total,
          ord_date: ord_date,
          username: grader_username
        };

    //SEND TO SERVER
      $.ajax({
        type: "POST",
        //url: window.localStorage.getItem("setSiteUrl") + "process.php",
        url: "https://dairy.digerp.com/milkfarming/farmers/insert.php",
        data: collectedMilkData,
        beforeSend: function(){
          //loadingElement('viewonmapuserbtn');
          loadingElement('btnCollectMilk', 'Please wait...');

          var entriesStringAll = window.localStorage.getItem("dairycollections");

          if(entriesStringAll == null) {

            dairyCollectedGrader.push(collectedMilkData);
            window.localStorage.setItem("dairycollections",JSON.stringify(dairyCollectedGrader));

          } else {

            dairyCollectedGrader = JSON.parse(entriesStringAll);

            dairyCollectedGrader.unshift(collectedMilkData);

            window.localStorage.setItem("dairycollections",JSON.stringify(dairyCollectedGrader));

          }

          

          dairyCollectedGrader.length = 0;
      
          
         
        },
        success: function(data){

          
          $('#farmer_option').val("");
          $('#farmer_option_id').val("");
          $('#memberNo').text("0");
          $('#weighingRes').text("0.0");

          closeLoading('btnCollectMilk');
          $('#collected').html(data);
        }
      });

      printDairyCollectionReceipt();
    }

}

function getMyDairyEntries() {

    dairyCollectedGrader.length = 0;
      //totLitres totEntries
    var entriesString = window.localStorage.getItem("dairycollections");

    if(entriesString == null) {

      $('#totLitres').text("0");
      $('#totEntries').text("0");

    } else {
      dairyCollectedGrader = JSON.parse(entriesString);

      //dairyCollectedGrader = arr.reverse();

      var tl = 0.0;
      $.each(dairyCollectedGrader, function(i,v){
          tl = tl + parseFloat(v.total);  
      });

      var roundedtl = Math.round(tl * 10) / 10

      var te = dairyCollectedGrader.length;

      $('#totLitres').text(roundedtl);
      $('#totEntries').text(te);
    }


    

}

function weighingScaleBluetoothDisconnect() {
    bluetoothSerial.disconnect(function(data) {
      //From weigh scale you will get every milli second updated values in array so choose the recent one
      
    }, function(err){

    });
}
function rePrintDairyCollectionReceipt(i) {

  weighingScaleBluetoothDisconnect();

  dairyCollectedGrader.length = 0;
      //totLitres totEntries
    var entriesString = window.localStorage.getItem("dairycollections");

    if(entriesString == null) {

      alert("Please Enter Collection to print.");

    } else {

        dairyCollectedGrader = JSON.parse(entriesString);


        try {
      //--------------------------------------------------------------------------------------------------
            

            var justify_center = '\x1B\x61\x01';
            var justify_left   = '\x1B\x61\x00';
            var qr_model       = '\x32';          // 31 or 32
            var qr_size        = '\x08';          // size
            var qr_eclevel     = '\x33';          // error correction level (30, 31, 32, 33 - higher)
            var qr_data        = 'http://digisoftsolutions.co.ke';
            var qr_pL          = String.fromCharCode((qr_data.length + 3) % 256);
            var qr_pH          = String.fromCharCode((qr_data.length + 3) / 256);

            var pname = res[0];
            var paddress = res[1];
            var ptype = res[2];

            var RECEIPT = ""+ 
                          "Member Name : "+ dairyCollectedGrader[i].supplier_name +"\n" +
                          "Member No : "+ dairyCollectedGrader[i].member_no +"\n" +
                          "Date  : "+ dairyCollectedGrader[i].ord_date +"\n" +
                          "Shift : "+ dairyCollectedGrader[i].shift +"\n" +
                          "Kgs   : "+ dairyCollectedGrader[i].total +"\n" +
                          "-------------------------------\n"+
                          "Collected by : "+ dairyCollectedGrader[i].username +"\n" +
                          "-------------------------------\n"+
                          "\n\n";


            //Connect to printer 
            BTPrinterObj.connect(function(data){
              //alert("s2: "+JSON.stringify(data));

              //var receiptRef = posReceiptRef.split('#');
              //var receiptNumberPosAct = 'POS#'+str_pad(receiptRef[2], 9, "0", "STR_PAD_LEFT");

              //1ST PRINT
              //Receipt Title Header
              BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + 'REPRINTED COPY','1','1');
              BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
              BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
              BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
              BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

              //Receipt Header
              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + '-------------------------------\n','1','1');
              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'Milk Collection Notification\n','1','1');
              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + '-------------------------------\n','1','1');
              BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
              //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
              //BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

              BTPrinterObj.printText(function(data){
                //Clear all pos data
                
              },null, justify_left +
              '\n\n' +
              RECEIPT +               // Print
              '\n\n' +
              'Served By: ' + window.localStorage.getItem("username") +
             '\n\n' +
              'THANKYOU' +
             '\n\n\n\n\n\n' +
             justify_left,'1','0');


              //2ND PRINT
              //Receipt Title Header
              BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + 'REPRINTED COPY','1','1');
              BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
              BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
              BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
              BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

              //Receipt Header
              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + '-------------------------------\n','1','1');
              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'Milk Collection Notification\n','1','1');
              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + '-------------------------------\n','1','1');
              BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
              //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
              //BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

              BTPrinterObj.printText(function(data){

                //CLEAR ALL POS DATA
                clearCpayData();
                
              },null, justify_left +
              '\n\n' +
              RECEIPT +               // Print
              '\n\n' +
              'Served By: ' + window.localStorage.getItem("username") +
             '\n\n' +
              'THANKYOU' +
             '\n\n\n\n\n\n' +
             justify_left,'1','0');

            },function(err){  

              clearCpayData();

            }, pname);



      //--------------------------------------------------------------------------------------------------

        } catch(err) {

        }
    }

    checkIfScaleIsConneted();
}

function printDairyCollectionReceipt() {

  weighingScaleBluetoothDisconnect();

  dairyCollectedGrader.length = 0;
      //totLitres totEntries
    var entriesString = window.localStorage.getItem("dairycollections");

    if(entriesString == null) {

      alert("Please Enter Collection to print.");

    } else {

        dairyCollectedGrader = JSON.parse(entriesString);


        try {
      //--------------------------------------------------------------------------------------------------
            

            var justify_center = '\x1B\x61\x01';
            var justify_left   = '\x1B\x61\x00';
            var qr_model       = '\x32';          // 31 or 32
            var qr_size        = '\x08';          // size
            var qr_eclevel     = '\x33';          // error correction level (30, 31, 32, 33 - higher)
            var qr_data        = 'http://digisoftsolutions.co.ke';
            var qr_pL          = String.fromCharCode((qr_data.length + 3) % 256);
            var qr_pH          = String.fromCharCode((qr_data.length + 3) / 256);

            var pname = res[0];
            var paddress = res[1];
            var ptype = res[2];

            var RECEIPT = ""+ 
                          "Member Name : "+ dairyCollectedGrader[0].supplier_name +"\n" +
                          "Member No : "+ dairyCollectedGrader[0].member_no +"\n" +
                          "Date  : "+ dairyCollectedGrader[0].ord_date +"\n" +
                          "Shift : "+ dairyCollectedGrader[0].shift +"\n" +
                          "Kgs   : "+ dairyCollectedGrader[0].total +"\n" +
                          "-------------------------------\n"+
                          "Collected by : "+ dairyCollectedGrader[0].username +"\n" +
                          "-------------------------------\n"+
                          "\n\n";


            //Connect to printer 
            BTPrinterObj.connect(function(data){
              //alert("s2: "+JSON.stringify(data));

              //var receiptRef = posReceiptRef.split('#');
              //var receiptNumberPosAct = 'POS#'+str_pad(receiptRef[2], 9, "0", "STR_PAD_LEFT");

              //1ST PRINT
              //Receipt Title Header
              BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
              BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
              BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
              BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

              //Receipt Header
              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + '-------------------------------\n','1','1');
              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'Milk Collection Notification\n','1','1');
              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + '-------------------------------\n','1','1');
              BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
              //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
              //BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

              BTPrinterObj.printText(function(data){
                //Clear all pos data
                
              },null, justify_left +
              '\n\n' +
              RECEIPT +               // Print
              '\n\n' +
              'Served By: ' + window.localStorage.getItem("username") +
             '\n\n' +
              'THANKYOU' +
             '\n\n\n\n\n\n' +
             justify_left,'1','0');


              //2ND PRINT
              //Receipt Title Header
              BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
              BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
              BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
              BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

              //Receipt Header
              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + '-------------------------------\n','1','1');
              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'Milk Collection Notification\n','1','1');
              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + '-------------------------------\n','1','1');
              BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
              //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
              //BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

              BTPrinterObj.printText(function(data){

                //CLEAR ALL POS DATA
                clearCpayData();
                
              },null, justify_left +
              '\n\n' +
              RECEIPT +               // Print
              '\n\n' +
              'Served By: ' + window.localStorage.getItem("username") +
             '\n\n' +
              'THANKYOU' +
             '\n\n\n\n\n\n' +
             justify_left,'1','0');

            },function(err){  

              clearCpayData();

            }, pname);



      //--------------------------------------------------------------------------------------------------

        } catch(err) {

        }
    }

    checkIfScaleIsConneted();
}

function dairyCollectionPdf() {

  dairyCollectedGrader.length = 0;
      //totLitres totEntries
    var entriesString = window.localStorage.getItem("dairycollections");

    if(entriesString == null) {

      alert("Please Enter Collection to export pdf.");

    } else {

      /*

      var RECEIPT = ""+ 
                          "Member Name : "+ dairyCollectedGrader[0].supplier_name +"\n" +
                          "Member No : "+ dairyCollectedGrader[0].member_no +"\n" +
                          "Date  : "+ dairyCollectedGrader[0].ord_date +"\n" +
                          "Shift : "+ dairyCollectedGrader[0].shift +"\n" +
                          "Kgs   : "+ dairyCollectedGrader[0].total +"\n" +
                          "-------------------------------\n"+
                          "Collected by : "+ dairyCollectedGrader[0].username +"\n" +
                          "-------------------------------\n"+
                          "\n\n";*/

        dairyCollectedGrader = JSON.parse(entriesString);

         var tblContents = '';

        
        var timehder = '<center><h3>Date: '+ dairyCollectedGrader[0].ord_date +'</h3></center><br>';

        if(dairyCollectedGrader.length > 0) {

            tblContents += '<tr>'+
              '<td>'+ dairyCollectedGrader[0].supplier_id +'</td>'+
              '<td>'+ dairyCollectedGrader[0].supplier_name +'</td>'+
              '<td>'+ dairyCollectedGrader[0].route_name +'</td>'+
              '<td>'+ dairyCollectedGrader[0].shift +'</td>'+
              '<td>'+ dairyCollectedGrader[0].total +'</td>'+
            '</tr>';
          

          var tableHeader = '<style>'+
          'table {'+
            'font-family: arial, sans-serif;'+
            'border-collapse: collapse;'+
            'width: 100%;'+
          '}'+
          'td, th {'+
            'border: 1px solid #dddddd;'+
            'text-align: left;'+
            'padding: 8px;'+
          '}'+
          'tr:nth-child(even) {'+
            'background-color: #dddddd;'+
          '}'+
          '.inner tr:nth-child(even) {'+
            'background-color: #f4f4f4;'+
          '}'+
          '</style>'+
          '<center><h2>DAIRY COLLECTION</h2></center>'+
          timehder +
          '<table>'+
          '<tr>'+
            '<th>Farmer ID</th>'+
            '<th>Farmer Name</th>'+
            '<th>Route</th>'+
            '<th>Shift</th>'+
            '<th>Weight</th>'+
          '</tr>'+
          tblContents +
          '</table>';

          var options = {
              font: {
                  size: 22,
                  italic: true,
                  align: 'center'
              },
              header: {
                  height: '6cm',
                  label: {
                      text: "",
                      font: {
                          bold: true,
                          size: 37,
                          align: 'center'
                      }
                  }
              },
              footer: {
                  height: '4cm',
                  label: {
                      text: '',
                      font: { align: 'center' }
                  }
              }
          };
         
          cordova.plugins.printer.print(tableHeader, options);

        } else {
          alert('No Records to export to PDF');
        }

    }

         


}

function setDefaultBranches(){

  $("#company").append(window.localStorage.getItem("setSiteBraches")); 
  

}

function checkConnection() {
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';

    if(networkState == Connection.NONE){
      onOffline();
    } else {
      onOnline();
    }

    //alert('Connection type: ' + states[networkState]);
}

function checkConnection2() {
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';

    if(networkState == Connection.NONE){
      onOffline2();
    } else {
      //onOnline();
    }

    //alert('Connection type: ' + states[networkState]);
}

function onOffline() {
    // Handle the offline event
    alert({
        title:'Internet Status',
        message:"Please Connect to Internet!!!",
        id: 'my-custom-internet-dialog',
        width:'90%',
        buttons:[
          {
            label: 'Ok',
            onclick: function(){

                closeAlert('my-custom-internet-dialog');
                //loading('Checking Internet...');
                setTimeout(function(){
                  //closeLoading();
                  checkConnection();
                }, 3000);
                

            }
          }
        ]
      });
    
}

function onOffline2() {
    // Handle the offline event
    alert({
        title:'Internet Status',
        message:"Please Connect to Internet!!!!",
        id: 'my-custom-internet2-dialog',
        width:'90%',
        buttons:[
          {
            label: 'Ok',
            onclick: function(){

                closeAlert('my-custom-internet2-dialog');
                //loading('Checking Internet....');
                setTimeout(function(){
                  //closeLoading();
                  checkConnection2();
                }, 3000);
                

            }
          }
        ]
      });
    
}

//
function registerUserToken(tkn){

  //Register logged in user token
  if(tkn == ""){

  } else {
      $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
      {
          tp: "regToken",
          token: tkn,
          cp: window.localStorage.getItem("cp"),
          uname: window.localStorage.getItem("username"),  
          id: window.localStorage.getItem("id")

      },
      function(data, status){
        
        
            
      });
  }
    
    
}
//
function initMaps(idparam){
  map = "";
  vlayer = "";
  vlayer = new OpenLayers.Layer.Vector( "Editable" );
  var apiKey = "AqTGBsziZHIJYYxgivLBf0hVdrAk9mWO5cQcb8Yux8sW5M8c8opEC2lZqKR1ZZXf";
  var osm = new OpenLayers.Layer.OSM();
 
  var ggl = new OpenLayers.Layer.Google('Google Road',{type: google.maps.MapTypeId.ROADMAP, numZoomLevels: 21, MAX_ZOOM_LEVEL: 22 });
    var sat = new OpenLayers.Layer.Google('Google Satellite', {type: google.maps.MapTypeId.SATELLITE, numZoomLevels: 21, MAX_ZOOM_LEVEL: 22});
    var hybrid = new OpenLayers.Layer.Google('Google Hybrid', {type: google.maps.MapTypeId.HYBRID, numZoomLevels: 21, MAX_ZOOM_LEVEL: 22});
    
    /*var mapcontainer = document.getElementById('map-canvas');
    mapcontainer.innerHTML = '<div id="tooltip" style="position:absolute; z-index:10000;background-color:#FFFFFF;display:none;padding:5px;"></div>';*/
      
    map = new OpenLayers.Map(idparam, {
        layers: [
      ggl,
      sat,
      hybrid
        ],
        center: new OpenLayers.LonLat(36.844459, -1.308410)
            // Google.v3 uses web mercator as projection, so we have to
            // transform our coordinates
            .transform('EPSG:4326', 'EPSG:3857'),
        zoom: 10
    });

    vectorLayer = new OpenLayers.Layer.Vector("Markers");
    lineLayer = new OpenLayers.Layer.Vector("Line Layer"); 

    map.addLayer(vectorLayer);
    map.addLayer(lineLayer);

    // Define markers as "features" of the vector layer: EPSG:900913
    /**/
    //alert('YES');
    if(idparam == 'onmapid'){
      loadUsersMap();
    }
    if(idparam == 'custlocmapid'){
      getAllCustomerLocation();
    }

    if(idparam == 'histmapid'){
      loadUsersHistMap();
    }

    if(idparam == 'onmapuserid'){
      getOnMapUserHistory();
    }

    //
    
}

function getOnMapUserHistory() {
  var objuser = MobileUI.objectByForm('frmOnMapUser');


  var user_date_option  = objuser.user_date_option;
  var user_id_option  = objuser.user_id_option;

  //alert(user_id_option + " # " + user_date_option);

  //GET CUSTOMETS CREDIT AMOUNT
    $.ajax({
      type: "POST",
      url: window.localStorage.getItem("setSiteUrl") + "process.php",
      data: {
        tp: "getuserslocationhistory",
        userid: user_id_option,
        userdate: user_date_option,
        uname: window.localStorage.getItem("username"),
        id: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
      },
      beforeSend: function(){
        loadingElement('viewonmapuserbtn');
        lineLayer.removeAllFeatures();
        lineLayer.destroyFeatures(); 
        lineLayer.addFeatures([]);

        vectorLayer.removeAllFeatures();
        vectorLayer.destroyFeatures();
        vectorLayer.addFeatures([]);

        //vectorLayer.setVisibility(0);
       
      },
      success: function(data){

        var locadata = JSON.parse(data);
        if(locadata.length == 0){
          alert('No data found!');
        } else {
          var newpoint = [];
          var lastindex = locadata.length - 1;
          $.each(locadata, function(index, value){

            if(index == 0){
              var feature = new OpenLayers.Feature.Vector(
                      new OpenLayers.Geometry.Point(parseFloat(value.lon), parseFloat(value.lat)).transform('EPSG:4326', 'EPSG:900913'),
                      {
                          description: ''
                      },
                      {label: '', labelOutlineColor: '#fff',labelOutlineWidth: 4, labelAlign: 'cm', labelXOffset: 36, labelYOffset: 10, strokeWidth: 4, strokeColor: '#fff', fontSize: "13px", fontWeight: "bold", fontFamily: 'roboto',externalGraphic: 'img/ic.png', graphicHeight: 36 }
              );

              vectorLayer.addFeatures(feature);
            }

            if(index == lastindex){
              var feature = new OpenLayers.Feature.Vector(
                      new OpenLayers.Geometry.Point(parseFloat(value.lon), parseFloat(value.lat)).transform('EPSG:4326', 'EPSG:900913'),
                      {
                          description: ''
                      },
                      {label: '', labelOutlineColor: '#fff',labelOutlineWidth: 4, labelAlign: 'cm', labelXOffset: 36, labelYOffset: 10, strokeWidth: 4, strokeColor: '#fff', fontSize: "13px", fontWeight: "bold", fontFamily: 'roboto',externalGraphic: 'img/ic.png', graphicHeight: 36 }
              );

              vectorLayer.addFeatures(feature);
            }

           
            newpoint.push(new OpenLayers.Geometry.Point(parseFloat(value.lon), parseFloat(value.lat)));
           
                
          });

          //Line layer initialization
          var line = new OpenLayers.Geometry.LineString(newpoint).transform('EPSG:4326', 'EPSG:3857');
          var style = { 
            strokeColor: '#000000', 
            strokeOpacity: 0.5,
            strokeWidth: 3
          };

          var lineFeature = new OpenLayers.Feature.Vector(line, null, style);
          lineLayer.addFeatures([lineFeature]);

            var bounds = lineLayer.getDataExtent();
            map.zoomToExtent(bounds);
            lineLayer.setVisibility(1);


            
          }
        
        

      },
      error: function(err){
        
      },
      complete: function(){
        closeLoading('viewonmapuserbtn');
        //alert(user_id_option);
        loadonrouteclients(user_id_option, user_date_option);
        
      }
    });
}

function loadonrouteclients(userid, userdate){

  $.ajax({
      type: "POST",
      url: window.localStorage.getItem("setSiteUrl") + "process.php",
      data: {
        tp: "getusercustomerslocation",
        userid: userid,
        userdate: userdate,
        uname: window.localStorage.getItem("username"),
        id: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
      },
      beforeSend: function(){
        //vectorLayer.removeAllFeatures();
        //vectorLayer.destroyFeatures();
        //vectorLayer.addFeatures([]);
       
      },
      success: function(data){

        var cuslocadata = JSON.parse(data);
        if(cuslocadata.length == 0){
          alert('No customer location data found!');
        } else {
          
          $.each(cuslocadata, function(index, value){

              if(parseInt(value.visit) == 1){
                  var feature1 = new OpenLayers.Feature.Vector(
                      new OpenLayers.Geometry.Point(parseFloat(value.lon), parseFloat(value.lat)).transform('EPSG:4326', 'EPSG:900913'),
                      {
                          description: ''
                      },
                      {label: value.br_name, labelOutlineColor: '#fff',labelOutlineWidth: 4, labelAlign: 'cm', labelXOffset: 36, labelYOffset: 10, strokeWidth: 4, strokeColor: '#fff', fontSize: "13px", fontWeight: "bold", fontFamily: 'roboto',externalGraphic: 'img/office-buildingg.png', graphicHeight: 36 }
                  );

                  vectorLayer.addFeatures(feature1);

              } else if(parseInt(value.visit) == 2){
                var feature2 = new OpenLayers.Feature.Vector(
                      new OpenLayers.Geometry.Point(parseFloat(value.lon), parseFloat(value.lat)).transform('EPSG:4326', 'EPSG:900913'),
                      {
                          description: ''
                      },
                      {label: value.br_name, labelOutlineColor: '#fff',labelOutlineWidth: 4, labelAlign: 'cm', labelXOffset: 36, labelYOffset: 10, strokeWidth: 4, strokeColor: '#fff', fontSize: "13px", fontWeight: "bold", fontFamily: 'roboto',externalGraphic: 'img/office-buildingr.png', graphicHeight: 36 }
                  );

                  vectorLayer.addFeatures(feature2);
              }  
                
          });
            
          }
        
        

      },
      error: function(err){
        
      },
      complete: function(){
        
        
      }
    });

}

function edtSelectesTarget(params) {
  //alert(params.userid);

  var loginUsrId = parseInt(window.localStorage.getItem("id"));
  
  $('#euser_option').val(params.username);
  $('#euser_option_id').val(params.userid);
  $('#etarget_id').val(params.id);
  
  $('#etarget_category').val(params.target_description);
  $('#etarget_period').val(params.target_date);
  $('#etarget_qty').val(params.target_qty);
  $('#etarget_comments').val(params.target_comments);
}


function edtSelectesCusTarget(params) {
  //alert(params.userid);

  var loginUsrId = parseInt(window.localStorage.getItem("id"));
  if(loginUsrId == parseInt(params.createdby)){
    if(params.target_description == ""){
      $('#eallcustomerchecker').prop('disabled', false);
      $('#eallcustomerchecker').prop('checked', true);
      $('#ecustomer_option').prop('disabled', true);
    } else {

      $('#ecustomer_option').prop('disabled', false);
    }
  } else {
    if(params.target_description == ""){
      $('#eallcustomerchecker').prop('disabled', true);
      $('#eallcustomerchecker').prop('checked', true);
    }
    $('#ecustomer_option').removeAttr('onclick');
    $('#ecustomer_option').prop('readonly', true);
  }
  $('#euser_option').val(params.username);
  $('#euser_option_id').val(params.userid);
  $('#etarget_id').val(params.id);
  $('#ecustomer_option').val(params.target_description);
  $('#etarget_period').val(params.target_date);
  $('#etarget_comments').val(params.target_comments);
}


function edtSelectesTask(params) {
  //alert(params.userid);
  $('#euser_option').val(params.username);
  $('#euser_option_id').val(params.userid);
  $('#etask_id').val(params.id);
  $('#etask_date').val(params.task_date);
  $('#etask_info').val(params.task_description);
  $('#etask_prevcomments').val(params.task_comments);
  $('#etask_comments').val("");
}

function setOnMapUserVal(uparams){
  $('#specuser').text(uparams.name);
  $('#user_id_option').val(uparams.id);
  //alert(uparams.id);
}

function setUsersLocationMap(){
  //alert(usersArray.length + ' a# ' + JSON.stringify(usersArray));
  //alert(usersArraySearch.length + ' s# ' + JSON.stringify(usersArraySearch));
  vectorLayer.removeAllFeatures();
  vectorLayer.destroyFeatures();//optional
  vectorLayer.addFeatures([]);
  vectorLayer.setVisibility(0);
  $.each(usersArraySearch, function(index, value){
        //console.log(value);
        //console.log(value.lat);
    var feature = new OpenLayers.Feature.Vector(
            new OpenLayers.Geometry.Point(parseFloat(value.lon), parseFloat(value.lat)).transform('EPSG:4326', 'EPSG:900913'),
            {
                description: ''
            },
            {label: value.uname, labelOutlineColor: '#fff',labelOutlineWidth: 4, labelAlign: 'cm', labelXOffset: 36, labelYOffset: 10, strokeWidth: 4, strokeColor: '#fff', fontSize: "13px", fontWeight: "bold", fontFamily: 'roboto',externalGraphic: 'img/ic.png', graphicHeight: 36 }
    );

    vectorLayer.addFeatures(feature);
        
  });

  var bounds = vectorLayer.getDataExtent();
  map.zoomToExtent(bounds);
  vectorLayer.setVisibility(1);
}

function onBatteryStatus(status) {
    //console.log("Level: " + status.level + " isPlugged: " + status.isPlugged);
  globalBatteryLevel = status.level;
}

function onSuccess(position){

   glat = position.coords.latitude; 
   glon = position.coords.longitude;
   gspeed = position.coords.speed;
   gheading = position.coords.heading; 
   ggps_time = position.timestamp;
   gaccuracy = position.coords.accuracy;
    //startLocationSeaker();

}

function onError(error){
  
}

function startLocationSeaker(){
  //var watchID = navigator.geolocation.watchPosition(onSuccess, onError, { maximumAge: 60000, timeout: 60000, enableHighAccuracy: true });
  

  var btnText = $('#starttrip').text();

  if(stripStartStatus == true){
    stt = true;
    //watchId = navigator.geolocation.watchPosition(onSuccess, onError, { maximumAge: 3000, timeout: 5000, enableHighAccuracy: true });
    $('#starttrip').text('End');
    $('#starttrip').removeClass('red');
    $('#starttrip').addClass('red-100');

  } else {
    stt = false;
    //navigator.geolocation.clearWatch(watchId);
    $('#starttrip').text('Start');
    $('#starttrip').removeClass('red-100');
    $('#starttrip').addClass('red');
  }
  
    
}

function timerOnLocationSender(){
  setTimeout(function(){
      if(glat != 0 && glon != 0){
          console.log('YES Loop '+ stt + ' glat=' + glat);
          //SEND SALEMANS LOCATION
          $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
          {
              tp: "sendloc",
              lat: glat, 
              lon: glon, 
              speed: gspeed, 
              heading: gheading, 
              gps_time: ggps_time,
              accuracy: gaccuracy,
              bat: globalBatteryLevel,
              cp: window.localStorage.getItem("cp"),
              uname: window.localStorage.getItem("username"),  
              id: window.localStorage.getItem("id")

          },
          function(data, status){
            
            //alert('SENT');
            
                
          });
      
        }

        timerOnLocationSender();

    }, 60000);
}

function getCustomerCurrentLocation(){
  navigator.geolocation.getCurrentPosition(function(position){
    $("#lat").val(position.coords.latitude);
    $("#lon").val(position.coords.longitude);
  }, function(error){
    $("#lat").val("NA");
    $("#lon").val("NA");
  });
}

function onchangeUserView(){
  var cusobj = MobileUI.objectByForm('frmOnView');
  var alluserschecker = cusobj.alluserschecker;
  
  if(alluserschecker == true){
    //alert('YES');
    usersArraySearch = usersArray;
    $('#user_option').val("");
    $('#user_option_id').val("");
    //MobileUI.clearForm('frmOnView');
  } else {
    //alert('NO');
  }
}

function onchangeTargetView(){
  var tusobj = MobileUI.objectByForm('frmTaskReport');
  var alltuserschecker = tusobj.alltuserschecker;
  
  if(alltuserschecker == true){
    //alert('YES');
    //usersArraySearch = usersArray;
    $('#saleperson_option').val("");
    $('#saleperson_option_id').val("");
    //MobileUI.clearForm('frmOnView');
  } else {
    //alert('NO');
  }
}

function onchangeUserMap(){
  var cusobj = MobileUI.objectByForm('frmOnMap');
  var alluserschecker = cusobj.alluserschecker;
  
  if(alluserschecker == true){
    //alert('YES');
    usersArraySearch = usersArray;
    $('#user_option').val("");
    $('#user_option_id').val("");
    setUsersLocationMap();
    //MobileUI.clearForm('frmOnView');
  } else {
    //alert('NO');
  }
}
//
function eonChangeAllItems(){
  var stgobj = MobileUI.objectByForm('frmUpdateTarget');
  var eallitemschecker = stgobj.eallitemschecker;
  
  if(eallitemschecker == true){  
    $('#eitem_option').val("");
    $('#eitem_option_id').val("");
    $('#eitem_option').prop('disabled', true);
  } else {
    //$('#specItemsSel').removeAttr('disabled');
    $('#eitem_option').prop('disabled', false);

  }
}

function onChangeAllItems(){
  var stgobj = MobileUI.objectByForm('frmSetTarget');
  var allitemschecker = stgobj.allitemschecker;
  
  if(allitemschecker == true){  
    $('#item_option').val("");
    $('#item_option_id').val("");
    $('#item_option').prop('disabled', true);
  } else {
    //$('#specItemsSel').removeAttr('disabled');
    $('#item_option').prop('disabled', false);

  }
}


function onChangeAllCustomer(){
  var stgobj = MobileUI.objectByForm('frmSetTargetCustomer');
  var allcustomerchecker = stgobj.allcustomerchecker;
  
  if(allcustomerchecker == true){  
    $('#customer_option').val("");
    $('#customer_option_id').val("");
    $('#customer_option').prop('disabled', true);
  } else {
    //$('#specItemsSel').removeAttr('disabled');
    $('#customer_option').prop('disabled', false);

  }
}

function getHistoryReport(){

  var cusobj = MobileUI.objectByForm('frmHistoryReport');
  var user_option = cusobj.user_option;
  var user_option_id = cusobj.user_option_id;
  var user_date = cusobj.user_date;

  if(user_option == ""){
    alert("Please select a user.");
  } else if(user_date == ""){
    alert("Please enter date.");
  } else {

    loading('Please wait...');
      //closeLoading();
    //SAVE CUSTOMETS LOCATION
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getuserhistory",
        user_option: user_option,
        user_option_id: user_option_id,
        user_date: user_date,
        uname: window.localStorage.getItem("username"),
        id: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      closeLoading();
      var historyreportObj = JSON.parse(data);
      //historyReportArray
      if(historyreportObj.length == 0){
        historyReportArray.length = 0;
        alert("No Results Found..");
      } else {
        historyReportArray.length = 0;
        historyReportArray = JSON.parse(data);
      }
          
    });

  }

    

}

function getAllCustomerLocations(){
   MobileUI.clearForm('frmCustomerLocationsRequest');
 
 //console.log(branch_option_id + " # " + branch_option);

  loading('Please wait...');
      //closeLoading();
    //SAVE CUSTOMETS LOCATION
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getcustomerlocations",
        tp2: "all",
        branch_option: '',
        branch_option_id: '',
        uname: window.localStorage.getItem("username"),
        id: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      
      closeLoading();
      //console.log("############ LOCATION DATA ###########");
      //console.log(data);
      //console.log(data);
      //console.log(vectorLayer);
      setCustomerLocationsMarkers(data);
      //MobileUI.clearForm('frmCustomerLocationSetter');
      //alert("Customer Location Saved Successfully");
          
    });
}

function getAllCustomerLocation(){
  MobileUI.clearForm('frmCustomerLocationsRequest');
 
 //console.log(branch_option_id + " # " + branch_option);

  loading('Please wait...');
      //closeLoading();
    //SAVE CUSTOMETS LOCATION
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getcustomerlocations",
        branch_option: '',
        branch_option_id: '',
        uname: window.localStorage.getItem("username"),
        id: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      
      closeLoading();
      //console.log("############ LOCATION DATA ###########");
      //console.log(data);
      //console.log(data);
      //console.log(vectorLayer);
      setCustomerLocationsMarkers(data);
      //MobileUI.clearForm('frmCustomerLocationSetter');
      //alert("Customer Location Saved Successfully");
          
    });

}

function getCustomerLocation(){
  var cusobj = MobileUI.objectByForm('frmCustomerLocationsRequest');
  var branch_option = cusobj.branch_option;
  var branch_option_id = cusobj.branch_option_id;
 

 //console.log(branch_option_id + " # " + branch_option);
  if(branch_option == "" || branch_option_id == ""){
      alert("Plase select a Branch..");
  } else {
      loading('Please wait...');
        //closeLoading();
      //SAVE CUSTOMETS LOCATION
      $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
      {
          tp: "getcustomerlocations",
          branch_option: branch_option,
          branch_option_id: branch_option_id,
          uname: window.localStorage.getItem("username"),
          id: window.localStorage.getItem("id"),
          cp: window.localStorage.getItem("cp")
      },
      function(data, status){
        
        closeLoading();
        console.log("############ LOCATION DATA ###########");
        console.log(data);
        //console.log(data);
        //console.log(vectorLayer);
        setCustomerLocationsMarkers(data);
        //MobileUI.clearForm('frmCustomerLocationSetter');
        //alert("Customer Location Saved Successfully");
            
      });
  }

      
}

function getMapHistory(){
  var cusobj = MobileUI.objectByForm('frmOnMapHistory');
  var user_option = cusobj.user_option;
  var user_option_id = cusobj.user_option_id;
  var user_date = cusobj.user_date;

  if(user_option == ""){
    alert("Please select a user.");
  } else if(user_date == ""){
    alert("Please enter date.");
  } else {
    loading('Please wait...');
      //closeLoading();
    //SAVE CUSTOMETS LOCATION
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getuserhistory",
        user_option: user_option,
        user_option_id: user_option_id,
        user_date: user_date,
        uname: window.localStorage.getItem("username"),
        id: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      
      closeLoading();
      
      setMapHistoryMarkers(data);
      //MobileUI.clearForm('frmCustomerLocationSetter');
      //alert("Customer Location Saved Successfully");
          
    });
  }

    

}

function setCustomerLocationsMarkers(data){

  var objdata = JSON.parse(data);
  $('#cusloccount').text(objdata.length);
  //console.log(data);
  //console.log(objdata);

  if(objdata[0].lat == "" || objdata[0].lon == "") {
    vectorLayer.removeAllFeatures();
    vectorLayer.destroyFeatures();//optional
    vectorLayer.addFeatures([]);
    vectorLayer.setVisibility(0);
    alert("Customer Location Not Set..");
  } else {

      vectorLayer.removeAllFeatures();
      vectorLayer.destroyFeatures();//optional
      vectorLayer.addFeatures([]);
      vectorLayer.setVisibility(0);

      var lat;
      var lon;
      var rad;
      var brname;

      
      $.each(objdata, function (index, objvalue) {
          $.each(objvalue, function (name, value) {
              if (name == 'lat') {
                  lat = value;
              } else if (name == 'lon') {
                  lon = value;
              } else if (name == 'rad') {
                  rad = value;
              } else if (name == 'br_name') {
                  brname = value;
              }

          });

          var feature = new OpenLayers.Feature.Vector(
                new OpenLayers.Geometry.Point(parseFloat(lon), parseFloat(lat)).transform('EPSG:4326', 'EPSG:900913'),
                {
                    description: ''
                },
                {label: brname, labelOutlineColor: '#fff',labelOutlineWidth: 4, labelAlign: 'cm', labelXOffset: 36, labelYOffset: 10, strokeWidth: 4, strokeColor: '#fff', fontSize: "13px", fontWeight: "bold", fontFamily: 'roboto',externalGraphic: 'img/ic.png', graphicHeight: 36 }
        );

        vectorLayer.addFeatures(feature);
      });

      var bounds = vectorLayer.getDataExtent();
      map.zoomToExtent(bounds);
      vectorLayer.setVisibility(1);

  }

    
}

function setMapHistoryMarkers(data){

  var objdata = JSON.parse(data);

  console.log("LOCATION LENGTH -> "+objdata.length);

  if(objdata.length == 0) {
    vectorLayer.removeAllFeatures();
    vectorLayer.destroyFeatures();//optional
    vectorLayer.addFeatures([]);
    vectorLayer.setVisibility(0);
    alert("No Results Found..");
  } else {

      vectorLayer.removeAllFeatures();
      vectorLayer.destroyFeatures();//optional
      vectorLayer.addFeatures([]);
      vectorLayer.setVisibility(0);

      var server_time;
      var speed;
      var heading;
      var lat;
      var lon;
      var address;

      
      $.each(objdata, function (index, objvalue) {
          $.each(objvalue, function (name, value) {
              if (name == 'server_time') {
                  server_time = value;
              } else if (name == 'speed') {
                  speed = value;  
              } else if (name == 'heading') {
                  heading = parseInt(value);
              } else if (name == 'lat') {
                  lat = value;
              } else if (name == 'lon') {
                  lon = value;
              } else if (name == 'address') {
                  address = value;
              }

          });

          var feature = new OpenLayers.Feature.Vector(
                new OpenLayers.Geometry.Point(parseFloat(lon), parseFloat(lat)).transform('EPSG:4326', 'EPSG:900913'),
                {
                    description: ''
                },
                {label: "", labelOutlineColor: '#fff',labelOutlineWidth: 4, labelAlign: 'cm', labelXOffset: 36, labelYOffset: 10, strokeWidth: 4, strokeColor: '#fff', fontSize: "13px", fontWeight: "bold", fontFamily: 'roboto',externalGraphic: 'img/ic.png', graphicHeight: 36 }
        );

        vectorLayer.addFeatures(feature);
      });

      var bounds = vectorLayer.getDataExtent();
      map.zoomToExtent(bounds);
      vectorLayer.setVisibility(1);

  }

    
}

function completeTask() {
   var tobj = MobileUI.objectByForm('frmUpdateTasks');
  var tid = tobj.etask_id;
  var user_option = tobj.euser_option;
  var user_option_id = tobj.euser_option_id;
  var task_info = tobj.etask_info;
  var task_comments = tobj.etask_comments;
  var task_date = tobj.etask_date;
  
  if(user_option == "") {
    alert("Please select a user");
  } else if(task_info == "") {
    alert("Please enter task details");
  } else if(task_date == "") {
    alert("Please enter task date");
  } else if(task_comments == "") {
    alert("Please enter comment");
  } else {

    $.ajax({
      type: "POST",
      url: window.localStorage.getItem("setSiteUrl") + "process.php",
      data: {
        tp: "completetask",
        user_option: user_option,
        user_option_id: user_option_id,
        task_info: task_info,
        task_comments: task_comments,
        tid: tid,
        task_date: task_date,
        uname: window.localStorage.getItem("username"),
        uid: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
      },
      beforeSend: function(){
        loading('Saving task...');

       
      },
      success: function(data){
        
        //alert("Customer Notes Saved Successfully");
        loadTasks();
        backPage();
        MobileUI.clearForm('frmUpdateTasks');
        setCurrentDefaultDate();
        //backPage();

      },
      error: function(err){
        
      },
      complete: function(){
        closeLoading();
        
      }
    });



  }
}

//updateTargetCusVisit
function updateTargetCusVisit() {
  var tobj = MobileUI.objectByForm('frmUpdateTargetCustomer');
  var tid = tobj.etarget_id;
  var user_option = tobj.euser_option;
  var user_option_id = tobj.euser_option_id;
  var customer_option = tobj.ecustomer_option;
  var target_comments = tobj.etarget_comments;
  var target_period = tobj.etarget_period

    //alert(user_option + " # " + target_period);

  
  if(user_option == "") {
    alert("Please select a user");
  } else if(target_period == "") {
    alert("Period can not be empty");
  } else {
    //alert(user_option + " # " + target_period);
    $.ajax({
      type: "POST",
      url: window.localStorage.getItem("setSiteUrl") + "process.php",
      data: {
        tp: "updatecustarget",
        user_option: user_option,
        user_option_id: user_option_id,
        target_comments: target_comments,
        tid: tid,
        target_period: target_period,
        uname: window.localStorage.getItem("username"),
        uid: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
      },
      beforeSend: function(){
        //console.log(user_option);
        //console.log(user_option_id);
        //console.log(target_info);
        //console.log(target_date);
        //console.log(tid);
        loading('Saving target...');

       
      },
      success: function(data){
        
        //alert("Customer Notes Saved Successfully");
        //console.log(data);
        loadTargets('3');
        backPage();
        MobileUI.clearForm('frmUpdateTargetCustomer');
        //setCurrentDefaultDate();
        //backPage();

      },
      error: function(err){
        
      },
      complete: function(){
        closeLoading();
        
      }
    });



  }
}

//completetask  updatetask
function UpdateTarget() {
  var tobj = MobileUI.objectByForm('frmUpdateTarget');
  var tid = tobj.etarget_id;
  var user_option = tobj.euser_option;
  var user_option_id = tobj.euser_option_id;
  var target_category = tobj.etarget_category;
  var target_qty = tobj.etarget_qty;
  var target_comments = tobj.etarget_comments;
  var target_period = tobj.etarget_period

    //alert(user_option + " # " + target_period);

  
  if(user_option == "") {
    alert("Please select a user");
  } else if(target_period == "") {
    alert("Period can not be empty");
  } else {
    //alert(user_option + " # " + target_period);
    $.ajax({
      type: "POST",
      url: window.localStorage.getItem("setSiteUrl") + "process.php",
      data: {
        tp: "updatetarget",
        user_option: user_option,
        user_option_id: user_option_id,
        target_category: target_category,
        target_qty: target_qty,
        target_comments: target_comments,
        tid: tid,
        target_period: target_period,
        uname: window.localStorage.getItem("username"),
        uid: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
      },
      beforeSend: function(){
        
        loading('Saving target...');

       
      },
      success: function(data){
        
        //alert("Customer Notes Saved Successfully");
        //console.log(data);
        closeLoading();
        loadTargets('0');
        backPage();
        MobileUI.clearForm('frmUpdateTarget');
        setCurrentDefaultDate();
        //backPage();

      },
      error: function(err){
        
      },
      complete: function(){
        
        
      }
    });



  }
}

//Update Task
function UpdateTask() {
  var tobj = MobileUI.objectByForm('frmUpdateTasks');
  var tid = tobj.etask_id;
  var user_option = tobj.euser_option;
  var user_option_id = tobj.euser_option_id;
  var task_info = tobj.etask_info;
  var task_comments = tobj.etask_comments;
  var task_date = tobj.etask_date;
  
  if(user_option == "") {
    alert("Please select a user");
  } else if(task_info == "") {
    alert("Please enter task details");
  } else if(task_date == "") {
    alert("Please enter task date");
  } else if(task_comments == "") {
    alert("Please enter comment");
  } else {

    $.ajax({
      type: "POST",
      url: window.localStorage.getItem("setSiteUrl") + "process.php",
      data: {
        tp: "updatetask",
        user_option: user_option,
        user_option_id: user_option_id,
        task_info: task_info,
        task_comments: task_comments,
        tid: tid,
        task_date: task_date,
        uname: window.localStorage.getItem("username"),
        uid: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
      },
      beforeSend: function(){
        loading('Saving task...');

       
      },
      success: function(data){
        
        //alert("Customer Notes Saved Successfully");
        loadTasks();
        backPage();
        MobileUI.clearForm('frmUpdateTasks');
        setCurrentDefaultDate();
        //backPage();

      },
      error: function(err){
        
      },
      complete: function(){
        closeLoading();
        
      }
    });



  }
}

function setQuantityTarget() {
  var usobj = MobileUI.objectByForm('frmSetQuantityTarget');
  var user_option = usobj.user_option;
  var user_option_id = usobj.user_option_id;
  var target_info = usobj.target_info;
  var target_date = usobj.target_date;

  /*console.log("############ CUSTOMER DETAILS ############");
  console.log(customer_option);
  console.log(customer_option_id);*/


  if(user_option == "") {
    alert("Please select a user");
  } else if(target_info == "") {
    alert("Please enter target quantity");
  } else if(target_date == "") {
    alert("Please enter target date");
  } else {

    $.ajax({
      type: "POST",
      url: window.localStorage.getItem("setSiteUrl") + "process.php",
      data: {
        tp: "setquantitytarget",
        user_option: user_option,
        user_option_id: user_option_id,
        target_info: target_info,
        target_date: target_date,
        uname: window.localStorage.getItem("username"),
        uid: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
      },
      beforeSend: function(){
        loading('Saving target...');

       
      },
      success: function(data){
        
        //alert("Customer Notes Saved Successfully");
        MobileUI.clearForm('frmSetTarget');
        setCurrentDefaultDate();
        loadTargets('1');

      },
      error: function(err){
        
      },
      complete: function(){
        closeLoading();
        
      }
    });



  }
}

//setTargetCusVisit
function setTargetCusVisit() {
  var usobj = MobileUI.objectByForm('frmSetTargetCustomer');
  var user_option = usobj.user_option;
  var user_option_id = usobj.user_option_id;
  var allcustomerchecker = usobj.allcustomerchecker;
  if(allcustomerchecker == true){ 
    var customer_option = "";
  } else {

    var customer_option = usobj.customer_option;
  }
  var target_period = usobj.target_period;


  /*console.log("############ CUSTOMER DETAILS ############");
  console.log(customer_option);
  console.log(customer_option_id);*/


  if(user_option == "") {
    alert("Please select a user");
  } else if(target_period == "") {
    alert("Please select target period");
  } else {

    $.ajax({
      type: "POST",
      url: window.localStorage.getItem("setSiteUrl") + "process.php",
      data: {
        tp: "setcustarget",
        user_option: user_option,
        user_option_id: user_option_id,
        customer_option: customer_option,
        target_period: target_period,
        uname: window.localStorage.getItem("username"),
        uid: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
      },
      beforeSend: function(){
        loading('Saving target...');

       
      },
      success: function(data){
        
        //alert("Customer Notes Saved Successfully");
        MobileUI.clearForm('frmSetTargetCustomer');
        setCurrentDefaultDate();
        loadTargets("3");

      },
      error: function(err){
        
      },
      complete: function(){
        closeLoading();
        
      }
    });



  }
}

function setTarget() {
  var usobj = MobileUI.objectByForm('frmSetTarget');
  var user_option = usobj.user_option;
  var user_option_id = usobj.user_option_id;
  /*var allitemschecker = usobj.allitemschecker;
  if(allitemschecker == true){ 
    var item_option = "";
  } else {

    var item_option = usobj.item_option;
  } */
  
  var target_category = usobj.target_category;
  var target_category_name = $("#target_category option:selected").text();
  var target_qty = usobj.target_qty;
  //var target_amount = usobj.target_amount;
  var target_period = usobj.target_period;

  console.log("############ TARGET DETAILS ############");
  console.log(user_option);
  console.log(user_option_id);
  console.log(target_category);
  console.log(target_category_name);
  console.log(target_qty);
  console.log(target_period);

  if(user_option == "") {
    alert("Please select a user");
  } else if(target_period == "") {
    alert("Please select target period");
  } else {

    $.ajax({
      type: "POST",
      url: window.localStorage.getItem("setSiteUrl") + "process.php",
      data: {
        tp: "settarget",
        user_option: user_option,
        user_option_id: user_option_id,
        //item_option: item_option,
        //target_amount: target_amount,
        target_category: target_category,
        target_category_name: target_category_name,
        target_qty: target_qty,
        target_period: target_period,
        uname: window.localStorage.getItem("username"),
        uid: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
      },
      beforeSend: function(){
        loading('Saving target...');

       
      },
      success: function(data){
        console.log(data);
        closeLoading();
        //alert("Customer Notes Saved Successfully");
        MobileUI.clearForm('frmSetTarget');
        setCurrentDefaultDate();
        loadTargets("0");

      },
      error: function(err){
        
      },
      complete: function(){
        
        
      }
    });



  }
}

function assignTask() {
  var usobj = MobileUI.objectByForm('frmAssignTasks');
  var user_option = usobj.user_option;
  var user_option_id = usobj.user_option_id;
  var task_info = usobj.task_info;
  var task_date = usobj.task_date;

  /*console.log("############ CUSTOMER DETAILS ############");
  console.log(customer_option);
  console.log(customer_option_id);*/


  if(user_option == "") {
    alert("Please select a user");
  } else if(task_info == "") {
    alert("Please enter task details");
  } else if(task_date == "") {
    alert("Please enter task date");
  } else {

    $.ajax({
      type: "POST",
      url: window.localStorage.getItem("setSiteUrl") + "process.php",
      data: {
        tp: "assigntask",
        user_option: user_option,
        user_option_id: user_option_id,
        task_info: task_info,
        task_date: task_date,
        uname: window.localStorage.getItem("username"),
        uid: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
      },
      beforeSend: function(){
        loading('Saving task...');

       
      },
      success: function(data){
        
        //alert("Customer Notes Saved Successfully");
        MobileUI.clearForm('frmAssignTasks');
        setCurrentDefaultDate();
        loadTasks();

      },
      error: function(err){
        
      },
      complete: function(){
        closeLoading();
        
      }
    });



  }
}


function saveGeneralNotes(){
  $('#btnGenNotes').prop('disabled', true);
  var genobj = MobileUI.objectByForm('frmGeneralNotes');
  var customer_option = genobj.gen_customer_option;
  var notes = genobj.gen_notes;

  var userStoredId = window.localStorage.getItem("id");

  /*console.log("############ CUSTOMER DETAILS ############");
  console.log(customer_option);
  console.log(customer_option_id);*/


  if(customer_option == "") {
    alert("Please enter a customer");
  } else if(notes == "") {
    alert("Please enter notes");
  } else if(parseInt(userStoredId) == 0) {
    alert("Your User ID is 0. Kindly logout and login again.");
  } else {

    navigator.geolocation.getCurrentPosition(function(position) {
    
        //SAVE CUSTOMETS LOCATION
        $.ajax({
          type: "POST",
          url: window.localStorage.getItem("setSiteUrl") + "process.php",
          data: {
            tp: "savegeneralnotes",
            customer_option: customer_option,
            lat: position.coords.latitude,
            lon: position.coords.longitude,
            notes: notes,
            uname: window.localStorage.getItem("username"),
            id: window.localStorage.getItem("id"),
            cp: window.localStorage.getItem("cp")
          },
          beforeSend: function(){
            
            loading('Saving General notes...');
            console.log(customer_option);
            console.log(notes);
            console.log(position.coords.latitude);
            console.log(position.coords.longitude);
            console.log(window.localStorage.getItem("username"));
            console.log(window.localStorage.getItem("id"));
            console.log(window.localStorage.getItem("cp"));

           
          },
          success: function(data){
            
            console.log(data);
            MobileUI.clearForm('frmGeneralNotes');
            loadGeneralNotes();
            //$('#btnCustNotes').prop('disabled', false);
            $('#btnGenNotes').prop('disabled', false);

          },
          error: function(err){
            
          },
          complete: function(){
            closeLoading();
            
            
          }
        });



    }, function(error){
      
    });
  }     

}


function saveCustomerNotes(){
  $('#btnCustNotes').prop('disabled', true);
  var cusobj = MobileUI.objectByForm('frmCustomerNotes');
  var customer_option = cusobj.customer_option;
  var customer_option_id = cusobj.customer_option_id;
  var notes = cusobj.notes;

  var userStoredId = window.localStorage.getItem("id");

  /*console.log("############ CUSTOMER DETAILS ############");
  console.log(customer_option);
  console.log(customer_option_id);*/


  if(customer_option == "") {
    alert("Please select a customer");
  } else if(notes == "") {
    alert("Please enter notes");
  } else if(parseInt(userStoredId) == 0) {
    alert("Your User ID is 0. Kindly logout and login again.");
  } else {

    navigator.geolocation.getCurrentPosition(function(position) {
    
        //SAVE CUSTOMETS LOCATION
        $.ajax({
          type: "POST",
          url: window.localStorage.getItem("setSiteUrl") + "process.php",
          data: {
            tp: "savecustomernotes",
            customer_option: customer_option,
            customer_option_id: customer_option_id,
            lat: position.coords.latitude,
            lon: position.coords.longitude,
            notes: notes,
            uname: window.localStorage.getItem("username"),
            id: window.localStorage.getItem("id"),
            cp: window.localStorage.getItem("cp")
          },
          beforeSend: function(){
            
            loading('Saving notes...');

           
          },
          success: function(data){
            //console.log(" ***** CUSTOMER NOTES ****** ");
            //alert(data);
            //alert("Customer Notes Saved Successfully");
            
              MobileUI.clearForm('frmCustomerNotes');
              loadCustomersNotes();
              //$('#btnCustNotes').prop('disabled', false);
            
            
            $('#btnCustNotes').prop('disabled', false);

          },
          error: function(err){
            
          },
          complete: function(){
            closeLoading();
            
            
          }
        });



    }, function(error){
      
    });
  }     

}

function loadCreditNotes(){
    var crdobj = MobileUI.objectByForm('formCreditNote');
    var customer_option = crdobj.customer_option;
    var customer_option_id = crdobj.customer_option_id;
    
    loading('Please wait...');
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "loadCreditNotes",
        customer_option: customer_option,
        customer_option_id: customer_option_id,
        uname: window.localStorage.getItem("username"),
        id: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      
      closeLoading();
      var crddata = JSON.parse(data);
      if(crddata.length == 0) {
        alert("No Credit Note Found.");
      } else {

        creditNoteArray = crddata;
        creditNoteArraySearch = crddata;

      }
          
    });
}

function saveCustomerCurrentLocation(){
    var cusobj = MobileUI.objectByForm('frmCustomerLocationSetter');
    var customer_option = cusobj.customer_option;
    var customer_option_id = cusobj.customer_option_id;
    var lat = cusobj.lat;
    var lon = cusobj.lon;
    //alert(customer_option_id);
    loading('Please wait...');
      //closeLoading();
    //SAVE CUSTOMETS LOCATION
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "savecustomerloc",
        customer_option: customer_option,
        customer_option_id: customer_option_id,
        lat: lat,
        lon: lon,
        uname: window.localStorage.getItem("username"),
        id: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      
      closeLoading();
      MobileUI.clearForm('frmCustomerLocationSetter');
      alert("Customer Location Saved Successfully");
          
    });
}

function success(res){
      //alert("s1: "+JSON.stringify(res));

      var justify_center = '\x1B\x61\x01';
      var justify_left   = '\x1B\x61\x00';
      var qr_model       = '\x32';          // 31 or 32
      var qr_size        = '\x08';          // size
      var qr_eclevel     = '\x33';          // error correction level (30, 31, 32, 33 - higher)
      var qr_data        = 'http://digisoftsolutions.co.ke';
      var qr_pL          = String.fromCharCode((qr_data.length + 3) % 256);
      var qr_pH          = String.fromCharCode((qr_data.length + 3) / 256);

      var pname = res[0];
      var paddress = res[1];
      var ptype = res[2];


      //Connect to printer 
      BTPrinterObj.connect(function(data){
        //alert("s2: "+JSON.stringify(data));

        var receiptRef = posReceiptRef.split('#');
        var receiptNumberPosAct = 'POS#'+str_pad(receiptRef[2], 9, "0", "STR_PAD_LEFT");

        //1ST PRINT
        //Receipt Title Header
        BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
        BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
        BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
        BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

        //Receipt Header
        BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'SALES RECEIPT\n','1','1');
        BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
        //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
        BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

        BTPrinterObj.printText(function(data){
          //Clear all pos data
          
        },null, justify_left +
        '\n\n' +
        itm +               // Print
        '\n\n\n' +
        'Served By: ' + window.localStorage.getItem("username") +
       '\n\n' +
        'THANKYOU' +
       '\n\n\n\n\n\n' +
       justify_left,'1','0');


        //2ND PRINT
        //Receipt Title Header
        BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
        BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
        BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
        BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

        //Receipt Header
        BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'SALES RECEIPT REPRINT\n','1','1');
        BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
        //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
        BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

        BTPrinterObj.printText(function(data){

          //CLEAR ALL POS DATA
          clearCpayData();
          
        },null, justify_left +
        '\n\n' +
        itm +               // Print
        '\n\n\n' +
        'Served By: ' + window.localStorage.getItem("username") +
       '\n\n' +
        'THANKYOU' +
       '\n\n\n\n\n\n' +
       justify_left,'1','0');

      },function(err){  

        clearCpayData();

      }, pname);


  }

function err(e){
    
    clearCpayData();
}

function testPrint(){
  
  //alert(phpdate('M j, Y, g:i a'));

  /*BTPrinterObj.list(function(res){

      var pname = res[0];
      var paddress = res[1];
      var ptype = res[2];

      //Connect to printer 
      BTPrinterObj.connect(function(data){
        //alert("s2: "+JSON.stringify(data))

        var receiptRef = posReceiptRef.split('#');

        //1st Print
        //Receipt Title Header
        BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
        BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
        BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
        BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

        //Receipt Header
        BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'ORDER ENTRY\n','1','1');
        BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
        BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
        BTPrinterObj.printTitle(null,null,'Ref: '+ receiptRef[0],'1','1');

        BTPrinterObj.printText(function(sd){
          //posReceiptRef = "";
        },null, justify_left +
        '\n\n' +
        window.localStorage.getItem("coy_name") +
        '\n\n' +
        window.localStorage.getItem("domicile") + 
        '\n\n' +
        'Tel: '+ window.localStorage.getItem("rphone") + 
        '\n\n' +
        'Email: '+ window.localStorage.getItem("remail") + 
        '\n\n\n' +
        'Served By: ' + window.localStorage.getItem("username") +
        '\n\n\n' +
        'THANKYOU' +
        '\n\n\n\n\n\n' +
        justify_left,'1','0');

        //2st Print
        //Receipt Title Header
        BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
        BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
        BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
        BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

        //Receipt Header
        BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'ORDER ENTRY\n','1','1');
        BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
       // BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
        BTPrinterObj.printTitle(null,null,'Ref: '+ receiptRef[2],'1','1');

        BTPrinterObj.printText(function(sd){
          posReceiptRef = "";
        },null, justify_left +
        '\n\n' +
        window.localStorage.getItem("coy_name") +
        '\n\n' +
        window.localStorage.getItem("domicile") + 
        '\n\n' +
        'Tel: '+ window.localStorage.getItem("rphone") + 
        '\n\n' +
        'Email: '+ window.localStorage.getItem("remail") + 
        '\n\n\n' +
        'Served By: ' + window.localStorage.getItem("username") +
        '\n\n\n' +
        'THANKYOU' +
        '\n\n\n\n\n\n' +
        justify_left,'1','0');

      },function(err){     
        //alert("e2: "+err);
      }, pname);


  },function(err){

  });*/

}

function reprintPOSReceipt() {

  
  var objPosPayments = JSON.parse(decodeURIComponent(posTransactionsObjectString.payments));
  var objPosItems = JSON.parse(decodeURIComponent(posTransactionsObjectString.pitems));

  var posPrintCustomer = posTransactionsObjectString.customername;
  var receiptNumberPos = str_pad(posTransactionsObjectString.id, 9, "0", "STR_PAD_LEFT");

  var posItm = "";
  
  var posItmPptholder = "";
  var posttlPaid = 0;
  var posttlBalance = 0;


  $.each(objPosPayments, function(index, value){
      console.log(value.Transtype + ' --------- ' + value.TransAmount);
      var ttm = "";
      if(value.Transtype == "MPESA"){
        posItmPptholder += 'MPESA  : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
      } else {
        posItmPptholder += 'CASH   : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
      }
      console.log(ttm);
      var posItmPp = "";
      var posItmPpt = "";
      $.each(value, function(key, cell){
          
            if(key == "TransAmount") {
              posttlPaid = posttlPaid + parseInt(cell);
            }

            if(key == "Transtype") {
              if(cell == "CASH"){
                posItmPpt = 'CASH PAID  :      ';
              } else if(cell == "MPESA"){
                posItmPpt = 'MPESA PAID :      ';
              } else {
                posItmPpt = cell+' PAID :      ';
              }
              
            }

            if(key == "TransAmount") {
              
                posItmPp = cell+'\n\n';
              
              
            }
              
        });

        //posItmPptholder += posItmPpt + posItmPp;

       
    });

  posttlBalance = posttlPaid - parseInt(posTransactionsObjectString.ptotal);

  $.each(objPosItems, function(index, value){
      //console.log(value[index]['item_option'] + ' --------- ' + value[index]['price']);

      posItm += value.item_option+' - '+value.item_option_id+'   '+value.price+'x'+value.quantity+'    '+value.total+'\n\n';

      
     /* $.each(value, function(key, cell){
          
            if(key == "item_option") {
              posItm += cell+'\n';
            }
            if(key == "quantity") {
              posItm += 'x'+cell;
            }
            if(key == "price") {
              posItm += cell;
            }
            if(key == "total") {
              posItm += '    '+cell+'   \n\n';
            }
              
        });*/
       
    });
   posItm += '\n\n\n';
   posItm += 'TOTAL  :              '+posTransactionsObjectString.ptotal+'\n\n';
   posItm += posItmPptholder;
   posItm += 'CHANGE :              '+posttlBalance+'\n\n';
   posItm += '\n\n\n';
   posItm += 'Served By: ' + window.localStorage.getItem("username") + '\n\n';
   posItm += 'Customer: ' + posPrintCustomer + '\n\n';
   posItm += 'THANKYOU' + '\n\n\n\n\n\n';

   console.log(posItm);

  console.log("R: "+receiptNumberPos);

  try {
     BTPrinterObj.list(function(res){
        //console.log("Success");
        var pname = res[0];
        var paddress = res[1];
        var ptype = res[2];
        //alert("s: "+ data) // bt status: true or false


        //Connect to printer 
        BTPrinterObj.connect(function(data){
          //alert("sc: "+JSON.stringify(data));

          var receiptRef = posReceiptRef.split('#');

          //1ST PRINT
          //Receipt Title Header
          BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
          BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
          BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
          BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

          //Receipt Header
          BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'SALES RECEIPT\n','1','1');
          BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
          //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
          BTPrinterObj.printTitle(null,null,'Ref: POS#'+ receiptNumberPos,'1','1');

          BTPrinterObj.printText(null,null, justify_left +
          '\n\n' +
          posItm +  
         '\n\n\n\n\n\n' +
         justify_left,'1','0');


          //2ND PRINT
          //Receipt Title Header
          BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
          BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
          BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
          BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

          //Receipt Header
          BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'SALES RECEIPT REPRINT\n','1','1');
          BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
          //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
          BTPrinterObj.printTitle(null,null,'Ref: POS#'+ receiptNumberPos,'1','1');

          BTPrinterObj.printText(null,null, justify_left +
          '\n\n' +
          posItm +  
         '\n\n\n\n\n\n' +
         justify_left,'1','0');

        },function(err){     
          alert("Printer Connection Error!!");
        }, pname);





      },function(err){
        //console.log("Error");
        alert(err+ '. Please enable and pair to the Bluetooth Printer and try again!!');
      });


  } catch(err) {

  }
   

}

function startToPrint3() {



  //console.log("################# POS BOOKING ITEMS #################");
  var cashp = 0;
  var cashc = 0;
  var ttDiscount = 0;
  var ttTax = 0;
  var customer = "";
   $.each(bookingItems, function(index, value){

      customer = value.customer_option;
      ttDiscount = ttDiscount + (parseFloat(value.discount) * parseFloat(value.quantity));
      ttTax = ttTax + (parseFloat(value.tax) * parseFloat(value.quantity));

      itm += value.item_option+' - '+value.item_option_id+'   '+value.price+'x'+value.quantity+'    '+value.total+'\n\n';


       
    });
   itm += '\n\n\n';
   itm += 'CUSTOMER : '+customer+'\n\n';
   itm += 'TOTAL          :      '+posTotal+'\n\n';
   itm += 'TOTAL DISCOUNT :      '+ttDiscount+'\n\n';
   if(window.localStorage.getItem("setSiteTax") == "1"){
    itm += 'TOTAL TAX      :      '+ttTax+'\n\n';
   }
   itm += 'CASH PAID      :      '+cashp+'\n\n';
   itm += 'CHANGE         :      '+cashc+'\n\n';
   //console.log(itm);
   //console.log("################# END #################");

    try {

      BTPrinterObj.list(success,err);
      
    } catch(err){
      clearCpayData();
    }
  
}

function startToPrint2() {

  var cashp = 0;
  var cashc = 0;

  $.each(paymentTypesUsed, function(i,v){
      cashp = cashp + parseInt(v.TransAmount);
  });

  var cashc = parseInt(cashp) - parseInt(posTotal);

  //console.log("################# POS ITEMS #################");

  var ttDiscount = 0;
  var ttTax = 0;
   $.each(bookingItems, function(index, value){

      ttDiscount = ttDiscount + (parseFloat(value.discount) * parseFloat(value.quantity));
      ttTax = ttTax + (parseFloat(value.tax) * parseFloat(value.quantity));

      itm += value.item_option+' - '+value.item_option_id+'   '+value.price+'x'+value.quantity+'    '+value.total+'\n\n';

             
       
    });
   itm += '\n\n\n';
   itm += 'TOTAL          :      '+posTotal+'\n\n';
   itm += 'TOTAL DISCOUNT :      '+ttDiscount+'\n\n';
   if(window.localStorage.getItem("setSiteTax") == "1"){
    itm += 'TOTAL TAX      :      '+ttTax+'\n\n';
   }
   itm += 'CASH PAID      :      '+cashp+'\n\n';
   itm += 'CHANGE         :      '+cashc+'\n\n';
   //console.log(itm);
   //console.log("################# END #################");

    try {

      BTPrinterObj.list(success,err);

    } catch(err){
      clearCpayData();
    }
  
}

function startToPrint(tp) {

  var cashp = 0;
  var cashc = 0;

  $.each(paymentTypesUsed, function(i,v){
      cashp = cashp + parseInt(v.TransAmount);
  });

  var cashc = parseInt(cashp) - parseInt(posTotal);


  //console.log("################# POS ITEMS #################");
  var ttDiscount = 0;
  var ttTax = 0;

   $.each(posItems, function(index, value){
      //console.log(value[index]['item_option'] + ' --------- ' + value[index]['price']);
      ttDiscount = ttDiscount + (parseFloat(value.discount) * parseFloat(value.quantity));
      ttTax = ttTax + (parseFloat(value.tax) * parseFloat(value.quantity));

      itm += value.item_option+' - '+value.item_option_id+'   '+value.price+'x'+value.quantity+'    '+value.total+'\n\n';
       
    });
   itm += '\n\n\n';
   itm += 'TOTAL          :      '+posTotal+'\n\n';
   itm += 'TOTAL DISCOUNT :      '+ttDiscount+'\n\n';
   if(window.localStorage.getItem("setSiteTax") == "1"){
    itm += 'TOTAL TAX      :      '+ttTax+'\n\n';
   }
   itm += 'CASH PAID      :      '+cashp+'\n\n';
   itm += 'CHANGE         :      '+cashc+'\n\n';
   //console.log(itm);
   //console.log("################# END #################");

    try {

      BTPrinterObj.list(success,err);
      
    } catch(err){
      clearCpayData();
    }
  
}

function getCustomer2(cusname, cusbrcode, cuslat, cuslon){
  //alert(cusname);
  //var cusobj = {in: 0, name: cusname};
  //MobileUI.formByObject('formCustomerSearch', cusobj);
  backPage();
  var decCusname = decodeURIComponent(cusname);
  $('#customer_option').val(decCusname);
  console.log("CUSTOMER ID: " + cusbrcode);
  $('#customer_option_id').val(cusbrcode);
  $('#discount').val("0");
  window.localStorage.setItem("userpay", decCusname);
                
  console.log(decCusname);
   // alert(data);
  
  
  window.localStorage.setItem("userpayid", cusbrcode);

  $('#lat').val(cuslat);
  $('#lon').val(cuslon);
   
}

function getCustomerNew(cusname, trno){
  console.log(cusname);
  //var cusobj = {in: 0, name: cusname};
  //MobileUI.formByObject('formCustomerSearch', cusobj);
  backPage();
  $('.customer_option').val(decodeURIComponent(cusname));
  //MobileUI.hide('listProfileGitHub');


      console.log("DEBTOR_NO: "+trno);
      
      $('#customer_option_id').val(trno);

      //Load Customer Specific Branches
      branchArray.length = 0;
      branchArraySearch.length = 0;
      $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
      {
          tp: "loadBranches",
          cuscode: trno,
          cp: window.localStorage.getItem("cp")
      },
      function(data, status){
        //closeLoading();
        
        branchArray = JSON.parse(data);

        $.each(branchArray, function(index, value){
            branchArraySearch.push(value);
            
        });

        console.log("####### DEF BRANCH #######");
        //console.log(branchArraySearch[0].branch_ref);
        //console.log(branchArraySearch[0].branch_code);
        //console.log(branchArraySearch);
        $("#branch_option").val(branchArraySearch[0].branch_ref);
        $("#branch_option_id").val(branchArraySearch[0].branch_code);
        //alert(branchArraySearch[0].debtor_no);
         getCreditLimit(branchArraySearch[0].debtor_no);
        //

        //$("#").val(branchArraySearch[0].);
        
        //loadItems();
            
      });
          
    
}

function getCustomer(cusname){
  console.log(cusname);
  //var cusobj = {in: 0, name: cusname};
  //MobileUI.formByObject('formCustomerSearch', cusobj);
  backPage();
  $('.customer_option').val(decodeURIComponent(cusname));
  //MobileUI.hide('listProfileGitHub');

  //GET ID OF BRANCH SELECTED
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getCustomerId",
        cus: cusname,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){

      console.log("DEBTOR_NO: "+data);
      
      $('#customer_option_id').val(data);

      //Load Customer Specific Branches
      branchArray.length = 0;
      branchArraySearch.length = 0;
      $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
      {
          tp: "loadBranches",
          cuscode: data,
          cp: window.localStorage.getItem("cp")
      },
      function(data, status){
        //closeLoading();
        
        branchArray = JSON.parse(data);

        $.each(branchArray, function(index, value){
            branchArraySearch.push(value);
            
        });

        console.log("####### DEF BRANCH #######");
        //console.log(branchArraySearch[0].branch_ref);
        //console.log(branchArraySearch[0].branch_code);
        //console.log(branchArraySearch);
        $("#branch_option").val(branchArraySearch[0].branch_ref);
        $("#branch_option_id").val(branchArraySearch[0].branch_code);
        //alert(branchArraySearch[0].debtor_no);
         getCreditLimit(branchArraySearch[0].debtor_no);
        //

        //$("#").val(branchArraySearch[0].);
        
        //loadItems();
            
      });
          
    });
}

function getCreditLimit(dt) {

    //GET CUSTOMETS CREDIT AMOUNT
    $.ajax({
      type: "POST",
      url: window.localStorage.getItem("setSiteUrl") + "process.php",
      data: {
        tp: "getcustomerscredit",
        dn: dt,
        uname: window.localStorage.getItem("username"),
        id: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
      },
      beforeSend: function(){
        //loading('Checking Credit...');

        $('#cdtval').text("0");
       
      },
      success: function(data){
        var cdtval = parseInt(data);
        if(cdtval >= 0){
          $('#cdtval').removeClass('red');
          $('#cdtval').addClass('green');
          $('#cdtval').text(cdtval);
        } else {
          $('#cdtval').addClass('red');
          $('#cdtval').removeClass('green');
          $('#cdtval').text(cdtval);
        }
        

      },
      error: function(err){
        
      },
      complete: function(){
        //closeLoading();
        
      }
    });


}

function setCashValuesBooking(){
  var obj = MobileUI.objectByForm('formCashPayment');
  var name = obj.cashcustomername;
  var TransID = Math.floor(Date.now() / 1000);
  var TransAmount = obj.cashpayment;
  var Transtype = obj.paytype;
  var Cheque = obj.pref;
  partialPaymentTotal = parseInt($("#cppaid").text());
  //formCashPayment
  if(name == ""){
    alert('Please Enter Clients Name');
  } else {
    //var pcashObj = {name: name, TransID: TransID, TransAmount: TransAmount, Auto: TransID, TransTime: TransID, MSISDN: '', Transtype: Transtype};


    var tempTot = 0;
    var alltempTot = 0;

    $.each(paymentTypesUsed, function(i,v){
        tempTot = tempTot + parseInt(v.TransAmount);
    });

    alltempTot = parseInt(TransAmount) + tempTot;

    var tempbal = parseInt(posTotal) - alltempTot;


    if(parseInt(tempbal) <= 0) {
      var cptt = parseInt(TransAmount) - Math.abs(tempbal);
    } else {
      var cptt = TransAmount;
    }

    
    partialPaymentTotal = parseInt(TransAmount) + parseInt(partialPaymentTotal);
   var mbal = parseInt(partialPaymentTotal) - parseInt(posTotal);

    

   var pcashObj = {name: name, TransID: TransID, TransAmount: cptt, Auto: TransID, TransTime: TransID, MSISDN: '', Transtype: Transtype, p: '0', Cheque: Cheque};
   paymentTypesUsed.push(pcashObj);
    console.log("%%%%%%%%%%%% CASH PAYMENT %%%%%%%%%%%%%%%%");
    console.log("PAID: "+alltempTot);
    console.log("BALANCE: "+tempbal);
    console.log(pcashObj);


    if(parseInt(partialPaymentTotal) < parseInt(posTotal)){

      //$("#mpesapaymentfield").val(pcashObj.TransAmount);
      $("#cptotal").text(posTotal);
      $("#cppaid").text(partialPaymentTotal);
      $("#cpbalance").text(mbal);
      //$("#mtransId").val(pcashObj.Auto);
      //$('#mtransIdRef').val(pcashObj.TransID);
    } else {
      //$("#mpesapaymentfield").val(pcashObj.TransAmount);
      $("#mcptotal").text(posTotal);
      $("#mcppaid").text(partialPaymentTotal);
      $("#mcpbalance").text(mbal);
      //$("#mtransId").val(pcashObj.Auto);
      //$('#mtransIdRef').val(pcashObj.TransID);
    }
    
    
    gAuto = pcashObj.Auto;
    gTransID = pcashObj.TransID;
  }
  
}

function setCashValues(){
  var obj = MobileUI.objectByForm('formCashPayment');
  var name = obj.cashcustomername;
  var TransID = Math.floor(Date.now() / 1000);
  var TransAmount = obj.cashpayment;
  var Transtype = obj.paytype;
  var Cheque = obj.pref;
  //formCashPayment
  if(name == ""){
    alert('Please Enter Clients Name');
  } else {

    var tempTot = 0;
    var alltempTot = 0;

    $.each(paymentTypesUsed, function(i,v){
        tempTot = tempTot + parseInt(v.TransAmount);
    });

    alltempTot = parseInt(TransAmount) + tempTot;

    var tempbal = parseInt(posTotal) - alltempTot;

    if(parseInt(tempbal) <= 0) {
      var cptt = parseInt(TransAmount) - Math.abs(tempbal);
    } else {
      var cptt = TransAmount;
    }

    
    partialPaymentTotal = parseInt(TransAmount) + parseInt(partialPaymentTotal);
   var mbal = parseInt(partialPaymentTotal) - parseInt(posTotal);
   
  

   var pcashObj = {name: name, TransID: TransID, TransAmount: cptt, Auto: TransID, TransTime: TransID, MSISDN: '', Transtype: Transtype, Cheque: Cheque};
    paymentTypesUsed.push(pcashObj);
    console.log("%%%%%%%%%%%% CASH PAYMENT %%%%%%%%%%%%%%%%");
    console.log("PAID: "+alltempTot);
    console.log("BALANCE: "+tempbal);
    console.log(pcashObj);


    if(parseInt(partialPaymentTotal) < parseInt(posTotal)){

      //$("#mpesapaymentfield").val(pcashObj.TransAmount);
      $("#cptotal").text(posTotal);
      $("#cppaid").text(alltempTot);
      $("#cpbalance").text(tempbal);
      //$("#mtransId").val(pcashObj.Auto);
      //$('#mtransIdRef').val(pcashObj.TransID);
    } else {
      //$("#mpesapaymentfield").val(pcashObj.TransAmount);
      $("#mcptotal").text(posTotal);
      $("#mcppaid").text(alltempTot);
      $("#mcpbalance").text(tempbal);
      //$("#mtransId").val(pcashObj.Auto);
      //$('#mtransIdRef').val(pcashObj.TransID);
    }
    
    
    gAuto = pcashObj.Auto;
    gTransID = pcashObj.TransID;
  }
  
}
//

function setBookingMpesaValues(params){
  //paymentTypesUsed.length = 0;
  
  partialPaymentTotal = parseInt($("#mcppaid").text());
  paymentTypesUsed.push(params);
  partialPaymentTotal = parseInt(params.TransAmount) + parseInt(partialPaymentTotal);
   var mbal = parseInt(partialPaymentTotal) - parseInt(posTotal);
  if(parseInt(partialPaymentTotal) < parseInt(posTotal)){

    //$("#mpesapaymentfield").val(params.TransAmount);
    $("#mcptotal").text(posTotal);
    $("#mcppaid").text(partialPaymentTotal);
    $("#mcpbalance").text(mbal);
    $("#mtransId").val(params.Auto);
    $('#mtransIdRef').val(params.TransID);
  } else {
    //$("#mpesapaymentfield").val(params.TransAmount);
    $("#mcptotal").text(posTotal);
    $("#mcppaid").text(partialPaymentTotal);
    $("#mcpbalance").text(mbal);
    $("#mtransId").val(params.Auto);
    $('#mtransIdRef').val(params.TransID);
  }


  gAuto = params.Auto;
  gTransID = params.TransID;
}

function setMpesaValues(params){
  //paymentTypesUsed.length = 0;
  partialPaymentTotal = 0;
  paymentTypesUsed.push(params);

  
  $.each(paymentTypesUsed, function(index, value){
    if(value != undefined) {
      partialPaymentTotal = partialPaymentTotal + parseInt(value.TransAmount);
    }
    
  });

  

  //paymentTypesUsed.push(params);
  //partialPaymentTotal = parseInt(params.TransAmount) + parseInt(partialPaymentTotal);
   var mbal = parseInt(partialPaymentTotal) - parseInt(posTotal);
  if(parseInt(partialPaymentTotal) < parseInt(posTotal)){

    $("#mpesapaymentfield").val(params.TransAmount);
    $("#mcptotal").text(posTotal);
    $("#mcppaid").text(partialPaymentTotal);
    $("#mcpbalance").text(mbal);
    $("#mtransId").val(params.Auto);
    $('#mtransIdRef').val(params.TransID);
  } else {
    $("#mpesapaymentfield").val(params.TransAmount);
    $("#mcptotal").text(posTotal);
    $("#mcppaid").text(partialPaymentTotal);
    $("#mcpbalance").text(mbal);
    $("#mtransId").val(params.Auto);
    $('#mtransIdRef').val(params.TransID);
  }
  
  
  gAuto = params.Auto;
  gTransID = params.TransID;
  //alert(params.Auto + '' + params.TransID);
  //
}

function setMpesaValuesCustomerPayment(params){

  $("#amount").val(parseInt(params.TransAmount));
  $("#mcptotal").text(params.TransID);
  
}

/*function alertCustomerForm(){
      alert({
        title:'Find Customer',
        message:"<div id='template-alert-custom'><div class='list' id='formCustomerSearch'><div class='item icon ion-search'><input type='text'  id='cussearch' placeholder='Customer Search...' onkeyup='autochangedd()'></div><div style='height:200px;overflow:scroll;'><div class='item hidden grey-200' id='listProfileGitHub' data='customersArraySearch' onclick='getCustomer(&quot;${name}&quot;)'><label><h3>${name}</h3></label></div></div></div></div>",
        id: 'my-custom-search-dialog',
        width:'90%',
        buttons:[
          {
            label: 'Cancel',
            onclick: function(){
              $sval = $('#cussearch').val();
                if($sval == ""){
                    alert("Please Select a Customer");
                } else {
                    //alert($sval);
                    //github_usernameold
                    $('#customer_option').val($sval);
                }

                closeAlert('my-custom-search-dialog');
            }
          }
        ]
      });
    }*/
//

function getRoute(rname){

  backPage();
  $('#route_option').val(rname);
  //MobileUI.hide('listBranch');

  //GET ID OF BRANCH SELECTED
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getRouteId",
        rt: rname,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      
      $('#route_option_id').val(data);
          
    });   


}

function getCusBranch(brname){
  //alert(cusname);
  //var cusobj = {in: 0, name: cusname};
  //MobileUI.formByObject('formCustomerSearch', cusobj);
  backPage();
  $('#branch_option').val(brname);
  //MobileUI.hide('listBranch');

  //GET ID OF BRANCH SELECTED
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getBranchId",
        br: brname,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      
      $('#branch_option_id').val(data);
          
    });
}

function getBranch(brname){
  //alert(cusname);
  //var cusobj = {in: 0, name: cusname};
  //MobileUI.formByObject('formCustomerSearch', cusobj);
  backPage();
  $('#branch_option').val(brname);
  //MobileUI.hide('listBranch');

  //GET ID OF BRANCH SELECTED
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getBranchId",
        br: brname,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      
      $('#branch_option_id').val(data);
          
    });
}

/*function alertBranchForm(){
      alert({
        title:'Find Branch',
        message:"<div id='template-alert-custom'><div class='list' id='formBranchSearch'><div class='item icon ion-search'><input type='text'  id='brsearch' placeholder='Branch Search...' onkeyup='autochangedbr()'></div><div style='height:200px;overflow:scroll;'><div class='item hidden grey-200' id='listBranch' data='branchArraySearch' onclick='getBranch(&quot;${name}&quot;)'><label><h3>${name}</h3></label></div></div></div></div>",
        id: 'my-branch-search-dialog',
        width:'90%',
        buttons:[
          {
            label: 'Cancel',
            onclick: function(){
              $sval = $('#brsearch').val();
                if($sval == ""){
                    alert("Please Select a Customer");
                } else {
                    //alert($sval);
                    //github_usernameold
                    $('#branch_option').val($sval);
                }

                closeAlert('my-branch-search-dialog');
            }
          }
        ]
      });
    }*/

  function selectDate() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        minDate: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#task_date').val(seld);
            
        }
    });
  }

  function selectDefDate1() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        minDate: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('.selectDefDate1').val(seld);
            
        }
    });
  }

  function selectDefDate2() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        minDate: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('.selectDefDate2').val(seld);
            
        }
    });
  }

  function selectWorkOrderStartDate() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        minDate: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#start_date').val(seld);
            
        }
    });
  }

  function selectWorkOrderEndDate() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        minDate: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#end_date').val(seld);
            
        }
    });
  }

  function selectBookingDate() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        minDate: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#booking_date').val(seld);
            
        }
    });
  }

  function selectPosTransDate() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#postrans_date').val(seld);

            var dobj = MobileUI.objectByForm('formPosTransactionsSearch');
            var postrans_date = dobj.postrans_date;
            //alert(postrans_date);
             loading('Loading...');
            $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
            {
                tp: 'loadPOSTransaction',
                postrans_date: postrans_date,
                id: window.localStorage.getItem("id"),
                cp: window.localStorage.getItem("cp")
            },
            function(data, status){
              closeLoading();
              console.log('################ POS TRANSACTIONS ################');
              console.log(JSON.parse(data));
              var dcount = (JSON.parse(data)).length;
              //alert(dcount);
              if(dcount == 0){
                alert('No Transactions.');
                posTransactionsArray.length = 0;
                posTransactionsArraySearch.length = 0;
              } else {
                posTransactionsArray = JSON.parse(data);
                posTransactionsArraySearch = JSON.parse(data);
                posTransactionsToPdf(postrans_date);
              }
                  
            });
            
        }
    });
  }

  function selectSalesExpensesReportDate() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#spayment_date').val(seld);
            
        }
    });
  }

  function selectCusPayReportDate() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#rpayment_date').val(seld);
            
        }
    });
  }

  function addOrderDate() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#due_date').val(seld);
            
        }
    });
  }

  function selectExpenseEntryDate() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#payment_date').val(seld);
            
        }
    });
  }

  function selectBookingReportDate() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#booking_date').val(seld);
            
        }
    });
  }

  function onviewdatedef() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#user_date_option').val(seld);
            
        }
    });
  }

  function selectReportFilterDatePicker() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#from_date').val(seld);
            
        }
    });
  }

  function selectBookingDate() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        minDate: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#booking_date').val(seld);
            
        }
    });
  }

  function selectManageOrdersDateFrom() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#start_date').val(seld);
            
        }
    });
  }

  function selectManageOrdersDateTo() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#end_date').val(seld);
            
        }
    });
  }


  function selectTargetDate() {
    var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        minDate: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#target_date').val(seld);
            
        }
    });
  }

  function getUserOnly(name,uid){
    //alert(cusname);
    //var cusobj = {in: 0, name: cusname};
    //MobileUI.formByObject('formCustomerSearch', cusobj);
    //alert(name);
    backPage();
    $('#user_option').val(name);

    $('#user_option_id').val(uid);
  }

  function getUser(name){
    //alert(cusname);
    //var cusobj = {in: 0, name: cusname};
    //MobileUI.formByObject('formCustomerSearch', cusobj);
    //alert(name);
    backPage();
    $('#user_option').val(name);

    //GET PRICE OF ITEM SELECTED
    //var tp = "getItemPrice";
      //loading('Please wait...');
      //closeLoading();
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getUserId",
        it: name,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      var usrdata = JSON.parse(data);
      //alert(data);

      $('#user_option_id').val(usrdata[0].userid);
      
      usersArraySearch = usrdata;

      setUsersLocationMap();
      //$('#alluserschecker').val(usrdata.userid);
      $('#alluserschecker').prop("checked", false);
          
    });
  }

  function sichangedBatch(){

    var scobj = MobileUI.objectByForm('frmBooking');


    userbatch = scobj.userbatch;


  }

  function sichangedMode() {


    var scobj = MobileUI.objectByForm('frmBooking');

   // scobj.scannerTotal = 0;

    //var selIdItem = $('#mode_prices').val();

    var selIdItem = scobj.mode_prices;
    var code = scobj.item_option_id;
    var rate = scobj.rate;

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getItemPriceWithId",
        it: code,
        pid: selIdItem,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      console.log(data);
      var sepArr = data.split("|");
      var ratep = parseInt(rate) / (100 + parseInt(rate));
      var txamt = ratep * parseInt(sepArr[0]);
      $('#price').val(sepArr[0]);
      $('#quantityAval').val(sepArr[1]);

      $('#tax').val(txamt.toFixed(2));
          
    });

  }

  //Get Manufacture Item
  function getSelectedManufactureItem(itname, code){

      $('#item_option').val(itname);
      $('#item_option_id').val(code);

      backPage();
      
  }

  //Get Item Function getItems()
  function getSelectedItem(itname, code, rate){

    //itname = decodeURIComponent(itname);

      $('#price').val("0");
      $('#quantityAval').val("0");
      $('#tax').val("0");
    //alert(cusname);
    //var cusobj = {in: 0, name: cusname};
    //MobileUI.formByObject('formCustomerSearch', cusobj);
    backPage();
    //alert(code);
    $('#item_option').val(itname);
    $('#item_option_id').val(code);
    $('#rate').val(rate);

    console.log("UID == "+window.localStorage.getItem("id"));


    //var scobj = MobileUI.objectByForm('frmBooking');

   // scobj.scannerTotal = 0;

    var selIdItem = $('#mode_prices').val();

    //var selIdItem = scobj.mode_prices;

    //alert("SelvAL: " + selIdItem);

    //GET PRICE OF ITEM SELECTED
    //var tp = "getItemPrice";
      //loading('Please wait...');
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getItemPriceWithId",
        it: code,
        pid: selIdItem,
        cp: window.localStorage.getItem("cp"),
        id: window.localStorage.getItem("id")
    },
    function(data, status){
      console.log(data);
      var sepArr = data.split("|");
      var ratep = parseInt(rate) / (100 + parseInt(rate));
      var txamt = ratep * parseInt(sepArr[0]);
      $('#price').val(sepArr[0]);
      $('#quantityAval').val(sepArr[1]);

      $('#tax').val(txamt.toFixed(2));
          
    });
    //MobileUI.hide('listItems');
  }

  //Get Item Function getItems()
  function getItem(itname){
    //alert(cusname);
    //var cusobj = {in: 0, name: cusname};
    //MobileUI.formByObject('formCustomerSearch', cusobj);
    backPage();
    $('#item_option').val(itname);

    var selIdItem = $('#mode_prices').val();

    //alert("SelvAL: " + selIdItem);

    //GET PRICE OF ITEM SELECTED
    //var tp = "getItemPrice";
      //loading('Please wait...');
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getItemPrice",
        it: itname,
        pid: selIdItem,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      var sepArr = data.split("|");
      $('#price').val(sepArr[0]);
      $('#quantityAval').val(sepArr[1]);
          
    });

    //GET ID OF ITEM SELECTED
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getItemId",
        it: itname,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      
      $('#item_option_id').val(data);
          
    });
    //MobileUI.hide('listItems');
  }

  function alertGPSWithButtons(){
      alert({
        title:'GPS STATUS',
        message:"You cannot use this module without allowing GPS on device.",
        id: 'my-items-search-dialog',
        width:'90%',
        buttons:[
          {
            label: 'Denie',
            onclick: function(){
              
                closeAlert('my-items-search-dialog');
                backPage();
            }
          },{
            label: 'Allow',
            onclick: function(){
              
                closeAlert('my-items-search-dialog');
                cordova.plugins.locationAccuracy.request(function () {
                    //handleSuccess("Location accuracy request successful");
                }, function (error) {
                    //onErrorGPS("Error requesting location accuracy: " + JSON.stringify(error));
                    if (error) {
                        // Android only
                        alertGPSWithButtons();
                        
                    }
                }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY 
                );
            }
          }
        ]
      });
    }

function autocashreceived() {
  var obj = MobileUI.objectByForm('formCashPayment');
  var paid  = obj.cashpayment;
  var prevPaid = parseInt($("#cppaid").text());
  var prevBalance = parseInt($("#cpbalance").text());

  

  if(paid == ""){
    $("#cptotal").text(posTotal);
    $("#cppaid").text("0");
    $("#cpbalance").text("0");
  } else {

    var atempTot = 0;
    var aalltempTot = 0;

    $.each(paymentTypesUsed, function(i,v){
        atempTot = atempTot + parseInt(v.TransAmount);
    });

    paid  = parseInt(obj.cashpayment) + parseInt(atempTot);;
    var bal = paid - parseInt(posTotal);
     $("#cptotal").text(posTotal);
     $("#cppaid").text(paid);
     $("#cpbalance").text(bal);

     console.log("prevPaid = "+ prevPaid);
     console.log("paying = "+ paid);
     console.log("Total = "+ posTotal);
    }
}

function genReport(){
  var fobj = MobileUI.objectByForm('formReportsFilter');
  var rtype = fobj.rtype;
  var from_date = fobj.from_date;
  var rsalesperson = fobj.rsalesperson;
  var rsalespersonTxt = $("#rsalesperson option:selected").text();
  //alert(rtype +' # ' + from_date +' # ' + to_date);
  //specReportData
  if(rtype == ""){
    alert('Please select report type.');
  } else if(from_date == ""){
    alert('Please select date.');
  } else {
    loading('Please wait...');
    reportFilterArray.length = 0;
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: 'reports',
        rtype: rtype,
        from_date: from_date,
        rsalesperson: rsalesperson,
        uname: window.localStorage.getItem("username"),
        id: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      closeLoading();
      //aler(JSON.stringify(data));
      console.log(data);
      //console.log(status); herey
      reportFilterArray = JSON.parse(data);
      reportFilterToPdf(rtype,rsalesperson,rsalespersonTxt,from_date);

      //console.log(rsdata);
      /*
      specReportData = rsdata;

      var qtyTotal = 0;
      var amountTotal = 0;
      //console.log(from_date);

      var timeInSeconds = Date.parse(from_date) / 1000;
      //console.log(timeInSeconds);
      var sprepHdate = phpdate('F j, Y',timeInSeconds);


      $.each(specReportData, function(index, value){
    
          $.each(value, function(key, cell){

            if(key == 'ramount') {
              amountTotal = amountTotal + parseInt(cell);
            }

            if(key == 'rqty') {
              qtyTotal = qtyTotal + parseInt(cell);
            }
           
                
          });
      });

      openPage('specReport',{timeInSeconds: sprepHdate, qtyTotal: qtyTotal, amountTotal: formatMoney(amountTotal)}, setSpecReportVals);
      */
          
    });
  }
    
}

function setSpecReportVals(sparams){

      $('#sprepHdate').text(sparams.timeInSeconds);
      $('#ttItems').text(sparams.qtyTotal);
      $('#ttTotal').text('KES '+sparams.amountTotal);
}

function formatMoney(number, decPlaces, decSep, thouSep) {
  decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
  decSep = typeof decSep === "undefined" ? "." : decSep;
  thouSep = typeof thouSep === "undefined" ? "," : thouSep;
  var sign = number < 0 ? "-" : "";
  var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
  var j = (j = i.length) > 3 ? j % 3 : 0;

  return sign +
    (j ? i.substr(0, j) + thouSep : "") +
    i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
    (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}

function autochangedMp(){
  var obj = MobileUI.objectByForm('formMpesaSearch');
  //MobileUI.disable('formUserGitHub');
  //alert("KEYUP: " + obj.github_username);
  //userGitHub = JSON.parse(res.text);

  var mpsearch  = obj.mpesasearch;
  //console.log('Search Val = '+mpsearch);
  //console.log(obj);
  mpesaArraySearch.length = 0;
  
  if(mpsearch == ""){
    //MobileUI.hide('listProfileGitHub');
    //customersArraySearch = customersArray;
    $.each(mpesaArray, function(index, value){
        mpesaArraySearch.push(value);
        
    });
  } else {
    $.each(mpesaArray, function(index, value){
      //console.log('MORIGINAL array '+index, value);
        $.each(value, function(key, cell){
          //console.log('Marrs ' + cell + ' sval: ' + key + ' index: ' + index);
          var lcell = cell.toLowerCase();
          var lsearch = mpsearch.toLowerCase();
            if(lcell.indexOf(lsearch) !== -1){
              //var sobj = {in: index, name: cell};
              //console.log("YES");
              //console.log(mpesaArray[index]);
              mpesaArraySearch.push(mpesaArray[index]);
              //console.log('found in array '+index, cell);
            }
              
        });
    });

    /*if(customersArraySearch.length > 0){
      $("").val();
    }*/
    //console.log("######## FOUND OBJECT ########");
    //console.log(customersArraySearch);
    MobileUI.show('mpesaTransactionList');
  }

    

}

function autochangedsalesperson(){
  var obj = MobileUI.objectByForm('formSalesPersonSearch');

  var spsearch  = obj.salespersonsearch;
  console.log('Search Val = '+spsearch);

  
  if(spsearch == ""){
    //MobileUI.hide('listItems');
    //itemsArraySearch = itemsArray;
    $.each(salespersonArray, function(index, value){
      salespersonArraySearch.push(value);
    });
    
  } else {
    $.each(salespersonArray, function(index, value){
      //console.log('ORIGINAL array '+index, value);
        $.each(value, function(key, cell){
          //console.log('arrs ' + cell + ' sval: ' + key + ' index: ' + index);
          var lcell = cell.toLowerCase();
          var lsearch = spsearch.toLowerCase();
            if(lcell.indexOf(lsearch) !== -1){
              //var sobj = {in: index, description: cell};
              salespersonArraySearch.push(salespersonArray[index]);
              //console.log('found in array '+index, cell);
            }
              
        });
    });   

  }
}

//
function getBookingDiscountVal() {
  alert({
        title:'Discount',
        //message:'Add Customer <b>'+ cus +'</b>',
        message: '<div class="row"><div class="col"></i><input type="number" style="margin-left:60px;margin-top:10px;" id="discountval" placeholder="Enter Discount Value" autofocus><br><input type="password" style="margin-left:60px;margin-top:10px;" id="pin" placeholder="Enter Pin"></div><div class="col"><button class="" id="loadingbtn"></button></div></div>',
        width:'90%',
        id: 'add-discount',
        buttons:[
          {
            label: 'Authorize',
            onclick: function(){
              //You code when user click in OK button.
                var discountval = $("#discountval").val();
                var pin = $("#pin").val();

                var obj = MobileUI.objectByForm('frmBooking');

                var premprice  = obj.price;

                /*var itemtt = 0;
                $.each(posItems, function(i,v){
                  itemtt = itemtt + parseInt(v.total);
                });*/


                //alert(tel);
                if(discountval == "") {
                    alert("Discount can not be empty");
                } else if(pin == "") {
                    alert("Pin can not be empty");
                } else if(parseInt(discountval) >= parseInt(premprice)) {
                    alert("Discount can not be greater or equal to items cost value");
                } else {
                    //alert($thr);
                    //var postvals = "cname"+cus+"&telephone="+tel+"&csname="+cus+"&id="+window.localStorage.getItem("id");
                    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
                    {
                        tp: 'varify-pin',
                        pin: pin,
                        cp: window.localStorage.getItem("cp"),
                        id: window.localStorage.getItem("id")
                    },
                    function(data, status){
                        //console.log(data);
                        closeAlert('add-discount');
                        if(data == "1"){
                          //console.log("YES");
                          $('#discount').val(discountval);
                        } else {
                          alert("Invalid Pin");
                        }
                        
                    });
        
                    

                }
                /*var postvals = "cname"+cus+"&telephone="+telephone+"&csname="+cus+"&id="+window.localStorage.getItem("id");
        
                loading('Please wait...');
                */
            }
          },
          {
            label:'Cancel',
            onclick: function(){
              //You code when user click in OK button.
              closeAlert();
            }
          }
        ]
      });
}

function getDiscountVal() {
  alert({
        title:'Discount',
        //message:'Add Customer <b>'+ cus +'</b>',
        message: '<div class="row"><div class="col"></i><input type="number" style="margin-left:60px;margin-top:10px;" id="discountval" placeholder="Enter Discount Value" autofocus><br><input type="password" style="margin-left:60px;margin-top:10px;" id="pin" placeholder="Enter Pin"></div><div class="col"><button class="" id="loadingbtn"></button></div></div>',
        width:'90%',
        id: 'add-discount',
        buttons:[
          {
            label: 'Authorize',
            onclick: function(){
              //You code when user click in OK button.
                var discountval = $("#discountval").val();
                var pin = $("#pin").val();

                var obj = MobileUI.objectByForm('frmPOS');

                var premprice  = obj.price;

                /*var itemtt = 0;
                $.each(posItems, function(i,v){
                  itemtt = itemtt + parseInt(v.total);
                });*/


                //alert(tel);
                if(discountval == "") {
                    alert("Discount can not be empty");
                } else if(pin == "") {
                    alert("Pin can not be empty");
                } else if(parseInt(discountval) >= parseInt(premprice)) {
                    alert("Discount can not be greater or equal to items cost value");
                } else {
                    //alert($thr);
                    //var postvals = "cname"+cus+"&telephone="+tel+"&csname="+cus+"&id="+window.localStorage.getItem("id");
                    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
                    {
                        tp: 'varify-pin',
                        pin: pin,
                        cp: window.localStorage.getItem("cp"),
                        id: window.localStorage.getItem("id")
                    },
                    function(data, status){
                        //console.log(data);
                        closeAlert('add-discount');
                        if(data == "1"){
                          //console.log("YES");
                          $('#discount').val(discountval);
                        } else {
                          alert("Invalid Pin");
                        }
                        
                    });
        
                    

                }
                /*var postvals = "cname"+cus+"&telephone="+telephone+"&csname="+cus+"&id="+window.localStorage.getItem("id");
        
                loading('Please wait...');
                */
            }
          },
          {
            label:'Cancel',
            onclick: function(){
              //You code when user click in OK button.
              closeAlert();
            }
          }
        ]
      });
}

function addCusForm(cus){
      alert({
        title:'No Results Found',
        //message:'Add Customer <b>'+ cus +'</b>',
        message: 'Add Customer <b>'+ cus +'</b><div class="row"><div class="col"></i><input type="number" style="margin-left:60px;margin-top:10px;" id="invsearch" placeholder="Enter Phone Number" autofocus></div><div class="col"><button class="" id="loadingbtn"></button></div></div>',
        width:'90%',
        id: 'add-booking-customer',
        buttons:[
          {
            label: 'Add',
            onclick: function(){
              //You code when user click in OK button.
                var tel = $("#invsearch").val();
                //alert(tel);
                if(tel == "") {
                    alert("Please enter phone number");
                } else {
                    //alert($thr);
                    //var postvals = "cname"+cus+"&telephone="+tel+"&csname="+cus+"&id="+window.localStorage.getItem("id");
        
                    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
                    {
                        tp: 'customer',
                        cname: cus + ' ' + tel,
                        telephone: tel,
                        csname: cus,
                        address: '',
                        cp: window.localStorage.getItem("cp"),
                        id: window.localStorage.getItem("id")
                    },
                    function(data, status){
                        
                          closeAlert('add-booking-customer');
                        
                          customersArraySearch2.length = 0;
                          loadingElement('sCustomer', 'Please wait...');

                          $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
                          {
                              tp: 'loadCustomersBranches',
                              name: cus,
                              cp: window.localStorage.getItem("cp")
                          },
                          function(data, status){
                            //closeLoading();
                            closeLoading('sCustomer');
                            //console.log(data);
                            var jobj = JSON.parse(data);
                            
                            if(jobj.length == 0){

                                addCusForm(search);

                            } else {

                                customersArraySearch2 = jobj;

                              
                            }
                            
                                
                          });
                          ///
                            

                        
                    });

                }
                /*var postvals = "cname"+cus+"&telephone="+telephone+"&csname="+cus+"&id="+window.localStorage.getItem("id");
        
                loading('Please wait...');
                */
            }
          },
          {
            label:'Cancel',
            onclick: function(){
              //You code when user click in OK button.
              closeAlert();
            }
          }
        ]
      });
    }

function autochangedd2New(){
  var obj = MobileUI.objectByForm('formCustomerSearch2');

  var search  = obj.cussearch;
  console.log('Search Val = '+search);
  
  if(search == ""){
    
    alert("Please enter search value");
 
  } else if(search.length < 2){
    
    alert("Error, search value cannot be less than 4 characters.");
 
  } else {
    customersArraySearch.length = 0;
    loadingElement('sCustomer', 'Please wait...');

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: 'loadCustomersBranches',
        name: search,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      //closeLoading();
      closeLoading('sCustomer');
      //console.log(data);
      var jobj = JSON.parse(data);
      
      if(jobj.length == 0){

          addCusForm(search);

      } else {

          customersArraySearch2 = jobj;

        
      }
      
          
    });

  
    //console.log("######## FOUND OBJECT ########");
    //console.log(customersArraySearch);
    MobileUI.show('listProfileGitHub2');
  }

    

}

function productItemSpecificSearch(itmcode, frm){
  var bbcode = itmcode.toString();
  if(bbcode != "") {
    //itemsArraySearchNew.length = 0;
    loading('Please wait...');

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: 'loadProductSpecificItems',
        name: bbcode,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      closeLoading();
      //closeLoading('sProduct');
      //$("#scannerRes2").text(data);
      //alert(data);
      var jobj = JSON.parse(data);
      
      if(jobj.length == 0){

          alert('Item not found!!');

      } else {

          if(frm =="frmPOS") {
            getBarcodeSelectedItemPrice(jobj[0].description, jobj[0].stock_id, "frmPOS");
          } else if(frm == "frmBooking") {
            getBookingBarcodeSelectedItemPrice(jobj[0].description, jobj[0].stock_id, "frmBooking");
          } else if(frm == "frmAddOrder") {
            getBarcodeSelectedOrderItemPrice(jobj[0].description, jobj[0].stock_id, "frmAddOrder");
          }
    
      }
      
          
    });

    //MobileUI.show('listItems');
  }
  

    

}



function productFarmerSearch(){
  var obj = MobileUI.objectByForm('formFarmerSearch');

  var search  = obj.frsearch;
  console.log('Search Val = '+search);
  
  if(search == ""){
    
    alert("Please enter search value");
 
  } else {
    farmerArraySearch.length = 0;
    loadingElement('sFarmer', 'Please wait...');
    //closeLoading('sFarmer');

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: 'loadProductFarmers',
        name: search,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      //closeLoading();
      closeLoading('sFarmer');
      //console.log(data);
      var jobj = JSON.parse(data);
      
      if(jobj.length == 0){

          alert('No match found!!');

      } else {

          farmerArraySearch = jobj;

        
      }
      
          
    });

    MobileUI.show('listFarmers');
  }

    

}

function manufactureItemSearch(){
  var obj = MobileUI.objectByForm('formmanuItemsSearch');

  var search  = obj.manitsearch;
  console.log('Search Val = '+search);
  
  if(search == ""){
    
    alert("Please enter search value");
 
  } else if(search.length < 2){
    
    alert("Error, search value cannot be less than 2 characters.");
 
  } else {
    manufactureItemsArraySearch.length = 0;
    loadingElement('sManufacture', 'Please wait...');

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: 'loadProductItems',
        name: search,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      //closeLoading();
      closeLoading('sManufacture');
      //console.log(data);
      var jobj = JSON.parse(data);
      
      if(jobj.length == 0){

          alert('Item not found!!');

      } else {

          manufactureItemsArraySearch = jobj;

        
      }
      
          
    });

    MobileUI.show('listItems');
  }

    

}

function productItemSearch(){
  var obj = MobileUI.objectByForm('formItemsSearch');

  var search  = obj.itsearch;
  console.log('Search Val = '+search);
  
  if(search == ""){
    
    alert("Please enter search value");
 
  } else if(search.length < 2){
    
    alert("Error, search value cannot be less than 2 characters.");
 
  } else {
    itemsArraySearchNew.length = 0;
    loadingElement('sProduct', 'Please wait...');

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: 'loadProductItems',
        name: search,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      //closeLoading();
      closeLoading('sProduct');
      //console.log(data);
      var jobj = JSON.parse(data);
      
      if(jobj.length == 0){

          alert('Item not found!!');

      } else {

          itemsArraySearchNew = jobj;

        
      }
      
          
    });

    MobileUI.show('listItems');
  }

    

}

function productItemSearchAll(){
  var obj = MobileUI.objectByForm('formItemsSearch');

  var search  = obj.itsearch;
  console.log('Search Val = '+search);
  
  if(search == ""){
    
    alert("Please enter search value");
 
  } else if(search.length < 2){
    
    alert("Error, search value cannot be less than 2 characters.");
 
  } else {
    itemsArraySearchNew.length = 0;
    loadingElement('sProduct', 'Please wait...');

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: 'loadProductItemsAll',
        name: search,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      //closeLoading();
      closeLoading('sProduct');
      //console.log(data);
      var jobj = JSON.parse(data);
      
      if(jobj.length == 0){

          alert('Item not found!!');

      } else {

          itemsArraySearchNew = jobj;

        
      }
      
          
    });

    MobileUI.show('listItems');
  }

    

}

function autochangedd2(){
  var obj = MobileUI.objectByForm('formCustomerSearch2');
  //MobileUI.disable('formUserGitHub');
  //alert("KEYUP: " + obj.github_username);
  //userGitHub = JSON.parse(res.text);

  var search  = obj.cussearch;
  console.log('Search Val = '+search);
  //console.log(obj);
  //console.log(customersArraySearch2.length);
  //$('#sCustomer').prop('disabled', true);

  //alert(search);
  
  
  if(search == ""){
    
    alert("Please enter search value");
 
  } else if(search.length < 4){
    
    alert("Error, search value cannot be less than 4 characters.");
 
  } else {
    customersArraySearch2.length = 0;
    loadingElement('sCustomer', 'Please wait...');

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: 'loadCustomersBranches',
        name: search,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      //closeLoading();
      closeLoading('sCustomer');
      //console.log(data);
      var jobj = JSON.parse(data);
      
      if(jobj.length == 0){

          alert('No data found.');

      } else {

          customersArraySearch2 = jobj;

        
      }
      
          
    });

  
    //console.log("######## FOUND OBJECT ########");
    //console.log(customersArraySearch);
    MobileUI.show('listProfileGitHub2');
  }

    

}


function loadSalesPerson(idparam) {
  //salespersonArraySearch
  loadingElement(idparam, 'Please wait...');

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: 'loadSalesPerson',
        id: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      //closeLoading();
      closeLoading(idparam);
      //console.log(data);
      var jobj = JSON.parse(data);
      
      if(jobj.length == 0){

          alert('No salesperson found.');

      } else {

          salespersonArray = jobj;
          salespersonArraySearch = jobj;

      }
      
          
    });
}

function autochangeddNew(){
  var obj = MobileUI.objectByForm('formCustomerSearch');

  var search  = obj.cussearch;
  console.log('Search Val = '+search);
  
  if(search == ""){
    
    alert("Please enter search value");
 
  } else if(search.length < 2){
    
    alert("Error, search value cannot be less than 4 characters.");
 
  } else {
    customersArraySearch.length = 0;
    loadingElement('sCustomer', 'Please wait...');

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: 'loadCustomersBranches',
        name: search,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      //closeLoading();
      closeLoading('sCustomer');
      console.log(data);
      var jobj = JSON.parse(data);
      
      if(jobj.length == 0){

          addCusForm(search);

      } else {

          customersArraySearch = jobj;

        
      }


      
          
    });

  
    //console.log("######## FOUND OBJECT ########");
    //console.log(customersArraySearch);
    //MobileUI.show('listProfileGitHub2');
  }

    

}

function autochangedd(){
  var obj = MobileUI.objectByForm('formCustomerSearch');
  //MobileUI.disable('formUserGitHub');
  //alert("KEYUP: " + obj.github_username);
  //userGitHub = JSON.parse(res.text);

  var search  = obj.cussearch;
  console.log('Search Val = '+search);
  
  customersArraySearch.length = 0;
  
  if(search == ""){
    console.log("NULL");
    //MobileUI.hide('listProfileGitHub');
    //customersArraySearch = customersArray;
    $.each(customersArray, function(index, value){
        customersArraySearch.push(value);
        
    });
  } else {
    console.log("searching...");
    console.log(customersArray);
    $.each(customersArray, function(index, value){
      console.log('ORIGINAL array '+index, value);
        $.each(value, function(key, cell){
          console.log('arrs ' + cell + ' sval: ' + key + ' index: ' + index);
          var lcell = cell.toLowerCase();
          var lsearch = search.toLowerCase();
            if(lcell.indexOf(lsearch) !== -1){
              cell.replace(/'/g, "\\'");
              cell.replace(/&#039;/g, '&#92;&#039;');
              var sobj = {in: index, name: cell};
              customersArraySearch.push(sobj);
              console.log(customersArraySearch);
            }
              
        });
    });

    /*if(customersArraySearch.length > 0){
      $("").val();
    }*/
    //console.log("######## FOUND OBJECT ########");
    //console.log(customersArraySearch);
    MobileUI.show('listProfileGitHub');
  }

    

}

//

function autochangedbroute(){
  var obj = MobileUI.objectByForm('formRouteSearch');
  //MobileUI.disable('formUserGitHub');
  //alert("KEYUP: " + obj.github_username);
  //userGitHub = JSON.parse(res.text);

  var search  = obj.rsearch;
  //console.log('Search Val = '+search);
  //console.log(obj);
  routeArraySearch.length = 0;
  
  if(search == ""){
    //MobileUI.hide('listBranch');
    //customersArraySearch = customersArray;
    //routeArraySearch = routeArray;
    $.each(routeArray, function(index, value){
      routeArraySearch.push(value);
    });
    
  } else {
    $.each(routeArray, function(index, value){
      //console.log('ORIGINAL array '+index, value);
        $.each(value, function(key, cell){
          //console.log('arrs ' + cell + ' sval: ' + key + ' index: ' + index);
          var lcell = cell.toLowerCase();
          var lsearch = search.toLowerCase();
            if(lcell.indexOf(lsearch) !== -1){
              var sobj = {description: cell};
              routeArraySearch.push(sobj);
              //console.log('found in array '+index, cell);
            }
              
        });
    });
    //console.log("######## FOUND OBJECT ########");
    //console.log(branchArraySearch);
    MobileUI.show('listRoutes');
  }
}

function autochangedbrcus(){
  var obj = MobileUI.objectByForm('formCustomerBranchSearch');
  //MobileUI.disable('formUserGitHub');
  //alert("KEYUP: " + obj.github_username);
  //userGitHub = JSON.parse(res.text);

  var search  = obj.cusbrsearch;
  //console.log('Search Val = '+search);
  //console.log(obj);
  branchcusArraySearch.length = 0;
  
  if(search == ""){
    //MobileUI.hide('listBranch');
    //customersArraySearch = customersArray;
    //branchcusArraySearch = branchcusArray;
    $.each(branchcusArray, function(index, value){
      branchcusArraySearch.push(value);
    });
    
  } else {
    $.each(branchcusArray, function(index, value){
      //console.log('ORIGINAL array '+index, value);
        $.each(value, function(key, cell){
          //console.log('arrs ' + cell + ' sval: ' + key + ' index: ' + index);
          var lcell = cell.toLowerCase();
          var lsearch = search.toLowerCase();
            if(lcell.indexOf(lsearch) !== -1){
              var sobj = {in: index, br_name: cell};
              branchcusArraySearch.push(sobj);
              //console.log('found in array '+index, cell);
            }
              
        });
    });
    //console.log("######## FOUND OBJECT ########");
    //console.log(branchArraySearch);
    MobileUI.show('listCusBranch');
  }

    

}

//autochangedbr BRANCH
function autochangedbr(){
  var obj = MobileUI.objectByForm('formBranchSearch');
  //MobileUI.disable('formUserGitHub');
  //alert("KEYUP: " + obj.github_username);
  //userGitHub = JSON.parse(res.text);

  var search  = obj.brsearch;
  //console.log('Search Val = '+search);
  //console.log(obj);
  branchArraySearch.length = 0;
  
  if(search == ""){
    //MobileUI.hide('listBranch');
    //customersArraySearch = customersArray;
     $.each(branchArray, function(index, value){
        branchArraySearch.push(value);
        
    });
  } else {
    $.each(branchArray, function(index, value){
      //console.log('ORIGINAL array '+index, value);
        $.each(value, function(key, cell){
          //console.log('arrs ' + cell + ' sval: ' + key + ' index: ' + index);
          var lcell = cell.toLowerCase();
          var lsearch = search.toLowerCase();
            if(lcell.indexOf(lsearch) !== -1){
              var sobj = {in: index, br_name: cell};
              branchArraySearch.push(sobj);
              //console.log('found in array '+index, cell);
            }
              
        });
    });
    //console.log("######## FOUND OBJECT ########");
    //console.log(branchArraySearch);
    MobileUI.show('listBranch');
  }

    

}
//

//autochangedit ITEMS
function autochangeduser(){
  var obj = MobileUI.objectByForm('formUsersSearch');
  //MobileUI.disable('formUserGitHub');


  var search  = obj.usersearch;
  console.log('Search Val = '+search);
  //console.log(obj);
  usersArraySearch.length = 0;
  
  if(search == ""){
    //console.log('EMPTY Search Val = '+search);
    //usersArraySearch = usersArray;
    $.each(usersArray, function(index, value){
      usersArraySearch.push(value);
    });
   
  } else {
    //console.log(usersArray);
    $.each(usersArray, function(index, value){
      //console.log('ORIGINAL array '+index, value);
        $.each(value, function(key, cell){
          //console.log('arrs ' + cell + ' sval: ' + key + ' index: ' + index);

          var lcell = cell.toLowerCase();
          var lsearch = search.toLowerCase();
          //console.log('CELL: ' + lcell + ' SEARCH: ' + lsearch);
            if(lcell.indexOf(lsearch) !== -1){
              //var sobj = {in: index, description: cell};
              usersArraySearch.push(usersArray[index]);
              //console.log('found in array '+index, cell);
            }
              
        });
    });
    
    //MobileUI.show('listUsers');
  }

    

}

//autochangedit ITEMS
function autochangedit(){
  var obj = MobileUI.objectByForm('formItemsSearch');
 

  var tsearch  = obj.itsearch;
  //console.log('Search Val = '+tsearch);
  //console.log(obj);
  itemsArraySearch.length = 0;
  
  if(tsearch == ""){
    //MobileUI.hide('listItems');
    //itemsArraySearch = itemsArray;
    $.each(itemsArray, function(index, value){
      itemsArraySearch.push(value);
    });
    
  } else {
    $.each(itemsArray, function(index, value){
      //console.log('ORIGINAL array '+index, value);
        $.each(value, function(key, cell){
          //console.log('arrs ' + cell + ' sval: ' + key + ' index: ' + index);
          var lcell = cell.toLowerCase();
          var lsearch = tsearch.toLowerCase();
            if(lcell.indexOf(lsearch) !== -1){
              //var sobj = {in: index, description: cell};
              itemsArraySearch.push(itemsArray[index]);
              //console.log('found in array '+index, cell);
            }
              
        });
    });
    
    //MobileUI.show('listItems');
  }

    

}

function resetPassword() {
  var robj = MobileUI.objectByForm('frmResetPassword');
  var rusername = robj.rusername;
  var rpassword = robj.rpassword;
  var npassword = robj.npassword;
  var cnpassword = robj.cnpassword;

  if(rpassword == ""){
    alert('Your old password is required');
  } else if(npassword == ""){
    alert('Please enter new password');
  } else if(npassword != cnpassword){
    alert('Please confirm with the same new passowrd.');
  } else {

    $.ajax({
      type: "POST",
      url: window.localStorage.getItem("setSiteUrl") + "process.php",
      data: {
        tp: 'resetPassword',
        id: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp"),
        ruser: rusername,
        oldp: rpassword,
        newp: npassword
      },
      beforeSend: function(){
        loading('Reseting...');
        backPage();
       
      },
      success: function(data){
        //console.log("######### SUCCESS ###########");
        if(data == "1") {
          alert('Password Reset Successful.');
          logout();
        } else {
          alert('Password Reset Failed. Please try again.');
        }
        

      },
      error: function(err){
        
      },
      complete: function(){
        closeLoading();
        
      }
    });
  }

}

function loadSpTrip(){
  $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: 'loadSpTrip',
        uname: window.localStorage.getItem("username"),
        id: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      //closeLoading();

      console.log(data);
      var jobj = JSON.parse(data);
      
      if(jobj.length == 0){

          

          routeItems.length = 0;
          $('#starttrip').text('Start');
          $('#starttrip').removeClass('black');
          $('#starttrip').addClass('red');
          $('#updateRouteBtn').hide();

          $('#stt').text('Start Trip');
          $('#stt').removeClass('black');
          $('#stt').addClass('red');

      } else {

          console.log("################ SPECIFIC TRIP ####################");
          var decObj = JSON.parse(jobj[0].description);
          console.log(decObj);
          routeItems = decObj;

          $('#starttrip').text('End');
          $('#starttrip').removeClass('red');
          $('#starttrip').addClass('black');
          $('#updateRouteBtn').show();

          $('#stt').text('Trips');
          $('#stt').removeClass('red');
          $('#stt').addClass('black');

        
      }
      
          
    });
}

//OPEN PAGE
document.addEventListener('openPage', function(e){
//It does something when the page is opened
//You can access name of page in: e.detail.page
//loadSalesPerson 
//
  if(e.detail.page == 'searchCustomer.html'){
  
    MobileUI.show('listProfileGitHub');
  } else if(e.detail.page == 'orderStatus.html'){
    setCurrentDefaultDate();
    loadSalesPerson('genorderrepbtn');
    
  } else if(e.detail.page == 'taskReport.html'){
    setCurrentDefaultDate();
    loadSalesPerson('gentaskrepbtn');
    
  } else if(e.detail.page == 'tripsModule.html'){
    setCurrentDefaultDate();
    loadSalesPerson('gettripsbtn');
    
  } else if(e.detail.page == 'assign-task.html'){
    setCurrentDefaultDate();
    loadUsers();
    loadTasks();
    
  } else if(e.detail.page == 'sales-target.html'){
    //setCurrentDefaultDate();
    loadSalesUsers();
    loadTargetCategories();
    loadTargets("0");
    
  } else if(e.detail.page == 'target-dashboard.html'){
    
    //loadSummarySales();
    salesSummaryArr.length = 0;

    loadSummaryTargetCategories();

    //loadSummaryTargets("0");
    
  } else if(e.detail.page == 'target-dashboard-admin.html'){
    
    //loadSummarySales();
    salesSummaryArr.length = 0;

    loadSummaryTargetCategories();

    //loadSummaryTargets("0");
    
  } else if(e.detail.page == 'customer-visit.html'){
    //setCurrentDefaultDate();
    loadUsers();
    loadTargets("3");
    
  } else if(e.detail.page == 'sales-quantity.html'){
    //setCurrentDefaultDate();
    loadUsers();
    loadTargets("1");
    
  } else if(e.detail.page == 'searchBranch.html'){
    
    MobileUI.show('listBranch');
  } else if(e.detail.page == 'searchItem.html'){
    
    MobileUI.show('listItems');
  } else if(e.detail.page == 'mpesaPaymentSearch.html'){
    MobileUI.show('mpesaTransactionList');
    loadMpesaTransaction();
  } else if(e.detail.page == 'mpesaPaymentSearch2.html'){
    //MobileUI.show('mpesaTransactionList');
    loadMpesaTransaction();
  } else if(e.detail.page == 'addOrder.html'){
    setCurrentDueDate();
    loadPriceSelector();
  } else if(e.detail.page == 'salesInvoice.html'){
    
    loadPriceSelector();
    loadBatch();
  } else if(e.detail.page == 'posOpt.html'){
      loadItems();

      var roles = JSON.parse(window.localStorage.getItem("roles"));

      if(roles.indexOf("pPOS") !== -1){
          console.log("pPOS YES");
          $('#pos').css("visibility", "visible");
      } else {
          $('#pos').css({"pointer-events": "none"});
          $('#pos').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("pBookings") !== -1){
          console.log("pBookings YES");
          $('#booking').css("visibility", "visible");
      } else {
        $('#booking').css({"pointer-events": "none"});
        $('#booking').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
        //$('#booking').css({"pointer-events": "none"});
      }

      if(roles.indexOf("pMy Bookings") !== -1){
          console.log("pMy Bookings YES");
          $('#mybooking').css("visibility", "visible");
      } else {

        $('#mybooking').css({"pointer-events": "none"});
        $('#mybooking').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }
  } else if(e.detail.page == 'sales.html'){
    
      var roles = JSON.parse(window.localStorage.getItem("roles"));

      if(roles.indexOf("sOrder Entry") !== -1){
          console.log("sOrder Entry YES");
          $('#order-entry').css("visibility", "visible");
      } else {
          $('#order-entry').css({"pointer-events": "none"});
          $('#order-entry').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("sCustomer Payment") !== -1){
          console.log("sCustomer Payment YES");
          $('#customer-payment').css("visibility", "visible");
      } else {
        $('#customer-payment').css({"pointer-events": "none"});
        $('#customer-payment').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("sSales Expenses") !== -1){
          console.log("sSales Expenses YES");
          $('#sales-expenses').css("visibility", "visible");
      } else {

        $('#sales-expenses').css({"pointer-events": "none"});
        $('#sales-expenses').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }

      if(roles.indexOf("sTarget") !== -1){
          console.log("sTarget YES");
          $('#target').css("visibility", "visible");
      } else {

        $('#target').css({"pointer-events": "none"});
        $('#target').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }

      if(roles.indexOf("sCustomer Add") !== -1){
          console.log("sCustomer Add YES");
          $('#customer-add').css("visibility", "visible");
      } else {

        $('#customer-add').css({"pointer-events": "none"});
        $('#customer-add').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }

      if(roles.indexOf("sSales Invoicing") !== -1){
          console.log("sSales Invoicing YES");
          $('#sales-invoicing').css("visibility", "visible");
      } else {

        $('#sales-invoicing').css({"pointer-events": "none"});
        $('#sales-invoicing').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }

      if(roles.indexOf("sManage Orders") !== -1){
          console.log("sManage Orders YES");
          $('#manage-orders').css("visibility", "visible");
      } else {

        $('#manage-orders').css({"pointer-events": "none"});
        $('#manage-orders').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }

      if(roles.indexOf("sDirect Sales") !== -1){
          console.log("sDirect Sales YES");
          $('#direct-sales').css("visibility", "visible");
      } else {

        $('#direct-sales').css({"pointer-events": "none"});
        $('#direct-sales').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }

  } else if(e.detail.page == 'location.html'){
  
      var roles = JSON.parse(window.localStorage.getItem("roles"));

      if(roles.indexOf("lOn View") !== -1){
          console.log("lOn View YES");
          $('#on-view').css("visibility", "visible");
      } else {
          $('#on-view').css({"pointer-events": "none"});
          $('#on-view').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("lOn Map") !== -1){
          console.log("lOn Map YES");
          $('#on-map').css("visibility", "visible");
      } else {
        $('#on-map').css({"pointer-events": "none"});
        $('#on-map').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("lHistory on Map") !== -1){
          console.log("lHistory on Map YES");
          $('#history-map').css("visibility", "visible");
      } else {

        $('#history-map').css({"pointer-events": "none"});
        $('#history-map').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }

      if(roles.indexOf("lHistory Report") !== -1){
          console.log("lHistory Report YES");
          $('#history-report').css("visibility", "visible");
      } else {

        $('#history-report').css({"pointer-events": "none"});
        $('#history-report').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }

      if(roles.indexOf("lSet Customer Location") !== -1){
          console.log("lSet Customer Location YES");
          $('#loccustomer').css("visibility", "visible");
      } else {

        $('#loccustomer').css({"pointer-events": "none"});
        $('#loccustomer').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }

      if(roles.indexOf("lView Customer Locations") !== -1){
          console.log("lView Customer Locations YES");
          $('#view-loccustomers').css("visibility", "visible");
      } else {

        $('#view-loccustomers').css({"pointer-events": "none"});
        $('#view-loccustomers').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }

      if(roles.indexOf("lCustomer Notes") !== -1){
          console.log("lCustomer Notes YES");
          $('#customer-notes').css("visibility", "visible");
      } else {

        $('#customer-notes').css({"pointer-events": "none"});
        $('#customer-notes').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }

      if(roles.indexOf("lGeneral Notes") !== -1){
          console.log("lGeneral Notes YES");
          $('#general-notes').css("visibility", "visible");
      } else {

        $('#general-notes').css({"pointer-events": "none"});
        $('#general-notes').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }

  } else if(e.detail.page == 'inventory.html'){
    
      var roles = JSON.parse(window.localStorage.getItem("roles"));

      if(roles.indexOf("iDirect Stock Transfer") !== -1){
          console.log("iDirect Stock Transfer YES");
          $('#stock-transfer').css("visibility", "visible");
      } else {
          $('#stock-transfer').css({"pointer-events": "none"});
          $('#stock-transfer').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("iStock Requisition") !== -1){
          console.log("iStock Requisition YES");
          $('#stock-requisition').css("visibility", "visible");
      } else {
        $('#stock-requisition').css({"pointer-events": "none"});
        $('#stock-requisition').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("iRequisition Approval") !== -1){
          console.log("iRequisition Approval YES");
          $('#stock-approval').css("visibility", "visible");
      } else {

        $('#stock-approval').css({"pointer-events": "none"});
        $('#stock-approval').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }

      if(roles.indexOf("iRequisition Issue") !== -1){
          console.log("iRequisition Issue YES");
          $('#stock-issue').css("visibility", "visible");
      } else {

        $('#stock-issue').css({"pointer-events": "none"});
        $('#stock-issue').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }

      if(roles.indexOf("iStock Take") !== -1){
          console.log("iStock Take YES");
          $('#stock-take').css("visibility", "visible");
      } else {

        $('#stock-take').css({"pointer-events": "none"});
        $('#stock-take').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');

      }
  } else if(e.detail.page == 'manufactering.html'){
    //manufacture advance-manufacture
      var roles = JSON.parse(window.localStorage.getItem("roles"));

      if(roles.indexOf("mPackaging") !== -1){
          console.log("mPackaging YES");
          $('#manufacture').css("visibility", "visible");
      } else {
          $('#manufacture').css({"pointer-events": "none"});
          $('#manufacture').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("mWork Order") !== -1){
          console.log("mWork Order YES");
          $('#advance-manufacture').css("visibility", "visible");
      } else {
        $('#advance-manufacture').css({"pointer-events": "none"});
        $('#advance-manufacture').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

  } else if(e.detail.page == 'reports.html'){
//  
      var roles = JSON.parse(window.localStorage.getItem("roles"));

      if(roles.indexOf("rStock Take Report") !== -1){
          console.log("rStock Take Report YES");
          $('#stockTakeReport').css("visibility", "visible");
      } else {
          $('#stockTakeReport').css({"pointer-events": "none"});
          $('#stockTakeReport').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("rProduction Report") !== -1){
          console.log("rProduction Report YES");
          $('#workorderProductionReport').css("visibility", "visible");
      } else {
          $('#workorderProductionReport').css({"pointer-events": "none"});
          $('#workorderProductionReport').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("rWork Order Report") !== -1){
          console.log("rWork Order Report YES");
          $('#workorderReport').css("visibility", "visible");
      } else {
          $('#workorderReport').css({"pointer-events": "none"});
          $('#workorderReport').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("rUser Store Balance") !== -1){
          console.log("rUser Store Balance YES");
          $('#userStoreBalance').css("visibility", "visible");
      } else {
          $('#userStoreBalance').css({"pointer-events": "none"});
          $('#userStoreBalance').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("rBookings Report") !== -1){
          console.log("rBookings Report YES");
          $('#bookingsReport').css("visibility", "visible");
      } else {
          $('#bookingsReport').css({"pointer-events": "none"});
          $('#bookingsReport').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("rCustomer Payment") !== -1){
          console.log("rCustomer Payment YES");
          $('#customerPaymentReport').css("visibility", "visible");
      } else {
          $('#customerPaymentReport').css({"pointer-events": "none"});
          $('#customerPaymentReport').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("rSales Expenses") !== -1){
          console.log("rSales Expenses YES");
          $('#salesExpensesReport').css("visibility", "visible");
      } else {
          $('#salesExpensesReport').css({"pointer-events": "none"});
          $('#salesExpensesReport').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("rTarget Report") !== -1){
          console.log("rTarget Report YES");
          $('#targetReport').css("visibility", "visible");
      } else {
          $('#targetReport').css({"pointer-events": "none"});
          $('#targetReport').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("rTask Report") !== -1){
          console.log("rTask Report YES");
          $('#taskReport').css("visibility", "visible");
      } else {
          $('#taskReport').css({"pointer-events": "none"});
          $('#taskReport').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("rReport Filter") !== -1){
          console.log("rReport Filter YES");
          $('#reportFilter').css("visibility", "visible");
      } else {
          $('#reportFilter').css({"pointer-events": "none"});
          $('#reportFilter').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("rPOS Transactions") !== -1){
          console.log("rPOS Transactions YES");
          $('#posTransactions').css("visibility", "visible");
      } else {
          $('#posTransactions').css({"pointer-events": "none"});
          $('#posTransactions').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("rTrips") !== -1){
          console.log("rTrips YES");
          $('#tripsModule').css("visibility", "visible");
      } else {
          $('#tripsModule').css({"pointer-events": "none"});
          $('#tripsModule').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

      if(roles.indexOf("rOrder Status") !== -1){
          console.log("rOrder Status YES");
          $('#orderStatus').css("visibility", "visible");
      } else {
          $('#orderStatus').css({"pointer-events": "none"});
          $('#orderStatus').append('<div style="background-color: rgba(299, 299, 299, 0.85);border-radius: 5px;position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;display: flex;align-items: center;justify-content: center;"><i class="icon ion-locked red-900"></i></div>');
      }

  } else if(e.detail.page == 'customerPayment.html'){
    setCurrentDefaultDate();
    loadCustomerPayments();
  } else if(e.detail.page == 'salesExpenses.html'){
    setCurrentDefaultDate();
    loadSalesExpenses();
  } else if(e.detail.page == 'onView.html'){
    loadUsers();
  } else if(e.detail.page == 'onMap.html'){
    //loadUsers();
    initMaps('onmapid');

  } else if(e.detail.page == 'onMapUser.html'){
    //loadUsers();
    setCurrentDefaultDate();
    initMaps('onmapuserid');

  } else if(e.detail.page == 'historyMap.html'){
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
      if(dd<10){
          dd='0'+dd
      } 
      if(mm<10){
          mm='0'+mm
      } 

      today = yyyy+'-'+mm+'-'+dd;
      $("#user_date").val(today);
    
    //usersArraySearch = usersArray;
    //alert("YES");
    initMaps('histmapid');

  } else if(e.detail.page == 'historyReport.html'){
      loadUsers();
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
      if(dd<10){
          dd='0'+dd
      } 
      if(mm<10){
          mm='0'+mm
      } 

      today = yyyy+'-'+mm+'-'+dd;
      $("#user_date").val(today);

  } else if(e.detail.page == 'customerLocations.html'){
    branchcusArraySearch = branchcusArray;
    
    initMaps('custlocmapid');

  } else if(e.detail.page == 'customerLocation.html'){
  
    loadUnSetCustomers();
    

  } else if(e.detail.page == 'customerNotes.html'){
    $('#btnCustNotes').prop('disabled', false);
    loadCustomersNotes();
    
  } else if(e.detail.page == 'generalNotes.html'){
    //$('#btnCustNotes').prop('disabled', false);
    loadGeneralNotes();
    
  } else if(e.detail.page == 'searchUser.html'){
    usersArraySearch = usersArray;
  } else if(e.detail.page == 'posTransactions.html'){
    //setCurrentDefaultDate();
    //setCurrentDefaultDate|setdefdate
    var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
      if(dd<10){
          dd='0'+dd
      } 
      if(mm<10){
          mm='0'+mm
      } 

      today = yyyy+'-11-28';
      $(".setdefdate").val(today);
    loadPOSTransaction(today);
  } else if(e.detail.page == 'selectRoute.html'){
      
    loadRoutes();
    loadSpTrip();
    
    //routeArraySearch = 

  } else if(e.detail.page == 'cashPayment.html'){
    $('#poscashbtn').prop('disabled', false);
  } else if(e.detail.page == 'mpesaPayment.html'){
    $('#posmpesabtn').prop('disabled', false);
  } else if(e.detail.page == 'resetPassword.html'){
    $('#rusername').val(window.localStorage.getItem("username"));
  } else if(e.detail.page == 'reportFilter.html'){
    setCurrentDefaultDate();
    $("#rsalesperson").append(gblSysUsersb);
  } else if(e.detail.page == 'booking.html'){

    $("#fsalesp").append(gblSysUsers);
    setCurrentDefaultDate();
    //mehere
  } else if(e.detail.page == 'directSales.html'){

    $("#fsalesp").append(gblSysUsers);
    setCurrentDefaultDate();
    //mehere
  } else if(e.detail.page == 'manageOrders.html'){

    setCurrentDefaultDate();
    //mehere
  } else if(e.detail.page == 'mybooking.html'){
    
    var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
      if(dd<10){
          dd='0'+dd
      } 
      if(mm<10){
          mm='0'+mm
      } 

      today = yyyy+'-'+mm+'-'+dd;
      $("#booking_date").val(today);
    loadMyBookings();
  } else if(e.detail.page == 'bookingsReport.html'){
    
    var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
      if(dd<10){
          dd='0'+dd
      } 
      if(mm<10){
          mm='0'+mm
      } 

      today = yyyy+'-'+mm+'-'+dd;
      $(".setdefdate").val(today);
  } else if(e.detail.page == 'customerPaymentReport.html'){
    
    var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
      if(dd<10){
          dd='0'+dd
      } 
      if(mm<10){
          mm='0'+mm
      } 

      today = yyyy+'-'+mm+'-'+dd;
      $(".setdefdate").val(today);
  } else if(e.detail.page == 'salesExpensesReport.html'){
    
    var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
      if(dd<10){
          dd='0'+dd
      } 
      if(mm<10){
          mm='0'+mm
      } 

      today = yyyy+'-'+mm+'-'+dd;
      $(".setdefdate").val(today);
  } else if(e.detail.page == 'posTransactions.html'){
    
    var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
      if(dd<10){
          dd='0'+dd
      } 
      if(mm<10){
          mm='0'+mm
      } 

      today = yyyy+'-'+mm+'-'+dd;
      $(".setdefdate").val(today);
  } else if(e.detail.page == 'pos.html'){

    $("#fsalesp").append(gblSysUsers);
    calculateTotal('frmPOSBarcodeScannerMain');

  } else if(e.detail.page == 'barcodeScanner.html'){

    calculateTotal('frmBarcodeScanner');

  }

  
});

function loadRoutes(){
  loading('Loading Routes...');
  $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
  {
      tp: 'loadRoutes',
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
  },
  function(data, status){
    closeLoading();
    //aler(JSON.stringify(data));
    console.log(data);
    routeArray = JSON.parse(data);
     console.log(routeArray);
    routeArraySearch = JSON.parse(data);
        
  });

}

function selectReportFilterDate() {
  var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
     if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 

    today = yyyy+'-'+mm+'-'+dd;
    //console.log(today);
    $("#from_date").val(today);
}

function setCurrentDefaultDate(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
     if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 

    today = yyyy+'-'+mm+'-'+dd;
    //console.log(today);
    $(".setdefdate").val(today);
}

function setDefaultReportDates() {

  var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
     if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 

    today = yyyy+'-'+mm+'-'+dd;
    
    $("#start_date").val(today);
    $("#end_date").val(today);

}

function setCurrentDueDate(){
  var today = new Date();
    var dd = today.getDate() + 1;
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
     if(dd<10){
            dd='0'+dd
        } 
        if(mm<10){
            mm='0'+mm
        } 

    today = yyyy+'-'+mm+'-'+dd;
    //console.log(today);
    $("#due_date").attr('min',today);
    $("#due_date").val(today);
}

function getCurrentDateFormat(){
  var dtoday = new Date();
  dtoday.toLocaleFormat('%h %a %d %b %Y'); // 30-Dec-2011

  //dtoday = yyyy+'-'+mm+'-'+dd;
  return dtoday;
}

function loadMpesaTransaction(){
  var tp = "loadMpesaTransaction";
  loading('Please wait...');
  mpesaArray.length = 0;
  mpesaArraySearch.length = 0;
  $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
  {
      tp: tp,
      cp: window.localStorage.getItem("cp")
  },
  function(data, status){
    closeLoading();
    //aler(JSON.stringify(data));
    //console.log("############### MPESA TRANSACTION ###############");
    //console.log(data);
    //console.log(status);
    var mpesadata = JSON.parse(data);
    $.each(mpesadata, function(index, value) {
      var refName = value.name;
      //mpesadata[index].name = refName.replace(/'/g,"\\'");
      mpesadata[index].name = refName.replace(/'/g,"");
    });
    mpesaArray = mpesadata;
    mpesaArraySearch = mpesadata;
    
        
  });

  //closeLoading();
}

//funcPosTransactions
function funcPosTransactions() {
  loadPOSTransaction();
}

function getSalesPerson(rid,ruser) {
   backPage();
  $('#saleperson_option').val(ruser);
  $('#saleperson_option_id').val(rid);
}

function loadMyBookings(){
  var clObj = MobileUI.objectByForm('frmMyBooking');
  var customer_option = clObj.customer_option;
  var customer_option_id = clObj.customer_option_id;
  var booking_date = clObj.booking_date;
  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'fetchmybookings',
      customer_option: customer_option,
      customer_option_id: customer_option_id,
      booking_date: booking_date,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      bookingItemClient.length = 0;
     
    },
    success: function(data){

      //console.log(data);
      //console.log(JSON.parse(data));
      bookingItemClient = JSON.parse(data);
      


    },
    error: function(err){
      
    },
    complete: function(){
      closeLoading();
      
    }
  });
}

function fetchTrips() {
  
  var fetchTripsObj = MobileUI.objectByForm('frmFetchTrips');
  var saleperson_option = fetchTripsObj.saleperson_option;
  var saleperson_option_id = fetchTripsObj.saleperson_option_id;
  var trip_date = fetchTripsObj.trip_date;

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'fetchtripsroutes',
      saleperson_option: saleperson_option,
      saleperson_option_id: saleperson_option_id,
      trip_date: trip_date,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      getTripsArray.length = 0;
     
    },
    success: function(data){

      //console.log(data);
      //console.log(JSON.parse(data));
      var fta = JSON.parse(data);
      //getTripsArray = ;
      $.each(fta, function(index, value){
        var indroutes = JSON.parse(value);
        
        getTripsArray = indroutes;
        //console.log(indroutes);
      });


    },
    error: function(err){
      
    },
    complete: function(){
      closeLoading();
      
    }
  });

}

function posTransactionsToPdf(tdate){

  var tblContents = '';

  
  var timehder = '<center><h3>Date: '+ tdate +'</h3></center><br>';

  if(posTransactionsArraySearch.length > 0) {
    $.each(posTransactionsArraySearch, function(index, value){
      //console.log(value);
      var items = JSON.parse(value.pitems);
      var itemsStrContent = '';
      var itemsStr = '';

      var pay = JSON.parse(value.payments);
      var payStrContent = '';
      var payStr = '';
      
      if(items.length > 0){
        $.each(items, function(i,v){
          itemsStrContent += '<tr>'+
            '<td>'+ v.item_option +'</td>'+
            '<td>'+ v.quantity +'</td>'+
            '<td>'+ v.price +'</td>'+
          '</tr>';
        });
        itemsStr += '<table class="inner" style="border:none;">'+
        itemsStrContent + 
        '</table>';
      }

      if(pay.length > 0){
        $.each(pay, function(i,v){
          payStrContent += '<tr>'+
            '<td>'+ v.Transtype +'</td>'+
            '<td>'+ v.TransID +'</td>'+
            '<td>'+ v.TransAmount +'</td>'+
          '</tr>';
        });
        payStr += '<table class="inner" style="border:none;">'+
        payStrContent + 
        '</table>';
      }

      tblContents += '<tr>'+
        '<td>POS#'+ str_pad(value.id, 9, "0", "STR_PAD_LEFT") +'</td>'+
        '<td>'+ value.customername +'</td>'+
        '<td>'+ value.ptotal +'</td>'+
        '<td>'+ itemsStr +'</td>'+
        '<td>'+ payStr +'</td>'+
        '<td>'+ value.uname +'</td>'+
      '</tr>';
    });

    var tableHeader = '<style>'+
    'table {'+
      'font-family: arial, sans-serif;'+
      'border-collapse: collapse;'+
      'width: 100%;'+
    '}'+
    'td, th {'+
      'border: 1px solid #dddddd;'+
      'text-align: left;'+
      'padding: 8px;'+
    '}'+
    'tr:nth-child(even) {'+
      'background-color: #dddddd;'+
    '}'+
    '.inner tr:nth-child(even) {'+
      'background-color: #f4f4f4;'+
    '}'+
    '</style>'+
    '<center><h2>POS TRANSACTIONS</h2></center>'+
    timehder +
    '<table>'+
    '<tr>'+
      '<th>No</th>'+
      '<th>Name</th>'+
      '<th>Total</th>'+
      '<th>Items</th>'+
      '<th>Payments</th>'+
      '<th>Served By</th>'+
    '</tr>'+
    tblContents +
    '</table>';

    var options = {
        font: {
            size: 22,
            italic: true,
            align: 'center'
        },
        header: {
            height: '6cm',
            label: {
                text: "",
                font: {
                    bold: true,
                    size: 37,
                    align: 'center'
                }
            }
        },
        footer: {
            height: '4cm',
            label: {
                text: '',
                font: { align: 'center' }
            }
        }
    };
   
    cordova.plugins.printer.print(tableHeader, options);

  } else {
    alert('No Records to export to PDF');
  }
    
}


function salesExpensesReportToPdf(sdate){

  var tblContents = '';
  var timehder = '';

  
  timehder += '<center><h3>Date: '+ sdate +'</h3></center><br>';
  

  if(salesExpensesReportArray.length > 0) {
    $.each(salesExpensesReportArray, function(index, value){
      
      tblContents += '<tr>'+
        '<td>'+ value.payment_date +'</td>'+
        '<td>'+ value.description +'</td>'+
        '<td>'+ value.mode_payment +'</td>'+
        '<td>'+ value.amount +'</td>'+
      '</tr>';
    });

    var tableHeader = '<style>'+
    'table {'+
      'font-family: arial, sans-serif;'+
      'border-collapse: collapse;'+
      'width: 100%;'+
    '}'+
    'td, th {'+
      'border: 1px solid #dddddd;'+
      'text-align: left;'+
      'padding: 8px;'+
    '}'+
    'tr:nth-child(even) {'+
      'background-color: #dddddd;'+
    '}'+
    '</style>'+
    '<center><h2>Sales Expenses Report</h2></center>'+
    timehder +
    '<table>'+
    '<tr>'+
      '<th>Payment Date</th>'+
      '<th>Description</th>'+
      '<th>Mode of Payment</th>'+
      '<th>Amount</th>'+
    '</tr>'+
    tblContents +
    '</table>';

    var options = {
        font: {
            size: 22,
            italic: true,
            align: 'center'
        },
        header: {
            height: '6cm',
            label: {
                text: "",
                font: {
                    bold: true,
                    size: 37,
                    align: 'center'
                }
            }
        },
        footer: {
            height: '4cm',
            label: {
                text: '',
                font: { align: 'center' }
            }
        }
    };
   
    cordova.plugins.printer.print(tableHeader, options);

  } else {
    alert('No Records to export to PDF');
  }
    
}

function customerPaymentReportToPdf(cus,md,sdate){

  var tblContents = '';
  var timehder = '';

  
  timehder += '<center><h3>Date: '+ sdate +'</h3></center><br>';
  

  if(customerPaymentReportArray.length > 0) {
    $.each(customerPaymentReportArray, function(index, value){
      //Payment Mode
      if(parseInt(value.mode_payment) == 0){
          var pmode = "NA";
      } else if(parseInt(value.mode_payment) == 1){
          var pmode = "Mpesa";
      } else if(parseInt(value.mode_payment) == 2){
          var pmode = "Cheque";
      } else if(parseInt(value.mode_payment) == 3){
          var pmode = "Cash";
      } else if(parseInt(value.mode_payment) == 4){
          var pmode = "Direct Deposit";
      }
      

      tblContents += '<tr>'+
        '<td>'+ value.payment_date +'</td>'+
        '<td>'+ value.customer_option +'</td>'+
        '<td>'+ value.reference +'</td>'+
        '<td>'+ value.amount +'</td>'+
        '<td>'+ pmode +'</td>'+
      '</tr>';
    });

    var tableHeader = '<style>'+
    'table {'+
      'font-family: arial, sans-serif;'+
      'border-collapse: collapse;'+
      'width: 100%;'+
    '}'+
    'td, th {'+
      'border: 1px solid #dddddd;'+
      'text-align: left;'+
      'padding: 8px;'+
    '}'+
    'tr:nth-child(even) {'+
      'background-color: #dddddd;'+
    '}'+
    '</style>'+
    '<center><h2>Customer Payment Report</h2></center>'+
    '<center>'+ cus +'</center>'+
    timehder +
    '<table>'+
    '<tr>'+
      '<th>Payment Date</th>'+
      '<th>Customer</th>'+
      '<th>Ref</th>'+
      '<th>Amount</th>'+
      '<th>Mode</th>'+
    '</tr>'+
    tblContents +
    '</table>';

    var options = {
        font: {
            size: 22,
            italic: true,
            align: 'center'
        },
        header: {
            height: '6cm',
            label: {
                text: "",
                font: {
                    bold: true,
                    size: 37,
                    align: 'center'
                }
            }
        },
        footer: {
            height: '4cm',
            label: {
                text: '',
                font: { align: 'center' }
            }
        }
    };
   
    cordova.plugins.printer.print(tableHeader, options);

  } else {
    alert('No Records to export to PDF');
  }
    
}

function reportFilterToPdf(typ,usr,usrname,sdate){

  var tblContents = '';
  var timehder = '';
  var postype = '';
  var posuser = '';

  
  timehder += '<center><h3>Date: '+ sdate +'</h3></center><br>';
  
  if(typ == "4"){

    postype = "POS DETAILED";
  
    if(usr == "0"){
      posuser = "ALL";
    } else {
      posuser = usrname.toUpperCase();
    }

    if(reportFilterArray.length > 0) {
      var inum = 1;
      $.each(reportFilterArray, function(index, value){
        //Payments
        var pArr = JSON.parse(value.payments);
        var pStr = "";
        var pCum = 0;
        $.each(pArr, function(i, v){
          pCum = pCum + parseInt(v.TransAmount);
          pStr += v.Transtype + '&nbsp;&nbsp;&nbsp;' + v.TransAmount + '<br>';
        });

        //Items
        var itmArr = JSON.parse(value.pitems);
        var itmStr = "";
        $.each(itmArr, function(i, v){
          
          tblContents += '<tr>'+
            '<td>'+ inum +'</td>'+
            '<td>'+ v.item_option_id +'</td>'+
            '<td>'+ v.item_option +'</td>'+
            '<td>'+ value.pdate +'</td>'+
            '<td>'+ v.quantity +'</td>'+
            '<td>'+ v.discount +'</td>'+
            '<td>'+ v.tax +'</td>'+
            '<td>'+ parseInt(v.quantity) * parseInt(v.price) +'</td>'+
          '</tr>';
          inum++;
        });
        
      });

      var tableHeader = '<style>'+
      'table {'+
        'font-family: arial, sans-serif;'+
        'border-collapse: collapse;'+
        'width: 100%;'+
      '}'+
      'td, th {'+
        'border: 1px solid #dddddd;'+
        'text-align: left;'+
        'padding: 8px;'+
      '}'+
      'tr:nth-child(even) {'+
        'background-color: #dddddd;'+
      '}'+
      '</style>'+
      '<center><h2>'+postype+' SALES REPORT<h2></center>'+
      '<center>SALESPERSON: '+posuser+'</center>'+
      timehder + 
      '<table>'+
      '<tr>'+
        '<th>#</th>'+
        '<th>STOCK ID</th>'+
        '<th>DESCRIPTION</th>'+
        '<th>DATE</th>'+
        '<th>QUANTITY</th>'+
        '<th>DISCOUNT</th>'+
        '<th>TAX</th>'+
        '<th>TOTAL</th>'+
      '</tr>'+
      tblContents +
      '</table>';

      var options = {
          font: {
              size: 22,
              italic: true,
              align: 'center'
          },
          header: {
              height: '6cm',
              label: {
                  text: "",
                  font: {
                      bold: true,
                      size: 37,
                      align: 'center'
                  }
              }
          },
          footer: {
              height: '4cm',
              label: {
                  text: '',
                  font: { align: 'center' }
              }
          }
      };
     
      cordova.plugins.printer.print(tableHeader, options);
    

    } else {
      alert('No Records to export to PDF');
    }

  } else if(typ == "5") {

    postype = "POS SUMMARY";
  
    if(usr == "0"){
      posuser = "ALL";
    } else {
      posuser = usrname.toUpperCase();
    }

    if(reportFilterArray.length > 0) {
      var inum = 1;
      $.each(reportFilterArray, function(i, v){


        tblContents += '<tr>'+
            '<td>'+ inum +'</td>'+
            '<td>'+ v.salepersonName +'</td>'+
            '<td>'+ v.sumtotal +'</td>'+
          '</tr>';
          inum++;
        
      });

      var tableHeader = '<style>'+
      'table {'+
        'font-family: arial, sans-serif;'+
        'border-collapse: collapse;'+
        'width: 100%;'+
      '}'+
      'td, th {'+
        'border: 1px solid #dddddd;'+
        'text-align: left;'+
        'padding: 8px;'+
      '}'+
      'tr:nth-child(even) {'+
        'background-color: #dddddd;'+
      '}'+
      '</style>'+
      '<center><h2>'+postype+' SALES REPORT<h2></center>'+
      '<center>SALESPERSON: '+posuser+'</center>'+
      timehder + 
      '<table>'+
      '<tr>'+
        '<th>#</th>'+
        '<th>SALESPERSON</th>'+
        '<th>TOTAL SALES (Ksh)</th>'+
      '</tr>'+
      tblContents +
      '</table>';

      var options = {
          font: {
              size: 22,
              italic: true,
              align: 'center'
          },
          header: {
              height: '6cm',
              label: {
                  text: "",
                  font: {
                      bold: true,
                      size: 37,
                      align: 'center'
                  }
              }
          },
          footer: {
              height: '4cm',
              label: {
                  text: '',
                  font: { align: 'center' }
              }
          }
      };
     
      cordova.plugins.printer.print(tableHeader, options);
    

    } else {
      alert('No Records to export to PDF');
    }

  }  else if(typ == "6") {

    postype = "POS DETAILED";
  
    if(usr == "0"){
      posuser = "ALL";
    } else {
      posuser = usrname.toUpperCase();
    }

    if(reportFilterArray.length > 0) {
      var inum = 1;
      $.each(reportFilterArray, function(index, value){
        //Payments
        var pArr = JSON.parse(value.payments);
        var pStr = "";
        var pCum = 0;
        $.each(pArr, function(i, v){
          pCum = pCum + parseInt(v.TransAmount);
          pStr += v.Transtype + '&nbsp;&nbsp;&nbsp;' + v.TransAmount + '<br>';
        });

        //Items
        var itmArr = JSON.parse(value.pitems);
        var itmStr = "";
        $.each(itmArr, function(i, v){
          
          tblContents += '<tr>'+
            '<td>'+ inum +'</td>'+
            '<td>'+ v.item_option_id +'</td>'+
            '<td>'+ v.item_option +'</td>'+
            '<td>'+ value.pdate +'</td>'+
            '<td>'+ v.quantity +'</td>'+
            '<td>'+ v.discount +'</td>'+
            '<td>'+ v.tax +'</td>'+
            '<td>'+ parseInt(v.quantity) * parseInt(v.price) +'</td>'+
          '</tr>';
          inum++;
        });
        
      });

      var tableHeader = '<style>'+
      'table {'+
        'font-family: arial, sans-serif;'+
        'border-collapse: collapse;'+
        'width: 100%;'+
      '}'+
      'td, th {'+
        'border: 1px solid #dddddd;'+
        'text-align: left;'+
        'padding: 8px;'+
      '}'+
      'tr:nth-child(even) {'+
        'background-color: #dddddd;'+
      '}'+
      '</style>'+
      '<center><h2>'+postype+' SALES REPORT<h2></center>'+
      '<center>USER: '+posuser+'</center>'+
      timehder + 
      '<table>'+
      '<tr>'+
        '<th>#</th>'+
        '<th>STOCK ID</th>'+
        '<th>DESCRIPTION</th>'+
        '<th>DATE</th>'+
        '<th>QUANTITY</th>'+
        '<th>DISCOUNT</th>'+
        '<th>TAX</th>'+
        '<th>TOTAL</th>'+
      '</tr>'+
      tblContents +
      '</table>';

      var options = {
          font: {
              size: 22,
              italic: true,
              align: 'center'
          },
          header: {
              height: '6cm',
              label: {
                  text: "",
                  font: {
                      bold: true,
                      size: 37,
                      align: 'center'
                  }
              }
          },
          footer: {
              height: '4cm',
              label: {
                  text: '',
                  font: { align: 'center' }
              }
          }
      };
     
      cordova.plugins.printer.print(tableHeader, options);
    

    } else {
      alert('No Records to export to PDF');
    }

  }  else {

    postype = "POS SUMMARY";
  
    if(usr == "0"){
      posuser = "ALL";
    } else {
      posuser = usrname.toUpperCase();
    }

    if(reportFilterArray.length > 0) {
      var inum = 1;
      $.each(reportFilterArray, function(i, v){


        tblContents += '<tr>'+
            '<td>'+ inum +'</td>'+
            '<td>'+ v.uname +'</td>'+
            '<td>'+ v.sumtotal +'</td>'+
          '</tr>';
          inum++;
        
      });

      var tableHeader = '<style>'+
      'table {'+
        'font-family: arial, sans-serif;'+
        'border-collapse: collapse;'+
        'width: 100%;'+
      '}'+
      'td, th {'+
        'border: 1px solid #dddddd;'+
        'text-align: left;'+
        'padding: 8px;'+
      '}'+
      'tr:nth-child(even) {'+
        'background-color: #dddddd;'+
      '}'+
      '</style>'+
      '<center><h2>'+postype+' SALES REPORT<h2></center>'+
      '<center>USER: '+posuser+'</center>'+
      timehder + 
      '<table>'+
      '<tr>'+
        '<th>#</th>'+
        '<th>USER</th>'+
        '<th>TOTAL SALES (Ksh)</th>'+
      '</tr>'+
      tblContents +
      '</table>';

      var options = {
          font: {
              size: 22,
              italic: true,
              align: 'center'
          },
          header: {
              height: '6cm',
              label: {
                  text: "",
                  font: {
                      bold: true,
                      size: 37,
                      align: 'center'
                  }
              }
          },
          footer: {
              height: '4cm',
              label: {
                  text: '',
                  font: { align: 'center' }
              }
          }
      };
     
      cordova.plugins.printer.print(tableHeader, options);
    

    } else {
      alert('No Records to export to PDF');
    }

  }
    
}

//bookingsReportArray
function bookingReportToPdf(cus,itm,sdate){

  var tblContents = '';
  var timehder = '';

  
  timehder += '<center><h3>Date: '+ sdate +'</h3></center><br>';
  

  if(bookingsReportArray.length > 0) {
    $.each(bookingsReportArray, function(index, value){
      //Payments
      var pArr = JSON.parse(value.payments);
      var pStr = "";
      var pCum = 0;
      $.each(pArr, function(i, v){
        pCum = pCum + parseInt(v.TransAmount);
        pStr += v.Transtype + '&nbsp;&nbsp;&nbsp;' + v.TransAmount + '<br>';
      });

      //Items
      var itmArr = JSON.parse(value.pitems);
      var itmStr = "";
      $.each(itmArr, function(i, v){
        itmStr += v.item_option + '<br>';
      });

      var itmBal = parseInt(value.ptotal) - pCum;

      tblContents += '<tr>'+
        '<td>'+ value.pdate +'</td>'+
        '<td>'+ value.customername +'</td>'+
        '<td>'+ value.ptotal +'</td>'+
        '<td>'+ pStr +'</td>'+
        '<td>'+ itmBal +'</td>'+
        '<td>'+ itmStr +'</td>'+
        '<td>'+ value.uname +'</td>'+
      '</tr>';
    });

    var tableHeader = '<style>'+
    'table {'+
      'font-family: arial, sans-serif;'+
      'border-collapse: collapse;'+
      'width: 100%;'+
    '}'+
    'td, th {'+
      'border: 1px solid #dddddd;'+
      'text-align: left;'+
      'padding: 8px;'+
    '}'+
    'tr:nth-child(even) {'+
      'background-color: #dddddd;'+
    '}'+
    '</style>'+
    '<center><h2>Booking Report</h2></center>'+
    '<center>'+ cus +'</center>'+
    '<center>'+ itm +'</center>'+
    timehder +
    '<table>'+
    '<tr>'+
      '<th>Created On</th>'+
      '<th>Customer</th>'+
      '<th>Total</th>'+
      '<th>Payments</th>'+
      '<th>Balance</th>'+
      '<th>Items</th>'+
      '<th>User</th>'+
    '</tr>'+
    tblContents +
    '</table>';

    var options = {
        font: {
            size: 22,
            italic: true,
            align: 'center'
        },
        header: {
            height: '6cm',
            label: {
                text: "",
                font: {
                    bold: true,
                    size: 37,
                    align: 'center'
                }
            }
        },
        footer: {
            height: '4cm',
            label: {
                text: '',
                font: { align: 'center' }
            }
        }
    };
   
    cordova.plugins.printer.print(tableHeader, options);

  } else {
    alert('No Records to export to PDF');
  }
    
}

function taskReportToPdf(p,s,start,end){

  var tblContents = '';
  var timehder = '';

  if(start == end){
    timehder += '<center><h3>Date: '+ start +'</h3></center><br>';
  } else {
    timehder += '<center><h3>Date: '+ start +' To '+ end +'</h3></center><br>';
  }

  if(taskReportArray.length > 0) {
    $.each(taskReportArray, function(index, value){
      if(value.status == 0){
        var tsts = "Complete";
      } else {
        var tsts = "Pending";
      }
      tblContents += '<tr>'+
        '<td>'+ value.server_date +'</td>'+
        '<td>'+ value.task_date +'</td>'+
        '<td>'+ value.task_description +'</td>'+
        '<td>'+ value.username +'</td>'+
        '<td>'+ tsts +'</td>'+
        '<td>'+ value.task_comments +'</td>'+
      '</tr>';
    });

    var tableHeader = '<style>'+
    'table {'+
      'font-family: arial, sans-serif;'+
      'border-collapse: collapse;'+
      'width: 100%;'+
    '}'+
    'td, th {'+
      'border: 1px solid #dddddd;'+
      'text-align: left;'+
      'padding: 8px;'+
    '}'+
    'tr:nth-child(even) {'+
      'background-color: #dddddd;'+
    '}'+
    '</style>'+
    '<center><h2>'+ s +' Tasks</h2></center>'+
    timehder +
    '<table>'+
    '<tr>'+
      '<th>Created On</th>'+
      '<th>Task Date</th>'+
      '<th>Description</th>'+
      '<th>User</th>'+
      '<th>Status</th>'+
      '<th>Comments</th>'+
    '</tr>'+
    tblContents +
    '</table>';

    var options = {
        font: {
            size: 22,
            italic: true,
            align: 'center'
        },
        header: {
            height: '6cm',
            label: {
                text: "",
                font: {
                    bold: true,
                    size: 37,
                    align: 'center'
                }
            }
        },
        footer: {
            height: '4cm',
            label: {
                text: '',
                font: { align: 'center' }
            }
        }
    };
   
    cordova.plugins.printer.print(tableHeader, options);

  } else {
    alert('No Records to export to PDF');
  }
    
}

function workOrderProductionToPdf(start,end){

  var tblContents = '';
  var timehder = '';

  if(start == end){
    timehder += '<center><h3>Date: '+ start +'</h3></center><br>';
  } else {
    timehder += '<center><h3>Date: '+ start +' To '+ end +'</h3></center><br>';
  }

  if(workOrderReportArray.length > 0) {
    $.each(workOrderReportArray, function(index, value){
      var mtyp = "";
      if(value.type == "0"){
        mtyp = "Assemble";
      } else if(value.type == "1"){
        mtyp = "Unassemble";
      }
      tblContents += '<tr>'+
        '<td>'+ value.count +'</td>'+
        '<td>'+ value.stock_id +'</td>'+
        '<td>'+ value.description +'</td>'+
      '</tr>';
    });

    var tableHeader = '<style>'+
    'table {'+
      'font-family: arial, sans-serif;'+
      'border-collapse: collapse;'+
      'width: 100%;'+
    '}'+
    'td, th {'+
      'border: 1px solid #dddddd;'+
      'text-align: left;'+
      'padding: 8px;'+
    '}'+
    'tr:nth-child(even) {'+
      'background-color: #dddddd;'+
    '}'+
    '</style>'+
    '<center><h2>Work Order Listing</h2></center>'+
    timehder +
    '<table>'+
    '<tr>'+
      '<th>Count</th>'+
      '<th>Stock ID</th>'+
      '<th>Description</th>'+
    '</tr>'+
    tblContents +
    '</table>';

    var options = {
        font: {
            size: 22,
            italic: true,
            align: 'center'
        },
        header: {
            height: '6cm',
            label: {
                text: "",
                font: {
                    bold: true,
                    size: 37,
                    align: 'center'
                }
            }
        },
        footer: {
            height: '4cm',
            label: {
                text: '',
                font: { align: 'center' }
            }
        }
    };
   
    cordova.plugins.printer.print(tableHeader, options);

  } else {
    alert('No Records to export to PDF');
  }
    
}

function workOrderToPdf(start,end){

  var tblContents = '';
  var timehder = '';

  if(start == end){
    timehder += '<center><h3>Date: '+ start +'</h3></center><br>';
  } else {
    timehder += '<center><h3>Date: '+ start +' To '+ end +'</h3></center><br>';
  }

  if(workOrderReportArray.length > 0) {
    $.each(workOrderReportArray, function(index, value){
      var mtyp = "";
      if(value.type == "0"){
        mtyp = "Assemble";
      } else if(value.type == "1"){
        mtyp = "Unassemble";
      }
      tblContents += '<tr>'+
        '<td>'+ mtyp +'</td>'+
        '<td>'+ value.id +'</td>'+
        '<td>'+ value.wo_ref +'</td>'+
        '<td>'+ value.location_name +'</td>'+
        '<td>'+ value.description +'</td>'+
        '<td>'+ value.units_reqd +'</td>'+
        '<td>'+ value.units_issued +'</td>'+
        '<td>'+ value.date_ +'</td>'+
        '<td>'+ value.required_by +'</td>'+
        '<td>'+ value.closed +'</td>'+
      '</tr>';
    });

    var tableHeader = '<style>'+
    'table {'+
      'font-family: arial, sans-serif;'+
      'border-collapse: collapse;'+
      'width: 100%;'+
    '}'+
    'td, th {'+
      'border: 1px solid #dddddd;'+
      'text-align: left;'+
      'padding: 8px;'+
    '}'+
    'tr:nth-child(even) {'+
      'background-color: #dddddd;'+
    '}'+
    '</style>'+
    '<center><h2>Work Order Listing</h2></center>'+
    timehder +
    '<table>'+
    '<tr>'+
      '<th>Type</th>'+
      '<th>#</th>'+
      '<th>Reference</th>'+
      '<th>Location</th>'+
      '<th>Item</th>'+
      '<th>Required</th>'+
      '<th>Manufactured</th>'+
      '<th>Delivered</th>'+
      '<th>Required By</th>'+
      '<th>Closed</th>'+
    '</tr>'+
    tblContents +
    '</table>';

    var options = {
        font: {
            size: 22,
            italic: true,
            align: 'center'
        },
        header: {
            height: '6cm',
            label: {
                text: "",
                font: {
                    bold: true,
                    size: 37,
                    align: 'center'
                }
            }
        },
        footer: {
            height: '4cm',
            label: {
                text: '',
                font: { align: 'center' }
            }
        }
    };
   
    cordova.plugins.printer.print(tableHeader, options);

  } else {
    alert('No Records to export to PDF');
  }
    
}

function orderStatusToPdf(p,s,start,end){

  var tblContents = '';
  var timehder = '';

  if(start == end){
    timehder += '<center><h3>Date: '+ start +'</h3></center><br>';
  } else {
    timehder += '<center><h3>Date: '+ start +' To '+ end +'</h3></center><br>';
  }

  if(orderReportArray.length > 0) {
    $.each(orderReportArray, function(index, value){
      tblContents += '<tr>'+
        '<td>'+ value.order_no +'</td>'+
        '<td>'+ value.reference +'</td>'+
        '<td>'+ value.br_name +'</td>'+
        '<td>'+ value.ord_date +'</td>'+
        '<td>'+ value.delivery_date +'</td>'+
        '<td>'+ value.TotQuantity +'</td>'+
        '<td>'+ value.OrderValue +'</td>'+
        '<td>'+ value.TotDelivered +'</td>'+
      '</tr>';
    });

    var tableHeader = '<style>'+
    'table {'+
      'font-family: arial, sans-serif;'+
      'border-collapse: collapse;'+
      'width: 100%;'+
    '}'+
    'td, th {'+
      'border: 1px solid #dddddd;'+
      'text-align: left;'+
      'padding: 8px;'+
    '}'+
    'tr:nth-child(even) {'+
      'background-color: #dddddd;'+
    '}'+
    '</style>'+
    '<center><h2>'+ s +' Orders</h2></center>'+
    '<center><h2>'+ p +'</h2></center>'+
    timehder +
    '<table>'+
    '<tr>'+
      '<th>No</th>'+
      '<th>Referece</th>'+
      '<th>Name</th>'+
      '<th>Order Date</th>'+
      '<th>Delivery Date</th>'+
      '<th>Qty</th>'+
      '<th>Value</th>'+
      '<th>Delivered</th>'+
    '</tr>'+
    tblContents +
    '</table>';

    var options = {
        font: {
            size: 22,
            italic: true,
            align: 'center'
        },
        header: {
            height: '6cm',
            label: {
                text: "",
                font: {
                    bold: true,
                    size: 37,
                    align: 'center'
                }
            }
        },
        footer: {
            height: '4cm',
            label: {
                text: '',
                font: { align: 'center' }
            }
        }
    };
   
    cordova.plugins.printer.print(tableHeader, options);

  } else {
    alert('No Records to export to PDF');
  }
    
}

function exportToPdf(p,s,start,end){

  var tableHeader = '<style>'+
  'table {'+
    'font-family: arial, sans-serif;'+
    'border-collapse: collapse;'+
    'width: 100%;'+
  '}'+
  'td, th {'+
    'border: 1px solid #dddddd;'+
    'text-align: left;'+
    'padding: 8px;'+
  '}'+
  'tr:nth-child(even) {'+
    'background-color: #dddddd;'+
  '}'+
  '</style>'+
  '<center><h2>Pending Orders</h2></center><br>'+
  '<table style="font-family: arial, sans-serif;border-collapse: collapse;width: 100%;">'+
  '<tr>'+
    '<th>Name</th>'+
    '<th>Price</th>'+
    '<th>Quantity</th>'+
    '<th>Total</th>'+
  '</tr>'+
  '<tr>'+
    '<td>1 KG MFALME MAIZE FLOUR BALE</td>'+
    '<td>1850</td>'+
    '<td>1</td>'+
    '<td>1850</td>'+
  '</tr>'+
  '<tr>'+
    '<td>1 KG MFALME MAIZE FLOUR BALE</td>'+
    '<td>1850</td>'+
    '<td>1</td>'+
    '<td>1850</td>'+
  '</tr>'+
  '<tr>'+
    '<td>1 KG MFALME MAIZE FLOUR BALE</td>'+
    '<td>1850</td>'+
    '<td>1</td>'+
    '<td>1850</td>'+
  '</tr>'+
  '</table>';

  var options = {
      font: {
          size: 22,
          italic: true,
          align: 'center'
      },
      header: {
          height: '6cm',
          label: {
              text: "",
              font: {
                  bold: true,
                  size: 37,
                  align: 'center'
              }
          }
      },
      footer: {
          height: '4cm',
          label: {
              text: '',
              font: { align: 'center' }
          }
      }
  };
 
  cordova.plugins.printer.print(tableHeader, options);
}

function generateSalesExpensesReport(){
  var bkObj = MobileUI.objectByForm('frmSalesExpensesReport');
  var sales_description = bkObj.sales_description
  var spayment_date = bkObj.spayment_date;
  

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'getsalesexpensesdata',
      sales_description: sales_description,
      spayment_date: spayment_date,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      salesExpensesReportArray.length = 0;
      
     
    },
    success: function(data){

      //console.log(data);
      //console.log(JSON.parse(data));
      salesExpensesReportArray = JSON.parse(data);
      salesExpensesReportToPdf(spayment_date);


    },
    error: function(err){
      
    },
    complete: function(){
      closeLoading();
      
    }
  }); 
  
}

function generateCustomerPaymentReport(){
  var bkObj = MobileUI.objectByForm('frmCustomerPaymentReport');
  var customer_option = bkObj.customer_option
  var customer_option_id = bkObj.customer_option_id;
  var mode_payment = bkObj.mode_payment;
  var payment_date = bkObj.rpayment_date;
  

  /*console.log("customer_option: " + customer_option);
  console.log("customer_option_id: " + customer_option_id);
  console.log("item_option: " + item_option);
  console.log("item_option_id: " + item_option_id);
  console.log("booking_id: " + booking_id);
  console.log("booking_type: " + booking_type);
  console.log("mode_prices: " + mode_prices);
  console.log("booking_date: " + booking_date);*/

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'getcustomerpaymentdata',
      customer_option: customer_option,
      customer_option_id: customer_option_id,
      mode_payment: mode_payment,
      payment_date: payment_date,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      customerPaymentReportArray.length = 0;
      
     
    },
    success: function(data){

      //console.log(data);
      //console.log(JSON.parse(data));
      customerPaymentReportArray = JSON.parse(data);
      customerPaymentReportToPdf(customer_option,mode_payment,payment_date);


    },
    error: function(err){
      
    },
    complete: function(){
      closeLoading();
      
    }
  }); 
  
}

function generateBookingsReport(){
  var bkObj = MobileUI.objectByForm('frmBookingReport');
  var customer_option = bkObj.customer_option
  var customer_option_id = bkObj.customer_option_id;
  var item_option = bkObj.item_option;
  var item_option_id = bkObj.item_option_id;
  var booking_id = bkObj.booking_id;
  var booking_type = bkObj.booking_type;
  var mode_prices = bkObj.mode_prices;
  var booking_date = bkObj.booking_date;
  

  /*console.log("customer_option: " + customer_option);
  console.log("customer_option_id: " + customer_option_id);
  console.log("item_option: " + item_option);
  console.log("item_option_id: " + item_option_id);
  console.log("booking_id: " + booking_id);
  console.log("booking_type: " + booking_type);
  console.log("mode_prices: " + mode_prices);
  console.log("booking_date: " + booking_date);*/

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'getbookingdata',
      customer_option: customer_option,
      customer_option_id: customer_option_id,
      item_option: item_option,
      item_option_id: item_option_id,
      booking_date: booking_date,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      bookingsReportArray.length = 0;
      
     
    },
    success: function(data){

      //console.log(data);
      //console.log(JSON.parse(data));
      bookingsReportArray = JSON.parse(data);
      bookingReportToPdf(customer_option,item_option,booking_date);


    },
    error: function(err){
      
    },
    complete: function(){
      closeLoading();
      
    }
  }); 
  
}

function generateTaskReport(){
  var taskObj = MobileUI.objectByForm('frmTaskReport');
  var alltuserschecker = taskObj.alltuserschecker
  var saleperson_option = taskObj.saleperson_option;
  var saleperson_option_id = taskObj.saleperson_option_id;
  var task_status = taskObj.task_status;
  var start_date = taskObj.start_date;
  var end_date = taskObj.end_date;
  var taskStatusString = "";
  if(task_status == "1"){
    taskStatusString = "All";
  } else if(task_status == "2"){
    taskStatusString = "Pending";
  } else if(task_status == "3"){
    taskStatusString = "Complete";
  }

 /* console.log("saleperson_option: " + saleperson_option);
  console.log("saleperson_option_id: " + saleperson_option_id);
  console.log("task_status: " + task_status);
  console.log("start_date: " + start_date);
  console.log("end_date: " + end_date);
  console.log("end_date: " + taskStatusString);*/

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'gettaskdata',
      saleperson_option: saleperson_option,
      saleperson_option_id: saleperson_option_id,
      task_status: task_status,
      start_date: start_date,
      end_date: end_date,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      taskReportArray.length = 0;
      
     
    },
    success: function(data){

      //console.log(data);
      //console.log(JSON.parse(data));
      taskReportArray = JSON.parse(data);
      taskReportToPdf(saleperson_option,taskStatusString,start_date,end_date);


    },
    error: function(err){
      
    },
    complete: function(){
      closeLoading();
      
    }
  }); 
  
}

function detailsAddOrder(param) {

  orderDetailsArray.length = 0;

  orderDetailsArray = JSON.parse(decodeURIComponent(param.details));
  $.each(orderDetailsArray,function(i,v){
      v['total'] = parseInt(v.unit_price) * parseInt(v.quantity);
  });
  console.log("###### ORDER ITEMS #######");
  console.log(orderDetailsArray);

}

function generateOrderPDF() {

  //console.log(orderStatusPdfArr);

  orderStatusToPdf(orderStatusPdfArr[0],orderStatusPdfArr[1],orderStatusPdfArr[2],orderStatusPdfArr[3]);

}

function generateWorkOrderProductionReportPDF() {

  var orderObj = MobileUI.objectByForm('frmWorkOrderProduction');
  var start_date = orderObj.start_date;
  var end_date = orderObj.end_date;

  workOrderProductionToPdf(start_date,end_date);

}

//
function generateWorkOrderProductionReport(){
  var orderObj = MobileUI.objectByForm('frmWorkOrderProduction');
  var start_date = orderObj.start_date;
  var end_date = orderObj.end_date;
 
  console.log("start_date: " + start_date);
  console.log("end_date: " + end_date);

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'getworkorderproduceditems',
      start_date: start_date,
      end_date: end_date,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      workOrderProductionReportArray.length = 0;
     
    },
    success: function(data){

      //console.log(data);
      closeLoading();

      workOrderProductionReportArray = JSON.parse(data);

      console.log(workOrderProductionReportArray);

      
      


    },
    error: function(err){
      
    },
    complete: function(){
      
      
    }
  });
  
}

function generateStockTakeReport(){
  var stockTakeObj = MobileUI.objectByForm('frmStockTake');
  var start_date = stockTakeObj.start_date;
  var end_date = stockTakeObj.end_date;
 
  console.log("start_date: " + start_date);
  console.log("end_date: " + end_date);

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'getworkorderdata',
      start_date: start_date,
      end_date: end_date,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      workOrderReportArray.length = 0;
     
    },
    success: function(data){

      //console.log(data);
      closeLoading();

      stockTakeReportArray = JSON.parse(data);

      console.log(stockTakeReportArray);

      //workOrderToPdf(start_date,end_date);
      


    },
    error: function(err){
      
    },
    complete: function(){
      
      
    }
  });
  
}

function generateWorkOrderReport(){
  var orderObj = MobileUI.objectByForm('frmWorkOrder');
  var start_date = orderObj.start_date;
  var end_date = orderObj.end_date;
 
  console.log("start_date: " + start_date);
  console.log("end_date: " + end_date);

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'getworkorderdata',
      start_date: start_date,
      end_date: end_date,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      workOrderReportArray.length = 0;
     
    },
    success: function(data){

      //console.log(data);
      closeLoading();

      workOrderReportArray = JSON.parse(data);

      console.log(workOrderReportArray);

      workOrderToPdf(start_date,end_date);
      


    },
    error: function(err){
      
    },
    complete: function(){
      
      
    }
  });
  
}

function generateOrder(){
  var orderObj = MobileUI.objectByForm('frmOrderStatus');
  var saleperson_option = orderObj.saleperson_option;
  var saleperson_option_id = orderObj.saleperson_option_id;
  var order_status = orderObj.order_status;
  var start_date = orderObj.start_date;
  var end_date = orderObj.end_date;
  var orderStatusString = "";
  if(order_status == "1"){
    orderStatusString = "All";
  } else if(order_status == "2"){
    orderStatusString = "Pending";
  } else if(order_status == "3"){
    orderStatusString = "Processed";
  }

  console.log("saleperson_option: " + saleperson_option);
  console.log("saleperson_option_id: " + saleperson_option_id);
  console.log("order_status: " + order_status);
  console.log("start_date: " + start_date);
  console.log("end_date: " + end_date);

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'getorderdata',
      saleperson_option: saleperson_option,
      saleperson_option_id: saleperson_option_id,
      order_status: order_status,
      start_date: start_date,
      end_date: end_date,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      orderReportArray.length = 0;
      orderStatusPdfArr.length = 0;
      
      
     
    },
    success: function(data){

      //console.log(orderStatusPdfArr);
      //console.log(JSON.parse(data));
      orderReportArray = JSON.parse(data);

      if(orderReportArray.length == 0) {
        $('#genorderreppdfbtn').hide();
      } else {
        $('#genorderreppdfbtn').show();
      }
      orderStatusPdfArr.push(saleperson_option);
      orderStatusPdfArr.push(orderStatusString);
      orderStatusPdfArr.push(start_date);
      orderStatusPdfArr.push(end_date);
      


    },
    error: function(err){
      
    },
    complete: function(){
      closeLoading();
      
    }
  });
  
}

function getManageOrders(){
  var orderObj = MobileUI.objectByForm('frmManageOrders');
  
  var start_date = orderObj.start_date;
  var end_date = orderObj.end_date;
  
  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'getorderdetails',
      start_date: start_date,
      end_date: end_date,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      orderReportArray.length = 0;
      
     
    },
    success: function(data){

      //console.log(data);
      //console.log(JSON.parse(data));
      orderReportArray = JSON.parse(data);
      //orderStatusToPdf(saleperson_option,orderStatusString,start_date,end_date);


    },
    error: function(err){
      
    },
    complete: function(){
      closeLoading();
      
    }
  });
  
}

function autochangedpostrans(){
  var dobj = MobileUI.objectByForm('formPosTransactionsSearch');
  var postrans_date = dobj.postrans_date;
  //alert(postrans_date);
   loading('Loading...');
  $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
  {
      tp: 'loadPOSTransaction',
      postrans_date: postrans_date,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
  },
  function(data, status){
    closeLoading();
    console.log('################ POS TRANSACTIONS ################');
    console.log(JSON.parse(data));
    var dcount = (JSON.parse(data)).length;
    //alert(dcount);
    if(dcount == 0){
      alert('No Transactions.');
      posTransactionsArray.length = 0;
      posTransactionsArraySearch.length = 0;
    } else {
      posTransactionsArray = JSON.parse(data);
      posTransactionsArraySearch = JSON.parse(data);
      //posTransactionsToPdf(postrans_date);
    }
        
  });
}

function loadPOSTransaction(datee){
  loading('Please wait...POS');
  $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
  {
      tp: 'loadPOSTransaction',
      postrans_date: datee,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
  },
  function(data, status){
    closeLoading();
    //alert(JSON.stringify(data));
    var dcount = (JSON.parse(data)).length;
    //alert(dcount);
    if(dcount == 0){
      //alert('No Transactions.');
      posTransactionsArray.length = 0;
      posTransactionsArraySearch.length = 0;
    } else {
      posTransactionsArray = JSON.parse(data);
      posTransactionsArraySearch = JSON.parse(data);
    }
      
      
  });
 
}

//Load General Notes
function loadGeneralNotes() {
  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'loadgeneralnotes',
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      generalNotesArray.length = 0;
     
    },
    success: function(data){

      
      generalNotesArray = JSON.parse(data);


    },
    error: function(err){
      
    },
    complete: function(){
      closeLoading();
      
    }
  });
}

//Load Customer Notes 
function loadCustomersNotes() {
  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'loadcustomersnotes',
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      customersNotesArray.length = 0;
     
    },
    success: function(data){

      
      customersNotesArray = JSON.parse(data);


    },
    error: function(err){
      
    },
    complete: function(){
      closeLoading();
      
    }
  });
}

function loadUnSetCustomers() {
  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'loadunsetcustomers',
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      notSetCustomersArray.length = 0;
     
    },
    success: function(data){

      
      notSetCustomersArray = JSON.parse(data);


    },
    error: function(err){
      
    },
    complete: function(){
      closeLoading();
      
    }
  });
}

function loadCustomers(){
  var tp = "loadCustomers";
  

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      //loading('Loading customers...');
      customersArray.length = 0;
      customersArraySearch.length = 0;
     
    },
    success: function(data){
      //console.log("######### SUCCESS ###########");

      var temparr = JSON.parse(data);

      /*$.each(temparr, function(i,v){

        var myStr = v.name;
        var newStr = myStr.replace(/&#92;/g, ''); 


        temparr[i]['name'] = newStr;

      });  */
      
      customersArray = temparr;
      customersArraySearch = temparr;
      console.log("CUSTOMERS");
      console.log(customersArraySearch);

      //closeLoading();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ###########");
      //console.log(customersArray);
      loadBranches();
    }
  });

}

function loadBranches(){
  var tp = "loadBranches";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      //loading('Loading branches...');
      
    },
    success: function(data){
      //console.log("######### SUCCESS BRANCHES ###########");
      branchArray.length = 0;
      branchArraySearch.length = 0;
      branchArray = JSON.parse(data);
      branchArraySearch = branchArray;
      //console.log(branchArray);

      //closeLoading();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR BRANCHES ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE BRANCHES ###########");
      //console.log(branchArray);
      loadItems();
    }
  });

}

function loadBatch() {
  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'userBatch',
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id")
    },
    beforeSend: function(){
      loading('Loading Batch...');
      //pricesArray.length = 0;
      //pricesArraySearch.length = 0;
      
    },
    success: function(data){
      closeLoading();
      console.log("######### BATCH NUMBERS ASSIGNED ###########");
      
      //pricesArray = JSON.parse(data);
      //pricesArraySearch = JSON.parse(data);
      //console.log(itemsArray);
      var poptions = JSON.parse(data);

      console.log(poptions);
      $.each(poptions, function(index, value) {
          $('#userbatch').append('<option value="'+value.trans_no+'" >'+value.trans_no+'</option>');
      });
      //$('#mode_prices').append();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });

}


function loadPriceSelector(){
    $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'loadPriceSelector',
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      //loading('Loading items...');
      //pricesArray.length = 0;
      //pricesArraySearch.length = 0;
      
    },
    success: function(data){
      console.log("######### PRICES CATEGORIES ###########");
      
      //pricesArray = JSON.parse(data);
      //pricesArraySearch = JSON.parse(data);
      //console.log(itemsArray);
      var poptions = JSON.parse(data);

      console.log(poptions);
      $.each(poptions, function(index, value) {
          $('#mode_prices').append('<option value="'+value.id+'" >'+value.sales_type+'</option>');
      });
      //$('#mode_prices').append();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });
}

function saveManufactureData() {
  //console.log(storeItems);
//advance-manufacture.html
  var obj = MobileUI.objectByForm('frmManufacture');
  var mtype = obj.mtype;
  var item_option = obj.item_option;
  var item_option_id = obj.item_option_id;
  var quantity = obj.quantity;
  var destination = obj.destination;
  var destinationcode = obj.destinationcode;

  var tp = "savemanufacturedata";

  if(item_option == ""){

    alert('Please select Item to Manufacture');

  } else if(destination == ""){

    alert('Please select Destination');

  } else {

      $.ajax({
        type: "POST",
        url: window.localStorage.getItem("setSiteUrl") + "process.php",
        data: {
          tp: tp,
          mtype: mtype,
          item_option: item_option,
          item_option_id: item_option_id,
          quantity: quantity,
          destination: destination,
          destinationcode: destinationcode,
          cp: window.localStorage.getItem("cp"),
          id: window.localStorage.getItem("id")
        },
        beforeSend: function(){
          loading('Processing...');

          woReqArr.length = 0;
          
          
        },
        success: function(data){
          closeLoading();

          console.log(data);

          if(data == ""){

          } else {
            var res = data.split("|");

            if(res[0] == '0'){

              alert('Insufficient Items: \n'+ res[1]);

            } else {

            

            console.log(res[1]);
            
            woReqArr = JSON.parse(res[1]);
          

            var woref = res[0];
            var woid = woReqArr[0].workorder_id;
            if(mtype == "0"){
              var wotype = "Assemble";
            } else if(mtype == "0"){
              var wotype = "Unassemble";
            } else {
              var wotype = "Na";
            }

            console.log(res[1]);

            console.log(woref);
            console.log(woid);
            console.log(wotype);
            console.log(woReqArr);
            
            alert('Done');

            itmwo = "";
        //

            $.each(woReqArr, function(index, value){

              var qres = parseFloat(value.units_req) * parseFloat(quantity);

              itmwo += value.description+' - '+value.location_name+'   x'+qres+'    \n\n'; 
               
            });

            console.log("##############################");
            console.log(itm);

            //*********************

            
              try {

                BTPrinterObj.list(function(res){

                  var pname = res[0];
                  var paddress = res[1];
                  var ptype = res[2];
                  var htp = window.localStorage.getItem("cp");
                  

                  //Connect to printer 
                  BTPrinterObj.connect(function(data){

                    //1ST PRINT
                    //Receipt Title Header
                    BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                    BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                    BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                    BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                    //Receipt Header
                    BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'WORK ORDER: '+ woid+'\n','1','1');
                    BTPrinterObj.printTitle(null,null,'Reference: '+ woref,'1','1');
                    BTPrinterObj.printTitle(null,null,'Type: '+ wotype,'1','1');
                    BTPrinterObj.printTitle(null,null,'Item: '+ item_option,'1','1');
                    BTPrinterObj.printTitle(null,null,'Quantity: '+ quantity,'1','1');
                    BTPrinterObj.printTitle(null,null,'Destination: '+ destination,'1','1');
                    BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

                    BTPrinterObj.printText(function(data){
                      //Clear all pos data
                      
                      
                    },null, justify_left +
                    '\n\n' +
                    itmwo + 
                    '\n\n\n' +
                    'Served By: ' + window.localStorage.getItem("username") +
                    '\n\n\n' +
                    'THANKYOU' +
                    '\n\n\n\n\n\n' +
                   justify_left,'1','0');

                    //2ND PRINT
                    //Receipt Title Header
                    BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                    BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                    BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                    BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                    //Receipt Header
                    BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'WORK ORDER: '+ woid+'\n','1','1');
                    BTPrinterObj.printTitle(null,null,'Reference: '+ woref,'1','1');
                    BTPrinterObj.printTitle(null,null,'Type: '+ wotype,'1','1');
                    BTPrinterObj.printTitle(null,null,'Item: '+ item_option,'1','1');
                    BTPrinterObj.printTitle(null,null,'Quantity: '+ quantity,'1','1');
                    BTPrinterObj.printTitle(null,null,'Destination: '+ destination,'1','1');
                    BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

                    BTPrinterObj.printText(function(data){
                      //Clear all pos data
                      
                      
                    },null, justify_left +
                    '\n\n' +
                    itmwo + 
                    '\n\n\n' +
                    'Served By: ' + window.localStorage.getItem("username") +
                   '\n\n\n' +
                    'THANKYOU' +
                   '\n\n\n\n\n\n' +
                   justify_left,'1','0');

                  },function(err){     
                    
                    //Printer conn error
                    

                  }, pname);
                },function(err){

                    //Blutooth conn error
                    
                });

              } catch(err) {
                  
              } 




            //********************



            clearManufactureValues();
          }
          }

        },
        error: function(err){
          //console.log();
          //console.log("######### ERROR ITEMS ###########");
          //console.log(err);
        },
        complete: function(){
          //closeLoading();
          //console.log();
          //console.log("######### COMPLETE ITEMS ###########");
          //console.log(itemsArray);
          
        }
      });
  }

}

function clearManufactureValues() {
  MobileUI.clearForm('frmManufacture');
  $("#mode_prices").val("1");
  $("#quantity").val("1");
  storeItems.length = 0;
  woReqArr.length = 0;
}

function clearStoreValues() {
  MobileUI.clearForm('frmStockTransfer');
  $("#mode_prices").val("1");
  $("#quantity").val("1");
  storeItems.length = 0;

}

function clearStockTakeValues() {
  MobileUI.clearForm('frmStockTake');
  $("#mode_prices").val("1");
  $("#quantity").val("1");
  storeItems.length = 0;

}

function clearStoreRequisitionValues() {
  MobileUI.clearForm('frmStockRequisition');
  $("#mode_prices").val("1");
  $("#quantity").val("1");
  storeItems.length = 0;

}

function clearAdvManuValues() {
  MobileUI.clearForm('frmAdvManufacture');
  $("#quantity").val("1");
  advManuItems.length = 0;

}

function clearAdvManuProValues() {
  MobileUI.clearForm('frmAdvManufactureProduce');
  $("#quantity").val("1");
  advManuProdItems.length = 0;

}

function deleteAdvManuItems(index){
  //delete posItems[index];
  advManuItems.splice(index, 1);
  
}

function deleteStoreItems(index){
  //delete posItems[index];
  storeItems.splice(index, 1);
  
}

function editAdvManuItems(index){
  indexEdit = index;
  MobileUI.formByObject('frmAdvManufacture', advManuItems[index]);
}

function editStoreItems(index){
  indexEdit = index;
  MobileUI.formByObject('frmStockTransfer', storeItems[index]);
}

function editApprovalStoreItems(index){
  indexEdit = index;
  MobileUI.formByObject('frmStockApproval', storeItems[index]);
  $('#btnstockapprove').hide();
  $('#btnstockedit').show();
}

function editIssueStoreItems(index){
  indexEdit = index;
  MobileUI.formByObject('frmStockIssue', storeItems[index]);
  $('#btnstockapprove').hide();
  $('#btnstockedit').show();
}

function editAdvManufactureProduce(index){
  indexEdit = index;
  MobileUI.formByObject('frmAdvManufactureProduce', advManuProdItems[index]);
}

function deleteAdvManufactureProduce(index){
  //delete posItems[index];
  advManuProdItems.splice(index, 1);
  
}

//PRINT STOCK TRANSFER


function saveStoreTransferConfirm() {
  backPage();
  saveStoreTransfer();
}


function saveStoreTransfer() {
  //console.log(storeItems);

  var obj = MobileUI.objectByForm('frmStockTransfer');
  var forigin = obj.origincode;
  var foriginName = obj.origin;
  var fdestination = obj.destinationcode;
  var fdestinationName = obj.destination;
  var fmemo = obj.memo;

  var tp = "stocktransfer";

  console.log(forigin);
  console.log(foriginName);
  console.log(fdestination);
  console.log(fdestinationName);

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      origin: forigin,
      destination: fdestination,
      memo: fmemo,
      items: JSON.stringify(storeItems),
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id")
    },
    beforeSend: function(){
      loading('Processing...');
      
    },
    success: function(data){
      closeLoading();
      console.log(data);
      alert('Done');

      itm = "";
//
      $.each(storeItems, function(index, value){

        itm += value.item_option+' - '+value.item_option_id+'   x'+value.quantity+'    \n\n'; 
         
      });


      try {

        BTPrinterObj.list(function(res){

          var pname = res[0];
          var paddress = res[1];
          var ptype = res[2];
          var htp = window.localStorage.getItem("cp");
        

          //Connect to printer 
          BTPrinterObj.connect(function(data){

            //1ST PRINT
            //Receipt Title Header
            BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
            BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
            BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
            BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

            //Receipt Header
            BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'STORE TRANSFER\n','1','1');
            BTPrinterObj.printTitle(null,null,'Origin: '+ foriginName,'1','1');
            BTPrinterObj.printTitle(null,null,'Destination: '+ fdestinationName,'1','1');
            BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

            BTPrinterObj.printText(function(data){
              //Clear all pos data
              
              
            },null, justify_left +
            '\n\n' +
            itm + 
            '\n\n\n' +
            'Served By: ' + window.localStorage.getItem("username") +
            '\n\n\n' +
            'THANKYOU' +
            '\n\n\n\n\n\n' +
           justify_left,'1','0');

            //2ND PRINT
            //Receipt Title Header
            BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
            BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
            BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
            BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

            //Receipt Header
            BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'STORE TRANSFER REPRINT\n','1','1');
            BTPrinterObj.printTitle(null,null,'Origin: '+ foriginName,'1','1');
            BTPrinterObj.printTitle(null,null,'Destination: '+ fdestinationName,'1','1');
            BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

            BTPrinterObj.printText(function(data){
              //Clear all pos data
              
              
            },null, justify_left +
            '\n\n' +
            itm + 
            '\n\n\n' +
            'Served By: ' + window.localStorage.getItem("username") +
           '\n\n\n' +
            'THANKYOU' +
           '\n\n\n\n\n\n' +
           justify_left,'1','0');

          },function(err){     
            
            //Printer conn error
            

          }, pname);
        },function(err){

            //Blutooth conn error
            
        });

      } catch(err) {
          
      }

      clearStoreValues();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  }); 



}

function reprintStockTakeReport() {
      itm = "";
      var foriginName = "TO-ADD";
      //
      $.each(stockTakeReportItems, function(index, value){

        itm += value.description+' - '+value.stock_id+'   x'+value.quantity_after+'    \n\n'; 
         
      });


      try {

        BTPrinterObj.list(function(res){

          var pname = res[0];
          var paddress = res[1];
          var ptype = res[2];
          var htp = window.localStorage.getItem("cp");
        

          //Connect to printer 
          BTPrinterObj.connect(function(data){

            //1ST PRINT
            //Receipt Title Header
            BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
            BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
            BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
            BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

            //Receipt Header
            BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'STOCK TAKE\n','1','1');
            BTPrinterObj.printTitle(null,null,'Store: '+ foriginName,'1','1');
            BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

            BTPrinterObj.printText(function(data){
              //Clear all pos data
              
              
            },null, justify_left +
            '\n\n' +
            itm + 
            '\n\n\n' +
            'Served By: ' + window.localStorage.getItem("username") +
            '\n\n\n' +
            'THANKYOU' +
            '\n\n\n\n\n\n' +
           justify_left,'1','0');

            //2ND PRINT
            //Receipt Title Header
            BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
            BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
            BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
            BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

            //Receipt Header
            BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'STOCK TAKE REPRINT\n','1','1');
            BTPrinterObj.printTitle(null,null,'Store: '+ foriginName,'1','1');
            BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

            BTPrinterObj.printText(function(data){
              //Clear all pos data
              
              
            },null, justify_left +
            '\n\n' +
            itm + 
            '\n\n\n' +
            'Served By: ' + window.localStorage.getItem("username") +
           '\n\n\n' +
            'THANKYOU' +
           '\n\n\n\n\n\n' +
           justify_left,'1','0');

          },function(err){     
            
            //Printer conn error
            

          }, pname);
        },function(err){

            //Blutooth conn error
            
        });

      } catch(err) {
          
      }
}

function saveStockTake() {
  //console.log(storeItems);

  var obj = MobileUI.objectByForm('frmStockTake');
  var forigin = obj.origincode;
  var foriginName = obj.origin;
  var fmemo = obj.memo;

  var tp = "stocktake";

  console.log(forigin);
  console.log(foriginName);

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      origin: forigin,
      memo: fmemo,
      items: JSON.stringify(storeItems),
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id")
    },
    beforeSend: function(){
      loading('Processing...');
      
    },
    success: function(data){
      closeLoading();
      console.log(data);
      alert('Done');

      itm = "";
//
      $.each(storeItems, function(index, value){

        itm += value.item_option+' - '+value.item_option_id+'   x'+value.quantity+'    \n\n'; 
         
      });


      try {

        BTPrinterObj.list(function(res){

          var pname = res[0];
          var paddress = res[1];
          var ptype = res[2];
          var htp = window.localStorage.getItem("cp");
        

          //Connect to printer 
          BTPrinterObj.connect(function(data){

            //1ST PRINT
            //Receipt Title Header
            BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
            BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
            BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
            BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

            //Receipt Header
            BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'STOCK TAKE\n','1','1');
            BTPrinterObj.printTitle(null,null,'Store: '+ foriginName,'1','1');
            BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

            BTPrinterObj.printText(function(data){
              //Clear all pos data
              
              
            },null, justify_left +
            '\n\n' +
            itm + 
            '\n\n\n' +
            'Served By: ' + window.localStorage.getItem("username") +
            '\n\n\n' +
            'THANKYOU' +
            '\n\n\n\n\n\n' +
           justify_left,'1','0');

            //2ND PRINT
            //Receipt Title Header
            BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
            BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
            BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
            BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

            //Receipt Header
            BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'STOCK TAKE REPRINT\n','1','1');
            BTPrinterObj.printTitle(null,null,'Store: '+ foriginName,'1','1');
            BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

            BTPrinterObj.printText(function(data){
              //Clear all pos data
              
              
            },null, justify_left +
            '\n\n' +
            itm + 
            '\n\n\n' +
            'Served By: ' + window.localStorage.getItem("username") +
           '\n\n\n' +
            'THANKYOU' +
           '\n\n\n\n\n\n' +
           justify_left,'1','0');

          },function(err){     
            
            //Printer conn error
            

          }, pname);
        },function(err){

            //Blutooth conn error
            
        });

      } catch(err) {
          
      }

      clearStockTakeValues();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  }); 



}

function openStockIssue() {
  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'getstockissuedata',
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      //pricesArray.length = 0;
      //pricesArraySearch.length = 0;
      
    },
    success: function(data){

      closeLoading();

      //console.log(data);
     
      stockIssueArray = JSON.parse(data);

      console.log(stockIssueArray);

    

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });
}


function openStockApproval() {
  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'getstockapprovaldata',
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Please wait...');
      //pricesArray.length = 0;
      //pricesArraySearch.length = 0;
      
    },
    success: function(data){

      closeLoading();

      //console.log(data);
     
      stockRequisitionApprovalArray = JSON.parse(data);

      console.log(stockRequisitionApprovalArray);

    

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });
}

function saveStoreRequisition() {
  //console.log(storeItems);

  var obj = MobileUI.objectByForm('frmStockRequisition');
  var forigin = obj.origincode;
  var foriginName = obj.origin;
  var fdestination = obj.destinationcode;
  var fdestinationName = obj.destination;

  var tp = "stockrequisition";

  console.log(forigin);
  console.log(foriginName);
  console.log(fdestination);
  console.log(fdestinationName);

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      origin: forigin,
      destination: fdestination,
      items: JSON.stringify(storeItems),
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id")
    },
    beforeSend: function(){
      loading('Processing...');
      
    },
    success: function(data){
      closeLoading();
      console.log(data);
      alert('Done');

      itm = "";
//

      $.each(storeItems, function(index, value){

        itm += value.item_option+' - '+value.item_option_id+'   x'+value.quantity+'    \n\n'; 
         
      });


      try {

        BTPrinterObj.list(function(res){

          var pname = res[0];
          var paddress = res[1];
          var ptype = res[2];
          var htp = window.localStorage.getItem("cp");
          

          //Connect to printer 
          BTPrinterObj.connect(function(data){

            //1ST PRINT
            //Receipt Title Header
            BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
            BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
            BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
            BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

            //Receipt Header
            BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'STOCK REQUISITION\n','1','1');
            BTPrinterObj.printTitle(null,null,'Origin: '+ foriginName,'1','1');
            BTPrinterObj.printTitle(null,null,'Destination: '+ fdestinationName,'1','1');
            BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

            BTPrinterObj.printText(function(data){
              //Clear all pos data
              
              
            },null, justify_left +
            '\n\n' +
            itm + 
            '\n\n\n' +
            'Served By: ' + window.localStorage.getItem("username") +
            '\n\n\n' +
            'THANKYOU' +
            '\n\n\n\n\n\n' +
           justify_left,'1','0');

            //2ND PRINT
            //Receipt Title Header
            BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
            BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
            BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
            BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

            //Receipt Header
            BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'STOCK REQUISITION REPRINT\n','1','1');
            BTPrinterObj.printTitle(null,null,'Origin: '+ foriginName,'1','1');
            BTPrinterObj.printTitle(null,null,'Destination: '+ fdestinationName,'1','1');
            BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

            BTPrinterObj.printText(function(data){
              //Clear all pos data
              
              
            },null, justify_left +
            '\n\n' +
            itm + 
            '\n\n\n' +
            'Served By: ' + window.localStorage.getItem("username") +
           '\n\n\n' +
            'THANKYOU' +
           '\n\n\n\n\n\n' +
           justify_left,'1','0');

          },function(err){     
            
            //Printer conn error
            

          }, pname);
        },function(err){

            //Blutooth conn error
            
        });

      } catch(err) {
          
      }

      clearStoreRequisitionValues();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  }); 



}

function saveStoreIssue() {
  //console.log(storeItems);

  var obj = MobileUI.objectByForm('frmStockIssue');
  var forderid = obj.orderid;
  var fmemo = obj.memo;


  var forigin = storeItems[0].origincode;
  var foriginName = storeItems[0].origin;
  var fdestination = storeItems[0].destinationcode;
  var fdestinationName = storeItems[0].destination;
    

  var tp = "stockissue";

  console.log(forderid);
  console.log(storeItems);
  

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      orderid: forderid,
      origin: forigin,
      destination: fdestination,
      memo: fmemo,
      items: JSON.stringify(storeItems),
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id")
    },
    beforeSend: function(){
      loading('Processing...');
      
    },
    success: function(data){
      closeLoading();
      console.log(data);
      alert('Done');

      itm = "";
//

      $.each(storeItems, function(index, value){

        itm += value.item_option+' - '+value.item_option_id+'   x'+value.iquantity+'    \n\n'; 
         
      });


      try {

        BTPrinterObj.list(function(res){

          var pname = res[0];
          var paddress = res[1];
          var ptype = res[2];
          var htp = window.localStorage.getItem("cp");
          

          //Connect to printer 
          BTPrinterObj.connect(function(data){

            //1ST PRINT
            //Receipt Title Header
            BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
            BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
            BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
            BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

            //Receipt Header
            BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'STOCK ISSUE\n','1','1');
            BTPrinterObj.printTitle(null,null,'ID: '+ forderid,'1','1');
            BTPrinterObj.printTitle(null,null,'Origin: '+ foriginName,'1','1');
            BTPrinterObj.printTitle(null,null,'Destination: '+ fdestinationName,'1','1');
            BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

            BTPrinterObj.printText(function(data){
              //Clear all pos data
              
              
            },null, justify_left +
            '\n\n' +
            itm + 
            '\n\n\n' +
            'Served By: ' + window.localStorage.getItem("username") +
            '\n\n\n' +
            'THANKYOU' +
            '\n\n\n\n\n\n' +
           justify_left,'1','0');

            //2ND PRINT
            //Receipt Title Header
            BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
            BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
            BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
            BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

            //Receipt Header
            BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'STOCK ISSUE REPRINT\n','1','1');
            BTPrinterObj.printTitle(null,null,'ID: '+ forderid,'1','1');
            BTPrinterObj.printTitle(null,null,'Origin: '+ foriginName,'1','1');
            BTPrinterObj.printTitle(null,null,'Destination: '+ fdestinationName,'1','1');
            BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

            BTPrinterObj.printText(function(data){
              //Clear all pos data
              
              
            },null, justify_left +
            '\n\n' +
            itm + 
            '\n\n\n' +
            'Served By: ' + window.localStorage.getItem("username") +
           '\n\n\n' +
            'THANKYOU' +
           '\n\n\n\n\n\n' +
           justify_left,'1','0');

          },function(err){     
            
            //Printer conn error
            

          }, pname);
        },function(err){

            //Blutooth conn error
            
        });

      } catch(err) {
          
      }

      backPage();
      backPage();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  }); 



}

function saveStoreApproval() {
  //console.log(storeItems);

  var obj = MobileUI.objectByForm('frmStockApproval');
  var forderid = obj.orderid;

  var foriginName = storeItems[0].origin;
  var fdestinationName = storeItems[0].destination;
  

  var tp = "stockapproval";

  console.log(forderid);
  console.log(storeItems);
  

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      orderid: forderid,
      items: JSON.stringify(storeItems),
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id")
    },
    beforeSend: function(){
      loading('Processing...');
      
    },
    success: function(data){
      closeLoading();
      console.log(data);
      alert('Done');

      itm = "";
//

      $.each(storeItems, function(index, value){

        itm += value.item_option+' - '+value.item_option_id+'   x'+value.aquantity+'    \n\n'; 
         
      });


      try {

        BTPrinterObj.list(function(res){

          var pname = res[0];
          var paddress = res[1];
          var ptype = res[2];
          var htp = window.localStorage.getItem("cp");
          

          //Connect to printer 
          BTPrinterObj.connect(function(data){

            //1ST PRINT
            //Receipt Title Header
            BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
            BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
            BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
            BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

            //Receipt Header
            BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'STOCK APPROVAL\n','1','1');
            BTPrinterObj.printTitle(null,null,'ID: '+ forderid,'1','1');
            BTPrinterObj.printTitle(null,null,'Origin: '+ foriginName,'1','1');
            BTPrinterObj.printTitle(null,null,'Destination: '+ fdestinationName,'1','1');
            BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

            BTPrinterObj.printText(function(data){
              //Clear all pos data
              
              
            },null, justify_left +
            '\n\n' +
            itm + 
            '\n\n\n' +
            'Served By: ' + window.localStorage.getItem("username") +
            '\n\n\n' +
            'THANKYOU' +
            '\n\n\n\n\n\n' +
           justify_left,'1','0');

            //2ND PRINT
            //Receipt Title Header
            BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
            BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
            BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
            BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

            //Receipt Header
            BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'STOCK APPROVAL REPRINT\n','1','1');
            BTPrinterObj.printTitle(null,null,'ID: '+ forderid,'1','1');
            BTPrinterObj.printTitle(null,null,'Origin: '+ foriginName,'1','1');
            BTPrinterObj.printTitle(null,null,'Destination: '+ fdestinationName,'1','1');
            BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

            BTPrinterObj.printText(function(data){
              //Clear all pos data
              
              
            },null, justify_left +
            '\n\n' +
            itm + 
            '\n\n\n' +
            'Served By: ' + window.localStorage.getItem("username") +
           '\n\n\n' +
            'THANKYOU' +
           '\n\n\n\n\n\n' +
           justify_left,'1','0');

          },function(err){     
            
            //Printer conn error
            

          }, pname);
        },function(err){

            //Blutooth conn error
            
        });

      } catch(err) {
          
      }

      backPage();
      backPage();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  }); 



}

function confirmProduction(param) {

  $('#cmain_item').val(param.m);
  $('#cmain_quantity').val(param.q);
  $('#cwoidh').text(param.w);

}

function confirmProductionData() {

  var objMain = MobileUI.objectByForm('frmAdvManufactureProduceMain');
  var fitem = objMain.main_item;
  var fquantity = objMain.main_quantity;

  var obj = MobileUI.objectByForm('frmAdvManufactureProduce');
  var fwoid = obj.woidn;


  openPage('confirm-production',{m:fitem, q:fquantity, w:fwoid}, confirmProduction);

}

function confirmSaveAdvManuProduce() {

  backPage();

  saveAdvManuProduce();

}


function saveAdvManuProduce() {
  //console.log(storeItems);

  var objMain = MobileUI.objectByForm('frmAdvManufactureProduceMain');
  var fquantity = objMain.main_quantity;

  var obj = MobileUI.objectByForm('frmAdvManufactureProduce');
  var fwoid = obj.woidn;
  var fwodate = obj.wodate;
  var fworef = obj.woref;
  var fwodesc = obj.wodesc;
  var hfwotype = obj.wotype;
  var fwoloc = obj.woloc;
  var fwoqty = obj.woqty;

  if(hfwotype == '0') {
    var fwotype = 'Assemble';
  } else if(hfwotype == '0') {
    var fwotype = 'Unassemble';
  } else {
    var fwotype = 'Advance';
  }

  console.log(fquantity);

  console.log(fwoid);
  console.log(fwodate);
  console.log(fworef);
  console.log(fwodesc);
  console.log(fwoloc);
  console.log(fwotype);

  var tp = "savemanufactureproducedata";


  if(fquantity == ""){

  } else {

    $.ajax({
      type: "POST",
      url: window.localStorage.getItem("setSiteUrl") + "process.php",
      data: {
        tp: tp,
        woid: fwoid,
        quantity: fquantity,
        items: JSON.stringify(advManuProdItems),
        cp: window.localStorage.getItem("cp"),
        id: window.localStorage.getItem("id")
      },
      beforeSend: function(){
        loading('Processing...');
        
      },
      success: function(data){
        closeLoading();
        console.log(data);
        alert('Done');

        var woreqq = JSON.parse(data);

        itmwo = "";
        itmwot = "";
          //

        $.each(advManuProdItems, function(index, value){

          itmwo += value.item_option+' - '+value.item_option_id+'   x'+value.quantity+'    \n\n'; 
           
        });

        if(woreqq.length > 0){
          $.each(woreqq, function(index, value){

            var qq = parseFloat(value.qty) * -1;

            itmwot += value.description+'   x'+qq+'    \n\n'; 
             
          });
        }

          


        //*********************

              
                try {

                  BTPrinterObj.list(function(res){

                    var pname = res[0];
                    var paddress = res[1];
                    var ptype = res[2];
                    var htp = window.localStorage.getItem("cp");
                    

                    //Connect to printer 
                    BTPrinterObj.connect(function(data){

                      //1ST PRINT
                      //Receipt Title Header
                      BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                      BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                      BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                      BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                      //Receipt Header
                      BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'PRODUCE WORK ORDER: '+ fwoid+'\n','1','1');
                      BTPrinterObj.printTitle(null,null,'Quantity: '+ fwoqty,'1','1');
                      BTPrinterObj.printTitle(null,null,'Reference: '+ fworef,'1','1');
                      BTPrinterObj.printTitle(null,null,'Type: '+ fwotype,'1','1');
                      BTPrinterObj.printTitle(null,null,'Item: '+ fwodesc,'1','1');
                      BTPrinterObj.printTitle(null,null,'Origin: '+ fwoloc,'1','1');
                      BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

                      BTPrinterObj.printText(function(data){
                        //Clear all pos data

                      },null, justify_left +
                      '\n\n' +
                      itmwo + 
                      '\n\n\n' +
                     justify_left,'1','0');

                      BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + "ISSUED ITEMS",'1','1');

                      BTPrinterObj.printText(function(data){
                        //Clear all pos data

                      },null, justify_left +
                      '\n\n' +
                      itmwot + 
                      '\n\n\n' +
                      'Served By: ' + window.localStorage.getItem("username") +
                      '\n\n\n' +
                      'THANKYOU' +
                      '\n\n\n\n\n\n' +
                     justify_left,'1','0');

                      //2ND PRINT
                      //Receipt Title Header
                      BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                      BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                      BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                      BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                      //Receipt Header
                      BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'PRODUCE WORK ORDER: '+ fwoid+'\n','1','1');
                      BTPrinterObj.printTitle(null,null,'Quantity: '+ fwoqty,'1','1');
                      BTPrinterObj.printTitle(null,null,'Reference: '+ fworef,'1','1');
                      BTPrinterObj.printTitle(null,null,'Type: '+ fwotype,'1','1');
                      BTPrinterObj.printTitle(null,null,'Item: '+ fwodesc,'1','1');
                      BTPrinterObj.printTitle(null,null,'Origin: '+ fwoloc,'1','1');
                      BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

                      BTPrinterObj.printText(function(data){
                        //Clear all pos data
                        
                        
                      },null, justify_left +
                      '\n\n' +
                      itmwo + 
                      '\n\n\n' +
                     justify_left,'1','0');

                      BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + "ISSUED ITEMS",'1','1');

                      BTPrinterObj.printText(function(data){
                        //Clear all pos data

                      },null, justify_left +
                      '\n\n' +
                      itmwot + 
                      '\n\n\n' +
                      'Served By: ' + window.localStorage.getItem("username") +
                      '\n\n\n' +
                      'THANKYOU' +
                      '\n\n\n\n\n\n' +
                     justify_left,'1','0');

                    },function(err){     
                      
                      //Printer conn error
                      

                    }, pname);
                  },function(err){

                      //Blutooth conn error
                      
                  });

                } catch(err) {
                    
                } 




              //********************


        clearAdvManuProValues();

      },
      error: function(err){
        //console.log();
        //console.log("######### ERROR ITEMS ###########");
        //console.log(err);
      },
      complete: function(){
        //closeLoading();
        //console.log();
        //console.log("######### COMPLETE ITEMS ###########");
        //console.log(itemsArray);
        
      }
    }); 

  }



}


function saveAdvManuItem() {
  //console.log(storeItems);

  var obj = MobileUI.objectByForm('frmAdvManufacture');
  var forigin = obj.origincode;
  var fwoid = obj.woid;
  var fwodate = obj.wodate;
  var fworef = obj.woref;
  var fwodesc = obj.wodesc;
  var hfwotype = obj.wotype;
  var fwoloc = obj.woloc;
  var fwoqty = obj.woqty;

  if(hfwotype == '0') {
    var fwotype = 'Assemble';
  } else if(hfwotype == '0') {
    var fwotype = 'Unassemble';
  } else {
    var fwotype = 'Advance';
  }


  var tp = "savemanufactureissuedata";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      woid: fwoid,
      origin: forigin,
      items: JSON.stringify(advManuItems),
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id")
    },
    beforeSend: function(){
      loading('Processing...');
      
    },
    success: function(data){
      closeLoading();
      console.log(data);
      alert('Done');

       itmwo = "";
//

      $.each(advManuItems, function(index, value){

        itmwo += value.item_option+' - '+value.item_option_id+'   x'+value.quantity+'    \n\n'; 
         
      });


      //*********************
            
              try {

                BTPrinterObj.list(function(res){

                  var pname = res[0];
                  var paddress = res[1];
                  var ptype = res[2];
                  var htp = window.localStorage.getItem("cp");
                  

                  //Connect to printer 
                  BTPrinterObj.connect(function(data){

                    //1ST PRINT
                    //Receipt Title Header
                    BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                    BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                    BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                    BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                    //Receipt Header
                    BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'ISSUE WORK ORDER: '+ fwoid+'\n','1','1');
                    BTPrinterObj.printTitle(null,null,'Quantity: '+ fwoqty,'1','1');
                    BTPrinterObj.printTitle(null,null,'Reference: '+ fworef,'1','1');
                    BTPrinterObj.printTitle(null,null,'Type: '+ fwotype,'1','1');
                    BTPrinterObj.printTitle(null,null,'Item: '+ fwodesc,'1','1');
                    BTPrinterObj.printTitle(null,null,'Origin: '+ fwoloc,'1','1');
                    BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

                    BTPrinterObj.printText(function(data){
                      //Clear all pos data
                      
                      
                    },null, justify_left +
                    '\n\n' +
                    itmwo + 
                    '\n\n\n' +
                    'Served By: ' + window.localStorage.getItem("username") +
                    '\n\n\n' +
                    'THANKYOU' +
                    '\n\n\n\n\n\n' +
                   justify_left,'1','0');

                    //2ND PRINT
                    //Receipt Title Header
                    BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                    BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                    BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                    BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                    //Receipt Header
                    BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'ISSUE WORK ORDER: '+ fwoid+'\n','1','1');
                    BTPrinterObj.printTitle(null,null,'Quantity: '+ fwoqty,'1','1');
                    BTPrinterObj.printTitle(null,null,'Reference: '+ fworef,'1','1');
                    BTPrinterObj.printTitle(null,null,'Type: '+ fwotype,'1','1');
                    BTPrinterObj.printTitle(null,null,'Item: '+ fwodesc,'1','1');
                    BTPrinterObj.printTitle(null,null,'Origin: '+ fwoloc,'1','1');
                    BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');

                    BTPrinterObj.printText(function(data){
                      //Clear all pos data
                      
                      
                    },null, justify_left +
                    '\n\n' +
                    itmwo + 
                    '\n\n\n' +
                    'Served By: ' + window.localStorage.getItem("username") +
                   '\n\n\n' +
                    'THANKYOU' +
                   '\n\n\n\n\n\n' +
                   justify_left,'1','0');

                  },function(err){     
                    
                    //Printer conn error
                    

                  }, pname);
                },function(err){

                    //Blutooth conn error
                    
                });

              } catch(err) {
                  
              } 




            //********************




      clearAdvManuValues();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });



}


//Add Production Byproduct Items
function addAdvManuProduce(){
  var obj = MobileUI.objectByForm('frmAdvManufactureProduce');
  MobileUI.hide('messageValidation');

  var qty = obj.quantity;
 
  if(obj.item_option == "" || obj.quantity == "") {
    alert('Please fill all inputs.');
  } else {

    if(indexEdit>=0){
      advManuProdItems[indexEdit] = obj;
      indexEdit=-1;
    } else {

      var gotsts = false;
      for(var i=0; i<advManuProdItems.length; i++) {
        //console.log(posItems[i]['item_option'].toString().toLowerCase());
        //console.log(obj.item_option.toLowerCase());
        if(advManuProdItems[i]['item_option_id'].toString().toLowerCase() == obj.item_option_id.toLowerCase()){
            advManuProdItems[i].quantity = parseInt(advManuProdItems[i].quantity) + parseInt(obj.quantity);
            
            gotsts = true;
            //index++; 
            //console.log(gotsts);
            break;
        }
        
      }

      //var sitem = search(posItems,obj.item_option);
      

      if(gotsts){
        //console.log(sitem[0].item_option);
        //console.log(sitem[0].quantity);
      } else {
        advManuProdItems.push(obj);
      }
      
    }


    $("#item_option").val("");
    $("#item_option_id").val("");
    $("#quantity").val("1");

  }

    
  //
}



//Add Advance Manufacture Items
function addAdvManuItem(){
  var obj = MobileUI.objectByForm('frmAdvManufacture');
  MobileUI.hide('messageValidation');

  var qty = obj.quantity;
  var origincode = obj.origincode;
  var itemCode = obj.item_option_id;

  $.post(window.localStorage.getItem("setSiteUrl") + "process.php", {
        tp: "getavailableqty",
        code: itemCode,
        store: origincode,
        cp: window.localStorage.getItem("cp"),
        id: window.localStorage.getItem("id")
    },
    function(data, status){
      console.log(data);

      var qtyCheck = data;

      console.log("ITEMCODE: "+ itemCode);
      console.log("STORE: "+ origincode);

      console.log("QTY-CHECK: "+ qtyCheck);

      if(parseInt(qtyCheck) == 0){
        alert('No enough quantity.');
      } else if(parseFloat(qty) > parseFloat(qtyCheck)){
        alert('Available quantity is '+ qtyCheck);
      } else if(obj.origin == "" || obj.item_option == "" || obj.quantity == "") {
        alert('Please fill all inputs.');
      } else {

        if(indexEdit>=0){
          advManuItems[indexEdit] = obj;
          indexEdit=-1;
        } else {

          var gotsts = false;
          for(var i=0; i<advManuItems.length; i++) {
            //console.log(posItems[i]['item_option'].toString().toLowerCase());
            //console.log(obj.item_option.toLowerCase());
            if(advManuItems[i]['item_option_id'].toString().toLowerCase() == obj.item_option_id.toLowerCase()){
                advManuItems[i].quantity = parseInt(advManuItems[i].quantity) + parseInt(obj.quantity);
                
                gotsts = true;
                //index++; 
                //console.log(gotsts);
                break;
            }
            
          }

          //var sitem = search(posItems,obj.item_option);
          

          if(gotsts){
            //console.log(sitem[0].item_option);
            //console.log(sitem[0].quantity);
          } else {
            advManuItems.push(obj);
          }
          
        }


        $("#item_option").val("");
        $("#item_option_id").val("");
        $("#quantity").val("1");

      }


    });
 

  //
}

function getQty(code, store) {

  $ret = "0";

  $.post(window.localStorage.getItem("setSiteUrl") + "process.php", {
        tp: "getavailableqty",
        code: code,
        store: store,
        cp: window.localStorage.getItem("cp"),
        id: window.localStorage.getItem("id")
    },
    function(data, status){
      console.log(data);

      $ret = data;
      
          
    });

  return $ret;

}

//Add Store Items
function addStockTakeItem(){
  var obj = MobileUI.objectByForm('frmStockTake');
  MobileUI.hide('messageValidation');

  var qty = obj.quantity;
  var itemCode = obj.item_option_id;
  var store = obj.origincode;


  $.post(window.localStorage.getItem("setSiteUrl") + "process.php", {
        tp: "getavailableqty",
        code: itemCode,
        store: store,
        cp: window.localStorage.getItem("cp"),
        id: window.localStorage.getItem("id")
    },
    function(data, status){
      console.log(data);

      var qtyCheck = data;

      console.log("ITEMCODE: "+ itemCode);
      console.log("STORE: "+ store);

      console.log("QTY-CHECK: "+ qtyCheck);

      if(obj.origin == "" || obj.item_option == "" || obj.quantity == "") {
        alert('Please fill all inputs.');
      } else {

        if(indexEdit>=0){
          storeItems[indexEdit] = obj;
          indexEdit=-1;
        } else {

          var gotsts = false;
          for(var i=0; i<storeItems.length; i++) {
            //console.log(posItems[i]['item_option'].toString().toLowerCase());
            //console.log(obj.item_option.toLowerCase());
            if(storeItems[i]['item_option_id'].toString().toLowerCase() == obj.item_option_id.toLowerCase()){
                storeItems[i].quantity = parseInt(storeItems[i].quantity) + parseInt(obj.quantity);
                
                gotsts = true;
                //index++; 
                //console.log(gotsts);
                break;
            }
            
          }

          //var sitem = search(posItems,obj.item_option);
          

          if(gotsts){
            //console.log(sitem[0].item_option);
            //console.log(sitem[0].quantity);
          } else {
            storeItems.push(obj);
          }
          
        }


        $("#item_option").val("");
        $("#item_option_id").val("");
        $("#mode_prices").val("1");
        $("#rate").val("");
        $("#quantity").val("1");

      }
      
          
    });
  //
}

//Add Store Items
function addStoreItem(){
  var obj = MobileUI.objectByForm('frmStockTransfer');
  MobileUI.hide('messageValidation');

  var qty = obj.quantity;
  var itemCode = obj.item_option_id;
  var store = obj.origincode;


  $.post(window.localStorage.getItem("setSiteUrl") + "process.php", {
        tp: "getavailableqty",
        code: itemCode,
        store: store,
        cp: window.localStorage.getItem("cp"),
        id: window.localStorage.getItem("id")
    },
    function(data, status){
      console.log(data);

      var qtyCheck = data;

      console.log("ITEMCODE: "+ itemCode);
      console.log("STORE: "+ store);

      console.log("QTY-CHECK: "+ qtyCheck);

      if(parseInt(qtyCheck) == 0){
        alert('No enough quantity.');
      } else if(parseFloat(qty) > parseFloat(qtyCheck)){
        alert('Available quantity is '+ qtyCheck);
      } else if(obj.origin == "" || obj.destination == "" || obj.item_option == "" || obj.quantity == "") {
        alert('Please fill all inputs.');
      } else {

        if(indexEdit>=0){
          storeItems[indexEdit] = obj;
          indexEdit=-1;
        } else {

          var gotsts = false;
          for(var i=0; i<storeItems.length; i++) {
            //console.log(posItems[i]['item_option'].toString().toLowerCase());
            //console.log(obj.item_option.toLowerCase());
            if(storeItems[i]['item_option_id'].toString().toLowerCase() == obj.item_option_id.toLowerCase()){
                storeItems[i].quantity = parseInt(storeItems[i].quantity) + parseInt(obj.quantity);
                
                gotsts = true;
                //index++; 
                //console.log(gotsts);
                break;
            }
            
          }

          //var sitem = search(posItems,obj.item_option);
          

          if(gotsts){
            //console.log(sitem[0].item_option);
            //console.log(sitem[0].quantity);
          } else {
            storeItems.push(obj);
          }
          
        }


        $("#item_option").val("");
        $("#item_option_id").val("");
        $("#mode_prices").val("1");
        $("#rate").val("");
        $("#quantity").val("1");

      }
      
          
    });
  //
}

//Add Store Items
function addStoreRequisitionItem(){
  var obj = MobileUI.objectByForm('frmStockRequisition');
  MobileUI.hide('messageValidation');

  var qty = obj.quantity;
 
  if(obj.origin == "" || obj.destination == "" || obj.item_option == "" || obj.quantity == "") {
    alert('Please fill all inputs.');
  } else {

    if(indexEdit>=0){
      storeItems[indexEdit] = obj;
      indexEdit=-1;
    } else {

      var gotsts = false;
      for(var i=0; i<storeItems.length; i++) {
        //console.log(posItems[i]['item_option'].toString().toLowerCase());
        //console.log(obj.item_option.toLowerCase());
        if(storeItems[i]['item_option_id'].toString().toLowerCase() == obj.item_option_id.toLowerCase()){
            storeItems[i].quantity = parseInt(storeItems[i].quantity) + parseInt(obj.quantity);
            
            gotsts = true;
            //index++; 
            //console.log(gotsts);
            break;
        }
        
      }

      //var sitem = search(posItems,obj.item_option);
      

      if(gotsts){
        //console.log(sitem[0].item_option);
        //console.log(sitem[0].quantity);
      } else {
        storeItems.push(obj);
      }
      
    }


    $("#item_option").val("");
    $("#item_option_id").val("");
    $("#mode_prices").val("1");
    $("#rate").val("");
    $("#quantity").val("1");

  }

    
  //
}

//Edit Store Issue Items
function editStoreIssueItem(){
  var obj = MobileUI.objectByForm('frmStockIssue');
  MobileUI.hide('messageValidation');

  console.log(storeItems[0]);

  var aaqty = parseInt(storeItems[0].aquantity);
  var iiqty = parseInt(obj.iquantity);
 
  if(obj.origin == "" || obj.destination == "" || obj.item_option == "" || obj.aquantity == "") {
    alert('Please fill all inputs.');
  } else if(iiqty > aaqty){

    alert('Issued Quantity can not be more than Approved Quantity.');

  } else {

    if(indexEdit>=0){
      storeItems[indexEdit] = obj;
      indexEdit=-1;
    } else {

      var gotsts = false;
      for(var i=0; i<storeItems.length; i++) {
        //console.log(posItems[i]['item_option'].toString().toLowerCase());
        //console.log(obj.item_option.toLowerCase());
        if(storeItems[i]['item_option_id'].toString().toLowerCase() == obj.item_option_id.toLowerCase()){
         
            storeItems[i].iquantity = parseInt(storeItems[i].iquantity) + parseInt(obj.iquantity);
            
            gotsts = true;
            //index++; 
            //console.log(gotsts);
            break;
        }
        
      }

      //var sitem = search(posItems,obj.item_option);
      

      if(gotsts){
        //console.log(sitem[0].item_option);
        //console.log(sitem[0].quantity);
      } else {
        storeItems.push(obj);
      }
      
    }

    //console.log(storeItems[0]);


    
    $("#item_option").val("");
    $("#item_option_id").val("");
    $("#mode_prices").val("1");
    $("#rate").val("");
    $("#aquantity").val("1");

    $("#btnstockedit").hide();
    $("#btnstockapprove").show();

  }

    
  //
}


//Edit Store Items
function editStoreApprovalItem(){
  var obj = MobileUI.objectByForm('frmStockApproval');
  MobileUI.hide('messageValidation');

  console.log(storeItems[0]);
 
  if(obj.origin == "" || obj.destination == "" || obj.item_option == "" || obj.aquantity == "") {
    alert('Please fill all inputs.');
  } else {

    if(indexEdit>=0){
      storeItems[indexEdit] = obj;
      indexEdit=-1;
    } else {

      var gotsts = false;
      for(var i=0; i<storeItems.length; i++) {
        //console.log(posItems[i]['item_option'].toString().toLowerCase());
        //console.log(obj.item_option.toLowerCase());
        if(storeItems[i]['item_option_id'].toString().toLowerCase() == obj.item_option_id.toLowerCase()){
          console.log(storeItems[i]);
            storeItems[i].aquantity = parseInt(storeItems[i].aquantity) + parseInt(obj.aquantity);
            
            gotsts = true;
            //index++; 
            //console.log(gotsts);
            break;
        }
        
      }

      //var sitem = search(posItems,obj.item_option);
      

      if(gotsts){
        //console.log(sitem[0].item_option);
        //console.log(sitem[0].quantity);
      } else {
        storeItems.push(obj);
      }
      
    }

    //console.log(storeItems[0]);


    
    $("#item_option").val("");
    $("#item_option_id").val("");
    $("#mode_prices").val("1");
    $("#rate").val("");
    $("#aquantity").val("1");

    $("#btnstockedit").hide();
    $("#btnstockapprove").show();

  }

    
  //
}



//Get Selected Store
function getSelectedStore(lname, lcode, ltype){

    if(ltype == 'o'){
      $('#origin').val(lname);
      $('#origincode').val(lcode);
    } else if(ltype == 'd'){
      $('#destination').val(lname);
      $('#destinationcode').val(lcode);
    } 

    backPage();
    
}

function loadDestinationStore(){
  var tp = "getdestinationstore";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id")
    },
    beforeSend: function(){
      loading('Loading Stores...');

     
      destinationStoreArray.length = 0;
      
    },
    success: function(data){
      closeLoading();
      console.log("######### SUCCESS DESTINATION STORE ###########");
      
      destinationStoreArray = JSON.parse(data);
      console.log(destinationStoreArray);

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });

}

function openIssueAdvManu(param) {
  //alert(param.woid);
  
  $('#woidh').text(param.woid);
  $('#woid').val(param.woid);
  $('#wodate').val(param.wodate);
  $('#woref').val(param.woref);
  $('#wodesc').val(param.wodesc);
  $('#wotype').val(param.wotype);
  $('#woloc').val(param.woloc);
  $('#woqty').val(param.woqty);
}

function openStockApproveView(param) {
  $('#origin').val(param.oname);
  $('#origincode').val(param.oid);
  $('#destination').val(param.dname);
  $('#destinationcode').val(param.did);
  $('#orderid').val(param.orderId);
  $('#quantity').val(param.qty);
  $('#btnstockedit').hide();
}

function stockApproveView(index) {
  var orderId = stockRequisitionApprovalArray[index].id;
  var origin_store = stockRequisitionApprovalArray[index].origin_store;
  var origin_store_name = stockRequisitionApprovalArray[index].origin_store_name;
  var dest_store = stockRequisitionApprovalArray[index].dest_store;
  var dest_store_name = stockRequisitionApprovalArray[index].dest_store_name;
  

  storeItems.length = 0;
  storeItems = JSON.parse(stockRequisitionApprovalArray[index].items);

  $.each(storeItems, function(index, value){

    value['aquantity'] = value.quantity;
     
  });

  console.log(storeItems);

  
  //storeItems[0]['aquantity'] = storeItems[0].quantity;
  //var qty = storeItems[0].quantity;
  var qty = 1;
  
  

  openPage('stock-approval-edit',{oid:origin_store, oname:origin_store_name, did:dest_store, dname:dest_store_name, orderId:orderId, qty:qty}, openStockApproveView);

}

function openStockIssueView(param) {
  $('#origin').val(param.oname);
  $('#origincode').val(param.oid);
  $('#destination').val(param.dname);
  $('#destinationcode').val(param.did);
  $('#orderid').val(param.orderId);
  $('#quantity').val(param.qty);
  $('#aquantity').val(param.aqty);
  $('#btnstockedit').hide();
}

function stockIssueView(index) {
  var orderId = stockIssueArray[index].id;
  var origin_store = stockIssueArray[index].origin_store;
  var origin_store_name = stockIssueArray[index].origin_store_name;
  var dest_store = stockIssueArray[index].dest_store;
  var dest_store_name = stockIssueArray[index].dest_store_name;


  storeItems.length = 0;
  storeItems = JSON.parse(stockIssueArray[index].items);

  $.each(storeItems, function(index, value){

    value['iquantity'] = value.aquantity;
     
  });

  console.log(storeItems);

  
  //storeItems[0]['iquantity'] = storeItems[0].aquantity;
  //var qty = storeItems[0].quantity;
  //var aqty = storeItems[0].aquantity;

  var qty = 1;
  var aqty = 1;
  
  

  openPage('stock-issue-edit',{oid:origin_store, oname:origin_store_name, did:dest_store, dname:dest_store_name, orderId:orderId, qty:qty, aqty:aqty}, openStockIssueView);

}

function issueAdvManu(index) {

  var woid = advanceManufactureArray[index].id;
  var wodate = advanceManufactureArray[index].date_;
  var woref = advanceManufactureArray[index].wo_ref;
  var wodesc = advanceManufactureArray[index].description;
  var wotype = advanceManufactureArray[index].type;
  var woloc = advanceManufactureArray[index].location_name;
  var woqty = advanceManufactureArray[index].units_reqd;

  loadOriginStore();
  loadManufacture();

  
  openPage('advance-manufacture-main',{woid:woid, wodate:wodate, woref:woref, wodesc:wodesc, wotype:wotype, woloc:woloc, woqty:woqty}, openIssueAdvManu);

}

function openProduceAdvManu(param) {
  //alert(param.woid); 

  $('#main_item').val(param.wodesc);
  $('#main_quantity').val(param.woqty);
  
  $('#woidh').text(param.woid);
  $('#woidn').val(param.woid);
  $('#wodate').val(param.wodate);
  $('#woref').val(param.woref);
  $('#wodesc').val(param.wodesc);
  $('#wotype').val(param.wotype);
  $('#woloc').val(param.woloc);
  $('#woqty').val(param.woqty);
}

function produceAdvManu(index) {

  console.log(advanceManufactureArray[index]);

  var woid = advanceManufactureArray[index].id;
  var wodate = advanceManufactureArray[index].date_;
  var woref = advanceManufactureArray[index].wo_ref;
  var wodesc = advanceManufactureArray[index].description;
  var wotype = advanceManufactureArray[index].type;
  var woloc = advanceManufactureArray[index].location_name;
  var woqty = advanceManufactureArray[index].units_reqd;

  loadManufacture();
  
  openPage('advance-manufacture-produce',{woid:woid, wodate:wodate, woref:woref, wodesc:wodesc, wotype:wotype, woloc:woloc, woqty:woqty}, openProduceAdvManu);

}

function openAdvanceManufacture() {
  loadWorkOrders();
}

function loadWorkOrders() {

  var tp = "getworkoerderitems";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id")
    },
    beforeSend: function(){
      loading('Loading Items...');
      advanceManufactureArray.length = 0;
      
            //openPage('advance-manufacture');
          
      
    },
    success: function(data){
      closeLoading();
      console.log("######### SUCCESS WORK ORDER ITEMS ###########");
      
      advanceManufactureArray = JSON.parse(data);
      console.log(advanceManufactureArray);

      //loadOriginStore();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });

}

function loadManufacture() {

  var tp = "getmanufactureitems";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id")
    },
    beforeSend: function(){
      loading('Loading Items...');
      manufactureItemsArraySearch.length = 0;
      
    },
    success: function(data){
      closeLoading();
      console.log("######### SUCCESS MANUFACTURE ITEMS ###########");
      
      manufactureItemsArraySearch = JSON.parse(data);
      console.log(manufactureItemsArraySearch);

      loadDestinationStore();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });

}

function loadOriginStore(){
  var tp = "getoriginstore";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id")
    },
    beforeSend: function(){
      loading('Loading Stores...');
      originStoreArray.length = 0;
      
    },
    success: function(data){
      closeLoading();
      console.log("######### SUCCESS ORIGIN STORE ###########");
      
      originStoreArray = JSON.parse(data);
      console.log(originStoreArray);

      loadDestinationStore();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });

}

function loadItems(){
  var tp = "loadItems";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      //loading('Loading items...');
      itemsArray.length = 0;
      itemsArraySearch.length = 0;
      
    },
    success: function(data){
      console.log("######### SUCCESS ITEMS ###########");
      
      itemsArray = JSON.parse(data);
      itemsArraySearch = JSON.parse(data);
      //console.log(itemsArray);

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      //closeLoading();
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);

      loadSpTrip();
      
    }
  });

}

function loadUsersHistMap() {
  var tp = "loadUsers";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      //loading('Loading users...');
      usersArray.length = 0;
      usersArraySearch.length = 0;
      usersArrayUnChanged.length = 0;
      vectorLayer.removeAllFeatures();
      vectorLayer.destroyFeatures();//optional
      vectorLayer.addFeatures([]);
      vectorLayer.setVisibility(0);
      
      
    },
    success: function(data){
      //console.log("######### SUCCESS USERS ###########");
      
      //alert(data);
      var specuserarr = JSON.parse(data);
      usersArray = JSON.parse(data);
      usersArraySearch = JSON.parse(data);
      usersArrayUnChanged = JSON.parse(data);
      
      /*$.each(specuserarr, function(index, value){
        //alert(value.id + " --> " + index);
            //console.log(value);
            //console.log(value.lat);
        var feature = new OpenLayers.Feature.Vector(
                new OpenLayers.Geometry.Point(parseFloat(value.lon), parseFloat(value.lat)).transform('EPSG:4326', 'EPSG:900913'),
                {
                    description: ''
                },
                {label: value.uname, labelOutlineColor: '#fff',labelOutlineWidth: 4, labelAlign: 'cm', labelXOffset: 36, labelYOffset: 10, strokeWidth: 4, strokeColor: '#fff', fontSize: "13px", fontWeight: "bold", fontFamily: 'roboto',externalGraphic: 'img/ic.png', graphicHeight: 36 }
        );

        vectorLayer.addFeatures(feature);
            
      });

      var bounds = vectorLayer.getDataExtent();
      map.zoomToExtent(bounds);
      vectorLayer.setVisibility(1);*/


    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR USERS ###########");
      //console.log(err);
    },
    complete: function(){
      //console.log();
      //console.log("######### COMPLETE USERS ###########");
      //console.log(usersArray);
      //closeLoading();
    }
  });
}

function loadUsersMap(){
  var tp = "loadUsers";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Loading users...');
      usersArray.length = 0;
      usersArraySearch.length = 0;
      usersArrayUnChanged.length = 0;
      vectorLayer.removeAllFeatures();
      vectorLayer.destroyFeatures();//optional
      vectorLayer.addFeatures([]);
      vectorLayer.setVisibility(0);
      
      
    },
    success: function(data){
      //console.log("######### SUCCESS USERS ###########");
      
      //alert(data);
      var specuserarr = JSON.parse(data);
      usersArray = JSON.parse(data);
      usersArraySearch = JSON.parse(data);
      usersArrayUnChanged = JSON.parse(data);
      
      $.each(specuserarr, function(index, value){
        //alert(value.id + " --> " + index);
            //console.log(value);
            //console.log(value.lat);
        var feature = new OpenLayers.Feature.Vector(
                new OpenLayers.Geometry.Point(parseFloat(value.lon), parseFloat(value.lat)).transform('EPSG:4326', 'EPSG:900913'),
                {
                    description: ''
                },
                {label: value.uname, labelOutlineColor: '#fff',labelOutlineWidth: 4, labelAlign: 'cm', labelXOffset: 36, labelYOffset: 10, strokeWidth: 4, strokeColor: '#fff', fontSize: "13px", fontWeight: "bold", fontFamily: 'roboto',externalGraphic: 'img/ic.png', graphicHeight: 36 }
        );

        vectorLayer.addFeatures(feature);
            
      });

      var bounds = vectorLayer.getDataExtent();
      map.zoomToExtent(bounds);
      vectorLayer.setVisibility(1);


    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR USERS ###########");
      //console.log(err);
    },
    complete: function(){
      //console.log();
      //console.log("######### COMPLETE USERS ###########");
      //console.log(usersArray);
      closeLoading();
    }
  });

}

//tasksArray
function loadTasks() {
  var tp = "loadTasks";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      uname: window.localStorage.getItem("username"),
      uid: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Loading tasks...');
      tasksArray.length = 0;
      
      
    },
    success: function(data){
      console.log("######### SUCCESS TASKS ###########");
      console.log(data);
      //alert(data);
      tasksArray = JSON.parse(data);

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR USERS ###########");
      //console.log(err);
    },
    complete: function(){
      //console.log();
      //console.log("######### COMPLETE USERS ###########");
      //console.log(usersArray);
      closeLoading();
    }
  });
}

function loadTargetCategories() {
  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'targetCategories',
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id"),
      uname: window.localStorage.getItem("username"),
    },
    beforeSend: function(){
      loading('Loading Target Categories...');
      //pricesArray.length = 0;
      //pricesArraySearch.length = 0;
      
    },
    success: function(data){
      closeLoading();
      console.log("######### TARGET CATEGORIES ###########");
      
      //pricesArray = JSON.parse(data);
      //pricesArraySearch = JSON.parse(data);
      //console.log(itemsArray);
      var poptions = JSON.parse(data);

      console.log(poptions);
      $.each(poptions, function(index, value) {
          $('#target_category').append('<option value="'+value.category_id+'" >'+value.description+'</option>');
      });
      //$('#mode_prices').append();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });

}

function exportTargetSummaryPdf() {
  //loadSummarySales('0','0');

  /*var myDate = new Date(); // From model.
 
    cordova.plugins.DateTimePicker.show({
        mode: "date",
        date: myDate,
        success: function(newDate) {
            // Handle new date.
            console.log(newDate);

            var dd = newDate.getDate();
            var mm = newDate.getMonth()+1; //January is 0!
            var yyyy = newDate.getFullYear();
             if(dd<10){
                    dd='0'+dd
                } 
                if(mm<10){
                    mm='0'+mm
                } 

            var seld = yyyy+'-'+mm+'-'+dd;
            //alert(seld);
            $('#due_date').val(seld);
            
        }
    }); */

  /*var tblContents = '';

  
  var timehder = '<center><h3>Date: '+ tdate +'</h3></center><br>';

  if(salesSummaryArrpdf.length > 0) {
    $.each(salesSummaryArrpdf, function(index, value){
      //console.log(value);
      var items = JSON.parse(value.pitems);
      var itemsStrContent = '';
      var itemsStr = '';

      var pay = JSON.parse(value.payments);
      var payStrContent = '';
      var payStr = '';
      
      if(items.length > 0){
        $.each(items, function(i,v){
          itemsStrContent += '<tr>'+
            '<td>'+ v.item_option +'</td>'+
            '<td>'+ v.quantity +'</td>'+
            '<td>'+ v.price +'</td>'+
          '</tr>';
        });
        itemsStr += '<table class="inner" style="border:none;">'+
        itemsStrContent + 
        '</table>';
      }

      if(pay.length > 0){
        $.each(pay, function(i,v){
          payStrContent += '<tr>'+
            '<td>'+ v.Transtype +'</td>'+
            '<td>'+ v.TransID +'</td>'+
            '<td>'+ v.TransAmount +'</td>'+
          '</tr>';
        });
        payStr += '<table class="inner" style="border:none;">'+
        payStrContent + 
        '</table>';
      }

      tblContents += '<tr>'+
        '<td>POS#'+ str_pad(value.id, 9, "0", "STR_PAD_LEFT") +'</td>'+
        '<td>'+ value.customername +'</td>'+
        '<td>'+ value.ptotal +'</td>'+
        '<td>'+ itemsStr +'</td>'+
        '<td>'+ payStr +'</td>'+
        '<td>'+ value.uname +'</td>'+
      '</tr>';
    });

    var tableHeader = '<style>'+
    'table {'+
      'font-family: arial, sans-serif;'+
      'border-collapse: collapse;'+
      'width: 100%;'+
    '}'+
    'td, th {'+
      'border: 1px solid #dddddd;'+
      'text-align: left;'+
      'padding: 8px;'+
    '}'+
    'tr:nth-child(even) {'+
      'background-color: #dddddd;'+
    '}'+
    '.inner tr:nth-child(even) {'+
      'background-color: #f4f4f4;'+
    '}'+
    '</style>'+
    '<center><h2>POS TRANSACTIONS</h2></center>'+
    timehder +
    '<table>'+
    '<tr>'+
      '<th>No</th>'+
      '<th>Name</th>'+
      '<th>Total</th>'+
      '<th>Items</th>'+
      '<th>Payments</th>'+
      '<th>Served By</th>'+
    '</tr>'+
    tblContents +
    '</table>';

    var options = {
        font: {
            size: 22,
            italic: true,
            align: 'center'
        },
        header: {
            height: '6cm',
            label: {
                text: "",
                font: {
                    bold: true,
                    size: 37,
                    align: 'center'
                }
            }
        },
        footer: {
            height: '4cm',
            label: {
                text: '',
                font: { align: 'center' }
            }
        }
    };
   
    cordova.plugins.printer.print(tableHeader, options);

  } else {
    alert('No Records to export to PDF');
  } */
}

function loadSummarySales(ctid, salesid) {
  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'salessummary',
      category: ctid,
      salespId: salesid,
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id"),
      //id: 41,
      uname: window.localStorage.getItem("username"),
    },
    beforeSend: function(){
      loading('Loading Details...');
      salesSummaryArr.length = 0;
      if(parseInt(ctid) == 0 && parseInt(salesid) == 0){
        salesSummaryArrpdf.length = 0;
      }
      
      //pricesArraySearch.length = 0;
      
    },
    success: function(data){
      closeLoading();
      console.log("######### SALES SUMMARY ###########");
      
      //pricesArray = JSON.parse(data);
      //pricesArraySearch = JSON.parse(data);
      
      salesSummaryArr = JSON.parse(data);
      if(parseInt(ctid) == 0 && parseInt(salesid) == 0){
        salesSummaryArrpdf = JSON.parse(data);
        console.log("****** PDF *******");
        console.log(salesSummaryArrpdf);
      }
      

      var salesSummaryArrObj = salesSummaryArr;

      //console.log(JSON.stringify(salesSummaryArrObj));

      salesSummaryArrObj.sort(function(a,b){
        // Turn your strings into dates, and then subtract them
        // to get a value that is either negative, positive, or zero.
        return new Date(b.tran_date) - new Date(a.tran_date);
      });

      //PHP LIKE DATE

      var w1def = phpdate('Y-m') + '-01';
      var w1 = phpdate('Y-m') + '-07';

      var w2def = phpdate('Y-m') + '-08';
      var w2 = phpdate('Y-m') + '-14';

      var w3def = phpdate('Y-m') + '-15';
      var w3 = phpdate('Y-m') + '-21';

      var w4def = phpdate('Y-m') + '-22';
      var w4 = phpdate('Y-m') + '-31';


     
      var resultWeek1 = salesSummaryArrObj.filter(a => {
        var date = new Date(a.tran_date);
        return (date >= new Date(w1def) && date <= new Date(w1));
      });
      var resultWeek1Val = 0;
      if(resultWeek1.length > 0){
        $.each(resultWeek1, function(i,v) {
            var tweight = parseInt(v.material_weight) * parseInt(v.quantity);
            resultWeek1Val = resultWeek1Val + tweight;
        });
      }
        



      var resultWeek2 = salesSummaryArrObj.filter(a => {
        var date = new Date(a.tran_date);
        return (date >= new Date(w2def) && date <= new Date(w2));
      });
      var resultWeek2Val = 0;
      if(resultWeek2.length > 0){
        $.each(resultWeek2, function(i,v) {
            var tweight = parseInt(v.material_weight) * parseInt(v.quantity);
            resultWeek2Val = resultWeek2Val + tweight;
        });
      }



      var resultWeek3 = salesSummaryArrObj.filter(a => {
        var date = new Date(a.tran_date);
        return (date >= new Date(w3def) && date <= new Date(w3));
      });
      var resultWeek3Val = 0;
      if(resultWeek3.length > 0){
        $.each(resultWeek3, function(i,v) {
            var tweight = parseInt(v.material_weight) * parseInt(v.quantity);
            resultWeek3Val = resultWeek3Val + tweight;
        });
      }



      var resultWeek4 = salesSummaryArrObj.filter(a => {
        var date = new Date(a.tran_date);
        return (date >= new Date(w4def) && date <= new Date(w4));
      });
      var resultWeek4Val = 0;
      if(resultWeek4.length > 0){
        $.each(resultWeek4, function(i,v) {
            var tweight = parseInt(v.material_weight) * parseInt(v.quantity);
            resultWeek4Val = resultWeek4Val + tweight;
        });
      }



      var totalSalest = 0;
      var totalWeight = 0;
      $.each(salesSummaryArr, function(i,v) {
          var tcost = parseInt(v.unit_price) * parseInt(v.quantity);
          var tweight = parseInt(v.material_weight) * parseInt(v.quantity);

          totalSalest = totalSalest + tcost;
          totalWeight = totalWeight + tweight;
      });

      console.log("ctid="+ctid);

      if(ctid == '17') {
        totalWeight = parseInt(totalWeight) / 24;
        //gbltargettt = parseInt(gbltargettt) / 24;
        $('#tsalessummarydiffLBL').text('Difference (Bales)');
        $('#tsummaryLBL').text('Target (Bales)');
        $('#tsalessummaryLBL').text('Sales (Bales)');
      } else if(ctid == '16') {
        totalWeight = parseInt(totalWeight) / 70;
        //gbltargettt = parseInt(gbltargettt) / 70;
        $('#tsalessummarydiffLBL').text('Difference (Bags)');
        $('#tsummaryLBL').text('Target (Bags)');
        $('#tsalessummaryLBL').text('Sales (Bags)');
      } else if(ctid == '27') {
        totalWeight = parseInt(totalWeight) / 50;
        //gbltargettt = parseInt(gbltargettt) / 50;
        $('#tsalessummarydiffLBL').text('Difference (Bales)');
        $('#tsummaryLBL').text('Target (Bales)');
        $('#tsalessummaryLBL').text('Sales (Bales)');
      } else {
        $('#tsalessummarydiffLBL').text('Difference (Kg)');
        $('#tsummaryLBL').text('Target (Kg)');
        $('#tsalessummaryLBL').text('Sales (Kg)');
      }

      //$('#tsummary').text(formatterWeight.format(gbltargettt));

      $('#ttotalsummary').text(formatter.format(totalSalest));
      $('#tsalessummary').text(formatterWeight.format(totalWeight));



      var targetdiff = parseInt(gbltargettt) - parseInt(totalWeight);

      var weeklytarget = parseInt(gbltargettt) / 4;

      if(targetdiff > 0){
        $('#tsalessummarydiff').css("color", "red");
      } else {
        $('#tsalessummarydiff').css("color", "green");
      }

      $('#tsalessummarydiff').text(formatterWeight.format(targetdiff));



      new Chartist.Bar('#chartist-strauss', {
        labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4'],
        series: [
          [weeklytarget, weeklytarget, weeklytarget, weeklytarget],
          [resultWeek1Val, resultWeek2Val, resultWeek3Val, resultWeek4Val]
        ]
      });

      //$(".ct-series-b").css({"stroke": "rgba(250,150,243,.7)"});

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });

}

function categoryChange() {
  var sel = $('#dtarget_category option:selected').val();
  var salesp = $('#dtarget_salesperson option:selected').val();


  if(salesp == undefined){
    salesp = window.localStorage.getItem("id");
  }
 
  
  //loadSummarySales(sel,salesp);
  loadSummaryTargetsThree('0',sel,salesp);
}

function salesPersonChange() {
  var sel = $('#dtarget_category option:selected').val();
  var salesp = $('#dtarget_salesperson option:selected').val();

  if(salesp == undefined){
    salesp = window.localStorage.getItem("id");
  }
  
  loadSummaryTargetsThree('0',sel,salesp);
  
}

function loadSummaryTargetSalesPerson() {
  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'loadUsers',
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      //loading('Loading Salesperson...');
      //pricesArray.length = 0;
      //pricesArraySearch.length = 0;
      
    },
    success: function(data){
      //closeLoading();
      console.log("######### TARGET SALESPERSON ###########");
      
      //pricesArray = JSON.parse(data);
      //pricesArraySearch = JSON.parse(data);
      console.log(data);
      var poptions = JSON.parse(data);

      var myid = window.localStorage.getItem("id");

      console.log(poptions);
      $('#dtarget_salesperson').append('<option value="0" >All Salesperson</option>');
      $.each(poptions, function(index, value) {
          if(myid == value.userid){
            $('#dtarget_salesperson').append('<option value="'+value.userid+'" selected>'+value.uname+'</option>');
          } else {
            $('#dtarget_salesperson').append('<option value="'+value.userid+'" >'+value.uname+'</option>');
          }
      });
      //$('#mode_prices').append();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });

}

function loadSummaryTargetCategories() {
  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'targetCategories',
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id"),
      uname: window.localStorage.getItem("username"),
    },
    beforeSend: function(){
      loading('Loading Target Categories...');
      //pricesArray.length = 0;
      //pricesArraySearch.length = 0;
      
    },
    success: function(data){
      closeLoading();
      console.log("######### TARGET CATEGORIES ###########");
      
      //pricesArray = JSON.parse(data);
      //pricesArraySearch = JSON.parse(data);
      //console.log(itemsArray);
      var poptions = JSON.parse(data);

      console.log(poptions);
      $('#dtarget_category').append('<option value="0" >All Categories</option>');
      $.each(poptions, function(index, value) {
          $('#dtarget_category').append('<option value="'+value.category_id+'" >'+value.description+'</option>');
      });
      //$('#mode_prices').append();
      loadSummaryTargetSalesPerson();

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });

}

function openTargetSalesPerson(){
  $('#salestargetmenu').css("visibility","hidden");
}


function manageTargetsModule() {

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'checksalesmanager',
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id"),
      uname: window.localStorage.getItem("username"),
    },
    beforeSend: function(){
      
      
    },
    success: function(data){
      
      console.log("######### SERVER USER ROLE CHECK ###########");
     
      console.log(data);
      if(parseInt(data) == 1){
        openPage('target');
      } else {
        openPage('target',{},openTargetSalesPerson);
      }
      

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });

}

function openTargetsModule() {

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: 'checksalesmanager',
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id"),
      uname: window.localStorage.getItem("username"),
    },
    beforeSend: function(){
      
      
    },
    success: function(data){
      
      console.log("######### SERVER USER ROLE CHECK ###########");
     
      console.log(data);
      if(parseInt(data) == 1){
        openPage('target-dashboard-admin',{},openTargetDashboard);
      } else {
        openPage('target-dashboard',{},openTargetDashboard);
      }
      

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR ITEMS ###########");
      //console.log(err);
    },
    complete: function(){
      
      //console.log();
      //console.log("######### COMPLETE ITEMS ###########");
      //console.log(itemsArray);
      
    }
  });

}

function openTargetDashboard() {
  //loadSummaryTargets('0');
  var sel = $('#dtarget_category option:selected').val();
  var salesp = $('#dtarget_salesperson option:selected').val();

  
  if(sel == undefined){
    sel = "0";
  }

  if(salesp == undefined){
    salesp = window.localStorage.getItem("id");
  }
  console.log("******* DEF");
  console.log(salesp);
  console.log(sel);
  
  //loadSummarySales(sel,salesp);
  loadSummaryTargetsThree('0',sel,salesp)
  
}

function loadSummaryTargetsThree(typ,ctid,pid) {
  var tp = "loadTargets";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      type: typ,
      category: ctid,
      pid: pid,
      uname: window.localStorage.getItem("username"),
      uid: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Loading targets...');
      //targetArray.length = 0;
      gbltargettt = "0";
      
      
    },
    success: function(data){
      closeLoading();
      console.log("######### SUMMARY TARGETS ###########");
      
      //alert(data);
      var strget = JSON.parse(data);

      console.log(strget);

      var expensesStr = "0";

      $.each(strget, function(i,v) {

        gbltargettt = parseInt(gbltargettt) + parseInt(v.target_qty);

        if(i == 0){
          expensesStr = v.expences;
        }
        
        

      });

      $('#expences').text(formatter.format(expensesStr));

      if(ctid == '17') {
        gbltargettt = parseInt(gbltargettt) / 24;
        $('#tsalessummarydiffLBL').text('Variance (Bales)');
        $('#tsummaryLBL').text('Target (Bales)');
        $('#tsalessummaryLBL').text('Sales (Bales)');
      } else if(ctid == '16') {
        gbltargettt = parseInt(gbltargettt) / 70;
        $('#tsalessummarydiffLBL').text('Variance (Bags)');
        $('#tsummaryLBL').text('Target (Bags)');
        $('#tsalessummaryLBL').text('Sales (Bags)');
      } else if(ctid == '27') {
        gbltargettt = parseInt(gbltargettt) / 50;
        $('#tsalessummarydiffLBL').text('Variance (Bales)');
        $('#tsummaryLBL').text('Target (Bales)');
        $('#tsalessummaryLBL').text('Sales (Bales)');
      } else {
        $('#tsalessummarydiffLBL').text('Variance (Kg)');
        $('#tsummaryLBL').text('Target (Kg)');
        $('#tsalessummaryLBL').text('Sales (Kg)');
      }

      //gbltargettt = strget[0].target_qty;


      $("#tsummary").text(formatterWeight.format(gbltargettt));

      loadSummarySales(ctid,pid);

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR USERS ###########");
      //console.log(err);
    },
    complete: function(){
      //console.log();
      //console.log("######### COMPLETE USERS ###########");
      //console.log(usersArray);
      
    }
  });
}


function loadSummaryTargetsTwo(typ,ctid,pid) {
  var tp = "loadTargets";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      type: typ,
      category: ctid,
      pid: pid,
      uname: window.localStorage.getItem("username"),
      uid: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Loading targets...');
      //targetArray.length = 0;
      gbltargettt = "0";
      
      
    },
    success: function(data){
      closeLoading();
      console.log("######### SUMMARY TARGETS ###########");
      
      //alert(data);
      var strget = JSON.parse(data);

      console.log(strget);

      $.each(strget, function(i,v) {

        gbltargettt = parseInt(gbltargettt) + parseInt(v.target_qty);

      });

      if(ctid == '17') {
        gbltargettt = parseInt(gbltargettt) / 24;
        $('#tsalessummarydiffLBL').text('Variance (Bales)');
        $('#tsummaryLBL').text('Target (Bales)');
        $('#tsalessummaryLBL').text('Sales (Bales)');
      } else if(ctid == '16') {
        gbltargettt = parseInt(gbltargettt) / 70;
        $('#tsalessummarydiffLBL').text('Variance (Bags)');
        $('#tsummaryLBL').text('Target (Bags)');
        $('#tsalessummaryLBL').text('Sales (Bags)');
      } else if(ctid == '27') {
        gbltargettt = parseInt(gbltargettt) / 50;
        $('#tsalessummarydiffLBL').text('Variance (Bales)');
        $('#tsummaryLBL').text('Target (Bales)');
        $('#tsalessummaryLBL').text('Sales (Bales)');
      } else {
        $('#tsalessummarydiffLBL').text('Variance (Kg)');
        $('#tsummaryLBL').text('Target (Kg)');
        $('#tsalessummaryLBL').text('Sales (Kg)');
      }

      //gbltargettt = strget[0].target_qty;


      $("#tsummary").text(formatterWeight.format(gbltargettt));

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR USERS ###########");
      //console.log(err);
    },
    complete: function(){
      //console.log();
      //console.log("######### COMPLETE USERS ###########");
      //console.log(usersArray);
      
    }
  });
}


function loadSummaryTargets(typ) {
  var tp = "loadTargets";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      type: typ,
      uname: window.localStorage.getItem("username"),
      uid: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Loading targets...');
      //targetArray.length = 0;
      gbltargettt = "0";
      
      
    },
    success: function(data){
      closeLoading();
      console.log("######### SUMMARY TARGETS ###########");
      
      //alert(data);
      var strget = JSON.parse(data);

      console.log(strget);

      $.each(strget, function(i,v) {

        gbltargettt = parseInt(gbltargettt) + parseInt(v.target_qty);

      });

      //gbltargettt = strget[0].target_qty;


      $("#tsummary").text(formatterWeight.format(gbltargettt));

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR USERS ###########");
      //console.log(err);
    },
    complete: function(){
      //console.log();
      //console.log("######### COMPLETE USERS ###########");
      //console.log(usersArray);
      
    }
  });
}

//targetArray
function loadTargets(typ) {
  var tp = "loadTargets";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      type: typ,
      uname: window.localStorage.getItem("username"),
      uid: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Loading targets...');
      targetArray.length = 0;
      
      
    },
    success: function(data){
      closeLoading();
      console.log("######### SUCCESS TARGETS ###########");
      console.log(data);
      //alert(data);
      targetArray = JSON.parse(data);

    },
    error: function(err){
      //console.log();
      //console.log("######### ERROR USERS ###########");
      //console.log(err);
    },
    complete: function(){
      //console.log();
      //console.log("######### COMPLETE USERS ###########");
      //console.log(usersArray);
      
    }
  });
}


function loadSalesExpenses(){
  var tp = "loadsalesexpenses";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Loading Sales Expenses...');
      salesExpensesArray.length = 0;
      
      
    },
    success: function(data){
     //console.log("################## SALES EXPENSES ##################");
     //console.log(data)
     closeLoading();
      salesExpensesArray = JSON.parse(data);

    },
    error: function(err){

    },
    complete: function(){
      
      
    }
  });

}

function loadCustomerPayments(){
  var tp = "loadcustomerpayment";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Loading Customer Payments...');
      customerPaymentArray.length = 0;
      
      
    },
    success: function(data){
     console.log("################## CUSTOMER PAYMENTS ##################");
     console.log(data)
     closeLoading();
      customerPaymentArray = JSON.parse(data);

    },
    error: function(err){

    },
    complete: function(){
      
    }
  });

}

function loadCustomerPayments2(){
  var tp = "loadcustomerpayment";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      id: window.localStorage.getItem("id"),
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      customerPaymentArray.length = 0;
      
      
    },
    success: function(data){
     //console.log("################## CUSTOMER PAYMENTS ##################");
     //console.log(data)
     
      customerPaymentArray = JSON.parse(data);

    },
    error: function(err){

    },
    complete: function(){
      
    }
  });

}

function loadSalesUsers(){
  var tp = "loadSalesUsers";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Loading sales users...');
      salesUsersArray.length = 0;
      
      
    },
    success: function(data){
      closeLoading();
      console.log("###### SALES USERS #######");
      console.log(data);
     
      salesUsersArray = JSON.parse(data);

    },
    error: function(err){

    },
    complete: function(){

      
    }
  });

}

function loadUsers(){
  var tp = "loadUsers";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Loading users...');
      usersArray.length = 0;
      usersArraySearch.length = 0;
      usersArrayUnChanged.length = 0;
      
      
    },
    success: function(data){
     
      usersArray = JSON.parse(data);
      usersArraySearch = JSON.parse(data);
      usersArrayUnChanged = JSON.parse(data);

    },
    error: function(err){

    },
    complete: function(){

      closeLoading();
    }
  });

}

MobileUI.getManuType = function (sval) {

  var res = "";

  if(sval == '0'){
    res = 'Assemble';
  } else if(sval == '1'){
    res = 'Unassemble';
  } else {
    res = 'Advanced'
  }

  return res;
}

MobileUI.escapeCustomerVal = function (sval) {

  //preg_replace( "/&#039;/", "&#92;&#039;", $rowc['name']);
  
  sval = sval.replace(/&#039;/g, '&#92;&#039;');
  return sval;
}


MobileUI.escapePosVals = function (sval) {
  return encodeURIComponent(sval);
}

MobileUI.setTaskStatus = function (sval) {
  var sts = parseInt(sval);
  var retval = "";
  if(sts == 0){
    retval = "grey";
  } else {
    retval = "green";
  }
  return retval;
}

MobileUI.setTaskStatusText = function (sval) {
  var sts = parseInt(sval);
  var retval = "";
  if(sts == 0){
    retval = "Complete";
  } else {
    retval = "New";
  }
  return retval;
}

MobileUI.setCusVisitLbl = function (sval) {
  
  var retval = "";
  if(sval == ""){
    retval = "All";
  } else {
    retval = sval;
  }
  return retval;
}

MobileUI.taskStatus = function (sval) {
  var sts = parseInt(sval);
  var retval = "";
  if(sts == 0){
    retval = "Complete";
  } else {
    retval = "Pending";
  }
  return retval;
}

MobileUI.paymentMode = function (sval) {
  var sts = parseInt(sval);
  var retval = "";
  if(sts == 1){
    retval = "Mpesa";
  } else if(sts == 2){
    retval = "Cheque";
  } else if(sts == 3){
    retval = "Cash";
  } else {
    retval = "Direct Deposit";
  }
  return retval;
}

MobileUI.calcTotalSales = function (q,p) {
  var qty = parseInt(q);
  var price = parseInt(p);
  var retval = qty * price;
  
  //return formatMoney(retval);
  return formatter.format(retval);
}

MobileUI.calcTotalWeight = function (q,w) {
  var qty = parseInt(q);
  var weight = parseInt(w);
  var retval = qty * weight;
  
  //return formatMoney(retval,0);
  return formatterWeight.format(retval);
}

MobileUI.setTaskStatusButton = function (sval,usrval) {
  var sts = parseInt(sval);
  var usr = parseInt(usrval);
  var loginUsrId = parseInt(window.localStorage.getItem("id"));
  var retval = "";
  if(sts == 0){
    retval = "display:none;";
  } else if(sts == 1 && usr != loginUsrId){
    retval = "display:none;";
  } else {
    retval = "";
  }
  return retval;
}

MobileUI.checkPairedStatus = function (sts) {
  
  var retval = "";
  if(parseInt(sts) == 1){
    retval = "display:none;";
  } else {
    retval = "";
  }
  return retval;
}

MobileUI.checkUnPairedStatus = function (sts) {
  
  var retval = "";
  if(parseInt(sts) == 0){
    retval = "display:none;";
  } else {
    retval = "";
  }
  return retval;
}

MobileUI.setPosTransNumber = function (sval) {
  var sposRef = str_pad(sval, 9, "0", "STR_PAD_LEFT");
  return 'POS#'+sposRef;
}

MobileUI.formatMoney = function (sval) {
  return formatMoney(sval);
}

MobileUI.timeSince = function (sval) {
  jQuery.timeago.settings.allowFuture = true;
  jQuery.timeago.settings.allowPast = false;
  return jQuery.timeago(sval);
}

MobileUI.bookingPayments = function (sval) {
  var ppAr = JSON.parse(sval);
  var retval = "";
  $.each(ppAr,function(i, v){
    retval += '&nbsp;&nbsp;-&nbsp;&nbsp;' + v.Transtype + '&nbsp;&nbsp;&nbsp;' + v.TransAmount + '<br>';
  });
  
  return retval;
}

MobileUI.bookingItems = function (sval) {
  var ppAr = JSON.parse(sval);
  var retval = "";
  $.each(ppAr,function(i, v){
    retval += '&nbsp;&nbsp;-&nbsp;&nbsp;' + v.item_option + '<br>';
  });
  
  return retval;
}

MobileUI.itemSetter = function (sval) {
  var retval = "";
  if(sval == ""){
    retval = "All Items";
  } else {
    retval = sval;
  }
  
  return retval;
}

MobileUI.hideEmptyVal = function (sval) {
  var retval = "";
  if(sval == ""){
    retval = "display:none;";
  } else {
    retval = "";
  }
  
  return retval;
}

MobileUI.hideTaxInput = function () {
  var selttv = window.localStorage.getItem("setSiteTax");
  if(selttv == "0"){
    retval = "display:none;";
  } else {
    retval = "";
  }
  
  return retval;
}

MobileUI.hideDeleteOnPrevPayments = function (ps) {
  var retval = "";
    
  if(ps == "1"){
    retval = "display:none;";
  } else {
    retval = "";
  }
  

  console.log(retval);
  
  return retval;
}

//timeSince
//

function resetDirectSalesFrm() {
  MobileUI.clearForm('frmBooking');
  $("#mode_prices").val("1");
  $("#quantity").val("1");
  $("#discount").val("0");
  $("#tax").val("0");

  if(window.localStorage.getItem("setSiteTax") == "0"){
    $('.taxrow').hide();
  }

  $("#fsalesp").css("pointer-events","none");
  

  bookingUpdate = "0";
  if(paymentTypesUsed.length > 0){
    $.each(paymentTypesUsed, function(index, value){
      if(value != undefined) {
        value['p'] = "0";
      }
      
    });
  }
}

function resetBookingFrm() {
  MobileUI.clearForm('frmBooking');
  $("#mode_prices").val("1");
  $("#quantity").val("1");
  $("#discount").val("0");
  $("#tax").val("0");

  if(window.localStorage.getItem("setSiteTax") == "0"){
    $('.taxrow').hide();
  }
  

  bookingUpdate = "0";
  if(paymentTypesUsed.length > 0){
    $.each(paymentTypesUsed, function(index, value){
      if(value != undefined) {
        value['p'] = "0";
      }
      
    });
  }
}

function bookingDetailsc(param){

  posBalance = 0;

  bookingUpdate =  "1";
  
  paymentTypesUsed = JSON.parse(decodeURIComponent(param.payments));
  bookingItems = JSON.parse(decodeURIComponent(param.pitems));

  //console.log(paymentTypesUsed);

  console.log();

  posTotal = parseInt(param.ptotal);
  bookingPayId = param.id;

  if(paymentTypesUsed.length > 0){
    var ttpaid = 0;
    $.each(paymentTypesUsed, function(index, value){
      if(value != undefined) {
        value['p'] = "1";
        ttpaid = ttpaid + parseInt(value.TransAmount);
      }
      
    });

    //console.log(paymentTypesUsed);

    posBalance = parseInt(ttpaid) - parseInt(posTotal);
  } else {
    posBalance = parseInt(posBalance) - parseInt(posTotal);
  }

  $("#scannerBalance").val(posBalance);

  //$('#postotal').text(param.ptotal);
  window.localStorage.setItem("userpay",param.customername);
  $('#scannerTotal').val(param.ptotal);
  $('.customer_option').val(param.customername);
  $('#customer_option_id').val(param.customerid);
  $('#booking_id').val(param.id);
  $('#booking_type').val(param.ptype);
  $('#btnBookingSave').hide();
  $('#btnBookingUpdate').hide(); 

  console.log(param.id);
  if(param.ptype == "CASH" || param.ptype == "MPESA"){
    $('#btnPartialPay').hide();
    $('#btnPartialPayUpdate').show();
  } 
  

  //phpdate('M j, Y, g:ia')
  //reprintPOSReceipt();


}

function stockTakeDetailsc(param){

  console.log(param);
  stockTakeReportItems.length = 0;
  //stockTakeReportItems
  //stockTakeReportArray

  $.each(stockTakeReportArray, function(index, value){
      if(value.stock_take_id == param.id){
        stockTakeReportItems.push(value);
      }
    
     
  });
  

  $('#cname').text("Stock Take: "+param.id);
  //$('#stockTakeDate').text(phpdate('M j, Y, g:i a'));
  $('#stockTakeDate').text(param.sdate);

}

function posTransactionDetailsc(param){

  console.log(param);
  //alert(param.payments);
  posTransactionsArraySearchPayments.length = 0;
  posTransactionsArraySearchItems.length = 0;
  for (element in posTransactionsObjectString) {
    delete posTransactionsObjectString[element];
  }
  

  posTransactionsObjectString = param;
  posTransactionsArraySearchPayments = JSON.parse(decodeURIComponent(param.payments));
  posTransactionsArraySearchItems = JSON.parse(decodeURIComponent(param.pitems));

  var posRef = str_pad(param.id, 9, "0", "STR_PAD_LEFT");

  $('#cname').text(param.customername);
  $('#servedby').text(param.uname);
  $('#posRef').text('POS#'+posRef);
  $('#cdtimepos').text(phpdate('M j, Y, g:i a'));
  

  //phpdate('M j, Y, g:ia')
  //reprintPOSReceipt();


}

function addCustomer() {
  var cname = $('#cname').val();
  var csname = $('#csname').val();
  var telephone = $('#telephone').val();
  var address = $('#address').val();
  var tp = "customer";
  if(cname == "" || telephone == "" || csname == "" || address == "") {
        alert("Please fill (*) mandatory fields");
  } else {
      var postvals = "cname"+cname+"&telephone="+telephone+"&csname="+csname+"&id="+window.localStorage.getItem("id");
        
        loading('Please wait...');
        $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
        {
            tp: tp,
            cname: cname,
            telephone: telephone,
            csname: csname,
            address: address,
            cp: window.localStorage.getItem("cp"),
            id: window.localStorage.getItem("id")
        },
        function(data, status){
            closeLoading();
            if(data == "false"){
                alert("Unable to add Customer, Error occured");
            } else {
               
                alert("Registration Successful");
                

            }
        });
  }
}

function getReceiptHeaderVals(){
  //loading('Please wait...');
  $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
  {
      tp: "receipt",
      cp: window.localStorage.getItem("cp")
  },
  function(data, status){
      //closeLoading();
      console.log("######### RECEIPT HEADER FETCH RESPONSE ##########");
      console.log(data);
      console.log(status);
      if(data == "false"){
          alert("Unable to get Receipt Headers");
         //alert("https://titan.jamexexpress.com/Login/appauthonticate: " + data);
      } else {
          var objreceipthitems = JSON.parse(data);
          

          window.localStorage.setItem("coy_name", objreceipthitems[0]);
          window.localStorage.setItem("domicile", objreceipthitems[1]);
          window.localStorage.setItem("remail", objreceipthitems[2]);
          window.localStorage.setItem("rphone", objreceipthitems[3]);
          //window.menu.enableSwiper('myMenu')
          //$("#usr").text(window.localStorage.getItem("username"));
          //$("#org").text(window.localStorage.getItem("orgname"));

      }

      //alert("Data: " + data + "\nStatus: " + status);
  });
}

function refreshAddOrder(){
  loadCustomers();
  loadPriceSelector();
  loadItems();
}

function addOrder() {
  var customer_option = $(".customer_option").val();
  var customer_option_id = $("#customer_option_id").val();
  var branch_option = $("#branch_option").val();
  var branch_option_id = $("#branch_option_id").val();
  /*var item_option = $("#item_option").val();
  var item_option_id = $("#item_option_id").val();
  var price = $("#price").val();
  var quantity = $("#quantity").val();*/
  var due_date = $("#due_date").val();

  var tp = "addorder";
  if(customer_option == "" || branch_option == "") {
        alert("Please fill (*) mandatory fields");
  } else {
        console.log("CUS: "+ customer_option + " # " + customer_option_id);
        console.log("BRA: "+ branch_option + " # " + branch_option_id);
        console.log("DUE: "+ due_date);
        console.log("Total: "+ orderTotal);
        console.log(orderItems);

        
        loading('Please wait...');
        $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
        {
            tp: tp,
            customer_option: customer_option,
            customer_option_id: customer_option_id,
            branch_option: branch_option,
            branch_option_id: branch_option_id,
            items: JSON.stringify(orderItems),
            orderTotal: orderTotal,
            due_date: due_date,
            cp: window.localStorage.getItem("cp"),
            id: window.localStorage.getItem("id")
        },
        function(data, status){
          console.log(data);
          posReceiptRef = data.trim();
          var receiptRef = posReceiptRef.split('#');
          //var receiptNumberPosAct = 'POS#'+str_pad(receiptRef[2], 9, "0", "STR_PAD_LEFT");
            closeLoading();
            if(data == "false"){
                alert("Unable to add Order, Error occured");
            } else {
               
                alert("Order Entry Successful....");

                $.each(orderItems, function(index, value){
                  //console.log(value[index]['item_option'] + ' --------- ' + value[index]['price']);

                  oitm += value.item_option +' - '+value.item_option_id+ '   ' + value.quantity + 'x' + value.price + '   '+ parseInt(value.quantity) * parseInt(value.price) + '\n\n';
                  
                   
                });

                posReceiptRef = "";
                oitm  = "";
                MobileUI.clearForm('frmAddOrder');
                $("#quantity").val("1");
                orderItems.length = 0;
                orderTotal = 0;
                setCurrentDueDate();

                /*

                try{

                  BTPrinterObj.list(function(res){

                    var pname = res[0];
                    var paddress = res[1];
                    var ptype = res[2];
                    var htp = window.localStorage.getItem("cp");
                    var ttprint = "";
                    var ptype = "";

                    //alert("AO:"+pname);
                    //Connect to printer 
                    BTPrinterObj.connect(function(data){

                      //1ST PRINT
                      //Receipt Title Header
                      BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                      BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                      BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                      BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                      //Receipt Header
                      BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'ORDER ENTRY','1','1');
                      BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
                      //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
                      BTPrinterObj.printTitle(null,null,'Ref: '+ receiptRef,'1','1');


                      BTPrinterObj.printText(function(data){
                        //Clear all pos data
                        
                        
                      },null, justify_left +
                      '\n\n' +
                      'Customer: ' + customer_option +
                      '\n\n' +
                      'Branch: ' + branch_option +
                      '\n\n' +
                      oitm +
                      '\n\n' +
                      'Due Date  :  ' + due_date +
                      '\n\n\n' +
                      'Served By: ' + window.localStorage.getItem("username") +
                     '\n\n\n' +
                      'THANKYOU' +
                     '\n\n\n\n\n\n' +
                     justify_left,'1','0');


                      //2ND PRINT
                      //Receipt Title Header
                      BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                      BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                      BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                      BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                      //Receipt Header
                      BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'ORDER ENTRY','1','1');
                      BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
                      //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
                      BTPrinterObj.printTitle(null,null,'Ref: '+ receiptRef,'1','1');


                      BTPrinterObj.printText(function(data){
                        //Clear all pos data
                        //
                        posReceiptRef = "";
                        oitm  = "";
                        MobileUI.clearForm('frmAddOrder');
                        $("#quantity").val("1");
                        orderItems.length = 0;
                        orderTotal = 0;
                        setCurrentDueDate();
                        
                      },null, justify_left +
                      '\n\n' +
                      'Customer: ' + customer_option +
                      '\n\n' +
                      'Branch: ' + branch_option +
                      '\n\n' +
                      oitm +
                      '\n\n' +
                      'Due Date  :  ' + due_date +
                      '\n\n\n' +
                      'Served By: ' + window.localStorage.getItem("username") +
                     '\n\n\n' +
                      'THANKYOU' +
                     '\n\n\n\n\n\n' +
                     justify_left,'1','0');

                    },function(err){ 

                      //Connection to printer error 
                      //alert("Printer conn error");   
                      posReceiptRef = "";
                      oitm  = "";
                      MobileUI.clearForm('frmAddOrder');
                      $("#quantity").val("1");
                      orderItems.length = 0;
                      orderTotal = 0;
                      setCurrentDueDate();


                    }, pname);
                  },function(err){

                      //Bluetooth error
                      //alert("Bluetooth conn error");   
                      posReceiptRef = "";
                      oitm  = "";
                      MobileUI.clearForm('frmAddOrder');
                      $("#quantity").val("1");
                      orderItems.length = 0;
                      orderTotal = 0;
                      setCurrentDueDate();
                  });
                
                } catch(err) {
                  alert("NO PRINTER!!");
                } */
                

            }
        });
  }

    //alert($duedate);
}

function getCompanySites(param) {
  loading('Please wait...');
  $.post("https://digerp.com/config_site.php",
  {
      tp: 'getsites',
      cmp: param.st
  },
  function(data, status){
      closeLoading();

      var objcsites = JSON.parse(data);
      var selsbr = "";
      $.each(objcsites, function(i,v){
        

        selsbr += '<option value="'+v.company_prefix+'">'+v.branch+'</option>';

        
      });

      $("#company").append(selsbr);
      window.localStorage.setItem("setSiteBraches", selsbr);
      
      var ssname = window.localStorage.getItem("setSiteName");
      console.log(ssname.toUpperCase());
      $("#exitsitebtn").html("EXIT "+ ssname.toUpperCase());
      //alert("Data: " + data + "\nStatus: " + status);
  });
}


function setCompany(){
    var setcomp = $("#setcomp").val();
    var type = "setcomp";
    if(setcomp == ""){
        alert("Please enter site name");
    } else {
        loading('Please wait...');
        $.post("https://digerp.com/config_site.php",
        {
            tp: type,
            cmp: setcomp
        },
        function(data, status){
            closeLoading();
            console.log(data);
            var objsitedd = JSON.parse(data);
            if(objsitedd.length == 0){
                alert("Site does not exist!!!");
               //alert("https://titan.jamexexpress.com/Login/appauthonticate: " + data);
            } else {
                
                window.localStorage.setItem("setSite", 1);
                window.localStorage.setItem("setSiteName", setcomp);
                window.localStorage.setItem("setSiteUrl", objsitedd[0].company_url);
                window.localStorage.setItem("setSiteTax", objsitedd[0].tax);
                
                openPage('login', {st: setcomp}, getCompanySites);

            }

            //alert("Data: " + data + "\nStatus: " + status);
        });
    }
}



function login(){
    $username = $("#username").val();
    $password = $("#password").val();
    $company = $("#company").val();
    $type = "login";
    if($username == "" || $password == "" || $company == ""){
        alert("Please fill all fields");
    } else {
        loading('Please wait...');
        $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
        {
            tp: $type,
            un: $username,
            up: $password,
            cp: $company
        },
        function(data, status){
            closeLoading();
            console.log("######### LOGIN RESPONSE ##########");
            console.log(data);
            //console.log(status);
            if(data == "false"){
                alert("Incorrect username or password");
               //alert("https://titan.jamexexpress.com/Login/appauthonticate: " + data);
            } else {
                var objuser = JSON.parse(data);

// window.localStorage.setItem("setSite", 1);
                window.localStorage.setItem("loggedIn", 1);
                window.localStorage.setItem("username", objuser.user_id);
                window.localStorage.setItem("realname", objuser.real_name);
                window.localStorage.setItem("id", objuser.id);
                window.localStorage.setItem("roleid", objuser.role_id);
                window.localStorage.setItem("phone", objuser.phone);
                window.localStorage.setItem("email", objuser.email);
                window.localStorage.setItem("cp", $company);

                $.post(window.localStorage.getItem("setSiteUrl") + "process.php",{
                    tp: 'roles',
                    roleid: objuser.role_id,
                    id: objuser.id,
                    cp: $company
                },
                function(data, status){

                  window.localStorage.setItem("roles", data);

                });
                //window.menu.enableSwiper('myMenu')
                //window.localStorage.removeItem("userpay");
                //$("#usr").text(window.localStorage.getItem("username"));
                //$("#org").text(window.localStorage.getItem("orgname"));
                getSystemUsers();
                loadCustomers();
                //openPage('index');
                backPage();
                backPage();

            }

            //alert("Data: " + data + "\nStatus: " + status);
        });
    }
}

function logout(){
    window.localStorage.removeItem("loggedIn");
    window.localStorage.removeItem("username");
    window.localStorage.removeItem("realname");
    window.localStorage.removeItem("id");
    window.localStorage.removeItem("roleid");
    window.localStorage.removeItem("phone");
    window.localStorage.removeItem("email");
    window.localStorage.removeItem("coy_name");
    window.localStorage.removeItem("domicile");
    window.localStorage.removeItem("remail");
    window.localStorage.removeItem("rphone");

    window.localStorage.setItem("loggedIn", 0);

    openPage('login',{}, setDefaultBranches);
}

function sitelogout(){
  window.localStorage.removeItem("setSite");
  window.localStorage.removeItem("setSiteName");
  window.localStorage.removeItem("setSiteBraches");
  window.localStorage.removeItem("setSiteUrl"); 
  window.localStorage.removeItem("setSiteTax");

  if(window.localStorage.getItem("loggedIn") == 1) {
    window.localStorage.removeItem("loggedIn");
    window.localStorage.removeItem("username");
    window.localStorage.removeItem("realname");
    window.localStorage.removeItem("id");
    window.localStorage.removeItem("roleid");
    window.localStorage.removeItem("phone");
    window.localStorage.removeItem("email");
    window.localStorage.removeItem("coy_name");
    window.localStorage.removeItem("domicile");
    window.localStorage.removeItem("remail");
    window.localStorage.removeItem("rphone");

    window.localStorage.setItem("loggedIn", 0);
    window.localStorage.setItem("setSite", 0);
  }

  openPage('company');

}

function parseCusData(cusd) {

  //alert(cusd.cus); customersArray
  //console.log(customersArray);
  //console.log(birds);
  // local
    $('#autocomplete').tinyAutocomplete({
      data: birds,
      showNoResults: true,
      lastItemTemplate: '<li class="autocomplete-item autocomplete-item-last">Show all results for "{{title}}"</li>',
      onSelect: function(el, val) {
        if(val == null) {
          $('.results').html('All results for "' + $(this).val() + '" would go here');
        }
        else {
          $(this).val( val.title );
          //$('.results').html('<h1>' + val.title + '</h1>');
        }
      }
    });

}

function paymodechange() {
    var siobj = MobileUI.objectByForm('frmCustomerPayment');
    var mode_payment = siobj.mode_payment;

    console.log("## PAY MODE SELECTED ##");
    console.log(mode_payment);

    if(parseInt(mode_payment) == 1){
      //$('#amount').val(1000);
      //$('#reference').val("OATYEHJNKSB");
      $('#amount').prop('readonly', true);
      $('#reference').prop('readonly', true);
      
      openPage('mpesaPaymentSearchCustomerPayment');
    } else {

      $('#amount').val("");
      $('#reference').val("");
      $('#amount').prop('readonly', false);
      $('#reference').prop('readonly', false);
    }
    
}

function salesInvoicePay() {
    var siobj = MobileUI.objectByForm('frmBooking');
    var customer_option = siobj.customer_option;
    var customer_option_id = siobj.customer_option_id;

    console.log(customer_option);
    console.log(customer_option_id);

    //var cobjj = {customer_option: customer_option, customer_option_id: customer_option_id};
    //MobileUI.formByObject('frmCustomerPayment', cobjj);

    $(".customer_option").val(customer_option);
    $("#customer_option_id").val(customer_option_id);
}

function postTotalVal(param) {

  var tt = param.tt;
  var title = param.title;
  var defptype = "";
  if(title == "Eazzypay"){
    defptype = "EAZZYPAY";
  } else if(title == "Visa"){
    defptype = "VISA";
  } else if(title == "Cheque"){
    defptype = "CHEQUE";
  } else {
    defptype = "CASH";
  }

  $("#ptype").text(title);
  $("#paytype").val(defptype);
  $("#cptotal").text(posTotal);

  var mpaid = parseInt($("#cppaid").text());
  var mbalnce = parseInt($("#cppaid").text()) - parseInt($("#cptotal").text());
  if(mpaid <= parseInt($("#cptotal").text())){
    $("#cpbalance").text(mbalnce);
  } else {
    $("#cpbalance").text(mbalnce);
  }
  //$("#f-salesp").val(window.localStorage.getItem("id"));
  
}

function postTotalVal2(param){

  var tt = param.tt;
  var title = param.title;
  var defptype = "";
  if(title == "Eazzypay"){
    defptype = "EAZZYPAY";
  } else if(title == "Visa"){
    defptype = "VISA";
  } else if(title == "Cheque"){
    defptype = "CHEQUE";
  } else {
    defptype = "CASH";
  }

  $("#ptype").text(title);
  $("#paytype").val(defptype);
  $("#cptotal").text(posTotal);

  $("#cashcustomername").val(window.localStorage.getItem("userpay"));
  var mpaid = parseInt($("#cppaid").text());
  var mbalnce = parseInt($("#cppaid").text()) - parseInt($("#cptotal").text());
  if(mpaid <= parseInt($("#cptotal").text())){
    $("#cpbalance").text(mbalnce);
  } else {
    $("#cpbalance").text(mbalnce);
  }
  //$("#f-salesp").val(window.localStorage.getItem("id"));
  
}

/*function loadBkCreditNotes(){
    var crdobj = MobileUI.objectByForm('formCreditNote');
    var customer_option = crdobj.customer_option;
    var customer_option_id = crdobj.customer_option_id;
    
    loading('Please wait...');
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "loadCreditNotes",
        customer_option: customer_option,
        customer_option_id: customer_option_id,
        uname: window.localStorage.getItem("username"),
        id: window.localStorage.getItem("id"),
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      
      closeLoading();
      var crddata = JSON.parse(data);
      if(crddata.length == 0) {
        alert("No Credit Note Found.");
      } else {

        creditNoteArray = crddata;
        creditNoteArraySearch = crddata;

      }
          
    });
} */

function bookCreditNoteVal(){
  //var orderObjb = MobileUI.objectByForm(frm);
  var cname = window.localStorage.getItem("userpay");
  var cid = window.localStorage.getItem("userpayid");
  var cobjj = {customer_option: cname, customer_option_id: cid};
  MobileUI.formByObject('formCreditNote', cobjj);
  loadCreditNotes();
  //CCC
}

function getSystemUsers(){
  var tp = "getUsers";

  $.ajax({
    type: "POST",
    url: window.localStorage.getItem("setSiteUrl") + "process.php",
    data: {
      tp: tp,
      cp: window.localStorage.getItem("cp")
    },
    beforeSend: function(){
      loading('Loading system users...');
      
      
    },
    success: function(data){
     
      var sysusr = JSON.parse(data);//heree
      var lgnUsr = window.localStorage.getItem("id");
      console.log(lgnUsr);
      $.each(sysusr, function(index, value){
          console.log(value.id + ' ## ' + value.user_id);
          if(lgnUsr == value.id){
            gblSysUsers += '<option value="'+value.id+'" selected>'+value.user_id+'</option>';
          } else {
            gblSysUsers += '<option value="'+value.id+'">'+value.user_id+'</option>';
          }

          gblSysUsersb += '<option value="'+value.id+'">'+value.user_id+'</option>';
          
      });

      //console.log(gblSysUsers);

    },
    error: function(err){

    },
    complete: function(){

      closeLoading();
    }
  });

}

function openOrderStatus(){
  orderReportArray.length = 0;
  $('#genorderreppdfbtn').hide();
}

//MENU CLICK
function dashMenuClick(res) {
//
  if(res == 'logout') {
    logout();
  } else if(res == 'profile') {
    listPrinters({type:'usb'},function(data){
        alert("Success");
        alert(data.toString()); // bt status: true or false
      },function(err){
        alert("Error");
        alert(err);
      });
    alert("Profile");
  } else if(res == 'sitelogout') {
    sitelogout();
  } else if(res == 'orderStatus') {
    openPage('orderStatus',{},openOrderStatus);
  } else if(res == 'tripsModule') {
    openPage('tripsModule');
  } else if(res == 'customer-add') {
    openPage('addcustomer');
  } else if(res == 'order-entry') {
    openPage('addOrder');
    
  } else if(res == 'manage-orders') {

    orderReportArray.length = 0;
    openPage('manageOrders');
  } else if(res == 'pos') {
    
    window.localStorage.setItem("userpay","");
    window.localStorage.setItem("userpayid","");
    openPage('pos',{}, setTaxRowVals);
  } else if(res == 'booking') {

    window.localStorage.setItem("userpay","");
    window.localStorage.setItem("userpayid","");
    bookingItems.length = 0;
    paymentTypesUsed.length = 0;
    openPage('booking',{},resetBookingFrm);
  } else if(res == 'direct-sales') {

    window.localStorage.setItem("userpay","");
    window.localStorage.setItem("userpayid","");
    bookingItems.length = 0;
    paymentTypesUsed.length = 0;
    openPage('directSales',{},resetDirectSalesFrm);
  } else if(res == 'sales-invoicing') {

    window.localStorage.setItem("userpay","");
    window.localStorage.setItem("userpayid","");
    bookingItems.length = 0;
    paymentTypesUsed.length = 0;
    openPage('salesInvoice',{},resetBookingFrm);
  } else if(res == 'mybooking') {
    openPage('mybooking'); 
  } else if(res == 'customer-payment') {
    openPage('customerPayment');
  } else if(res == 'sales-expenses') {
    openPage('salesExpenses');
  } else if(res == 'cash') {
    openPage('cashPayment',{tt: posTotal, title: 'Cash'}, postTotalVal);
    
  } else if(res == 'visa') {
    openPage('cashPayment',{tt: posTotal, title: 'Visa'}, postTotalVal);
    
  } else if(res == 'ezzypay') {
    openPage('cashPayment',{tt: posTotal, title: 'Eazzypay'}, postTotalVal);
    
  } else if(res == 'mpesa-paybill') {
    openPage('mpesaPaymentSearch');
  } else if(res == 'credit-note') {
    openPage('creditNoteSearch');
  } else if(res == 'credit-note2') {
    openPage('creditNoteSearch2',{}, bookCreditNoteVal);
  } else if(res == 'cash2') {
    openPage('cashPayment2',{tt: posTotal, title: 'Cash'}, postTotalVal2);
  } else if(res == 'cheque') {
    openPage('cashPayment',{tt: posTotal, title: 'Cheque'}, postTotalVal);
  } else if(res == 'cheque2') {
    openPage('cashPayment2',{tt: posTotal, title: 'Cheque'}, postTotalVal2);
  } else if(res == 'visa2') {
    openPage('cashPayment2',{tt: posTotal, title: 'Visa'}, postTotalVal2);
  } else if(res == 'ezzypay2') {
    openPage('cashPayment2',{tt: posTotal, title: 'Eazzypay'}, postTotalVal2);
  } else if(res == 'mpesa-paybill2') {
    openPage('mpesaPaymentSearch2');
  } else if(res == 'reports') {
    openPage('reportFilter');
    
  } else if(res == 'location') {
    openPage('location');
  } else if(res == 'on-view') {
    openPage('onView');
  } else if(res == 'on-map') {
    openPage('onMap');
  } else if(res == 'history-map') {
    openPage('historyMap');
  } else if(res == 'history-report') {
    openPage('historyReport');
  } else if(res == 'loccustomer') {
    openPage('customerLocation');
    //loadCustomers();
  } else if(res == 'view-loccustomers') {
    openPage('customerLocations');
  } else if(res == 'route') {
    // Make the request
    requestLocationAccuracy();
    openPage('selectRoute');
  } else if(res == 'customer-notes') {
    openPage('customerNotes');
  } else if(res == 'general-notes') {
    openPage('generalNotes');
  } else if(res == 'reset-password') {
    openPage('resetPassword');
  } else {
    alert("");
  }
  //
  //customer-location
  
}

/*function checkRoles() {
  var roles = JSON.parse(window.localStorage.getItem("roles"));

  if(roles.indexOf("pPOS") !== -1){
      console.log("pPOS YES");
      $('#pos').css("visibility", "visible");
  } else {
      $('#pos').css({"pointer-events": "none"});
  }

  if(roles.indexOf("pBookings") !== -1){
      console.log("pBookings YES");
      $('#booking').css("visibility", "visible");
  } else {
    $('#booking').css({"pointer-events": "none"});
  }

  if(roles.indexOf("pMy Bookings") !== -1){
      console.log("pMy Bookings YES");
      $('#mybooking').css("visibility", "visible");
  } else {

    $('#mybooking').css({"pointer-events": "none"});

  }

} */

function setTaxRowVals(){

  if(window.localStorage.getItem("setSiteTax") == "0"){
    $('.taxrow').hide();
  }
}

//SALES EXPENSES
function addSalesExpenses() {

  var description = $("#description").val();
  var amount = $("#amount").val();
  var mode_payment = $("#mode_payment").val();
  var payment_date = $("#payment_date").val();

  var tp = "addsalesexpenses";
  if(description == "" || mode_payment == "" || amount == "" || payment_date == "") {
        alert("Please fill (*) mandatory fields");
  } else {

        loading('Please wait...');
        $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
        {
            tp: tp,
            description: description,
            mode_payment: mode_payment,
            amount: amount,
            payment_date: payment_date,
            cp: window.localStorage.getItem("cp"),
            id: window.localStorage.getItem("id")
        },
        function(data, status){
          console.log(data);
            closeLoading();
            
            if(data == "false"){
                alert("Unable to Add Sales Expense, Error occured");
            } else {
               loadSalesExpenses();
                alert("Sales Expense Successful");
                

            }
        });
  }

    //alert($duedate);

}

//REPRINT CUSTOMER PAYMENT
function reprintCustomerPayment(mode_payment, idnn, customer_option, amount, reference) {

  var posReceiptRef = reference +'#there#'+idnn;
  //console.log(posReceiptRef);
  //console.log(mode_payment);
  //console.log(customer_option);
  //console.log(amount);
  //console.log(ptype);

  try {

        BTPrinterObj.list(function(res){

          var pname = res[0];
          var paddress = res[1];
          var ptype = res[2];
          var htp = window.localStorage.getItem("cp");
          var ttprint = "";
          var ptype = "";
//

          if(mode_payment == "1"){
            ptype += "Mpesa";
          } else if(mode_payment == "2"){
            ptype += "Cheque";
          } else if(mode_payment == "3"){
            ptype += "Cash";
          } else if(mode_payment == "4"){
            ptype += "Direct Deposit";
          }

          var receiptRef = posReceiptRef.split('#');
          var receiptNumberPosAct = 'POS#'+str_pad(receiptRef[2], 9, "0", "STR_PAD_LEFT");

          //Connect to printer 
          BTPrinterObj.connect(function(data){

            //1ST PRINT
            //Receipt Title Header
            BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
            BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
            BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
            BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

            //Receipt Header
            BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'CUSTOMER PAYMENT REPRINT\n','1','1');
            BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
            //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
            BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

            BTPrinterObj.printText(function(data){
              //Clear all pos data
              
              
            },null, justify_left +
            '\n\n' +
            'Customer: '+customer_option + 
            '\n\n' +
            'Amount: ' + amount +
            '\n\n' +
            'Payment Mode: ' + ptype +
            '\n\n' +
            'Ref: ' +  reference +            // Print
            '\n\n\n' +
            'Served By: ' + window.localStorage.getItem("username") +
           '\n\n\n' +
            'THANKYOU' +
           '\n\n\n\n\n\n' +
           justify_left,'1','0');

            //2ND PRINT
            //Receipt Title Header
            BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
            BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
            BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
            BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

            //Receipt Header
            BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'CUSTOMER PAYMENT REPRINT\n','1','1');
            BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
            //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
            BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

            BTPrinterObj.printText(function(data){
              //Clear all pos data
              //
              posReceiptRef = "";
              
            },null, justify_left +
            '\n\n' +
            'Customer: '+customer_option + 
            '\n\n' +
            'Amount: ' + amount +
            '\n\n' +
            'Payment Mode: ' + ptype +
            '\n\n' +
            'Ref: ' +  reference +            // Print
            '\n\n\n' +
            'Served By: ' + window.localStorage.getItem("username") +
           '\n\n\n' +
            'THANKYOU' +
           '\n\n\n\n\n\n' +
           justify_left,'1','0');

          },function(err){     
            
            //Printer conn error
            posReceiptRef = "";

          }, pname);
        },function(err){

            //Blutooth conn error
            posReceiptRef = "";
        });

      } catch(err) {
          posReceiptRef = "";
      } 
}

//CUSTOMER PAYMENT
function addCustomerPayment() {

  var customer_option = $(".customer_option").val();
  var customer_option_id = $("#customer_option_id").val();
  var mode_payment = $("#mode_payment").val();
  var amount = $("#amount").val();
  var reference = $("#reference").val();
  var payment_date = $("#payment_date").val();

  var tp = "addcustomerpayment";
 
  if(customer_option == "" || mode_payment == "" || amount == "" || reference == "" || payment_date == "") {
        alert("Please fill (*) mandatory fields");
  } else {

        loading('Please wait...');
        $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
        {
            tp: tp,
            customer_option: customer_option,
            customer_option_id: customer_option_id,
            mode_payment: mode_payment,
            amount: amount,
            reference: reference,
            payment_date: payment_date,
            cp: window.localStorage.getItem("cp"),
            id: window.localStorage.getItem("id")
        },
        function(data, status){
          console.log(data);
          closeLoading();
          posReceiptRef = data.trim();
          var rr = posReceiptRef.split('#');
          if(rr[2] == ""){

            alert("Error occured!! Please try again.");

          } else {

              if(data == "false"){
                  alert("Unable to Add Customer Payment, Error occured");
              } else {
                 
                  alert("Customer Payment Successful");
                  loadCustomerPayments2();//llllll

                  try {

                    BTPrinterObj.list(function(res){

                      var pname = res[0];
                      var paddress = res[1];
                      var ptype = res[2];
                      var htp = window.localStorage.getItem("cp");
                      var ttprint = "";
                      var ptype = "";
    //

                      if(mode_payment == "1"){
                        ptype += "Mpesa";
                      } else if(mode_payment == "2"){
                        ptype += "Cheque";
                      } else if(mode_payment == "3"){
                        ptype += "Cash";
                      } else if(mode_payment == "4"){
                        ptype += "Direct Deposit";
                      }

                      var receiptRef = posReceiptRef.split('#');
                      var receiptNumberPosAct = 'POS#'+str_pad(receiptRef[2], 9, "0", "STR_PAD_LEFT");

                      //Connect to printer 
                      BTPrinterObj.connect(function(data){

                        //1ST PRINT
                        //Receipt Title Header
                        BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                        BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                        BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                        BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                        //Receipt Header
                        BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'CUSTOMER PAYMENT\n','1','1');
                        BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
                        //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
                        BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

                        BTPrinterObj.printText(function(data){
                          //Clear all pos data
                          
                          
                        },null, justify_left +
                        '\n\n' +
                        'Customer: '+customer_option + 
                        '\n\n' +
                        'Amount: ' + amount +
                        '\n\n' +
                        'Payment Mode: ' + ptype +
                        '\n\n' +
                        'Ref: ' +  reference +            // Print
                        '\n\n\n' +
                        'Served By: ' + window.localStorage.getItem("username") +
                       '\n\n\n' +
                        'THANKYOU' +
                       '\n\n\n\n\n\n' +
                       justify_left,'1','0');

                        //2ND PRINT
                        //Receipt Title Header
                        BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                        BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                        BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                        BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                        //Receipt Header
                        BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'CUSTOMER PAYMENT REPRINT\n','1','1');
                        BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
                        //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
                        BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

                        BTPrinterObj.printText(function(data){
                          //Clear all pos data
                          //
                          posReceiptRef = "";
                          MobileUI.clearForm('frmCustomerPayment');
                          
                        },null, justify_left +
                        '\n\n' +
                        'Customer: '+customer_option + 
                        '\n\n' +
                        'Amount: ' + amount +
                        '\n\n' +
                        'Payment Mode: ' + ptype +
                        '\n\n' +
                        'Ref: ' +  reference +            // Print
                        '\n\n\n' +
                        'Served By: ' + window.localStorage.getItem("username") +
                       '\n\n\n' +
                        'THANKYOU' +
                       '\n\n\n\n\n\n' +
                       justify_left,'1','0');

                      },function(err){     
                        
                        //Printer conn error
                        posReceiptRef = "";
                        MobileUI.clearForm('frmCustomerPayment');

                      }, pname);
                    },function(err){

                        //Blutooth conn error
                        posReceiptRef = "";
                        MobileUI.clearForm('frmCustomerPayment');
                    });

                  } catch(err) {
                      posReceiptRef = "";
                      MobileUI.clearForm('frmCustomerPayment');
                  }
                  

              }

          }

            
              
        });
  }

    //alert($duedate);

}

function cpsuccess() {
  
}

function cperr() {

}

function initiateOrderScanner() {
  /*var params = {
    text_title: "Scan Barcode", // Android only
    text_instructions: "Please point your camera at the Barcode", // Android only
    camera: "back",
    drawSight: true //defaults to true, create a red sight/line in the center of the scanner view.
  };

  cloudSky.zBar.scan(params, function(s){
      //alert("S"+s);
      var itemname = "";
      var itemcode = " BF003";
      //itemsArraySearch h
      //getSelectedItem(itname, code)
      $.each(itemsArraySearch, function(index, value){
          //console.log(value.name);
          if(value.stock_id == itemcode){
            itemname = value.description;
          }
      });

      alert(s);
      getBarcodeSelectedOrderItemPrice(itemname, itemcode, "frmAddOrder");

      
  }, function(s){
      alert("E: "+s);
  });*/

  openPage('orderBarcodeScanner');

  var options = {
    x: 0,
    y: 52,
    width: window.screen.width,
    height: 226,
    camera: EmbeddedBarcodeReader.CAMERA_DIRECTION.BACK,
    toBack: false
  };

  EmbeddedBarcodeReader.startCamera(options);

  EmbeddedBarcodeReader.addBarcodeReadListener(function(readBarcode){
    // Log the barcode
    /*
    var itemname = "";
      var itemcode = " BF003";
      //itemsArraySearch h
      //getSelectedItem(itname, code)
      $.each(itemsArraySearch, function(index, value){
          //console.log(value.name);
          if(value.stock_id == itemcode){
            itemname = value.description;
          }
      });*/

      productItemSpecificSearch(readBarcode,"frmAddOrder");

      //getBarcodeSelectedOrderItemPrice(itemname, itemcode, "frmAddOrder");
  });


}

//

function initiateDirectSalesScanner() {

  openPage('directSalesBarcodeScanner');

  var options = {
    x: 0,
    y: 52,
    width: window.screen.width,
    height: 226,
    camera: EmbeddedBarcodeReader.CAMERA_DIRECTION.BACK,
    toBack: false
  };

  EmbeddedBarcodeReader.startCamera(options);

  EmbeddedBarcodeReader.addBarcodeReadListener(function(readBarcode){
    // Log the barcode
    //alert(readBarcode);
    /*
    var itemname = "";
      var itemcode = " BF003";
      //itemsArraySearch h
      //getSelectedItem(itname, code)
      $.each(itemsArraySearch, function(index, value){
          //console.log(value.name);
          if(value.stock_id == itemcode){
            itemname = value.description;
          }
      });*/

      productItemSpecificSearch(readBarcode,"frmBooking");

      //var scannerObj = {item_option: itemname, item_option_id: itemcode}; //

      //getBookingBarcodeSelectedItemPrice(itemname, itemcode, "frmBooking");


  });


}

function initiateBookingScanner() {

  openPage('bookingBarcodeScanner');

  var options = {
    x: 0,
    y: 52,
    width: window.screen.width,
    height: 226,
    camera: EmbeddedBarcodeReader.CAMERA_DIRECTION.BACK,
    toBack: false
  };

  EmbeddedBarcodeReader.startCamera(options);

  EmbeddedBarcodeReader.addBarcodeReadListener(function(readBarcode){
    // Log the barcode
    //alert(readBarcode);
    /*
    var itemname = "";
      var itemcode = " BF003";
      //itemsArraySearch h
      //getSelectedItem(itname, code)
      $.each(itemsArraySearch, function(index, value){
          //console.log(value.name);
          if(value.stock_id == itemcode){
            itemname = value.description;
          }
      });*/

      productItemSpecificSearch(readBarcode,"frmBooking");

      //var scannerObj = {item_option: itemname, item_option_id: itemcode}; //

      //getBookingBarcodeSelectedItemPrice(itemname, itemcode, "frmBooking");


  });


}

function getBookingBarcodeSelectedItemPrice(itname, code, frm){
  var selIdItem = "1";

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getItemPriceWithId",
        it: code,
        pid: selIdItem,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      var sepArr = data.split("|");

      var totalCalc = parseInt(sepArr[0]) * 1;

      var posItemObj = {item_option: itname, item_option_id: code, price: sepArr[0], quantityAval: sepArr[1], quantity: "1", total: totalCalc};

      var gotsts = false;

      if(bookingItems.length > 0) {

        for(var i=0; i<bookingItems.length; i++) {
          //console.log(posItems[i]['item_option'].toString().toLowerCase());
          //console.log(obj.item_option.toLowerCase());
          if(bookingItems[i]['item_option'].toString().toLowerCase() == posItemObj.item_option.toLowerCase()){
              bookingItems[i].quantity = parseInt(bookingItems[i].quantity) + parseInt(posItemObj.quantity);
              bookingItems[i].total = parseFloat(bookingItems[i].price) * parseInt(bookingItems[i].quantity);
              gotsts = true;
              //index++; 
              //console.log(gotsts);
              break;
          }
          
        }

      }

      if(gotsts){
        //console.log(sitem[0].item_option);
        //console.log(sitem[0].quantity);
      } else {
        bookingItems.push(posItemObj);
      }

    
      posTotal = 0;
      $.each(bookingItems, function(index, value){
          //console.log('POS array '+index, value);
          $.each(value, function(key, cell){
                
                if(key == "total") {
                  //console.log('POS arrs ' + cell + ' sval: ' + key + ' index: ' + index);
                  posTotal = posTotal + parseFloat(cell);
                }
                  
            });
           
        });

        posBalance = 0;
        if(paymentTypesUsed.length > 0){
          var ttpaid = 0;
          $.each(paymentTypesUsed, function(index, value){
            if(value != undefined) {
              ttpaid = ttpaid + parseInt(value.TransAmount);
            }
            
          });

          posBalance = parseInt(ttpaid) - parseInt(posTotal);
        } else {
          posBalance = parseInt(posBalance) - parseInt(posTotal);
        }


        var scobj = MobileUI.objectByForm('frmBookingBarcodeScanner');

        scobj.scannerTotal = posTotal;

        MobileUI.formByObject('frmBookingBarcodeScanner', scobj);
      });
}

function initiatePosScanner() {


  openPage('barcodeScanner');

  var options = {
    x: 0,
    y: 52,
    width: window.screen.width,
    height: 226,
    camera: EmbeddedBarcodeReader.CAMERA_DIRECTION.BACK,
    toBack: false
  };

  EmbeddedBarcodeReader.startCamera(options);

  EmbeddedBarcodeReader.addBarcodeReadListener(function(readBarcode){


    productItemSpecificSearch(readBarcode, "frmPOS");



  });

}


function stopOrderScanner() {
  backPage();
  EmbeddedBarcodeReader.stopCamera();
  calculateTotal('frmOrderBarcodeScanner');
}

function stopBookingScanner() {
  backPage();
  EmbeddedBarcodeReader.stopCamera();
  calculateTotal('frmBookingBarcodeScannerMain');
}

function stopScanner() {
  backPage();
  EmbeddedBarcodeReader.stopCamera();
  calculateTotal('frmOrderBarcodeScannerMain');
}

function getBarcodeSelectedOrderItemPrice(itname, code, frm){

    var orderObjb = MobileUI.objectByForm(frm);

    var selIdItem = "1";

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getItemPriceWithId",
        it: code,
        pid: selIdItem,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      var sepArr = data.split("|");

      var posItemObj = [{item_option: itname, item_option_id: code, price: sepArr[0], quantity: "1"}];
      var gotsts = false;

      if(orderItems.length > 0) {

        for(var i=0; i<orderItems.length; i++) {
          //console.log(posItems[i]['item_option'].toString().toLowerCase());
          //console.log(obj.item_option.toLowerCase());
          if(orderItems[i]['item_option'].toString().toLowerCase() == posItemObj.item_option.toLowerCase()){
              orderItems[i].quantity = parseInt(orderItems[i].quantity) + parseInt(posItemObj.quantity);
              orderItems[i].total = parseFloat(orderItems[i].price) * parseInt(orderItems[i].quantity);
              gotsts = true;
              //index++; 
              //console.log(gotsts);
              break;
          }
          
        }

      }
        

      //var sitem = search(posItems,obj.item_option);
      

      if(gotsts){
        //console.log(sitem[0].item_option);
        //console.log(sitem[0].quantity);
      } else {
        orderItems.push(posItemObj);
      }

          
    });
}

function getBarcodeSelectedItemPrice(itname, code, frm){

    var selIdItem = "1";

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
    {
        tp: "getItemPriceWithId",
        it: code,
        pid: selIdItem,
        cp: window.localStorage.getItem("cp")
    },
    function(data, status){
      var sepArr = data.split("|");

      var totalCalc = parseInt(sepArr[0]) * 1;

      var posItemObj = {item_option: itname, item_option_id: code, price: sepArr[0], quantityAval: sepArr[1], quantity: "1", total: totalCalc};

      var gotsts = false;

      if(posItems.length > 0) {

        for(var i=0; i<posItems.length; i++) {
          //console.log(posItems[i]['item_option'].toString().toLowerCase());
          //console.log(obj.item_option.toLowerCase());
          if(posItems[i]['item_option'].toString().toLowerCase() == posItemObj.item_option.toLowerCase()){
              posItems[i].quantity = parseInt(posItems[i].quantity) + parseInt(posItemObj.quantity);
              posItems[i].total = parseFloat(posItems[i].price) * parseInt(posItems[i].quantity);
              gotsts = true;
              //index++; 
              //console.log(gotsts);
              break;
          }
          
        }

      }
        

      //var sitem = search(posItems,obj.item_option);
      

      if(gotsts){
        //console.log(sitem[0].item_option);
        //console.log(sitem[0].quantity);
      } else {
        posItems.push(posItemObj);
      }

      posTotal = 0;
      $.each(posItems, function(index, value){
          //console.log('POS array '+index, value);
          $.each(value, function(key, cell){
                
                if(key == "total") {
                  //console.log('POS arrs ' + cell + ' sval: ' + key + ' index: ' + index);
                  posTotal = posTotal + parseFloat(cell);
                }
                  
            });
           
        });

      var scobj = MobileUI.objectByForm('frmBarcodeScanner');

      scobj.scannerTotal = posTotal;

      MobileUI.formByObject('frmBarcodeScanner', scobj);

          
    });
}

function orderAdd(index){
  orderItems[index].quantity = parseInt(orderItems[index].quantity) + 1;
  orderItems[index].total = parseFloat(orderItems[index].price) * parseInt(orderItems[index].quantity);

  calculateTotal('frmOrderBarcodeScanner');

}

function posAdd(index){
  posItems[index].quantity = parseInt(posItems[index].quantity) + 1;
  posItems[index].total = parseFloat(posItems[index].price) * parseInt(posItems[index].quantity);

  calculateTotal('frmBarcodeScanner');

}

function calculateTotal(sfrm){

  $('#scannerTotal').val(posTotal);
  
  
  if(sfrm == "frmBarcodeScanner" || sfrm == "frmPOSBarcodeScannerMain"){
    if(posItems.length > 0) {
      posTotal = 0;
      $.each(posItems, function(index, value){
          //console.log('POS array '+index, value);
          $.each(value, function(key, cell){
                
                if(key == "total") {
                  //console.log('POS arrs ' + cell + ' sval: ' + key + ' index: ' + index);
                  posTotal = posTotal + parseFloat(cell);
                }
                  
            });
           
        });

      var scobj = MobileUI.objectByForm(sfrm);

      scobj.scannerTotal = posTotal;

      MobileUI.formByObject(sfrm, scobj);
    }
  }


  if(sfrm == "frmBookingBarcodeScanner" || sfrm ==  "frmBookingBarcodeScannerMain"){

    console.log("YESS");
    if(bookingItems.length > 0) {
      posTotal = 0;
        $.each(bookingItems, function(index, value){
            //console.log('POS array '+index, value);
            $.each(value, function(key, cell){
                  
                  if(key == "total") {
                    //console.log('POS arrs ' + cell + ' sval: ' + key + ' index: ' + index);
                    posTotal = posTotal + parseFloat(cell);
                  }
                    
              });
             
          });

          posBalance = 0;
          if(paymentTypesUsed.length > 0){
            var ttpaid = 0;
            $.each(paymentTypesUsed, function(index, value){
              if(value != undefined) {
                ttpaid = ttpaid + parseInt(value.TransAmount);
              }
              
            });

            posBalance = parseInt(ttpaid) - parseInt(posTotal);
          } else {
            posBalance = parseInt(posBalance) - parseInt(posTotal);
          }


          var scobj = MobileUI.objectByForm(sfrm);

          scobj.scannerTotal = posTotal;
          scobj.scannerBalance = posBalance;

          MobileUI.formByObject(sfrm, scobj);
    }
  }
  console.log("TOTAL: = "+ posTotal);
  //$('#postotal').text(posTotal);
  $('#scannerTotal').val(posTotal);


}

function posMinus(index){
  if(parseInt(posItems[index].quantity) > 1){ 
    posItems[index].quantity = parseInt(posItems[index].quantity) - 1;
    posItems[index].total = parseFloat(posItems[index].price) * parseInt(posItems[index].quantity);

    calculateTotal('frmBarcodeScanner');

  }
}

function orderMinus(index){
  if(parseInt(orderItems[index].quantity) > 1){ 
    orderItems[index].quantity = parseInt(orderItems[index].quantity) - 1;
    orderItems[index].total = parseFloat(orderItems[index].price) * parseInt(orderItems[index].quantity);

    calculateTotal('frmOrderBarcodeScanner');

  }
}


function bookingAdd(index){
  bookingItems[index].quantity = parseInt(bookingItems[index].quantity) + 1;
  bookingItems[index].total = parseFloat(bookingItems[index].price) * parseInt(bookingItems[index].quantity);

  calculateTotal('frmBookingBarcodeScanner');

}


function bookingMinus(index){
  if(parseInt(bookingItems[index].quantity) > 1){ 
    bookingItems[index].quantity = parseInt(bookingItems[index].quantity) - 1;
    bookingItems[index].total = parseFloat(bookingItems[index].price) * parseInt(bookingItems[index].quantity);

    calculateTotal('frmBookingBarcodeScanner');

  }
}

function refreshAllLists() {
  loadCustomers();
  //startLocationSeaker();
  //loadUsers();
  //loadPOSTransaction();
  
  
}

function posTransactionPayMpesaPrint(){
  if(posReceiptRef == ""){
    alert('Please Submit Sale First');
  } else {

  }
  console.log(paymentTypesUsed);

  var dobj = MobileUI.objectByForm('formMpesaDetPayment');
  var mpesapaymentfield = dobj.mpesapaymentfield;


    //console.log("################# POS ITEMS #################");
              var mcashp = dobj.mpesapaymentfield;
              var mcashc = parseInt(partialPaymentTotal) - parseInt(posTotal);
              var ttDiscount = 0;
              var ttTax = 0;
               $.each(posItems, function(index, value){
                  //console.log(value[index]['item_option'] + ' --------- ' + value[index]['price']);
                  
                  ttDiscount = ttDiscount + (parseFloat(value.discount) * parseFloat(value.quantity));
                  ttTax = ttTax + (parseFloat(value.tax) * parseFloat(value.quantity));
                  itm += value.item_option+' - '+value.item_option_id+'   '+value.price+'x'+value.quantity+'    '+value.total+'\n\n';
                  /*$.each(value, function(key, cell){
                      
                        if(key == "item_option") {
                          itm += cell+'\n';
                        }
                        if(key == "quantity") {
                          itm += 'x'+cell;
                        }
                        if(key == "price") {
                          itm += cell;
                        }
                        if(key == "total") {
                          itm += '    '+cell+'   \n\n';
                        }
                          
                    });*/
                   
                });
               itm += '\n\n\n';
               itm += 'TOTAL          :      '+posTotal+'\n\n';
               itm += 'TOTAL DISCOUNT :      '+ttDiscount+'\n\n';

               if(window.localStorage.getItem("setSiteTax") == "1"){
                itm += 'TOTAL TAX      :      '+ttTax+'\n\n';
               }


              $.each(paymentTypesUsed, function(index, value){
                  //console.log(value.name);
                  if(value.Transtype == "MPESA"){
                    itm += 'MPESA : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                  } else if(value.Transtype == "CASH") {
                    itm += 'CASH : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                  } else {
                    itm += 'CREDIT-NOTE : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                  }
              });

              itm += 'CHANGE     :      '+mcashc+'\n\n';
               //console.log(itm);
               //console.log("################# END #################");
            try {
              BTPrinterObj.list(function(res){

                  var pname = res[0];
                  var paddress = res[1];
                  var ptype = res[2];

                  var receiptRef = posReceiptRef.split('#');
                  var receiptNumberPosAct = 'POS#'+str_pad(receiptRef[2], 9, "0", "STR_PAD_LEFT");


                    //Connect to printer 
                    BTPrinterObj.connect(function(data){
                      //alert("s2: "+JSON.stringify(data));

                      //1ST PRINT
                      //Receipt Title Header
                      BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                      BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                      BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                      BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                      //Receipt Header
                      BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'SALES RECEIPT\n','1','1');
                      BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
                      //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
                      BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

                      BTPrinterObj.printText(function(data){
                        //Clear all pos data
                        
                        
                      },null, justify_left +
                      '\n\n' +
                      itm + 
                      '\n\n\n' +
                      'Served By: ' + window.localStorage.getItem("username") +              // Print
                     '\n\n' +
                      'THANKYOU' +
                     '\n\n\n\n\n\n' +
                     justify_left,'1','0');

                      //2ND PRINT
                      //Receipt Title Header
                      BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                      BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                      BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                      BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                      //Receipt Header
                      BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'SALES RECEIPT REPRINT\n','1','1');
                      BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
                     // BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
                      BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

                      BTPrinterObj.printText(function(data){
                        
                        //openPage('pos');
                        
                      },null, justify_left +
                      '\n\n' +
                      itm +               // Print
                     '\n\n\n' +
                      'Served By: ' + window.localStorage.getItem("username") +
                      '\n\n' +
                      'THANKYOU' +
                     '\n\n\n\n\n\n' +
                     justify_left,'1','0');

                    },function(err){     
                      //alert("e2: "+err);
                    }, pname);

              },function(err){
                  
              });
            } catch(err) {

            }
              


}

function clearCpayData(){
  try {

    paymentTypesUsed.length = 0;
    partialPaymentTotal = 0;
    posReceiptRef = "";
    itm = "";
    posTotal = 0;
    posItems.length = 0;
    bookingItems.length = 0;
    paymentTypesUsed.length = 0;
    

    try{ MobileUI.clearForm('formCashPayment'); } catch(err){ }
    

    backPage();
    backPage();

    try{ MobileUI.clearForm('frmPOS'); } catch(err){ } 

    try{ MobileUI.clearForm('frmBooking'); } catch(err){ } 
    
    
    $("#quantity").val("1");
    $("#mode_prices").val("1");
    $("#discount").val("0");
    $("#tax").val("0");
    $("#scannerTotal").val("0");
    $("#scannerBalance").val("0");

  } catch(err) {
    console.log("ERROR!!");
    console.log(err);
  }
}

function clearCpayDataBooking(){
  try {
    paymentTypesUsed.length = 0;
    partialPaymentTotal = 0;
    posReceiptRef = "";
    itm = "";
    posTotal = 0;
    posItems.length = 0;
    bookingItems.length = 0;
    paymentTypesUsed.length = 0;
    

    try{ MobileUI.clearForm('formCashPayment'); } catch(err){ }
    backPage();
    backPage();

    
    try{ MobileUI.clearForm('frmPOS'); } catch(err){ } 
    try{ MobileUI.clearForm('frmBooking'); } catch(err){ } 
    
    
    

    $("#quantity").val("1");
    $("#mode_prices").val("1");
    $("#discount").val("0");
    $("#tax").val("0");
    $("#scannerTotal").val("0");
    $("#scannerBalance").val("0");
  } catch(err) {
    console.log("ERROR!!");
    console.log(err);
  }
  

}

function clearCpayDataPos(){
    
  
  try {

    paymentTypesUsed.length = 0;
    partialPaymentTotal = 0;
    posReceiptRef = "";
    itm = "";
    posTotal = 0;
    posItems.length = 0;
    bookingItems.length = 0;
    paymentTypesUsed.length = 0;



    try{ MobileUI.clearForm('formCashPayment'); } catch(err){ }

    backPage();
    backPage();


    try{ MobileUI.clearForm('frmPOS'); } catch(err){ } 
    try{ MobileUI.clearForm('frmBooking'); } catch(err){ }

    $("#quantity").val("1");
    $("#mode_prices").val("1");
    $("#discount").val("0");
    $("#tax").val("0");
    $("#scannerTotal").val("0");


  } catch(err) {
    console.log("ERROR!!");
    console.log(err);
  }
   
  
}

function clearOrderEntryData(){

  try {
    posReceiptRef = "";
    oitm  = "";
    MobileUI.clearForm('frmAddOrder');
    
    $(".customer_option").val("");
    $("#quantity").val("1");
    orderItems.length = 0;
    orderTotal = 0;
    setCurrentDueDate();

  } catch(err) {

  }

}

function clearMpayData(){

  try {
    paymentTypesUsed.length = 0;
    partialPaymentTotal = 0;
    posReceiptRef = "";
    itm = "";
    posTotal = 0;
    posItems.length = 0;
    MobileUI.clearForm('formMpesaDetPayment');

    backPage();
    backPage();
    MobileUI.clearForm('frmPOS');
    $("#quantity").val("1");
    $("#mode_prices").val("1");
    $("#discount").val("0");
    $("#tax").val("0");
    $("#scannerTotal").val("0");

  } catch(err){

  }

  
}

function clearMpayDataBooking(){

  try {

    paymentTypesUsed.length = 0;
    partialPaymentTotal = 0;
    posReceiptRef = "";
    itm = "";
    posTotal = 0;
    posItems.length = 0;
    MobileUI.clearForm('formMpesaDetPayment');

    backPage();
    backPage();
    MobileUI.clearForm('frmBooking');
    $("#quantity").val("1");
    $("#mode_prices").val("1");
    $("#discount").val("0");
    $("#tax").val("0");
    $("#scannerTotal").val("0");
    $("#scannerBalance").val("0");

  } catch(err){

  }
  
}

//SUBMIT Booking Mpesa PAYMENT DETAILS
function bookingTransactionPayMpesaDone() {
  
  var dobj = MobileUI.objectByForm('formMpesaDetPayment');
  var mpesapaymentfield = dobj.mpesapaymentfield;
  var mpesacustomername = dobj.mpesacustomername;
  var cpbooking_id = dobj.cpbooking_id;
  var dispatchSts = dobj.dispatchsts;
  if(dispatchSts){
    var dsts = "1";
  } else {
    var dsts = "0";
  }
  //var mpesatIdRef = dobj.mtransIdRef;
 // alert(bookingPayId);

    loading('Please wait...');
    $('#bookingmpesabtn').prop('disabled', true);
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
      {
          tp: 'booking-cash-payment',
          ttp: 'MPESA',
          ttpAuto: gAuto,
          mpesaRef: gTransID,
          total: posTotal,
          dsts: dsts,
          cpbooking_id: bookingPayId,
          mpesacustomername: mpesacustomername,
          pospayments: JSON.stringify(paymentTypesUsed),
          posdesc: JSON.stringify(bookingItems),
          cp: window.localStorage.getItem("cp"),
          uname: window.localStorage.getItem("username"),
          id: window.localStorage.getItem("id")
      },
      function(data, status){
        bookingPayId = "";
        console.log(data);
        //alert("R: "+data);
        posReceiptRef = data.trim();
        //alert("E: "+posReceiptRef);
        gAuto = "";
        gTransID = "";
          closeLoading();
          $('#bookingmpesabtn').prop('disabled', false);
          if(data == "false"){
              alert("Error occured");
          } else {

              backPage();
              backPage();
              backPage();

              //console.log("################# POS ITEMS #################");
              var mcashp = dobj.mpesapaymentfield;
              var clacBalance = 0;
              var ttDiscount = 0;
              var ttTax = 0;
             
               $.each(posItems, function(index, value){
                  //console.log(value[index]['item_option'] + ' --------- ' + value[index]['price']);
                  ttDiscount = ttDiscount + (parseFloat(value.discount) * parseFloat(value.quantity));
                  
                  ttTax = ttTax + (parseFloat(value.tax) * parseFloat(value.quantity));

                  itm += value.item_option+' - '+value.item_option_id+'   '+value.price+'x'+value.quantity+'    '+value.total+'\n\n';

                  clacBalance = clacBalance + parseInt(value.total);

                  
                   
                });
                var mcashc = parseInt(clacBalance) - parseInt(posTotal);
               itm += '\n\n\n';
               

              itm += 'TOTAL          :      '+posTotal+'\n\n';
              itm += 'TOTAL DISCOUNT :      '+ttDiscount+'\n\n';

              if(window.localStorage.getItem("setSiteTax") == "1"){
                itm += 'TOTAL TAX      :      '+ttTax+'\n\n';
              }


              $.each(paymentTypesUsed, function(index, value){
                  //console.log(value.name);
                  if(value.Transtype == "MPESA"){
                    itm += 'MPESA : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                  } else if(value.Transtype == "CASH") {
                    itm += 'CASH : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                  } else {
                    itm += 'CREDIT-NOTE : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                  }
              });

              itm += 'CHANGE     :      '+mcashc+'\n\n';
               //console.log(itm);
               //console.log("################# END #################");

              try {

                BTPrinterObj.list(function(res){

                    var pname = res[0];
                    var paddress = res[1];
                    var ptype = res[2];

                    var receiptRef = posReceiptRef.split('#');
                    var receiptNumberPosAct = 'POS#'+str_pad(receiptRef[2], 9, "0", "STR_PAD_LEFT");


                      //Connect to printer 
                      BTPrinterObj.connect(function(data){
                        //alert("s2: "+JSON.stringify(data));

                        //1ST PRINT
                        //Receipt Title Header
                        BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                        BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                        BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                        BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                        //Receipt Header
                        BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'SALES RECEIPT\n','1','1');
                        BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
                        //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
                        BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

                        BTPrinterObj.printText(function(data){
                          //Clear all pos data
                          
                          
                        },null, justify_left +
                        '\n\n' +
                        itm + 
                        '\n\n\n' +
                        'Customer: ' + mpesacustomername +              // Print mpesacustomername
                       '\n\n' +
                        'Served By: ' + window.localStorage.getItem("username") +              // Print mpesacustomername
                       '\n\n' +
                        'THANKYOU' +
                       '\n\n\n\n\n\n' +
                       justify_left,'1','0');

                        //2ND PRINT
                        //Receipt Title Header
                        BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                        BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                        BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                        BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                        //Receipt Header
                        BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'SALES RECEIPT REPRINT\n','1','1');
                        BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
                        //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
                        BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

                        BTPrinterObj.printText(function(data){
                          //Clear all pos data
                          clearMpayDataBooking();
                          
                        },null, justify_left +
                        '\n\n' +
                        itm +               // Print
                       '\n\n\n' +
                       'Customer: ' + mpesacustomername +              // Print mpesacustomername
                       '\n\n' +
                        'Served By: ' + window.localStorage.getItem("username") +
                        '\n\n' +
                        'THANKYOU' +
                       '\n\n\n\n\n\n' +
                       justify_left,'1','0');

                      },function(err){     
                        
                        //Printer conn error
                        clearMpayDataBooking();
                      }, pname);

                },function(err){
                    //Bluetooth conn error
                    clearMpayDataBooking();
                });

              } catch(err) {

              } 

            
          }
     //HHSDJSJDS
      });
  
  
}

//SUBMIT CREDIT NOTE PAYMENT DETAILS
function bookingTransactionPayCreditNoteDone() {
  //poscashbtn posmpesabtn
 
  var dobj = MobileUI.objectByForm('formMpesaDetPayment');
  var mpesapaymentfield = dobj.mpesapaymentfield;
  var mpesacustomername = dobj.mpesacustomername;
  //var mpesatIdRef = dobj.mtransIdRef;
  //alert(gTransID + ' # ' + gAuto);
  if(mpesapaymentfield == ""){
      alert("You Must select a Credit Note Transaction");
  } else if(mpesacustomername == ""){
      alert("Please Enter Customers Name");
  } else if(bookingItems.length == 0){
      alert("No Items To Submit");
  } else if(parseInt(partialPaymentTotal) < parseInt(posTotal)){
      alert("Invalid Payment");
  } else {
    loading('Please wait...');
    $('#posmpesabtn').prop('disabled', true);
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
      {
          tp: 'checkSelection',
          cp: window.localStorage.getItem("cp"),
          id: window.localStorage.getItem("id")
      },
      function(data, status){
        var sts = data.trim();
        //alert("E: "+posReceiptRef);
          closeLoading();
          $('#posmpesabtn').prop('disabled', false);

          if(sts == "1"){
            bookingPinEntryDialog('CREDIT-NOTE');
          } else {
            $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
            {
                tp: 'booking-cash-payment',
                ttp: 'CREDIT-NOTE',
                ttpAuto: gAuto,
                mpesaRef: gTransID,
                total: posTotal,
                mpesacustomername: mpesacustomername,
                pospayments: JSON.stringify(paymentTypesUsed),
                posdesc: JSON.stringify(bookingItems),
                cp: window.localStorage.getItem("cp"),
                uname: window.localStorage.getItem("username"),
                id: window.localStorage.getItem("id")
            },
            function(data, status){
              console.log(data);
              //alert("R: "+data);
              posReceiptRef = data.trim();
              //alert("E: "+posReceiptRef);
              gAuto = "";
              gTransID = "";
                closeLoading();
                $('#posmpesabtn').prop('disabled', false);
                if(data == "false"){
                    alert("Error occured");
                } else {
                  
                    

                    //console.log("################# POS ITEMS #################");
                    var mcashp = dobj.mpesapaymentfield;
                    var clacBalance = 0;
                    var ttDiscount = 0;
                    var ttTax = 0;
                   
                     $.each(bookingItems, function(index, value){

                        ttDiscount = ttDiscount + (parseFloat(value.discount) * parseFloat(value.quantity));
                        ttTax = ttTax + (parseFloat(value.tax) * parseFloat(value.quantity));

                        itm += value.item_option+' - '+value.item_option_id+'   '+value.price+'x'+value.quantity+'    '+value.total+'\n\n';

                        clacBalance = clacBalance + parseInt(value.total);

                        
                         
                      });
                      var mcashc = parseInt(clacBalance) - parseInt(posTotal);
                     itm += '\n\n\n';
                     //paymentTypesUsed
                     /*$.each(paymentTypesUsed, function(index, value){

                     });*/

                    itm += 'TOTAL          :      '+posTotal+'\n\n';
                    itm += 'TOTAL DISCOUNT :      '+ttDiscount+'\n\n';

                    if(window.localStorage.getItem("setSiteTax") == "1"){
                      itm += 'TOTAL TAX      :      '+ttTax+'\n\n';
                    }

                    $.each(paymentTypesUsed, function(index, value){
                        //console.log(value.name);
                        if(value.Transtype == "MPESA"){
                          itm += 'MPESA : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                        } else if(value.Transtype == "CASH") {
                          itm += 'CASH : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                        } else {
                          itm += 'CREDIT-NOTE : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                        }
                    });

                    itm += 'CHANGE     :      '+mcashc+'\n\n';
                     //console.log(itm);
                     //console.log("################# END #################");

                    try{

                      BTPrinterObj.list(function(res){

                          var pname = res[0];
                          var paddress = res[1];
                          var ptype = res[2];

                          var receiptRef = posReceiptRef.split('#');
                          var receiptNumberPosAct = 'POS#'+str_pad(receiptRef[2], 9, "0", "STR_PAD_LEFT");


                            //Connect to printer 
                            BTPrinterObj.connect(function(data){
                              //alert("s2: "+JSON.stringify(data));

                              //1ST PRINT
                              //Receipt Title Header
                              BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                              BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                              BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                              BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                              //Receipt Header
                              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'SALES RECEIPT\n','1','1');
                              BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
                              //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
                              BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

                              BTPrinterObj.printText(function(data){
                                //Clear all pos data
                                
                                
                              },null, justify_left +
                              '\n\n' +
                              itm + 
                              '\n\n\n' +
                              'Customer: ' + mpesacustomername +              // Print mpesacustomername
                             '\n\n' +
                              'Served By: ' + window.localStorage.getItem("username") +              // Print mpesacustomername
                             '\n\n' +
                              'THANKYOU' +
                             '\n\n\n\n\n\n' +
                             justify_left,'1','0');

                              //2ND PRINT
                              //Receipt Title Header
                              BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                              BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                              BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                              BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                              //Receipt Header
                              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'SALES RECEIPT REPRINT\n','1','1');
                              BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
                              //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
                              BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

                              BTPrinterObj.printText(function(data){
                                //Clear all pos data
                                
                                clearMpayDataBooking();
                                
                              },null, justify_left +
                              '\n\n' +
                              itm +               // Print
                             '\n\n\n' +
                             'Customer: ' + mpesacustomername +              // Print mpesacustomername
                             '\n\n' +
                              'Served By: ' + window.localStorage.getItem("username") +
                              '\n\n' +
                              'THANKYOU' +
                             '\n\n\n\n\n\n' +
                             justify_left,'1','0');

                            },function(err){     
                              //Printer conn error
                              clearMpayDataBooking();
                            }, pname);

                      },function(err){
                          //Bluetooth conn error
                          clearMpayDataBooking();

                      });
                      
                    } catch(err){
                      clearMpayDataBooking();
                    }
                    

                }
            });

          }
      });
  }
  
}

//SUBMIT CREDIT NOTE PAYMENT DETAILS
function posTransactionPayCreditNoteDone() {
  //poscashbtn posmpesabtn
 
  var dobj = MobileUI.objectByForm('formMpesaDetPayment');
  var mpesapaymentfield = dobj.mpesapaymentfield;
  var mpesacustomername = dobj.mpesacustomername;

  var processedPayment = 0;
  $.each(paymentTypesUsed, function(i,v){
    processedPayment = processedPayment + parseInt(v.TransAmount);
  });
  //var mpesatIdRef = dobj.mtransIdRef;
  //alert(gTransID + ' # ' + gAuto);
  if(mpesapaymentfield == ""){
      alert("You Must select a Mpesa Transaction");
  } else if(mpesacustomername == ""){
      alert("Please Enter Customers Name");
  } else if(posItems.length == 0){
      alert("No Items To Submit");
  } else if(parseInt(processedPayment) < parseInt(posTotal)){
      alert("Invalid Payment");
  } else {
    loading('Please wait...');
    $('#posmpesabtn').prop('disabled', true);
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
      {
          tp: 'checkSelection',
          cp: window.localStorage.getItem("cp"),
          id: window.localStorage.getItem("id")
      },
      function(data, status){
        var sts = data.trim();
        //alert("E: "+posReceiptRef);
          closeLoading();
          $('#posmpesabtn').prop('disabled', false);

          if(sts == "1"){
            posPinEntryDialog('CREDIT-NOTE');
          } else {
            $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
            {
                tp: 'cash-payment',
                ttp: 'CREDIT-NOTE',
                ttpAuto: gAuto,
                mpesaRef: gTransID,
                total: posTotal,
                mpesacustomername: mpesacustomername,
                pospayments: JSON.stringify(paymentTypesUsed),
                posdesc: JSON.stringify(posItems),
                cp: window.localStorage.getItem("cp"),
                uname: window.localStorage.getItem("username"),
                id: window.localStorage.getItem("id")
            },
            function(data, status){
              console.log(data);
              //alert("R: "+data);
              posReceiptRef = data.trim();
              //alert("E: "+posReceiptRef);
              gAuto = "";
              gTransID = "";
                closeLoading();
                $('#posmpesabtn').prop('disabled', false);
                if(data == "false"){
                    alert("Error occured");
                } else {
                
                    //console.log("################# POS ITEMS #################");
                    var mcashp = dobj.mpesapaymentfield;
                    var clacBalance = 0;
                    var ttDiscount = 0;
                    var ttTax = 0;
                   
                     $.each(posItems, function(index, value){
                        //console.log(value[index]['item_option'] + ' --------- ' + value[index]['price']);

                        ttDiscount = ttDiscount + (parseFloat(value.discount) * parseFloat(value.quantity));
                        ttTax = ttTax + (parseFloat(value.tax) * parseFloat(value.quantity));

                        itm += value.item_option+' - '+value.item_option_id+'   '+value.price+'x'+value.quantity+'    '+value.total+'\n\n';

                        clacBalance = clacBalance + parseInt(value.total);
                        
                      
                         
                      });
                      var mcashc = parseInt(clacBalance) - parseInt(posTotal);
                     itm += '\n\n\n';
                     //paymentTypesUsed
                     /*$.each(paymentTypesUsed, function(index, value){

                     });*/

                    itm += 'TOTAL          :      '+posTotal+'\n\n';
                    itm += 'TOTAL DISCOUNT :      '+ttDiscount+'\n\n';

                    if(window.localStorage.getItem("setSiteTax") == "1"){
                      itm += 'TOTAL TAX      :      '+ttTax+'\n\n';
                     }

                    $.each(paymentTypesUsed, function(index, value){
                        //console.log(value.name);
                        if(value.Transtype == "MPESA"){
                          itm += 'MPESA : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                        } else if(value.Transtype == "CASH") {
                          itm += 'CASH : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                        } else {
                          itm += 'CREDIT-NOTE : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                        }
                    });

                    itm += 'CHANGE     :      '+mcashc+'\n\n';
                     //console.log(itm);
                     //console.log("################# END #################");

                    try {

                      BTPrinterObj.list(function(res){

                          var pname = res[0];
                          var paddress = res[1];
                          var ptype = res[2];

                          var receiptRef = posReceiptRef.split('#');
                          var receiptNumberPosAct = 'POS#'+str_pad(receiptRef[2], 9, "0", "STR_PAD_LEFT");


                            //Connect to printer 
                            BTPrinterObj.connect(function(data){
                              //alert("s2: "+JSON.stringify(data));

                              //1ST PRINT
                              //Receipt Title Header
                              BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                              BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                              BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                              BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                              //Receipt Header
                              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'SALES RECEIPT\n','1','1');
                              BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
                              //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
                              BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

                              BTPrinterObj.printText(function(data){
                                //Clear all pos data
                                
                                
                              },null, justify_left +
                              '\n\n' +
                              itm + 
                              '\n\n\n' +
                              'Customer: ' + mpesacustomername +              // Print mpesacustomername
                             '\n\n' +
                              'Served By: ' + window.localStorage.getItem("username") +              // Print mpesacustomername
                             '\n\n' +
                              'THANKYOU' +
                             '\n\n\n\n\n\n' +
                             justify_left,'1','0');

                              //2ND PRINT
                              //Receipt Title Header
                              BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                              BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                              BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                              BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                              //Receipt Header
                              BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'SALES RECEIPT REPRINT\n','1','1');
                              BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
                              //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
                              BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

                              BTPrinterObj.printText(function(data){
                                //Clear all pos data
                                clearMpayDataBooking();
                                
                              },null, justify_left +
                              '\n\n' +
                              itm +               // Print
                             '\n\n\n' +
                             'Customer: ' + mpesacustomername +              // Print mpesacustomername
                             '\n\n' +
                              'Served By: ' + window.localStorage.getItem("username") +
                              '\n\n' +
                              'THANKYOU' +
                             '\n\n\n\n\n\n' +
                             justify_left,'1','0');

                            },function(err){     
                              //alert("e2: "+err);
                              clearMpayDataBooking();
                            }, pname);

                      },function(err){
                          clearMpayDataBooking();
                      });
                
                    } catch(err){
                      clearMpayDataBooking();
                    }
                    

                }
            });

          }
      });
  }
  
}

//
//SUBMIT Mpesa PAYMENT DETAILS
function posTransactionPayMpesaDone() {
  //poscashbtn posmpesabtn
 
  var dobj = MobileUI.objectByForm('formMpesaDetPayment');
  var mpesapaymentfield = dobj.mpesapaymentfield;
  var mpesacustomername = dobj.mpesacustomername;

  var processedPayment = 0;
  $.each(paymentTypesUsed, function(i,v){
    processedPayment = processedPayment + parseInt(v.TransAmount);
  });
  //var mpesatIdRef = dobj.mtransIdRef;
  //alert(gTransID + ' # ' + gAuto);
  if(mpesapaymentfield == ""){
      alert("You Must select a Mpesa Transaction");
  } else if(mpesacustomername == ""){
      alert("Please Enter Customers Name");
  } else if(posItems.length == 0){
      alert("No Items To Submit");
  } else if(parseInt(processedPayment) < parseInt(posTotal)){
      alert("Invalid Payment");
  } else {
    loading('Please wait...');
    $('#posmpesabtn').prop('disabled', true);
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
      {
          tp: 'cash-payment',
          ttp: 'MPESA',
          ttpAuto: gAuto,
          mpesaRef: gTransID,
          total: posTotal,
          mpesacustomername: mpesacustomername,
          pospayments: JSON.stringify(paymentTypesUsed),
          posdesc: JSON.stringify(posItems),
          cp: window.localStorage.getItem("cp"),
          uname: window.localStorage.getItem("username"),
          id: window.localStorage.getItem("id")
      },
      function(data, status){
        console.log(data);
        //alert("R: "+data);
        posReceiptRef = data.trim();
        //alert("E: "+posReceiptRef);
        gAuto = "";
        gTransID = "";
          closeLoading();
          $('#posmpesabtn').prop('disabled', false);
          if(data == "false"){
              alert("Error occured");
          } else {
            
              //console.log("################# POS ITEMS #################");
              var mcashp = dobj.mpesapaymentfield;
              var clacBalance = 0;
              var ttDiscount = 0;
              var ttTax = 0;
             
               $.each(posItems, function(index, value){
                  //console.log(value[index]['item_option'] + ' --------- ' + value[index]['price']);

                  ttDiscount = ttDiscount + (parseFloat(value.discount) * parseFloat(value.quantity));
                  ttTax = ttTax + (parseFloat(value.tax) * parseFloat(value.quantity));

                  itm += value.item_option+' - '+value.item_option_id+'   '+value.price+'x'+value.quantity+'    '+value.total+'\n\n';

                  clacBalance = clacBalance + parseInt(value.total);
                        
                  
                  
                   
                });
                var mcashc = parseInt(clacBalance) - parseInt(posTotal);
               itm += '\n\n\n';
               //paymentTypesUsed
               /*$.each(paymentTypesUsed, function(index, value){

               });*/

              itm += 'TOTAL          :      '+posTotal+'\n\n';
              itm += 'TOTAL DISCOUNT :      '+ttDiscount+'\n\n';

              if(window.localStorage.getItem("setSiteTax") == "1"){
                itm += 'TOTAL TAX      :      '+ttTax+'\n\n';
               }

              $.each(paymentTypesUsed, function(index, value){
                  //console.log(value.name);
                  if(value.Transtype == "MPESA"){
                    itm += 'MPESA : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                  } else if(value.Transtype == "CASH") {
                    itm += 'CASH : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                  } else {
                    itm += 'CREDIT-NOTE : '+ value.TransID +' : '+ value.TransAmount +'\n\n';
                  }
              });

              itm += 'CHANGE     :      '+mcashc+'\n\n';
               //console.log(itm);
               //console.log("################# END #################");

              try {

                BTPrinterObj.list(function(res){

                    var pname = res[0];
                    var paddress = res[1];
                    var ptype = res[2];

                    var receiptRef = posReceiptRef.split('#');
                    var receiptNumberPosAct = 'POS#'+str_pad(receiptRef[2], 9, "0", "STR_PAD_LEFT");


                      //Connect to printer 
                      BTPrinterObj.connect(function(data){
                        //alert("s2: "+JSON.stringify(data));

                        //1ST PRINT
                        //Receipt Title Header
                        BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                        BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                        BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                        BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                        //Receipt Header
                        BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'SALES RECEIPT\n','1','1');
                        BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
                        //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
                        BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

                        BTPrinterObj.printText(function(data){
                          //Clear all pos data
                          
                          
                        },null, justify_left +
                        '\n\n' +
                        itm + 
                        '\n\n\n' +
                        'Customer: ' + mpesacustomername +              // Print mpesacustomername
                       '\n\n' +
                        'Served By: ' + window.localStorage.getItem("username") +              // Print mpesacustomername
                       '\n\n' +
                        'THANKYOU' +
                       '\n\n\n\n\n\n' +
                       justify_left,'1','0');

                        //2ND PRINT
                        //Receipt Title Header
                        BTPrinterObj.printTitle(null,null,'\x1B\x21\x61' + window.localStorage.getItem("coy_name"),'1','1');
                        BTPrinterObj.printTitle(null,null,window.localStorage.getItem("domicile"),'1','1');
                        BTPrinterObj.printTitle(null,null,'Tel: '+window.localStorage.getItem("rphone"),'1','1');
                        BTPrinterObj.printTitle(null,null,'Email: '+window.localStorage.getItem("remail"),'1','1');

                        //Receipt Header
                        BTPrinterObj.printTitle(null,null,'\n\x1B\x21\x31' + 'SALES RECEIPT REPRINT\n','1','1');
                        BTPrinterObj.printTitle(null,null,'Date: '+ phpdate('M j, Y, g:ia'),'1','1');
                        //BTPrinterObj.printTitle(null,null,'Txn No: '+ receiptRef[1],'1','1');
                        BTPrinterObj.printTitle(null,null,'Ref: '+ receiptNumberPosAct,'1','1');

                        BTPrinterObj.printText(function(data){
                          //Clear all pos data
                          clearMpayData();
                          
                        },null, justify_left +
                        '\n\n' +
                        itm +               // Print
                       '\n\n\n' +
                       'Customer: ' + mpesacustomername +              // Print mpesacustomername
                       '\n\n' +
                        'Served By: ' + window.localStorage.getItem("username") +
                        '\n\n' +
                        'THANKYOU' +
                       '\n\n\n\n\n\n' +
                       justify_left,'1','0');

                      },function(err){     
                        //alert("e2: "+err);
                        clearMpayData();

                      }, pname);

                },function(err){
                    
                    clearMpayData();

                });
              
              } catch(err){
                clearMpayData();
              }
              

          }
      });
  }
  
}

/*function posTransactionPayCashPrint(){
    posTransactionPayCashPrint();
}*/

function bookingTransactionUpdate() {

  var bobj = MobileUI.objectByForm('frmBooking');
  var booking_id = bobj.booking_id;

  //paymentTypesUsed.length = 0;
  console.log(paymentTypesUsed);
  console.log(bookingItems);
  console.log(posTotal);
  console.log(bookingPayId);
  if(bookingItems.length == 0) {

  } else {
  
    loading('Please wait...');
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
      {
          tp: 'booking-cash-payment-up',
          ttp: 'CASH',
          total: posTotal,
          cpbooking_id: bookingPayId,
          pospayments: JSON.stringify(paymentTypesUsed),
          posdesc: JSON.stringify(bookingItems),
          uname: window.localStorage.getItem("username"),
          cp: window.localStorage.getItem("cp"),
          id: window.localStorage.getItem("id")
      },
      function(data, status){
        bookingPayId = "";
        console.log(data);
        //alert("R: "+data);
        posReceiptRef = data.trim();
        //alert("E: "+posReceiptRef);
          closeLoading();
          if(data == "false"){
              alert("Error occured");
          } else {

            startToPrint3();      

          }
      });
  }

}

function getUserStoreBalance() {
//userStoreBalanceArray
  userStoreBalanceArray.length = 0;
  loading('Getting Store...');
  $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
  {
      tp: 'userStore',
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id")
  },
  function(data, status){
    closeLoading();
    console.log("UID: "+window.localStorage.getItem("id"));
    
    $('#storeName').text(data);

    getUserStoreBalanceItems();
    
  });

}

function getUserStoreBalanceItems() {
  loading('Fetching Items...');
  $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
  {
      tp: 'userStoreBalance',
      cp: window.localStorage.getItem("cp"),
      id: window.localStorage.getItem("id")
  },
  function(data, status){
    closeLoading();

    var arrHolderB = JSON.parse(data);

    $.each(arrHolderB, function(i,v){
        console.log(formatMoney(v.balance));
        arrHolderB[i].balance = formatMoney(v.balance);
    });

    console.log(arrHolderB);

    userStoreBalanceArray = arrHolderB; 
    //console.log(data);
    //alert(data);
    //$('#storeName').text(data);
    
  });
}

function bookingTransactionSave() {


  paymentTypesUsed.length = 0;
  console.log("PAYMENT:");
  console.log(paymentTypesUsed);
  console.log("ITEMS:");
  console.log(bookingItems);
  console.log(posTotal);
  console.log(userbatch);
  

  if(bookingItems.length == 0) {
    alert("Add Items!!!");
  } else if(userbatch == "") {
    alert("Select Batch Number!!!");
  } else {
        loading('Please wait...');
        $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
        {
            tp: 'booking-cash-payment-up',
            ttp: 'CASH',
            total: posTotal,
            userbatch: userbatch,
            pospayments: JSON.stringify(paymentTypesUsed),
            posdesc: JSON.stringify(bookingItems),
            uname: window.localStorage.getItem("username"),
            cpbooking_id: bookingPayId,
            cp: window.localStorage.getItem("cp"),
            id: window.localStorage.getItem("id")
        },
        function(data, status){
          console.log(data);
          
          posReceiptRef = data.trim();
          //alert("E: "+posReceiptRef);
            closeLoading();
            if(data == "false"){
                alert("Error occured");
            } else {

              alert("Save Successful");
              startToPrint3();      

            }
        });
  }

  
      
}

//SUBMIT BOOKING CASH PAYMENT DETAILS
function bookingTransactionPayCashDone() {

  console.log(" ############## BOOKING UPDATE ############## ");
  console.log(bookingUpdate);
  
  var dobj = MobileUI.objectByForm('formCashPayment');
  var cashpayment = dobj.cashpayment;
  var cpbooking_id = dobj.cpbooking_id;
  var dispatchSts = dobj.dispatchsts;
  var paymentTT = dobj.paytype;


  if(dispatchSts){
    var dsts = "1";
  } else {
    var dsts = "0";
  }

  if(paymentTypesUsed.length > 0) {
    loading('Please wait...');
    $('#bookingcashbtn').prop('disabled', true);
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
      {
          tp: 'checkSelection',
          cp: window.localStorage.getItem("cp"),
          id: window.localStorage.getItem("id")
      },
      function(data, status){
        
        var sts = data.trim();
        //alert("E: "+posReceiptRef);
          closeLoading();
          $('#bookingcashbtn').prop('disabled', false);

          console.log(" $$$$ NEW PAYMENT OBJ $$$$ ");
          console.log(paymentTypesUsed);

        if(sts == "1"){
          bookingPinEntryDialog(paymentTT);
        } else {

          if(bookingUpdate == "1"){
            
              
              var postFunction = "booking-cash-payment-new";
            

          } else {

              var postFunction = "booking-cash-payment";
            

          }

          console.log(postFunction);

          $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
          {
              tp: postFunction,
              ttp: paymentTT,
              total: posTotal,
              dsts: dsts,
              pospayments: JSON.stringify(paymentTypesUsed),
              posdesc: JSON.stringify(bookingItems),
              uname: window.localStorage.getItem("username"),
              cpbooking_id: bookingPayId,
              cp: window.localStorage.getItem("cp"),
              id: window.localStorage.getItem("id")
          },
          function(data, status){
            alert(data);
            //alert("R: "+data);

            posReceiptRef = data.trim();
            //alert("E: "+posReceiptRef);
              closeLoading();
              $('#bookingcashbtn').prop('disabled', false);
              if(data == "false"){
                  alert("Error occured");
              } else {

                alert("Successful");
                
                 backPage();
                 backPage();
                 backPage();

                 startToPrint2();
                  

              }
          });
        }
    });
  } else {
    alert("Can not submit without payment");
  }
  
}

function cancelOrderAction(){

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php", {
        tp: "cancelOrder",
        order_no: window.localStorage.getItem("ordnum"),
        cp: window.localStorage.getItem("cp"),
        id: window.localStorage.getItem("id")
    },
    function(data, status){
      console.log(data);
      closeLoading();
      if(data == "false"){
        alert("Error occured");
      } else {
        alert("Order Cancel Successful");
        window.localStorage.setItem("ordnum","");
      }
        
    });

}

function invoiceOrderAction(){

    $.post(window.localStorage.getItem("setSiteUrl") + "process.php", {
        tp: "invoiceOrder",
        order_no: window.localStorage.getItem("ordnum"),
        cp: window.localStorage.getItem("cp"),
        id: window.localStorage.getItem("id")
    },
    function(data, status){
      console.log(data);
      closeLoading();
      bookingItems.length = 0;
      var jsonArr = JSON.parse(data);
      if(jsonArr.length > 0){

          //alert('alert yes');
        
         // alert(jsonArr[0].pitems);
          bookingItems = JSON.parse(jsonArr[0].pitems);
          posTotal = jsonArr[0].ptotal;
          bookingPayId = jsonArr[0].id;
          invoiceOrderActionSubmit();
      } else {
        alert("Order Got no Items");
      } 
        
    });

}

function invoiceOrderActionSubmit() {
  paymentTypesUsed.length = 0;
  console.log(paymentTypesUsed);
  console.log(bookingItems);
  console.log(posTotal);
  console.log();

  if(bookingItems.length == 0) {
    alert("No Items in Order!!!");
  } else {
        loading('Please wait...');
        $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
        {
            tp: 'booking-cash-payment-up',
            ttp: 'CASH',
            total: posTotal,
            source: 'order',
            order_no: window.localStorage.getItem("ordnum"),
            pospayments: JSON.stringify(paymentTypesUsed),
            posdesc: JSON.stringify(bookingItems),
            uname: window.localStorage.getItem("username"),
            cpbooking_id: bookingPayId,
            cp: window.localStorage.getItem("cp"),
            id: window.localStorage.getItem("id")
        },
        function(data, status){
          console.log(data);

          window.localStorage.setItem("ordnum","");
          
          posReceiptRef = data.trim();
          //alert("E: "+posReceiptRef);
            closeLoading();
            if(data == "false"){
                alert("Error occured");
            } else {

              alert("Save Successful");
              startToPrint3();      

            }
        });
  }

  
      
}

//ACTION SELECTED
function actionSelected(selvv) {

  selv = selvv;
    

}

//ORDER MANAGEMENT MENU
function orderMangementOptions(ordnum){

  window.localStorage.setItem("ordnum",ordnum);

  alert({
    title: 'Actions On Order: '+ ordnum,
    message: '',
    template: 'actions-dialog',
    width: '90%',
    buttons: [
      {
        label: 'Cancel',
        onclick: function(){

          window.localStorage.setItem("ordnum","");
  
          
          closeAlert();
        }
      },
        {
          label: 'Ok',
          onclick: function(){

            if(selv == 'cancel'){
              cancelOrderAction();
            } else if(selv == 'invoice'){
              invoiceOrderAction();
            } else {
              alert("Please Select An Action");
            }

          }
      }
    ]
  })
}

//PIN ENTRY ALERT POS
function posPinEntryDialog(typ) {
  var postTypePin = "";
  if(typ == "EAZZYPAY"){
    postTypePin = "eazzypay-payment-pin";
  } else if(typ == "VISA"){
    postTypePin = "visa-payment-pin";
  } else {
    postTypePin = "cash-payment-pin";
  }
  var pinEntryDialogSts = 0;
  alert({
        title:'User Pin',
        //message:'Add Customer <b>'+ cus +'</b>',
        message: '<div class="row"><div class="col"></i><input type="password" style="margin-left:60px;margin-top:10px;" id="pin" placeholder="Enter Pin"><br><br></div><div class="col"><button class="" id="loadingbtn"></button></div></div>',
        width:'90%',
        id: 'add-discount',
        buttons:[
          {
            label: 'Process',
            onclick: function(){
              //You code when user click in OK button.
                var pin = $("#pin").val();
                //var salespId = $("#f-salesp option:selected").val();
                //var salespName = $("#f-salesp option:selected").text();
                //console.log("ID: "+salespId+" NAME:"+salespName);
                if(pin == "") {
                    alert("Pin can not be empty");
                } else {
                                
                  closeAlert();
                  loading('Please wait...');
                  $('#poscashbtn').prop('disabled', true);
                  $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
                    {
                        tp: "cash-payment-pin",
                        ttp: typ,
                        total: posTotal,
                        pin: pin,
                        sid: "",
                        sname: "",
                        pospayments: JSON.stringify(paymentTypesUsed),
                        posdesc: JSON.stringify(posItems),
                        cp: window.localStorage.getItem("cp"),
                        id: window.localStorage.getItem("id")
                    },
                    function(data, status){
                      console.log(data);
                      closeLoading();
                      if(data == "0"){
                        alert("User does not exist!!");
                        $('#poscashbtn').prop('disabled', false);
                      } else {
                        //alert("R: "+data);
                        posReceiptRef = data.trim();
                        //alert("E: "+posReceiptRef);
                        
                        $('#poscashbtn').prop('disabled', false);
                        if(data == "false"){
                            alert("Error occured");
                        } else {
                          //posTotal = 0;
                          //posItems.length = 0;
                          //openPage('pos');
                          //alert("Cash Payment Successful");
                          //backPage();
                          //backPage();
                          posTransactionPayCashPrint('c');
                            

                        }
                      }
                        
                    });

                }
                
            }
          },
          {
            label:'Cancel',
            onclick: function(){
              //You code when user click in OK button.
              
              closeAlert();
            }
          }
        ]
      });

}

//PIN ENTRY ALERT BOOKING
function bookingPinEntryDialog(typ) {
  var pinEntryDialogSts = 0;
  alert({
        title:'User Pin',
        //message:'Add Customer <b>'+ cus +'</b>',
        message: '<div class="row"><div class="col"></i><input type="password" style="margin-left:60px;margin-top:10px;" id="pin" placeholder="Enter Pin"><br><br></div><div class="col"><button class="" id="loadingbtn"></button></div></div>',
        width:'90%',
        id: 'add-discount',
        buttons:[
          {
            label: 'Process',
            onclick: function(){
              //You code when user click in OK button.
                var pin = $("#pin").val();
                //var salespId = $("#f-salesp option:selected").val();
                //var salespName = $("#f-salesp option:selected").text();
                //console.log("ID: "+salespId+" NAME:"+salespName);
                if(pin == "") {
                    alert("Pin can not be empty");
                } else {
                                
                  closeAlert();
                  if(bookingUpdate == "1"){
                    var postFunction2 = "booking-cash-payment-pin-new";
                  } else {
                    var postFunction2 = "booking-cash-payment-pin";
                  }



                  loading('Please wait...');
                  $('#poscashbtn').prop('disabled', true);
                  $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
                    {
                        tp: postFunction2,
                        ttp: typ,
                        total: posTotal,
                        pin: pin,
                        sid: "",
                        sname: "",
                        pospayments: JSON.stringify(paymentTypesUsed),
                        posdesc: JSON.stringify(bookingItems),
                        cpbooking_id: bookingPayId,
                        cp: window.localStorage.getItem("cp"),
                        id: window.localStorage.getItem("id")
                    },
                    function(data, status){
                      console.log(data);
                      closeLoading();
                      var datat = data.trim();
                      alert(datat);
                      if(datat == "0"){
                        alert("User does not exist!!");
                        $('#poscashbtn').prop('disabled', false);
                      } else {
                        //alert("R: "+data);
                        posReceiptRef = data.trim();
                        //alert("E: "+posReceiptRef);
                        
                        $('#poscashbtn').prop('disabled', false);
                        if(data == "false"){
                            alert("Error occured");
                        } else {

                          alert("Successful");
                          //posTotal = 0;
                          //posItems.length = 0;
                          //openPage('pos');
                          //alert("Cash Payment Successful");
                          //backPage();
                          //backPage();
                          posTransactionPayCashPrint('b');
                            

                        }
                      }
                        
                    });

                }
                
            }
          },
          {
            label:'Cancel',
            onclick: function(){
              //You code when user click in OK button.
              
              closeAlert();
            }
          }
        ]
      });

}

//SUBMIT CASH PAYMENT DETAILS
function posTransactionPayCashDone() {

  
  var dobj = MobileUI.objectByForm('formCashPayment');
  var cashpayment = dobj.cashpayment;
  var paymentTT = dobj.paytype;
  var paymentChq = dobj.pref;

  var postType = "";
  

  var processedPayment = 0;
  $.each(paymentTypesUsed, function(i,v){
    processedPayment = processedPayment + parseInt(v.TransAmount);
  });

  console.log("TOTAL: " + processedPayment);
  
  //alert(dsts);
  if(posItems.length == 0){
      alert("No Items To Submit");
  } else if(parseInt(processedPayment) < parseInt(posTotal)){
      alert("Invalid Payment");
  } else {
    loading('Please wait...');
    $('#poscashbtn').prop('disabled', true);
    $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
      {
          tp: 'checkSelection',
          cp: window.localStorage.getItem("cp"),
          id: window.localStorage.getItem("id")
      },
      function(data, status){
        console.log(data);
        //alert("R: "+data);
        var sts = data.trim();
        //alert("E: "+posReceiptRef);
          closeLoading();
          $('#poscashbtn').prop('disabled', false);

        if(sts == "1"){
          posPinEntryDialog(paymentTT);
        } else {

          loading('Please wait...');
          $('#poscashbtn').prop('disabled', true);
          $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
            {
                tp: "cash-payment",
                ttp: paymentTT,
                total: posTotal,
                pospayments: JSON.stringify(paymentTypesUsed),
                posdesc: JSON.stringify(posItems),
                cp: window.localStorage.getItem("cp"),
                id: window.localStorage.getItem("id")
            },
            function(data, status){
              console.log(data);
              //alert("R: "+data);
              posReceiptRef = data.trim();
              //alert("E: "+posReceiptRef);
                closeLoading();
                $('#poscashbtn').prop('disabled', false);
                if(data == "false"){
                    alert("Error occured");
                } else {
                  //posTotal = 0;
                  //posItems.length = 0;
                  //openPage('pos');
                  //alert("Cash Payment Successful");
                  //backPage();
                  //backPage();
                  posTransactionPayCashPrint('c');
                  
                    

                }
            });
        }
          

          //%%%%%%%%%%%%%%%%%%%%%%%%
          
      });
  }
  
}

//CASH PAYMENT 
function posTransactionPayCashPrint(ttp) {
  console.log("CASH SUBMIT: "+posTotal);
  startToPrint(ttp);
  // window.cordova.plugins.printer.print("Hello\nWorld!");
}

//posTransactionPayCashPrint

function funcLoadMpesaTransaction(param){
  loadMpesaTransaction();
}

function bookingMpesaTotalVal(params) {

  $("#mcptotal").text(params.tt);
  $("#mpesacustomername").val(window.localStorage.getItem("userpay"));
  var bbalance = 0;
  var bpaid = 0;
  $.each(paymentTypesUsed, function(index, value){
    bpaid = bpaid + parseInt(value.TransAmount);
    //console.log(value.TransAmount);
  });
  bbalance = bpaid - parseInt(posTotal);

  $("#mcppaid").text(bpaid);
  $("#mcpbalance").text(bbalance);
}

function bookingTotalVal(prams){
  $("#cptotal").text(prams.tt);
  $("#cpbooking_id").val(prams.bid);
  $("#cashcustomername").val(window.localStorage.getItem("userpay"));
  var bbalance = 0;
  var bpaid = 0;
  $.each(paymentTypesUsed, function(index, value){
    bpaid = bpaid + parseInt(value.TransAmount);
    //console.log(value.TransAmount);
  });
  bbalance = parseInt(bpaid) - parseInt(posTotal);
  //cppaid cpbalance
  $('#cppaid').text(bpaid);
  if(parseInt(bbalance) <= parseInt($('#cptotal').text())){
    $("#cpbalance").text(bbalance);
  } else {
    $("#cpbalance").text(bbalance);
  }
  
}

function bookingTransactionPartialPayUpdate() {
  if(bookingItems.length == 0) {
    alert("Add Items!!");
  } else {
  var objBooking = MobileUI.objectByForm('frmBooking');
   var booking_type = objBooking.booking_type;
   var booking_id = objBooking.booking_id;

   if(booking_type == "CASH"){
      openPage('cashPayment2',{tt: posTotal, bid: booking_id}, bookingTotalVal);
   } else {
      $("#cpbooking_id").val(booking_id);
      openPage('mpesaPayment2',{tt: posTotal, bid: booking_id}, bookingMpesaTotalVal);
   }

 }
  
  //

}

//BOOKING PARTIAL PAYMENT 
function bookingTransactionPartialPay() {
  if(bookingItems.length == 0) {
    alert("Add Items!!");
  } else {
    console.log(posTotal);

     var objBooking = MobileUI.objectByForm('frmBooking');
     var aqty = objBooking.quantityAval;
     var qty = objBooking.quantity;


    // alert(aqty + " - " + qty);
    //Load Mpesa Transactions
    
    if(parseInt(posTotal) != 0){

      loadMpesaTransaction();
      
      openPage('payment2');

    } else {

      alert("You cannot submit Empty Values");
    }
  }
}

//POS PAYMENT
function posTransactionPay() {
  console.log(posTotal);

   var objPos = MobileUI.objectByForm('frmPOS');
   var aqty = objPos.quantityAval;
   var qty = objPos.quantity;

  // alert(aqty + " - " + qty);
  //Load Mpesa Transactions
  
  if(parseInt(posTotal) != 0){

      loadMpesaTransaction();
    
    openPage('payment');

  } else {

    alert("You cannot submit Empty Values");
  }
}

function search(data,search,objqty) {
  var obj = [], index=0;
  for(var i=0; i<data.length; i++) {
    for(key in data[i]){
        if(data[i][key].toString().toLowerCase().indexOf(search.toLowerCase())!=-1) {
              data[i].quantity = data[i].quantity + objqty;
              obj[index] = data[i];
              index++;
              break;
        }
    }
  }
   return obj;
}

function updateRoutes(){

  loading('Updating Trip...');
      $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
      {
          tp: 'updateTrip',
          description: JSON.stringify(routeItems),
          uname: window.localStorage.getItem("username"),
          id: window.localStorage.getItem("id"),
          cp: window.localStorage.getItem("cp")
      },
      function(data, status){
        closeLoading();
        if(data == "false"){
          alert("Error. Please try again.");
        } else {
          
          alert("Trip Update Successful.");
        }
            
      });

}

function addRouteSave(){

  var btnTxtv = $('#starttrip').text();

  if(btnTxtv == 'Start'){

    stripStartStatus = true;
    $('#starttrip').text('End');
    $('#starttrip').removeClass('red');
    $('#starttrip').addClass('black');
    $('#updateRouteBtn').show();

    $('#stt').text('Trips');
    $('#stt').removeClass('red');
    $('#stt').addClass('black');

    loading('Saving Trip...');
      $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
      {
          tp: 'startTrip',
          description: JSON.stringify(routeItems),
          uname: window.localStorage.getItem("username"),
          id: window.localStorage.getItem("id"),
          cp: window.localStorage.getItem("cp")
      },
      function(data, status){
        closeLoading();
        if(data == "false"){
          alert("Error. Please try again.");
        } else {
          
          alert("Start Trip Successful.");

          startLocationSeaker();
          backPage();
        }
            
      });

  } else if(btnTxtv == 'End'){

    routeItems.length = 0;
    stripStartStatus = false;
    $('#starttrip').text('Start');
    $('#starttrip').removeClass('black');
    $('#starttrip').addClass('red');
    $('#updateRouteBtn').hide();

    $('#stt').text('Start Trip');
    $('#stt').removeClass('black');
    $('#stt').addClass('red');

    loading('Ending Trip...');
      $.post(window.localStorage.getItem("setSiteUrl") + "process.php",
      {
          tp: 'endTrip',
          description: JSON.stringify(routeItems),
          uname: window.localStorage.getItem("username"),
          id: window.localStorage.getItem("id"),
          cp: window.localStorage.getItem("cp")
      },
      function(data, status){
        closeLoading();
        if(data == "false"){
          alert("Error. Please try again.");
        } else {

          MobileUI.clearForm('frmRoute');
          routeItems.length = 0;
         
  
          alert("End Trip Successful.");

          //startLocationSeaker();
          //backPage();
        }
            
      });




  }

  

}

function addRoute(){
  var obj = MobileUI.objectByForm('frmRoute');
  var route_option = obj.route_option;
  var route_option_id = obj.route_option_id;
  MobileUI.hide('messageValidation');

  gotsts = false;

  for(var i=0; i<routeItems.length; i++) {
      for(key in routeItems[i]){
          if(routeItems[i][key].toString().toLowerCase().indexOf(obj.route_option.toLowerCase())!=-1) {
                
                gotsts = true;
                //index++;
                break;
          }
      }
    }

  if(gotsts){
      //console.log(sitem[0].item_option);
      //console.log(sitem[0].quantity);
  } else {
    routeItems.push(obj);
  }

  MobileUI.clearForm('frmRoute');
  console.log("######### ROUTE ITEMS ##########");
  console.log(routeItems);

}

//CUSTOMER PAYMENT PAYMENT MODE
function paymentModeChange() {
  var obj = MobileUI.objectByForm('frmCustomerPayment');
  //MobileUI.disable('formUserGitHub');
  //alert("KEYUP: " + obj.github_username);
  //userGitHub = JSON.parse(res.text);

  var selpval  = obj.mode_payment;

  if(selpval == '1'){
    $('#reference').val('');
    $('#reference').attr('placeholder','Enter Mpesa Transaction ID');
  } else if(selpval == '2'){
    $('#reference').val('');
    $('#reference').attr('placeholder','Enter Cheque Number');
  } else if(selpval == '3'){
    $('#reference').val('Cash');
  } else if(selpval == '4'){
    $('#reference').val('');
    $('#reference').attr('placeholder','Enter Reference Number');
  }

  //alert(selpval);
} 

function changingModeOfPayment() {
  var objo = MobileUI.objectByForm('frmAddOrder');

  if(orderItems.length > 0){

      if(orderItems[0]['mode_prices'] == objo.mode_prices){
      
          
      } else {

        $('#mode_prices').val(orderItems[0]['mode_prices']);
        
        alert("You cannot Change Mode Of Pricing.");
      }
  }

}

//ADD ORDER OPERATION
function addOrderItem(){
  var objo = MobileUI.objectByForm('frmAddOrder');


  var oitem_option = objo.item_option;
  var oitem_option_id = objo.item_option_id;
  var oprice = objo.price;
  var oquantity = objo.quantity;
  var omode_prices = objo.mode_prices;
  //Load Mpesa Transactions

  //alert(omode_prices);

  objo['total'] = parseInt(oprice) * parseInt(oquantity);
  //console.log(obj);
  if(!oitem_option) {
      alert("Please select an item");
  } else if(!oprice || parseInt(oprice) == 0) {
      alert("Invalid Price");
  } else if(!oquantity || parseInt(oquantity) == 0) {
      alert("Invalid Quantity");
  } else {

    if(indexEdit>=0){
      orderItems[indexEdit] = objo;
      indexEdit=-1;
    } else {

      var gotsts = false;
      var checkModeOfPricing = false;

      if(orderItems.length > 0){

          if(orderItems[0]['mode_prices'] == objo.mode_prices){
              
              checkModeOfPricing = true;
              
          } else {
            checkModeOfPricing = false;
          }




          if(checkModeOfPricing){


            for(var i=0; i<orderItems.length; i++) {
            
                if(orderItems[i]['item_option'].toString().toLowerCase() == objo.item_option.toLowerCase()){
                      orderItems[i].quantity = parseInt(orderItems[i].quantity) + parseInt(objo.quantity);
                      orderItems[i].total = parseFloat(orderItems[i].price) * parseInt(orderItems[i].quantity);
                      gotsts = true;
                      //index++;
                      break;
                }
            }

            if(gotsts){
              //console.log(sitem[0].item_option);
              //console.log(sitem[0].quantity);
            } else {
              orderItems.push(objo);
            }

          } else {
            alert("Please use the same Mode Of Pricing");
          }








      } else {
        orderItems.push(objo);
      }

        
      
    }

    console.log("***** ORDER ITEMS ******");

    console.log(orderItems);

    orderTotal = 0;
    $.each(orderItems, function(index, value){
        //console.log('POS array '+index, value);
        $.each(value, function(key, cell){
              
              if(key == "total") {
                //console.log('POS arrs ' + cell + ' sval: ' + key + ' index: ' + index);
                orderTotal = orderTotal + parseFloat(cell);
              }
                
          });
         
      });

    //MobileUI.clearForm('frmPOS');
    $("#item_option").val("");
    $("#item_option_id").val("");
    $("#price").val("");
    $("#quantity").val("1");
  }

    
  //
}

//BOOKING OPERATION
function bookingTransaction(){
  var obj = MobileUI.objectByForm('frmBooking');
  MobileUI.hide('messageValidation');


  var aqty = obj.quantityAval;
  var qty = obj.quantity;
  var bkid = obj.booking_id;
  var bcustomer_option = obj.customer_option;
  var bcustomer_option_id = obj.customer_option_id;
  var bbooking_type = obj.booking_type;
  var bmode_prices = obj.mode_prices;
  var bdiscount = obj.discount;
  
  //Load Mpesa Transactions
  console.log(obj);

  obj['total'] = (obj.price - obj.discount) * obj.quantity;
  //console.log(obj);
  if(bcustomer_option == "" || bcustomer_option_id == "" ){

    alert("Customer Not Selected!!");
    
  } else if(parseInt(aqty) < parseInt(qty)){

    alert("Not Enough Quantity Available");
    
  } else if(!obj.item_option || !obj.price || parseInt(obj.price) == 0) {
    alert("Price can not be 0");
  } else {

    if(indexEdit>=0){
      bookingItems[indexEdit] = obj;
      indexEdit=-1;
    } else {

      var gotsts = false;
      for(var i=0; i<bookingItems.length; i++) {
        for(key in bookingItems[i]){
            if(bookingItems[i][key].toString().toLowerCase().indexOf(obj.item_option_id.toLowerCase())!=-1) {
                  bookingItems[i].quantity = parseInt(bookingItems[i].quantity) + parseInt(obj.quantity);
                  bookingItems[i].total = (parseFloat(bookingItems[i].price) - parseFloat(bookingItems[i].discount)) * parseInt(bookingItems[i].quantity);
                  gotsts = true;
                  //index++;
                  break;
            }
        }
      }

      if(gotsts){
        //console.log(sitem[0].item_option);
        //console.log(sitem[0].quantity);
      } else {
        bookingItems.push(obj);
      }
      
    }

    calculateTotal('frmBookingBarcodeScannerMain');
    

    MobileUI.clearForm('frmBooking');
    //bkid customer_option booking_type mode_prices
    $("#booking_id").val(bkid);
    $("#customer_option").val(bcustomer_option);
    $("#customer_option_id").val(bcustomer_option_id);
    $("#booking_type").val(bbooking_type);
    $("#mode_prices").val(bmode_prices);
    $("#quantity").val("1");

    console.log(bookingItems);

  }

    
  //
}

//POS OPERATION
function posTransaction(){
  var obj = MobileUI.objectByForm('frmPOS');
  MobileUI.hide('messageValidation');


  var aqty = obj.quantityAval;
  var qty = obj.quantity;
  var discount = obj.discount;
  //Load Mpesa Transactions

  obj['total'] = (obj.price - parseInt(discount)) * obj.quantity;
  //console.log(obj);
  if(parseInt(aqty) < parseInt(qty)){

    alert("Not Enough Quantity Available");
    
  } else if(!obj.item_option || !obj.price || parseInt(obj.price) == 0) {
    MobileUI.show('messageValidation');
    return false;
  } else {

    if(indexEdit>=0){
      posItems[indexEdit] = obj;
      indexEdit=-1;
    } else {

      var gotsts = false;
      for(var i=0; i<posItems.length; i++) {
        //console.log(posItems[i]['item_option'].toString().toLowerCase());
        //console.log(obj.item_option.toLowerCase());
        if(posItems[i]['item_option_id'].toString().toLowerCase() == obj.item_option_id.toLowerCase()){
            posItems[i].quantity = parseInt(posItems[i].quantity) + parseInt(obj.quantity);
            posItems[i].total = (parseFloat(posItems[i].price) - parseInt(posItems[i].discount)) * parseInt(posItems[i].quantity);
            gotsts = true;
            //index++; 
            //console.log(gotsts);
            break;
        }
        
      }

      //var sitem = search(posItems,obj.item_option);
      

      if(gotsts){
        //console.log(sitem[0].item_option);
        //console.log(sitem[0].quantity);
      } else {
        posItems.push(obj);
      }
      
    }

    
    posTotal = 0;
    $.each(posItems, function(index, value){
        //console.log('POS array '+index, value);
        $.each(value, function(key, cell){
              
              if(key == "total") {
                //console.log('POS arrs ' + cell + ' sval: ' + key + ' index: ' + index);
                posTotal = posTotal + parseFloat(cell);
              }
                
          });
         
      });

    //$("#postotal").text(posTotal);

    calculateTotal('frmPOSBarcodeScannerMain');

    MobileUI.clearForm('frmPOS');

    $("#quantity").val("1");
    $("#mode_prices").val("1");
    $("#discount").val("0");
    $("#tax").val("0");

    console.log(posItems);
    
    //bkid
  }

    
  //
}

function editOrderItems(index){
  indexEdit = index;
  MobileUI.formByObject('frmAddOrder', orderItems[index]);
}

function editPosItems(index){
  indexEdit = index;
  MobileUI.formByObject('frmPOS', posItems[index]);
}
//
function editBookingItems(index){
  indexEdit = index;
  MobileUI.formByObject('frmBooking', bookingItems[index]);
}

function deleteMpesaTrans(index){
  //delete paymentTypesUsed[index];
  paymentTypesUsed.splice(index, 1);
  //
 // alert(dtransObj.TransAmount);
  //Reset Paid
  var resetPaid = 0;

  if(paymentTypesUsed.length > 0){
    $.each(paymentTypesUsed, function(index, value){
      if(value != undefined) {
        resetPaid = resetPaid + parseInt(value.TransAmount);
      }
      
    });

    bbalance = parseInt(resetPaid) - parseInt($('#mcptotal').text());
    //cppaid cpbalance
    $('#mcppaid').text(resetPaid);
    if(parseInt(bbalance) <= parseInt($('#mcptotal').text())){
      $("#mcpbalance").text(bbalance);
    } else {
      $("#mcpbalance").text(bbalance);
    }
  } else {
    bbalance = parseInt(resetPaid) - parseInt($('#mcptotal').text());
    //cppaid cpbalance
    $('#mcppaid').text(resetPaid);
    if(parseInt(bbalance) <= parseInt($('#mcptotal').text())){
      $("#mcpbalance").text(bbalance);
    } else {
      $("#mcpbalance").text(bbalance);
    }
  }
    

}

function deleteCashTrans(index){
  //delete paymentTypesUsed[index];
  paymentTypesUsed.splice(index, 1);
  //
 // alert(dtransObj.TransAmount);
  //Reset Paid
  var resetPaid = 0;

  if(paymentTypesUsed.length > 0){
    //console.log(paymentTypesUsed);
    //console.log(paymentTypesUsed.length);

    $.each(paymentTypesUsed, function(index, value){
      if(value != undefined) {
        resetPaid = resetPaid + parseInt(value.TransAmount);
      }
        
    });

    bbalance = parseInt(resetPaid) - parseInt($('#cptotal').text());
    //cppaid cpbalance
    $('#cppaid').text(resetPaid);
    if(parseInt(bbalance) <= parseInt($('#cptotal').text())){
      $("#cpbalance").text(bbalance);
    } else {
      $("#cpbalance").text(bbalance);
    }
  } else {
    bbalance = parseInt(resetPaid) - parseInt($('#cptotal').text());
    //cppaid cpbalance
    $('#cppaid').text(resetPaid);
    if(parseInt(bbalance) <= parseInt($('#cptotal').text())){
      $("#cpbalance").text(bbalance);
    } else {
      $("#cpbalance").text(bbalance);
    }
  }
    

}

function deleteRouteItems(index){
  //delete routeItems[index];
  routeItems.splice(index, 1);
}

function deleteOrderItems(index){
  //delete orderItems[index];
  orderItems.splice(index, 1);
  orderTotal = 0;
  if(orderTotal.length > 0) {
    $.each(orderTotal, function(index, value){
        //console.log('DELETE POS array '+index, value);
        $.each(value, function(key, cell){
              
              if(key == "total") {
                //console.log('DELETE POS arrs ' + cell + ' sval: ' + key + ' index: ' + index);
                orderTotal = orderTotal + parseFloat(cell);
              }
                
          });
         
      });
  }
}

function deletePosItems(index){
  //delete posItems[index];
  posItems.splice(index, 1);
  calculateTotal('frmPOSBarcodeScannerMain');
  
}

function deleteBookingItems(index){
  //delete posItems[index];
  bookingItems.splice(index, 1);
  calculateTotal('frmBookingBarcodeScannerMain');
  
}

function deleteOrderScannerItems(index) {
  orderItems.splice(index, 1);
  calculateTotal('frmOrderBarcodeScanner');
  
}

function deletePosScannerItems(index){
  //delete posItems[index];
  posItems.splice(index, 1);
  calculateTotal('frmBarcodeScanner');
  
}

//################################# GPS ACCURACY #################################################

function onErrorGPS(error) {
    alert("The following error occurred: " + error);
}

function handleLocationAuthorizationStatus(status) {
    switch (status) {
        case cordova.plugins.diagnostic.permissionStatus.GRANTED:
            if(platform === "ios"){
                onErrorGPS("Location services is already switched ON");
            }else{
                _makeRequest();
            }
            break;
        case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
            requestLocationAuthorization();
            break;
        case cordova.plugins.diagnostic.permissionStatus.DENIED:
            if(platform === "android"){
                onErrorGPS("User denied permission to use location");
            }else{
                _makeRequest();
            }
            break;
        case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
            // Android only
            onErrorGPS("User denied permission to use location");
            break;
        case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
            // iOS only
            onErrorGPS("Location services is already switched ON");
            break;
    }
}

function requestLocationAuthorization() {
    try {
      cordova.plugins.diagnostic.requestLocationAuthorization(handleLocationAuthorizationStatus, onError);
    }  catch(err){

    }
}

function requestLocationAccuracy() {
    try {
      cordova.plugins.diagnostic.getLocationAuthorizationStatus(handleLocationAuthorizationStatus, onError);
    } catch(err){

    }
}

function _makeRequest(){

    try {
      cordova.plugins.locationAccuracy.canRequest(function(canRequest){
          if (canRequest) {
              cordova.plugins.locationAccuracy.request(function () {
                      //handleSuccess("Location accuracy request successful");
                  }, function (error) {
                      //onErrorGPS("Error requesting location accuracy: " + JSON.stringify(error));
                      if (error) {
                          // Android only
                          alertGPSWithButtons();
                          //onErrorGPS("error code=" + error.code + "; error message=" + error.message);
                          /*if(platform === "android"){
                            cordova.plugins.diagnostic.switchToLocationSettings();
                          }
                          if (platform === "android" && error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED) {
                              if (window.confirm("Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?")) {
                                  cordova.plugins.diagnostic.switchToLocationSettings();
                              }
                          }*/
                      }
                  }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY // iOS will ignore this
              );
          } else {
              // On iOS, this will occur if Location Services is currently on OR a request is currently in progress.
              // On Android, this will occur if the app doesn't have authorization to use location.
              onErrorGPS("Cannot request location accuracy");
          }
      });

    } catch(err){

    }
}


//################################################################################################

function timeSince(date) {

  var seconds = Math.floor((new Date() - date) / 1000);

  var interval = Math.floor(seconds / 31536000);

  if (interval > 1) {
    return interval + " years";
  }
  interval = Math.floor(seconds / 2592000);
  if (interval > 1) {
    return interval + " months";
  }
  interval = Math.floor(seconds / 86400);
  if (interval > 1) {
    return interval + " days";
  }
  interval = Math.floor(seconds / 3600);
  if (interval > 1) {
    return interval + " hours";
  }
  interval = Math.floor(seconds / 60);
  if (interval > 1) {
    return interval + " minutes";
  }
  return Math.floor(seconds) + " seconds";
}

function str_pad(str, pad_length, pad_string, pad_type){

  var len = pad_length - str.length;

  if(len < 0) return str;
    
  var pad = new Array(len + 1).join(pad_string);
    
  if(pad_type == "STR_PAD_LEFT") return pad + str;
    
  return str + pad;

}

function phpdate(format, timestamp) {
  //  discuss at: http://phpjs.org/functions/date/
  // original by: Carlos R. L. Rodrigues (http://www.jsfromhell.com)
  // original by: gettimeofday
  //    parts by: Peter-Paul Koch (http://www.quirksmode.org/js/beat.html)
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: MeEtc (http://yass.meetcweb.com)
  // improved by: Brad Touesnard
  // improved by: Tim Wiel
  // improved by: Bryan Elliott
  // improved by: David Randall
  // improved by: Theriault
  // improved by: Theriault
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Theriault
  // improved by: Thomas Beaucourt (http://www.webapp.fr)
  // improved by: JT
  // improved by: Theriault
  // improved by: Rafał Kukawski (http://blog.kukawski.pl)
  // improved by: Theriault
  //    input by: Brett Zamir (http://brett-zamir.me)
  //    input by: majak
  //    input by: Alex
  //    input by: Martin
  //    input by: Alex Wilson
  //    input by: Haravikk
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: majak
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  // bugfixed by: omid (http://phpjs.org/functions/380:380#comment_137122)
  // bugfixed by: Chris (http://www.devotis.nl/)
  //        note: Uses global: php_js to store the default timezone
  //        note: Although the function potentially allows timezone info (see notes), it currently does not set
  //        note: per a timezone specified by date_default_timezone_set(). Implementers might use
  //        note: this.php_js.currentTimezoneOffset and this.php_js.currentTimezoneDST set by that function
  //        note: in order to adjust the dates in this function (or our other date functions!) accordingly
  //   example 1: date('H:m:s \\m \\i\\s \\m\\o\\n\\t\\h', 1062402400);
  //   returns 1: '09:09:40 m is month'
  //   example 2: date('F j, Y, g:i a', 1062462400);
  //   returns 2: 'September 2, 2003, 2:26 am'
  //   example 3: date('Y W o', 1062462400);
  //   returns 3: '2003 36 2003'
  //   example 4: x = date('Y m d', (new Date()).getTime()/1000);
  //   example 4: (x+'').length == 10 // 2009 01 09
  //   returns 4: true
  //   example 5: date('W', 1104534000);
  //   returns 5: '53'
  //   example 6: date('B t', 1104534000);
  //   returns 6: '999 31'
  //   example 7: date('W U', 1293750000.82); // 2010-12-31
  //   returns 7: '52 1293750000'
  //   example 8: date('W', 1293836400); // 2011-01-01
  //   returns 8: '52'
  //   example 9: date('W Y-m-d', 1293974054); // 2011-01-02
  //   returns 9: '52 2011-01-02'

  var that = this;
  var jsdate, f;
  // Keep this here (works, but for code commented-out below for file size reasons)
  // var tal= [];
  var txt_words = [
    'Sun', 'Mon', 'Tues', 'Wednes', 'Thurs', 'Fri', 'Satur',
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ];
  // trailing backslash -> (dropped)
  // a backslash followed by any character (including backslash) -> the character
  // empty string -> empty string
  var formatChr = /\\?(.?)/gi;
  var formatChrCb = function(t, s) {
    return f[t] ? f[t]() : s;
  };
  var _pad = function(n, c) {
    n = String(n);
    while (n.length < c) {
      n = '0' + n;
    }
    return n;
  };
  f = {
    // Day
    d: function() { // Day of month w/leading 0; 01..31
      return _pad(f.j(), 2);
    },
    D: function() { // Shorthand day name; Mon...Sun
      return f.l()
        .slice(0, 3);
    },
    j: function() { // Day of month; 1..31
      return jsdate.getDate();
    },
    l: function() { // Full day name; Monday...Sunday
      return txt_words[f.w()] + 'day';
    },
    N: function() { // ISO-8601 day of week; 1[Mon]..7[Sun]
      return f.w() || 7;
    },
    S: function() { // Ordinal suffix for day of month; st, nd, rd, th
      var j = f.j();
      var i = j % 10;
      if (i <= 3 && parseInt((j % 100) / 10, 10) == 1) {
        i = 0;
      }
      return ['st', 'nd', 'rd'][i - 1] || 'th';
    },
    w: function() { // Day of week; 0[Sun]..6[Sat]
      return jsdate.getDay();
    },
    z: function() { // Day of year; 0..365
      var a = new Date(f.Y(), f.n() - 1, f.j());
      var b = new Date(f.Y(), 0, 1);
      return Math.round((a - b) / 864e5);
    },

    // Week
    W: function() { // ISO-8601 week number
      var a = new Date(f.Y(), f.n() - 1, f.j() - f.N() + 3);
      var b = new Date(a.getFullYear(), 0, 4);
      return _pad(1 + Math.round((a - b) / 864e5 / 7), 2);
    },

    // Month
    F: function() { // Full month name; January...December
      return txt_words[6 + f.n()];
    },
    m: function() { // Month w/leading 0; 01...12
      return _pad(f.n(), 2);
    },
    M: function() { // Shorthand month name; Jan...Dec
      return f.F()
        .slice(0, 3);
    },
    n: function() { // Month; 1...12
      return jsdate.getMonth() + 1;
    },
    t: function() { // Days in month; 28...31
      return (new Date(f.Y(), f.n(), 0))
        .getDate();
    },

    // Year
    L: function() { // Is leap year?; 0 or 1
      var j = f.Y();
      return j % 4 === 0 & j % 100 !== 0 | j % 400 === 0;
    },
    o: function() { // ISO-8601 year
      var n = f.n();
      var W = f.W();
      var Y = f.Y();
      return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0);
    },
    Y: function() { // Full year; e.g. 1980...2010
      return jsdate.getFullYear();
    },
    y: function() { // Last two digits of year; 00...99
      return f.Y()
        .toString()
        .slice(-2);
    },

    // Time
    a: function() { // am or pm
      return jsdate.getHours() > 11 ? 'pm' : 'am';
    },
    A: function() { // AM or PM
      return f.a()
        .toUpperCase();
    },
    B: function() { // Swatch Internet time; 000..999
      var H = jsdate.getUTCHours() * 36e2;
      // Hours
      var i = jsdate.getUTCMinutes() * 60;
      // Minutes
      var s = jsdate.getUTCSeconds(); // Seconds
      return _pad(Math.floor((H + i + s + 36e2) / 86.4) % 1e3, 3);
    },
    g: function() { // 12-Hours; 1..12
      return f.G() % 12 || 12;
    },
    G: function() { // 24-Hours; 0..23
      return jsdate.getHours();
    },
    h: function() { // 12-Hours w/leading 0; 01..12
      return _pad(f.g(), 2);
    },
    H: function() { // 24-Hours w/leading 0; 00..23
      return _pad(f.G(), 2);
    },
    i: function() { // Minutes w/leading 0; 00..59
      return _pad(jsdate.getMinutes(), 2);
    },
    s: function() { // Seconds w/leading 0; 00..59
      return _pad(jsdate.getSeconds(), 2);
    },
    u: function() { // Microseconds; 000000-999000
      return _pad(jsdate.getMilliseconds() * 1000, 6);
    },

    // Timezone
    e: function() { // Timezone identifier; e.g. Atlantic/Azores, ...
      // The following works, but requires inclusion of the very large
      // timezone_abbreviations_list() function.
      /*              return that.date_default_timezone_get();
       */
      throw 'Not supported (see source code of date() for timezone on how to add support)';
    },
    I: function() { // DST observed?; 0 or 1
      // Compares Jan 1 minus Jan 1 UTC to Jul 1 minus Jul 1 UTC.
      // If they are not equal, then DST is observed.
      var a = new Date(f.Y(), 0);
      // Jan 1
      var c = Date.UTC(f.Y(), 0);
      // Jan 1 UTC
      var b = new Date(f.Y(), 6);
      // Jul 1
      var d = Date.UTC(f.Y(), 6); // Jul 1 UTC
      return ((a - c) !== (b - d)) ? 1 : 0;
    },
    O: function() { // Difference to GMT in hour format; e.g. +0200
      var tzo = jsdate.getTimezoneOffset();
      var a = Math.abs(tzo);
      return (tzo > 0 ? '-' : '+') + _pad(Math.floor(a / 60) * 100 + a % 60, 4);
    },
    P: function() { // Difference to GMT w/colon; e.g. +02:00
      var O = f.O();
      return (O.substr(0, 3) + ':' + O.substr(3, 2));
    },
    T: function() { // Timezone abbreviation; e.g. EST, MDT, ...
      // The following works, but requires inclusion of the very
      // large timezone_abbreviations_list() function.
      /*              var abbr, i, os, _default;
      if (!tal.length) {
        tal = that.timezone_abbreviations_list();
      }
      if (that.php_js && that.php_js.default_timezone) {
        _default = that.php_js.default_timezone;
        for (abbr in tal) {
          for (i = 0; i < tal[abbr].length; i++) {
            if (tal[abbr][i].timezone_id === _default) {
              return abbr.toUpperCase();
            }
          }
        }
      }
      for (abbr in tal) {
        for (i = 0; i < tal[abbr].length; i++) {
          os = -jsdate.getTimezoneOffset() * 60;
          if (tal[abbr][i].offset === os) {
            return abbr.toUpperCase();
          }
        }
      }
      */
      return 'UTC';
    },
    Z: function() { // Timezone offset in seconds (-43200...50400)
      return -jsdate.getTimezoneOffset() * 60;
    },

    // Full Date/Time
    c: function() { // ISO-8601 date.
      return 'Y-m-d\\TH:i:sP'.replace(formatChr, formatChrCb);
    },
    r: function() { // RFC 2822
      return 'D, d M Y H:i:s O'.replace(formatChr, formatChrCb);
    },
    U: function() { // Seconds since UNIX epoch
      return jsdate / 1000 | 0;
    }
  };
  this.date = function(format, timestamp) {
    that = this;
    jsdate = (timestamp === undefined ? new Date() : // Not provided
      (timestamp instanceof Date) ? new Date(timestamp) : // JS Date()
      new Date(timestamp * 1000) // UNIX timestamp (auto-convert to int)
    );
    return format.replace(formatChr, formatChrCb);
  };
  return this.date(format, timestamp);
}

