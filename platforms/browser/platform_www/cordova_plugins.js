cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-printer/www/printer.js",
        "id": "cordova-plugin-printer.Printer",
        "pluginId": "cordova-plugin-printer",
        "clobbers": [
            "plugin.printer",
            "cordova.plugins.printer"
        ]
    },
    {
        "file": "plugins/cordova-plugin-device/www/device.js",
        "id": "cordova-plugin-device.device",
        "pluginId": "cordova-plugin-device",
        "clobbers": [
            "device"
        ]
    },
    {
        "file": "plugins/cordova-plugin-device/src/browser/DeviceProxy.js",
        "id": "cordova-plugin-device.DeviceProxy",
        "pluginId": "cordova-plugin-device",
        "runs": true
    },
    {
        "file": "plugins/cordova-plugin-btprinter/www/BluetoothPrinter.js",
        "id": "cordova-plugin-btprinter.BluetoothPrinter",
        "pluginId": "cordova-plugin-btprinter",
        "clobbers": [
            "BTPrinter"
        ]
    },
    {
        "file": "plugins/cordova-plugin-battery-status/www/battery.js",
        "id": "cordova-plugin-battery-status.battery",
        "pluginId": "cordova-plugin-battery-status",
        "clobbers": [
            "navigator.battery"
        ]
    },
    {
        "file": "plugins/cordova-plugin-battery-status/src/browser/BatteryProxy.js",
        "id": "cordova-plugin-battery-status.Battery",
        "pluginId": "cordova-plugin-battery-status",
        "runs": true
    },
    {
        "file": "plugins/cordova-plugin-fcm-with-dependecy-updated/www/FCMPlugin.js",
        "id": "cordova-plugin-fcm-with-dependecy-updated.FCMPlugin",
        "pluginId": "cordova-plugin-fcm-with-dependecy-updated",
        "clobbers": [
            "FCMPlugin"
        ]
    },
    {
        "file": "plugins/cordova-plugin-network-information/www/network.js",
        "id": "cordova-plugin-network-information.network",
        "pluginId": "cordova-plugin-network-information",
        "clobbers": [
            "navigator.connection",
            "navigator.network.connection"
        ]
    },
    {
        "file": "plugins/cordova-plugin-network-information/www/Connection.js",
        "id": "cordova-plugin-network-information.Connection",
        "pluginId": "cordova-plugin-network-information",
        "clobbers": [
            "Connection"
        ]
    },
    {
        "file": "plugins/cordova-plugin-network-information/src/browser/network.js",
        "id": "cordova-plugin-network-information.NetworkInfoProxy",
        "pluginId": "cordova-plugin-network-information",
        "runs": true
    },
    {
        "file": "plugins/skwas-cordova-plugin-datetimepicker/www/datetimepicker.js",
        "id": "skwas-cordova-plugin-datetimepicker.DateTimePicker",
        "pluginId": "skwas-cordova-plugin-datetimepicker",
        "clobbers": [
            "cordova.plugins.DateTimePicker"
        ]
    },
    {
        "file": "plugins/skwas-cordova-plugin-datetimepicker/www/utils.js",
        "id": "skwas-cordova-plugin-datetimepicker.utils",
        "pluginId": "skwas-cordova-plugin-datetimepicker"
    },
    {
        "file": "plugins/cordova-plugin-cszbar/www/zbar.js",
        "id": "cordova-plugin-cszbar.zBar",
        "pluginId": "cordova-plugin-cszbar",
        "clobbers": [
            "cloudSky.zBar"
        ]
    },
    {
        "file": "plugins/cordova-plugin-embedded-barcode-reader/www/EmbeddedBarcodeReader.js",
        "id": "cordova-plugin-embedded-barcode-reader.EmbeddedBarcodeReader",
        "pluginId": "cordova-plugin-embedded-barcode-reader",
        "clobbers": [
            "EmbeddedBarcodeReader"
        ]
    },
    {
        "file": "plugins/cordova-plugin-bluetooth-serial/www/bluetoothSerial.js",
        "id": "cordova-plugin-bluetooth-serial.bluetoothSerial",
        "pluginId": "cordova-plugin-bluetooth-serial",
        "clobbers": [
            "window.bluetoothSerial"
        ]
    },
    {
        "file": "plugins/cordova-plugin-bluetooth-serial/src/browser/bluetoothSerial.js",
        "id": "cordova-plugin-bluetooth-serial.BluetoothSerial_browser",
        "pluginId": "cordova-plugin-bluetooth-serial",
        "clobbers": [
            "window.bluetoothSerial"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-printer": "0.7.3",
    "cordova-plugin-whitelist": "1.3.4",
    "cordova-plugin-device": "2.0.3",
    "cordova-plugin-btprinter": "0.0.1-dev",
    "cordova-plugin-geolocation": "4.0.2",
    "cordova-plugin-battery-status": "2.0.3",
    "cordova-plugin-fcm-with-dependecy-updated": "4.1.1",
    "cordova-plugin-request-location-accuracy": "2.3.0",
    "cordova.plugins.diagnostic": "5.0.1",
    "cordova-plugin-network-information": "2.0.2",
    "skwas-cordova-plugin-datetimepicker": "2.1.1",
    "cordova-plugin-cszbar": "1.3.2",
    "cordova-plugin-embedded-barcode-reader": "0.9.1",
    "cordova-plugin-bluetooth-serial": "0.4.7"
}
// BOTTOM OF METADATA
});