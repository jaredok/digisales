cordova.define("cordova-plugin-vbmThermalPrinter.vbmThermalPrinter", function(require, exports, module) { function vbmThermalPrinter() {
}

vbmThermalPrinter.prototype.checkReConn = function (successCallback, errorCallback, jSONString4Conn) {
  cordova.exec(successCallback, errorCallback, "vbmThermalPrinter", "checkReConn", jSONString4Conn);
};

vbmThermalPrinter.prototype.checkLaConn = function (successCallback, errorCallback) {
  cordova.exec(successCallback, errorCallback, "vbmThermalPrinter", "checkLaConn", []);
};

vbmThermalPrinter.prototype.printRe = function (successCallback, errorCallback, jSONString4Print) {
  cordova.exec(successCallback, errorCallback, "vbmThermalPrinter", "printRe", jSONString4Print);
};

vbmThermalPrinter.prototype.printLa = function (successCallback, errorCallback, jSONString4Print) {
  cordova.exec(successCallback, errorCallback, "vbmThermalPrinter", "printLa", jSONString4Print);
};

vbmThermalPrinter.install = function () {
  if (!window.plugins) {
    window.plugins = {};
  }

  window.plugins.vbmThermalPrinter = new vbmThermalPrinter();
  return window.plugins.vbmThermalPrinter;
};

cordova.addConstructor(vbmThermalPrinter.install);
});
