cordova.define("cordova-plugin-mobiprint.mobiprint", function(require, exports, module) { var exec = require('cordova/exec');

//@ Perform an actual text string
exports.print = function(arg0, success, error) {
    exec(success, error, "mobiprint", "print", [arg0]);
};

//@ Test the functionality of the plugin
exports.test = function(arg0, success, error) {
    exec(success, error, "mobiprint", "test", [arg0]);
};

//@ Check whether paper is loaded
exports.checkPaper = function(arg0,success,error)
{
    arg0 = (arg0) ? arg0 : ".";
    exec(success,error,"mobiprint","paper",[arg0]);
};

//@ Perform an Image printing 
exports.printImage = function(arg0,success,error)
{
    exec(success, error, "mobiprint", "image", [arg0] );
}

//@ Print a space
exports.space = function(success,error)
{
    exec(success,error,"mobiprint","space",["space"]);
}
//@ Terminate the printing
exports.end = function(success,error)
{
    exec(success,error,"mobiprint","end",["end"]);
}


exports.custom = function(arg0,arg1,success,error)
{
    arg1 = (isNaN(arg1)) ? 1  : arg1;
    exec(success,error,"mobiprint","custom", [arg0,arg1]);
}

// function success(data)
// {
// 	alert(JSON.stringify(data));
// }

// function error(errorMessage)
// {
// 	alert(JSON.stringify(errorMessage));
// }

});
